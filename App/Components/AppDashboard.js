import React from 'react';
import _ from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity, Text } from 'react-native';

import { Feather } from './Icons';
import styles from './Styles/AppDashboard.styles';


function AppDashboard(props) {
  const { colors, appicon, apptitle, onPressButton,_styles,_style2} = props;
  return (
    <TouchableOpacity style={[styles.app,_styles]} onPress={onPressButton} >
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={colors}
        style={styles.appcircle}>
        <Feather style={styles.appicon} name={appicon} />
      </LinearGradient>
      <Text style={[styles.apptitle, _style2]}>{apptitle}</Text>
    </TouchableOpacity >
  );
}


import { compareOmitState } from '../Services/utils';
export default React.memo(
  AppDashboard,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);


