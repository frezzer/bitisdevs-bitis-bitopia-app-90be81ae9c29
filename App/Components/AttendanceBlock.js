import React from 'react';
import { Image, View, Text } from 'react-native';

import styles from './Styles/AttendanceBlockStyles';
import { compareOmitState } from '../../Services/utils';

const AttendanceBlock = (props) => {
  const { data_item } = this.props;
  return (
    <View style={styles.container}>
      <View style={styles.block1}>
        <View style={styles.block_image}>
          <Text style={styles.Text1}>Thứ 5</Text>
          <Text style={styles.Text2}>15</Text>
        </View>
        <View style={styles.block2}>
          <View style={{ flexDirection: 'column' }}>
            <View style={styles.info1}>
              <View style={styles.shift1}>
                <View style={styles.block2_1}>
                  <View style={styles.circle} />
                  <Text style={styles.Text3}> Time in: </Text>
                  <Text style={styles.Text4}>8:30</Text>
                </View>

                <View style={styles.block2_1}>
                  <View style={styles.circle} />
                  <Text style={styles.Text3}> Time out: </Text>
                  <Text style={styles.Text4}>8:30</Text>
                </View>

              </View>
              <View style={styles.shift2}>
                <View style={styles.block2_1}>
                  <View style={styles.circle} />
                  <Text style={styles.Text3}> Time in: </Text>
                  <Text style={styles.Text4}>--</Text>
                </View>

                <View style={styles.block2_1}>
                  <View style={styles.circle} />
                  <Text style={styles.Text3}> Time out: </Text>
                  <Text style={styles.Text4}>--</Text>
                </View>
              </View>
            </View>
            <View style={styles.info2}>
              <Text style={styles.Text5}>CHTT Nguyen Hue</Text>
              <View style={styles.block2_1}>
                <Text style={styles.Text3}>Tong: </Text>
                <Text style={styles.Text6}>8:30</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.lineStyle} />
    </View>
  );
};
function areEqual(prevProps, nextProps) {
  console.tron.log('areEqual', nextProps, prevProps);
  return compareOmitState(nextProps, prevProps, []);
}
export default React.memo(AttendanceBlock, areEqual);
