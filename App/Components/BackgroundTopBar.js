import React from 'react'
import { Platform, View, StyleSheet } from 'react-native'
import styles from './Styles/BackgroundTopBar.style'
import { Navigation } from '../Navigation1/Navigation';
import { Colors } from '../Themes';
import { compareOmitState } from '../Services/utils';

function BackgroundTopBar(props) {
  const [initiaRender, setInitiaRender] = React.useState(true);
  const [constants, setConstants] = React.useState({});

  React.useEffect(async () => {
    if (initiaRender) {
      setInitiaRender(false);
      Navigation.constants().then(constants => setConstants(constants));
      setConstants(constants);
    }
  }, []);


  if (initiaRender) return <View />
  const { color } = props;

  const styled = StyleSheet.create({
    container: {
      backgroundColor: color || Colors.transparent,
      height: Platform.select({
        ios: constants.topBarHeight||0 + 10,
        android: constants.statusBarHeight + constants.topBarHeight || 0
      })
    }
  });
  return (
    <View style={[styles.container, styled.container]} />
  )
}


export default React.memo(
  BackgroundTopBar,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);

