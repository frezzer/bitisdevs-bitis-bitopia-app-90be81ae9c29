import React from 'react';
import { Image, View, Text } from 'react-native';

import styles from './Styles/BillItemStyles'
import { Unicons } from '../Components/Icons'
import { compareOmitState, formatMoney } from '../Services/utils';
import { Colors } from '../Themes'
import moment from 'moment'
require('moment/locale/vi');

const BillItem = (props) => {
  const { data_item } = props
  return (
    <View style={styles.container}>
      <View style={styles.part1}>
        <Unicons style={styles.icon} name='receipt-alt' />
        <Text style={styles.text1}>#{data_item.id}</Text>
        <Text style={styles.text2}>Thứ {moment().format('d')}, {moment().format('hh:mm')}</Text>
      </View>
      <View style={styles.part2}>
        <View>
          <Text style={styles.text3}>Khách hàng: <Text style={{ ...styles.text3, color: Colors.grayLight }}>{data_item.customer_name}</Text></Text>
          <Text style={styles.text3}>Nhân viên: <Text style={{ ...styles.text3, color: Colors.grayLight }}>{data_item.seller_name}</Text></Text>
        </View>
        <View style={styles.total_block}>
          <Text style={styles.text3}>Tổng hóa đơn:</Text>
          <Text style={styles.text5}>{formatMoney(data_item.price)} đ</Text>
        </View>
      </View>
    </View>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BillItem, areEqual);
