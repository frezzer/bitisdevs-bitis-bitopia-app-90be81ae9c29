import React from 'react';
import {
  Image, View, Text, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';

import moment from 'moment';
import styles from './Styles/BlockNewsLike.styles';
import { Unicons, Feather } from '../Icons';
import { compareOmitState } from '../../Services/utils';
import { Colors, Metrics } from '../../Themes';
// import { Feather } from './Icons';


const BlockNewsLike = (props) => {
  const { onPress, iconColor } = props;
  const numberLike = 10;
  return (
    <View style={styles.element}>
      <TouchableOpacity onPress={onPress} style={{ justifyContent: 'center' }}>
        <Feather name="heart" color={iconColor ? '#D9D9D9' : '#FF641A'} style={styles.icon} />
      </TouchableOpacity>
      <View style={{ justifyContent: 'center' }}>
        <Text style={styles.like}>{`${numberLike} lượt thích`}</Text>
      </View>

    </View>


  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BlockNewsLike, areEqual);
