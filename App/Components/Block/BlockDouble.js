import React from 'react';
import {
  Image, View, Text, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import { Colors, Metrics } from '../../Themes';
import styles from './Styles/BlockDoubleStyles';
import { Unicons } from '../Icons';
import { compareOmitState } from '../../Services/utils';

const BlockDouble = (props) => {
  const onPressButton = _.get(props, ['onPressButton'], () => { });
  const btnDisalble = _.get(props, ['btnDisalble'], false);
  const buttonText1 = _.get(props, ['buttonText1'], 'Title');
  const buttonText2 = _.get(props, ['buttonText2'], '');
  const buttonText3 = _.get(props, ['buttonText3'], '');
  const buttonText4 = _.get(props, ['buttonText4'], '');
  const iconName = _.get(props, ['iconName'], 'clock-three');
  const imageLink = _.get(props, ['imageLink'], undefined);
  const hasArrow = _.get(props, ['hasArrow'], true);
  const _flatItem = _.get(props, ['flatItem'], false);
  const _stylesImage = _.get(props, ['_stylesImage'], {});
  const _stylesBtn = _.get(props, ['styles'], {});
  const _styleText1 = _.get(props, ['stylesText1'], {});
  const _styleText1_1 = _.get(props, ['stylesText1_1'], {});
  const _styleText2 = _.get(props, ['_styleText2'], {});

  // is {date} check {mont
  return (
    <TouchableOpacity
      style={[_stylesBtn, styles.container]}
      onPress={onPressButton}
      disabled={btnDisalble}
    >
      <View style={{
        borderBottomColor: !_flatItem ? Colors.block : null,
        borderBottomWidth: !_flatItem ? 1 : null,
        flex: 1,
        paddingVertical: Metrics.paddingXXSmall,
      }}
      >
        <View style={styles.element}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[_styleText1, styles.text1]}>{buttonText1}</Text>
              <Text style={[_styleText1_1, styles.text1_1]}>
                {' '}
                (
                {buttonText3}
                {' '}
                -
                {buttonText4}
                {' '}
                )
              </Text>
            </View>
            <Text style={[styles.text2, _styleText2]}>{buttonText2}</Text>
          </View>
          {hasArrow && <Unicons style={styles.icon_arrow} name="angle-right" />}
        </View>
      </View>
    </TouchableOpacity>
  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BlockDouble, areEqual);
