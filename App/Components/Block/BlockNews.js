import React from 'react';
import {
  Image, View, Text, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';

import moment from 'moment';
import styles from './Styles/BlockNews.styles';
import { Unicons, Feather } from '../Icons';
import { compareOmitState } from '../../Services/utils';
import { Colors, Metrics } from '../../Themes';
// import { Feather } from './Icons';


const BlockNews = (props) => {
  const { data_item,onPress } = props;
  const dataItem = _.get(props, 'dataNews');
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.element}>
        <View>
          <Image
            style={styles.image_info}
            source={require('../../Images/news.png')}
          />
        </View>
        <Text style={styles.despcription}>{dataItem.despcription}</Text>
        <Text style={styles.content}>{dataItem.content}</Text>
        <View style={styles.block2}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ marginRight: Metrics.marginTiny }}>
              <Image
                style={styles.imageUser}
                source={require('../../Images/news.png')}
              />
            </View>
            <Text style={styles.name}>HCNS Biti’s</Text>
            <View
              style={{
                //   height:15,
                borderLeftWidth: 1,
                borderLeftColor: Colors.grey4,
                marginHorizontal: Metrics.marginTiny,
              }}
            />
            <Text style={styles.name}>12/01/2020</Text>
          </View>
          <TouchableOpacity style={styles.buttonMark}>
            <Feather name="bookmark" style={styles.icon} />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>


  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BlockNews, areEqual);
