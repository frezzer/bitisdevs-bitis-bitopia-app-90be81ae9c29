import React from 'react';
import {
  Image, View, Text, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';

import moment from 'moment';
import styles from './Styles/BlockSaveNews.styles';
import { Unicons, Feather } from '../Icons';
import { compareOmitState } from '../../Services/utils';
import { Colors, Metrics } from '../../Themes';
// import { Feather } from './Icons';


const BlockNews = (props) => {
  const { cancle, bookmark } = props;
  const dataItem = _.get(props, 'dataNews');
  return (
    <View style={bookmark ? styles.block1 : styles.block}>
      <View style={{ flexDirection: 'row' }}>
        <Image
          style={styles.image}
          source={{ uri: dataItem.image_link }}
        />
        <View style={styles.blockInfo}>
          <Text numberOfLines={2} style={styles.despcription}>{dataItem.despcription}</Text>
          <Text numberOfLines={2} style={styles.content}>{dataItem.content}</Text>
        </View>
      </View>
      <View style={styles.block2}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Image
              style={styles.imageUser}
              source={require('../../Images/news.png')}
            />
          </View>
          <Text style={styles.name}>HCNS Biti’s</Text>
          <View
            style={bookmark ? styles.line : styles.line1}
          />
          <Text style={styles.name}>12/01/2020</Text>
        </View>
        { cancle ? (
          <TouchableOpacity>
            <Text style={styles.cancel}>
              Hủy lưu
            </Text>
          </TouchableOpacity>
        ) : null}
        { bookmark ? (
          <TouchableOpacity>
            <Feather name="bookmark" style={styles.iconFeather} />
          </TouchableOpacity>
        ) : null}


      </View>
    </View>


  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BlockNews, areEqual);
