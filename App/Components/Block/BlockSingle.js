import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import _ from 'lodash'

import styles from './Styles/BlockSingleStyles'
import { Unicons } from '../Icons'
import { compareOmitState } from '../../Services/utils';
import moment from 'moment'

const BlockSingle = (props) => {
  const onPressButton = _.get(props, ['onPressButton'], () => { })
  const { data_item } = props;
  // console.tron.log("TCL: BlockSingle -> data_item", data_item);
  const shift_name = _.get(props, ['shift_name'], 'chưa cập nhật');
  const shift_date = _.get(props, ['shift_date'], 'chưa cập nhật');

  return (
  
    <TouchableOpacity
      style={styles.container}
      onPress={onPressButton}
    >
      <View style={styles.block_employee}>
        <View style={styles.block_image}>
          <Image
            style={styles.image_info}
            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

          />
        </View>
        <View style={styles.info}>
          <Text style={styles.text1}>{'abc'}</Text>
        </View>
      </View>

    </TouchableOpacity>
  );
}
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BlockSingle, areEqual);
