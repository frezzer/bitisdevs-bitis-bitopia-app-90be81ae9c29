import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts, ApplicationStyles } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    ...ApplicationStyles.specialApp.dimensisons(375,62)

  },
  icon: {
    // fontSize: wp(30 / 375 * 100),
    // color: Colors.blue
  },
  icon_arrow: {
    fontSize: wp(20 / 375 * 100),
    color: Colors.grey5
  },
  image_block: {
    height: wp(38 / 375 * 100),
    width: wp(38 / 375 * 100),
    borderRadius: Metrics.boderSmall
  },
  element: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingLeft: Metrics.paddingMedium,
    flex: 1,
  },
  text1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9
  },
  text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.black,
    fontStyle: 'italic'
  },
  text1_1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey7

  },
  text5: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.green1
  },
  divider: {
    // backgroundColor:'red',
    flex: 1,
    paddingVertical: Metrics.paddingXXSmall,
  }
})
