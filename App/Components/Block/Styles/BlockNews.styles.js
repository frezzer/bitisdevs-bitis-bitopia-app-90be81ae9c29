import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  Metrics, Colors, Fonts, ApplicationStyles,
} from '../../../Themes';

export default StyleSheet.create({
  element: {
    width: wp(296 / 375 * 100),
    height: wp(300 / 375 * 100),
    backgroundColor: 'white',
    borderRadius: Metrics.marginMedium,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    marginVertical: Metrics.marginXTiny,
  },
  image_info: {
    width: wp(296 / 375 * 100),
    height: wp(158 / 375 * 100),
  },
  despcription: {
    marginTop: Metrics.marginTiny,
    paddingHorizontal: Metrics.marginMedium,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,

  },
  content: {
    marginTop: Metrics.marginTiny,
    paddingHorizontal: Metrics.marginMedium,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey8,
  },
  divider: {
    width: 10,
    backgroundColor: 'red',
  },
  imageUser: {
    width: wp(16 / 375 * 100),
    height: wp(16 / 375 * 100),
    borderRadius: Metrics.boderLSmall,
  },
  block2: {
    flexDirection: 'row',
    paddingHorizontal: Metrics.paddingMedium,
    paddingTop: Metrics.paddingXXSmall,
    justifyContent:'space-between',
    // backgroundColor:'yellow',
    alignItems:'center',
    marginTop:Metrics.marginTiny,
  },
  name: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.greybold,
  },
  buttonMark: {
    ...ApplicationStyles.specialApp.dimensisonsPercentage(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: wp(22 / 375 * 100),
    backgroundColor:Colors.white,
    position:'absolute',
    marginLeft: wp(245 / 375 * 100),
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 2,

    elevation: 3,
  },
  icon:{
    fontSize: 26,
    color: Colors.grey7,
  }
});
