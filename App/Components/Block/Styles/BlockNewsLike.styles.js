import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  Metrics, Colors, Fonts, ApplicationStyles,
} from '../../../Themes';

export default StyleSheet.create({
  element: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 76),
    // backgroundColor: 'red',
    flexDirection: 'row',
    height: wp(76 / 375 * 100),
    // justifyContent: 'center'
    // alignSelf:'center'
    borderTopWidth: 0.3,
    borderTopColor: '#eee',
  },
  like: {
    color: Colors.grey7,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginXSmall,
  },
  icon: {
    fontSize: 24,
  },
});
