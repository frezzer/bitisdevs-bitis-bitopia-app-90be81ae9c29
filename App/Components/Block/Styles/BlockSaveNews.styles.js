import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  Metrics, Colors, Fonts, ApplicationStyles,
} from '../../../Themes';

export default StyleSheet.create({
  block: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 76),
    // backgroundColor: 'red',
    flexDirection: 'column',
    // height: wp(76 / 375 * 100),
    justifyContent: 'center',
    // alignSelf:'center',
    borderTopWidth: 0.3,
    borderTopColor: '#eee',
    padding: Metrics.paddingMedium,
    // width: wp(325 / 375 * 100),

  },
  block1: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 76),
    // backgroundColor: 'red',
    flexDirection: 'column',
    // height: wp(76 / 375 * 100),
    justifyContent: 'center',
    // alignSelf:'center',
    borderTopWidth: 0.3,
    borderTopColor: '#eee',
    paddingTop: Metrics.paddingMedium,
    paddingHorizontal: Metrics.paddingMedium,
    // marginBottom: Metrics.marginXSTiny,
    // width: wp(325 / 375 * 100),

  },
  like: {
    color: Colors.grey7,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginXSmall,
  },
  icon: {
    fontSize: 24,
  },
  image: {
    ...ApplicationStyles.specialApp.dimensisons(72, 72),
    // backgroundColor: 'red',
    flex: 2,
  },
  blockInfo: {
    // backgroundColor: 'yellow',
    flex: 8,
    // ...ApplicationStyles.specialApp.dimensisons(72,72),
    marginLeft: Metrics.marginXSTiny,
    // marginRight: 50
  },
  despcription: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey9,
    // textAlignVertical: 'center'
  },
  content: {
    fontSize: Fonts.size.textXSmall,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
    marginTop: Metrics.marginXTiny,
  },
  imageUser: {
    width: wp(16 / 375 * 100),
    height: wp(16 / 375 * 100),
    borderRadius: Metrics.boderLSmall,
    marginRight: Metrics.marginTiny,
  },
  block2: {
    flexDirection: 'row',
    marginTop: Metrics.marginTiny,
    // marginBottom: Metrics.marginMedium
    justifyContent: 'space-between',
  },
  name: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.greybold,
  },
  cancel: {
    fontFamily: Fonts.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.orange1,
    // textAlign:'left'
  },
  iconFeather: {
    fontSize: wp(24 / 375 * 100),
    color: Colors.greybold,
    top: -10,
    // backgroundColor: 'red',
    // paddingBottom: -10
  },
  line1: {
    // height:15,
    borderLeftWidth: 1,
    borderLeftColor: Colors.grey4,
    marginHorizontal: Metrics.marginTiny,
  },
  line: {
    height: 15,
    borderLeftWidth: 1,
    borderLeftColor: Colors.grey4,
    marginHorizontal: Metrics.marginTiny,
  },
});
