import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingTiny,
    flexDirection: 'row',
    alignItems: 'center',
    borderTopColor: Colors.block,
    borderTopWidth: 1
  },
  icon: {
    fontSize: wp(30 / 375 * 100),
    color: Colors.blue
  },
  icon_arrow: {
    fontSize: wp(30/375*100),
    color: Colors.grayLight
  },
  element: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: Metrics.paddingMedium,
    flex: 1,
  },
  text1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium
  },
  subtext:{
    fontFamily: Fonts.type.fontItalic,
    color: Colors.grayLight
  },
  block_image: {
    justifyContent: 'center',
    flex:1
    
  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
})
