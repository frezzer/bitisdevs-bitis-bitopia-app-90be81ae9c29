import React from 'react';
import { Image, View, Text, TouchableWithoutFeedback } from 'react-native';
import styles from './Styles/BottomTabNavigatorStyles'
import { Unicons } from '../Components/Icons'
import { compareOmitState } from '../Services/utils';
import { Colors } from '../Themes'

const TabBarItem = React.memo((props) => {
  const { item, inactiveTintColor, activeTintColor, onPress, route, renderIcon, isFocused } = props;
  return (
    <TouchableWithoutFeedback
      onPress={onPress}
    >
      <View style={styles.tab_item}>
        {renderIcon({
          route: route,
          focused: isFocused,
          tintColor: isFocused ? activeTintColor : inactiveTintColor
        })}
      </View>
    </TouchableWithoutFeedback>
  );
}, areEqual);

/**
   * Actions
   *  - for flows and onPress attribute button
   * */

const Actions = {
  onPressTab(route, props) {
    props.navigation.navigate(route)
    // requestAnimationFrame(() => {
    // setRouteKeyTabBar(route.key);
    // jumpToIndex(route.key)
    // });
  }
}

const BottomTabNavigator = (props) => {
  const { navigation: { state: { index, routes } },
    onPress, activeTintColor, inactiveTintColor, renderIcon
  } = props;

  return (
    <View style={styles.container}>
      {routes.map((route, _index) => {
        return (
          <TabBarItem
            key={route.key}
            isFocused={index === _index}
            route={route}
            onPress={Actions.onPressTab.bind(this, route, props)}
            activeTintColor={activeTintColor}
            inactiveTintColor={inactiveTintColor}
            renderIcon={renderIcon}
          />
        )
      })}
    </View>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(BottomTabNavigator, areEqual);
