import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { Feather } from '../Icons';
import styles from './Styles/BackButtonsTopBar.style';
import { compareOmitState } from '../../Services/utils';

function BackButtonsTopBar(props) {
  const { onPress, name, _style } = props;
  return (
    <TouchableOpacity style={styles.backButtonTouchable} onPress={onPress}>
      <Feather style={[styles.backButtonButtonText, _style]} name={name} />
    </TouchableOpacity>
  );
}
export default React.memo(
  BackButtonsTopBar, (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
