import React from 'react';
import {
  View, Text, Button, TouchableOpacity, Linking,
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { compareOmitState } from '../../Services/utils';
// Styles
import styles from './Styles/ButtonHandle.styles';

const ButtonHandle = (props) => {
  const _onPressButton = _.get(props, ['onPressButton'], () => { });
  const _buttonText = _.get(props, ['buttonText'], ' Nhấn vào đây');
  const _styles = _.get(props, ['styles'], {});
  const _styleText = _.get(props, ['stylesText'], {});
  const _disable = _.get(props, 'disable', false);

  return (
    <TouchableOpacity
      style={[styles.Button, _styles]}
      onPress={_onPressButton}
      disabled={_disable}
    >
      <Text style={[styles.buttonText, _styleText]}>{_buttonText}</Text>
    </TouchableOpacity>
  );
};
export default ButtonHandle;
