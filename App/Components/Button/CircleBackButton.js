import React from 'react';
import _ from 'lodash';
import { TouchableOpacity, View, Text } from 'react-native';

import { Feather } from '../Icons';
import styles from './Styles/CircleBackButton.styles';
import { compareOmitState } from '../../Services/utils';

function CircleBackButton(props) {

  const { onPress } = props;
  return (
    <TouchableOpacity
      style={styles.backButtonTouchable}
      onPress={onPress}
    >
      <Feather style={styles.backButtonButtonText} name='arrow-left' />
    </TouchableOpacity>
  );
}

export default React.memo(
  CircleBackButton,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);


