import React from 'react';
import _ from 'lodash';
import { TouchableOpacity, View, Text } from 'react-native';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import { Feather } from '../Icons';
import styles from './Styles/CircleBackButton.styles';
import { compareOmitState } from '../../Services/utils';

function CircleButton(props) {
  const { onPress, name } = props;
  return (
    <TouchableOpacity
      style={styles.backButtonTouchable1}
      onPress={onPress}
    >
      {/* <Feather  /> */}
      <Feather style={styles.backButtonButtonText1} name={name} />
    </TouchableOpacity>
  );
}

export default React.memo(
  CircleButton,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
