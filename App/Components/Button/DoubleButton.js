import React from 'react';
import {
  View, Text, Button, TouchableOpacity,
  SafeAreaView, Linking
} from 'react-native';
import { connect } from 'react-redux'
import { compareOmitState } from '../../Services/utils';
import _ from 'lodash'
// Styles
import styles from './Styles/DoubleButton.styles'

const DoubleButton = (props) => {
  const onPressButton1 = _.get(props, 'onPressButton1', () => { });
  const onPressButton2 = _.get(props, 'onPressButton2', () => { });
  const buttonText1 = _.get(props, 'buttonText1', ' Nhấn vào đây');
  const buttonText2 = _.get(props, 'buttonText2', ' Nhấn vào đây');
  const _styles1 = _.get(props, 'styles1', {});
  const _styles2 = _.get(props, 'styles2', {});
  const _styles = _.get(props, 'styles', {});
  return (

    <View style={styles.container}>
      <View style={[styles.buttonArea, _styles]}>
        <TouchableOpacity style={[styles.Button1, _styles1]} onPress={onPressButton1}>
          <Text style={styles.buttonText}>{buttonText1}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.Button2, _styles2]} onPress={onPressButton2}>
          <Text style={styles.buttonText1}>{buttonText2}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default React.memo(
  DoubleButton,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);