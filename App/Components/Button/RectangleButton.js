import React from 'react';
import _ from 'lodash';
import { TouchableOpacity, View, Text } from 'react-native';

import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import { Feather } from '../Icons';
import styles from './Styles/RectangleButton.styles';
import { compareOmitState } from '../../Services/utils';
import { Metrics, Colors } from '../../Themes';


function RectangleButton(props) {
  const {
    onPress, name, _style, title,
  } = props;
  return (
  // <View style={{
  //   borderBottomColor: Colors.red ,
  //   borderBottomWidth:1,
  //   marginHorizontal:Metrics.marginMedium

    // }}>
    <TouchableOpacity style={styles.reactangle} onPress={onPress}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        {/* <Feather style={[styles.icon, _style]} name={name} /> */}
        <IconOutline name={name} style={[styles.icon, _style]} />

        <Text style={styles.title}>{title}</Text>
      </View>
      <View>
        <Feather style={[styles.iconRight]} name="chevron-right" />
      </View>

    </TouchableOpacity>
    // </View>
  );
}

export default React.memo(
  RectangleButton,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
