
import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes';

const backButtonWidth = 44;
// const confirmButtonHeight = 44;


export default StyleSheet.create({
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44,44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium
  },
});
