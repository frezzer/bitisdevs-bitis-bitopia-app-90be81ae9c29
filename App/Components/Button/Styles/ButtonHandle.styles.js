import { StyleSheet } from 'react-native';
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../Themes';

export default StyleSheet.create({
  buttonArea: {
    ...ApplicationStyles.specialApp.dimensisons(375,108),
    backgroundColor: Colors.white,
    ...ApplicationStyles.specialApp.alignCenter
  },
  Button: {
    ...ApplicationStyles.specialApp.dimensisons(343, 44),
    backgroundColor: Colors.blue,
    ...ApplicationStyles.specialApp.border(0, '#007AFF', 12),
    ...ApplicationStyles.specialApp.alignCenter
  },
  buttonText: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.white,
    fontSize: Fonts.size.textLarge,
    fontWeight: 'bold'

  }
});
