
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  Metrics, ApplicationStyles, Colors, Fonts,
} from '../../../Themes';
// import { widthPercentageToDP } from 'react-native-responsive-screen';


const backButtonWidth = 44;
// const confirmButtonHeight = 44;


export default StyleSheet.create({
  backButtonButtonText: {
    // paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    // marginRight: 5
  },
  backButtonTouchable1: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: wp(22 / 375 * 100),
    backgroundColor: 'white',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 2,

    elevation: 3,
    // opacity: 1,
  },
  reactangle: {
    ...ApplicationStyles.specialApp.dimensisons(375, 62),
    backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
  },
  title: {
    ...ApplicationStyles.specialApp.alignCenter,
  },
  backButtonButtonText1: {
    // paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.grey7,
  },
});
