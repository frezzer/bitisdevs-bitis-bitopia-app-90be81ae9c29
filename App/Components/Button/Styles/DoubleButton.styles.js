import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Fonts, Colors, Metrics,
} from '../../../Themes';
import colors from '../../../Themes/Colors';

export default StyleSheet.create({
  container: {
    // flex: 1,
  },
  content: {
    ...ApplicationStyles.specialApp.alignCenter,
  },
  buttonArea: {
    ...ApplicationStyles.specialApp.dimensisons(375, 108),
    backgroundColor: colors.white,
    ...ApplicationStyles.specialApp.alignCenter,
    flexDirection: 'row',
    // margin:Metrics.paddingSmall
  },
  Button1: {
    ...ApplicationStyles.specialApp.dimensisons(147, 44),
    backgroundColor: colors.white,
    // ...ApplicationStyles.specialApp.border(0, '#007AFF', 12),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    marginRight: Metrics.marginSmall,
    borderWidth: 1,
    borderColor: Colors.blue2,
  },
  button3: {
    backgroundColor: Colors.greenDark,
  },

  Button2: {
    ...ApplicationStyles.specialApp.dimensisons(147, 44),
    backgroundColor: colors.orange1,
    // ...ApplicationStyles.specialApp.border(0, '#FF5050', 12),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,

  },
  buttonText: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.blue2,
    fontSize: Fonts.size.textLarge,
    fontWeight: 'bold',
  },
  buttonText1: {

    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textLarge,
    fontWeight: 'bold',


  },
});
