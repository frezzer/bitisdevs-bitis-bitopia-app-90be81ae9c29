
import { StyleSheet } from 'react-native';
import {
  Metrics, ApplicationStyles, Colors, Fonts,
} from '../../../Themes';

const backButtonWidth = 44;
// const confirmButtonHeight = 44;


export default StyleSheet.create({
    icon: {
    // paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.blue2,
    marginRight: Metrics.marginXSTiny,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
  },
  reactangle: {
    ...ApplicationStyles.specialApp.dimensisons(375, 62),
    backgroundColor: 'white',
    justifyContent:'space-between',
    flexDirection:'row',
    alignItems:'center',
    // marginHorizontal:Metrics.paddingMedium
    paddingHorizontal: Metrics.marginMedium,
    // marginLeft: 16,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.grey4,

  },
  title:{
    ...ApplicationStyles.specialApp.alignCenter,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9
  },
  iconRight: {
    // paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textLarge,
    // fontWeight: 'bold',
    color: Colors.greybold,
    marginRight: Metrics.marginXSTiny,
  },
});
