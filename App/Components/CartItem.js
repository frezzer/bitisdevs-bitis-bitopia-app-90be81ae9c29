import React from 'react';
import {
  Image, View, Text, AsyncStorage,
} from 'react-native';
import Swipeout from 'react-native-swipeout';
// COMPONENTS
import moment from 'moment';
import styles from './Styles/BillItemStyles';
import { Unicons } from './Icons';
import { compareOmitState, formatMoney } from '../Services/utils';
import { Colors, Fonts } from '../Themes';
import screensName from '../Navigation/Screens';
import BlockDouble from './Block/BlockDouble';

require('moment/locale/vi');


const CartItem = (props) => {
  const { data_item, store_uuid } = props;
  console.tron.log('CartItem', store_uuid);
  // Buttons
  const swipeoutBtns = [
    {
      component: (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Unicons name="times-circle" style={{ fontSize: Fonts.size.textXHuge, color: Colors.white }} />
          <Text style={{ ...Fonts.style.caption, color: Colors.white, fontFamily: Fonts.type.fontBold }}>Xóa</Text>
        </View>
      ),
      backgroundColor: 'red',
      onPress: props.onPressDelete,
    },
  ];

  return (
    <Swipeout
      right={swipeoutBtns}
    // backgroundColor={Colors.red}
    >
      <BlockDouble
        buttonText1={`Mã hóa đơn: # ${store_uuid != null ? store_uuid : ''} - ${data_item.id}`}
        buttonText2={`Tổng hóa đơn: ${formatMoney(data_item.price)} VND`}
        iconName="receipt-alt"
        hasArrow={false}
        onPressButton={props.onPressDetail}
      />
    </Swipeout>

  );
};

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(CartItem, areEqual);
