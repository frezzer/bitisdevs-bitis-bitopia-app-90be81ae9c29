
import _ from 'lodash';
import React from 'react';
import {
  View, Text, ScrollView, TouchableOpacity, TextInput, ActivityIndicator, Button,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import Modal from 'react-native-modal';
// import CalendarPicker from 'react-native-calendar-picker';
// import moment from 'moment';
// import LineChart from '../../Lib/react-native-chart-kit/src/line-chart';
// import Labels from '../../Lib/react-native-chart-kit/src/labels';
// import DateTimePicker from '../DateTimePicker';
import { Colors, Metrics, Fonts } from '../../Themes';
import styles from './Styles/RevenueChart.styles';

export default function RevenueChart(props) {
  // const dtpickerRef = React.useRef(null);
  // const dtpickerRef1 = React.useRef(null);
  const data = _.get(props, ['data'], []);
  // const labels = _.get(props, ['labels'], []);
  // const onShowDateTimePicker = _.get(props, ['onShowDateTimePicker'], Function());
  // const hideDateTimePicker = _.get(props, ['hideDateTimePicker'], Function());
  // const handleDatePicked = _.get(props, ['handleDatePicked'], Function());
  // const DatePickerVisible = _.get(props, ['DatePickerVisible']);
  // const [initialRender, setInitialRender] = React.useState(true);
  // { isModalVisible: !this.state.isModalVisible }
  // const [modalVisible, setmodalVisible] = React.useState(false);
  // console.log('TCL: RevenueChart -> modalVisible', modalVisible);
  // const [selectedStartDate, setselectedStartDate] = React.useState(null);
  // const [selectedEndDate, setselectedEndDate] = React.useState(null);

  // const startDate = selectedStartDate ? moment(selectedStartDate).format('DD/MM/YYYY') : '';
  // console.tron.log("TCL: startDate", startDate)
  // const endDate = selectedEndDate ? moment(selectedEndDate).format('DD/MM/YYYY') : '';
  // console.tron.log("TCL: endDate", endDate)


  // const chartConfig = {
  //   backgroundGradientFrom: '#FFFFFF',
  //   backgroundGradientTo: '#FFFFFF',
  //   decimalPlaces: 0,
  //   color: () => 'rgba(136, 136, 136, 0.6)',
  //   // strokeWidth: 1 // optional, default 3
  // };
  // const dataChart = {
  //   labels,
  //   datasets: [{
  //     data,
  //     color: () => 'rgba(0, 122, 255, 0.8)',
  //   }],
  // };

  const isLoadingChart = !!data && data.length === 0;

  // const onDateChange = (date, type) => {
  //   // console.tron.log("TCL: SampleScreen -> type", type)
  //   // setselectedStartDate(date)
  //   if (type === 'END_DATE') {
  //     setselectedEndDate(date);
  //   } else {
  //     setselectedStartDate(date),
  //     setselectedEndDate(null);
  //   }
  // };
  // const toggleModal = (modalVisible) => {
  //   console.tron.log('TCL: toggleModal -> modalVisible', modalVisible);
  //   setmodalVisible({ modalVisible: !modalVisible });
  // };
  return (
    // <View style={styles.block4}>
    //   <View style={styles.block4_2}>
    //     <Text style={styles.Text3_1}>Doanh số</Text>
    //     <Text style={styles.Text13}> (Đơn vị tính : triệu đồng) </Text>
    //   </View>
    //   <View>
    //     <View style={styles.block_chart}>
    //       <View style={styles.block_lables}>
    //         <Labels
    //           data={dataChart}
    //           width={wp(40 / 375 * 100)}
    //           height={wp(224 / 375 * 100)}
    //           chartConfig={chartConfig}
    //           withVerticalLabels
    //           withHorizontalLabels
    //         />
    //       </View>
    //       <View style={styles.block_data}>
    //         <ScrollView
    //           horizontal
    //           automaticallyAdjustContentInsets={false}
    //           alwaysBounceVertical={false}
    //           alwaysBounceHorizontal
    //           showsHorizontalScrollIndicator={false}
    //           showsVerticalScrollIndicator={false}
    //         >
    //           <LineChart
    //             bezier
    //             data={dataChart}
    //             width={(60 * data.length)}
    //             height={wp(210 / 375 * 100)}
    //             chartConfig={chartConfig}
    //             withInnerLines
    //             withVerticalLabels
    //             withHorizontalLabels={false}
    //           // style={{ marginTop: Metrics.marginTiny, marginVertical: Metrics.marginMedium }}
    //           />

    //         </ScrollView>
    //       </View>
    //     </View>
    //     <View style={styles.block4_1}>
    //       {/* <View style={{ flexDirection: 'row' }}>
    //       </View> */}
    //       <View>
    //         <TouchableOpacity onPress={toggleModal}>
    //           <Text style={styles.Text2}>Thay đổi </Text>
    //         </TouchableOpacity>
    //         {/* <Modal isVisible={modalVisible}> */}
    //         {/* {modalVisible == false ?
    //         <View style={{ backgroundColor: 'white', paddingHorizontal: 25,position:'absolute' }}>
    //           <CalendarPicker
    //             startFromMonday={true}
    //             allowRangeSelection={true}
    //             // minDate={minDate}
    //             // maxDate={maxDate}
    //             todayBackgroundColor="#2883E7"
    //             selectedDayColor="#4BB67D"
    //             // selectedDayTextColor="#FF5050"
    //             onDateChange={onDateChange}
    //           />
    //           <View style={styles.button}>
    //             <Button title="Xác Nhận" onPress={toggleModal} />
    //           </View>
    //         </View> : null } */}
    //         {/* </Modal> */}
    //         {/* <Text>Từ ngày </Text>
    //         {/* <View style={{ flexDirection: 'row' }} >
    //         <TouchableOpacity onPress={onShowDateTimePicker('DateIn')}>
    //         <DateTimePicker
    //         ref={dtpickerRef} />
    //         <View>
    //         <TextInput
    //         style={styles.descriptionInput}
    //         returnKeyType='next'
    //         editable={false}
    //         pointerEvents='none'
    //         placeholder='ngày bắt đầu'
    //         placeholderTextColor={Colors.grayLight}
    //               />
    //               </View>
    //             <DateTimePicker
    //             isVisible={DatePickerVisible}
    //             mode='time'
    //             datePickerModeAndroid='spinner'
    //               onConfirm={(val) => { _handleDatePicked(val, 'DateIn') }}
    //               onCancel={() => { hideDateTimePicker('DateIn') }}
    //             />
    //             </TouchableOpacity>
    //             <Text style={styles.sub} >-</Text>
    //           <TouchableOpacity onPress={onShowDateTimePicker}>
    //           <DateTimePicker
    //               ref={dtpickerRef1} />
    //             <TextInput
    //               style={styles.descriptionInput}
    //               returnKeyType='next'
    //               editable={false}
    //               pointerEvents='none'
    //               placeholder='ngày kết thúc'
    //               placeholderTextColor={Colors.grayLight}
    //             // value={dob != null? moment(dob).format('DD/MM/YYYY'): 'chưa cập nhật'}
    //             />
    //           </TouchableOpacity>
    //         </View> */}
    //       </View>
    //     </View>
    //     {!!isLoadingChart && (
    //     <View style={styles.loadingChart}>
    //       <ActivityIndicator size="large" />
    //     </View>
    //     )}
    //   </View>
    // </View>
  );
}
