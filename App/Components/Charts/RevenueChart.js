
import _ from 'lodash';
import React from 'react';
import {
  View, Text, ScrollView, TouchableOpacity, TextInput, ActivityIndicator, Button, Dimensions,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import BarChart from '../../Lib/react-native-chart-kit/src/bar-chart';
import { Colors, Metrics, Fonts } from '../../Themes';
import styles from './Styles/RevenueChart.styles';
import Labels from '../../Lib/react-native-chart-kit/src/labels';
import RightButtonsTopBar from '../RightButtonsTopBar';
import screensName from '../../Navigation/Screens';


export default function RevenueChart(props) {
  const data = _.get(props, ['data'], []);
  const onPressFillter = _.get(props, ['onPressFillter'], []);

  return (
    <View>
      <View style={styles.block4}>
        <View style={styles.title}>
          <View>
            <Text style={styles.timeRange}>Năm nay</Text>
          </View>
          <View>
            <RightButtonsTopBar
              onPress={onPressFillter}
              name="filter"
              styleView={styles.headerRightView}
              styleIcon={{ color: Colors.blue2 }}
            />
          </View>
        </View>
        <View style={styles.block4_2}>
          <Text style={styles.Text13}>VND</Text>
        </View>
        <View>
          <View style={styles.block_chart}>
            <View style={styles.block_data}>
              <ScrollView
                horizontal
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
              >
                <BarChart
                  data={{
                    labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
                    datasets: [
                      {
                        data: [
                        // Math.random() * 100,
                        // Math.random() * 100,
                        // Math.random() * 100,
                        // Math.random() * 100,
                        // Math.random() * 100,
                        // Math.random() * 100,
                          '0',
                          '10',
                          '15',
                          '20',
                          '25',
                          '10',
                          '15',
                          '20',
                          '10',
                          '15',
                        ],
                      },
                    ],
                  }}
                  width={(60 * 10)} // from react-native
                  height={240}
                  withInnerLines
                  chartConfig={{
                  // backgroundColor: 'red',
                    backgroundGradientFrom: 'white',
                    backgroundGradientTo: 'white',
                    decimalPlaces: 0, // optional, defaults to 2dp
                    color: () => 'rgba(245,245,245)',
                    // color: () => 'rgba(0,0,245)',

                    labelColor: () => 'rgba(140,140,140)',
                    color1: 'red',
                    style: {
                      borderRadius: 16,
                    },
                    propsForDots: {
                      r: '6',
                      strokeWidth: '2',
                      stroke: Colors.green1,
                    },
                  }}
                />

              </ScrollView>
            </View>
            <View style={styles.block_lables}>
              <Labels
                data={{
                  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
                  datasets: [
                    {
                      data: [
                      // Math.random() * 100,
                      // Math.random() * 100,
                      // Math.random() * 100,
                      // Math.random() * 100,
                      // Math.random() * 100,
                      // Math.random() * 100,
                        '0',
                        '10',
                        '15',
                        '20',
                        '25',
                        '10',
                        '15',
                        '20',
                        '10',
                        '15',
                      ],
                    },
                  ],
                }}
                width={wp(40 / 375 * 100)}
                height={wp(224 / 375 * 100)}
                chartConfig={{
                  backgroundGradientFrom: '#FFFFFF',
                  backgroundGradientTo: '#FFFFFF',
                  decimalPlaces: 0,
                  color: () => 'rgba(130, 130, 130)',
                  // color: () => 'rgba(0, 0, 0)',

                // strokeWidth: 1 // optional, default 3
                }}
                withVerticalLabels
                withHorizontalLabels
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
