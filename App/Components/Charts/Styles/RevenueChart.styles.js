import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { App } from 'react-native-firebase';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


export default StyleSheet.create({
  block: {
    backgroundColor: Colors.theme.block,
  },

  container: {
    flex: 1,
    backgroundColor: Colors.block,
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blueOcean,
    textAlign: 'center',
  },
  Text3_1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginBottom: Metrics.marginXTiny,
    // marginLeft:Metrics.marginMedium
  },
  block4: {
    // marginVertical: Metrics.marginXSTiny,
    backgroundColor: Colors.white,
    height: wp(310 / 375 * 100),
  },
  block4_2: {
    ...ApplicationStyles.specialApp.padding(16, 0, 0, 0, 0, 18),

  },
  block4_1: {
    // height: wp(41 / 375 * 100),
    // marginVertical: Metrics.marginXSTiny,
    alignItems: 'center',
    // backgroundColor:'yellow'
    marginTop: Metrics.marginSmall,
  },
  block_lables: {
    ...ApplicationStyles.specialApp.dimensisons(60, 234),
    backgroundColor: Colors.white,
    paddingTop: Metrics.paddingTiny,
    position: 'absolute',

  },
  block_data: {

    // flex: 1,
    // height:wp(190/375*100),
    paddingRight: 12 / 375 * Metrics.width,
    // backgroundColor:'green',
    // paddingTop: 10,

  },
  block_chart: {
    flexDirection: 'row',
    // ...ApplicationStyles.specialApp.dimensisons(40, 174),
    // backgroundColor:'red',
    paddingBottom: Metrics.marginTiny,
  },

  Text13: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grayLight,
    fontSize: Fonts.size.textSmall,
    // marginBottom: Metrics.marginXSmall,
    // marginLeft:Metrics.marginMedium
  },
  descriptionInput: {
    // ...ApplicationStyles.specialApp.dimensisonsPercentage(),
    // paddingTop: 13,
    // paddingBottom: 12,
    // paddingHorizontal: 10,
    // paddingLeft: 0,

    ...ApplicationStyles.specialApp.padding(0, 0, 0, 0, 0, 10),
    // backgroundColor:'red',
    fontSize: Fonts.size.textLarge,
    textAlign: 'left',
    color: Colors.grayDark,
    fontFamily: Fonts.type.fontItalic,
  },
  sub: {
    fontSize: Fonts.size.textLarge,
    textAlign: 'left',
    color: Colors.grayDark,
    fontFamily: Fonts.type.fontItalic,
    // textAlign:'center',
    // backgroundColor:'blue'
  },
  loadingChart: {
    position: 'absolute',
    height: '100%',
    width: wp(343 / 375 * 100),
    left: wp(17.5 / 375 * 100),
    ...ApplicationStyles.specialApp.alignCenter,
  },
  button: {
    marginBottom: Metrics.marginSmall,
  },
  title: {
    ...ApplicationStyles.specialApp.dimensisons(375, 46),
    borderBottomWidth: 0.3,
    borderBottomColor: '#eee',
    justifyContent: 'space-between',
    // backgroundColor:'red',
    paddingHorizontal: Metrics.paddingMedium,
    // justifyContent:'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  timeRange: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    // textAlign:'center'
  },

});
