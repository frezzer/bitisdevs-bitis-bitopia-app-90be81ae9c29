import React from 'react'
import codePush from 'react-native-code-push'
import Config from '../Config/AppConfig'

class CodePushComponent extends React.Component {
	
	componentDidMount() {
    if (codePush && Config.useCodePush) {
    codePush.sync()
    }
	}

	render () {
		return null
	}
}

if (codePush && Config.useCodePush) {
  let codePushOptions = {
    updateDialog: true,
    installMode: codePush.InstallMode ? codePush.InstallMode.IMMEDIATE : undefined,
    updateDialog: {
      title: 'Deposit update',
      mandatoryContinueButtonLabel: 'Tiếp tục',
      optionalIgnoreButtonLabel: 'Từ chối',
      optionalInstallButtonLabel: 'Tiếp tục',
      mandatoryUpdateMessage: "Nhấn Tiếp tục để cập nhật phiên bản mới nhất",
      optionalUpdateMessage: 'Nhấn Tiếp tục để cập nhật phiên bản mới nhất'
    }
  };
  CodePushComponent = codePush(codePushOptions)(CodePushComponent)
}

export default CodePushComponent
