import React, { Component } from 'react'
import mDateTimePicker from 'react-native-modal-datetime-picker';
import 'moment/locale/vi';

// if you concerned about React Warning, you will know why code be here
mDateTimePicker.prototype.UNSAFE_componentWillReceiveProps =
  mDateTimePicker.prototype.UNSAFE_componentWillReceiveProps
  || mDateTimePicker.prototype.componentWillReceiveProps;
mDateTimePicker.prototype.componentWillReceiveProps = null;


class DateTimePicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
    };

  }

  _show = () => this.setState({ isDateTimePickerVisible: true });

  _hide = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    if(typeof this.props.callBack == 'function'){
      this.props.callBack(date);
    }
    this._hide();
  };

  render () {
    return (
      <mDateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hide}
          datePickerModeAndroid="spinner"
          titleIOS="Chọn ngày sinh"
          maximumDate={new Date()}
          isDarkModeEnabled={true}
        />
    )
  }
}

export default DateTimePicker;
