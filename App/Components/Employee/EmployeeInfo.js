import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import styles from './Styles/EmployeeInfo.styles'
import { compareOmitState } from '../../Services/utils';
import { Feather } from '../../Components/Icons'
import _ from 'lodash';


const EmployeeInfo = (props) => {
  const employee_name = _.get(props, ['employee_name'], 'updating');
  const onPressButton = _.get(props, ['onPressButton'], () => { })
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressButton}>
        <View style={styles.block1}>
          <View style={styles.block_image}>
            <Image
              style={styles.image_info}
              source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

            />
          </View>
          <View style={styles.block_info}>
            <Text style={styles.sub_text}>{employee_name}</Text>
          </View>


        </View>
      </TouchableOpacity>
      <View style={styles.lineStyle} />
    </View>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(EmployeeInfo, areEqual);
