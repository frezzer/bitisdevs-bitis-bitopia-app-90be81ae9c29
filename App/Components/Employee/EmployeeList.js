
import _ from 'lodash';
import React, { Component } from 'react';
import {
  TouchableOpacity, View, Text, Image,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

// Components
import EmployeeInfo from './EmployeeInfo';
import styles from './Styles/EmployeeList.styles';
import { compareOmitState } from '../../Services/utils';
import { Feather } from '../Icons';


const RenderItem = React.memo((props) => {
  const { item } = props;
  const onPressButton = _.get(props, ['onPressButton'], () => {});
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={onPressButton}
      >
        <View style={styles.block1}>
          <View style={styles.block_image}>
            <Image
              style={styles.image_info}
              source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

            />
          </View>
          <View style={styles.block_info}>
            <Text style={styles.sub_text}>{item.title}</Text>
          </View>
          <View style={styles.block_check}>
            <View style={styles.button_check}>
              <Feather style={styles.icon} name="chevron-right" />
            </View>
          </View>

        </View>
      </TouchableOpacity>
      <View style={styles.lineStyle} />
    </View>
  );
}, areEqual);


const EmployeeList = (props) => {
  const data = _.get(props, 'data', dataSample);
  const { onPressButton } = props;
  const renderItem = ({ item }) => <RenderItem item={item} />;
  return (
    <View>
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={data}
        renderItem={renderItem}
        initialNumToRender={3}

      />
    </View>
  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(EmployeeList, areEqual);


const dataSample = [
  {
    id: 1,
    title: ' Faker',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
  },
  {
    id: 2,
    title: 'Clid',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
  },
  {
    id: 3,
    title: 'Teddy',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
  },
  {
    id: 4,
    title: 'Effort',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
  },
  {
    id: 5,
    title: 'Khan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
  },
];
