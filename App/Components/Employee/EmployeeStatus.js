
import _ from 'lodash';
import React, { Component } from 'react';
import {
  TouchableOpacity, View, Text, Image, ScrollView,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

// Components
import StatusItem from './StatusItem';
import styles from './Styles/EmployeeList.styles';
import { compareOmitState } from '../../Services/utils';
import { Colors, Metrics } from '../../Themes';
import { Unicons, Feather } from '../Icons';


const EmployeeStatus = (props) => {
  const btnDisalble = _.get(props, ['btnDisalble'], false);
  const dataItem = _.get(props, 'dataEmployee');
  const hasArrow = _.get(props, ['hasArrow'], false);
  const renderItem = ({ item }) => {
    const firstName = _.get(item.user || item, 'firstName', '');
    const lastName = _.get(item.user || item, 'lastName', '');
    const name = _.concat([lastName, firstName ? ' ' : '', firstName]);

    return (
      <ScrollView id={item.id} style={styles.element}>
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => { props.callBackParent('saler', item)}}
            disabled={btnDisalble}
          >
            <View style={styles.block1}>
              <View style={styles.block_image}>
                <Image
                  style={styles.image_info}
                  source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}
                />
              </View>
              <View style={styles.block_info}>
                <View style={styles.block2}>
                  <Text style={styles.sub_text}>{name}</Text>
                  {/* <Text>{item.title}</Text> */}
                  <View
                    style={{
                      borderLeftWidth: 1,
                      borderLeftColor: Colors.black,
                      marginHorizontal: Metrics.marginTiny,
                    }}
                  />
                  <Text style={styles.position}>Chưa cập nhật</Text>
                </View>
                <Text style={styles.caption}>
                  { (() => {
                    switch (item.status) {
                      case false: return 'Chưa gửi chấm công';
                      case true: return 'Đã gửi chấm công';
                      case 'DONE': return 'Đã tan ca';
                      case 'LATE': return 'Chưa tan ca';
                      case 'DOING': return 'Chưa tan ca';
                      default: return 'chưa cập nhập';
                    }
                  })()}
                </Text>
              </View>
              <View style={styles.icon}>
                {hasArrow && <Feather style={styles.icon_arrow} name="chevron-right" />}
              </View>
            </View>
          </TouchableOpacity>

          <View style={styles.lineStyle} />
        </View>
      </ScrollView>
    );
  };

  return (
    <View>
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={dataItem}
        renderItem={renderItem}
        initialNumToRender={3}

      />
    </View>
  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(EmployeeStatus, areEqual);


const dataSample = [
  {
    id: 1,
    title: 'Pham Do Minh Quan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    status: 'đã tan ca',
  },
  {
    id: 2,
    title: 'Pham Do Minh Quan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    status: 'đã tan ca',
  },
  {
    id: 3,
    title: 'Pham Do Minh Quan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    status: 'đã tan ca',
  },
  {
    id: 4,
    title: 'Pham Do Minh Quan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    status: 'đã tan ca',
  },
  {
    id: 4,
    title: 'Pham Do Minh Quan',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    status: 'đã tan ca',
  },

];
