import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import styles from './Styles/EmployeeInfo.styles'
import { compareOmitState } from '../../Services/utils';
import { Feather } from '../Icons'
import { Colors, Metrics } from '../../Themes'

const StatusItem = (props) => {
  const { data_info } = props;
  console.tron.log("TCL: StatusItem -> data_section", data_info)
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <View style={styles.block1}>
          <View style={styles.block_image}>
            <Image
              style={styles.image_info}
              source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

            />
          </View>
          <View style={styles.block_info}>
            <View style={styles.block2}>
              <Text style={styles.sub_text}>Nhân viên 1</Text>
              <View
                style={{
                  borderLeftWidth: 1,
                  borderLeftColor: Colors.black,
                  marginHorizontal: Metrics.marginTiny,
                }}
              />
              <Text style={styles.position}>{'Trưởng cửa hàng'}</Text>
            </View>
            <Text style={styles.caption}>Chưa tan ca</Text>
          </View>


        </View>
      </TouchableOpacity>
      <View style={styles.lineStyle} />
    </View>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(StatusItem, areEqual);
