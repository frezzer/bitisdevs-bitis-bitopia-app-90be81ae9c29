import _ from 'lodash';
import { StyleSheet, Platform } from 'react-native';
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,

  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375,69),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent:'center',
    paddingHorizontal:Metrics.paddingMedium
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent:'center',
    marginLeft: Metrics.marginXSmall,
    flex:6
  },
  block_image: {
    justifyContent: 'center',
    flex:1
    
  },
  sub_text: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey9,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // marginBottom: Metrics.marginXTiny
  },
  caption:{
    fontFamily:Fonts.type.fontRegular,
    fontSize:Fonts.size.textSmall,
    color: Colors.orange2

  },
  block2:{
    flexDirection:'row'
  },
  position:{
    fontFamily:Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey7
  }
  


})
