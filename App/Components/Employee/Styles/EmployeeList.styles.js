/* eslint-disable no-mixed-operators */
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  lineStyle: {
    borderBottomWidth: 0.3,
    borderColor: Colors.grey4,
    // width:311
    marginHorizontal: Metrics.marginMedium,
  },
  image_info: {
    width: wp(38 / 375 * 100),
    height: wp(38 / 375 * 100),
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',
  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375, 69),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent: 'center',
    paddingHorizontal: Metrics.paddingMedium,
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent: 'center',
    marginLeft: Metrics.marginTiny,
    flex: 6,
  },
  block_image: {
    justifyContent: 'center',

  },
  sub_text: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey9,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // marginBottom: Metrics.marginXTiny
  },
  caption: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textM,
    color: Colors.orange2,
    marginTop: Metrics.marginXTiny,

  },
  block2: {
    flexDirection: 'row',
    // flex: 5
  },
  position: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey7,
  },
  icon: {
    justifyContent: 'center',
    // flex:1
  },

  icon_arrow: {
    fontSize: wp(20 / 375 * 100),
    color: Colors.grey5,
  },


});
