import _ from 'lodash'
import React, { Component } from 'react'
import { TouchableOpacity, Text, View, Image, Animated, Easing } from 'react-native';
import styles from './Styles/EmployeeInfoStyle';
import LottieView from 'lottie-react-native';
import FingerTap from '../Fixtures/fingertap.json';
import Barcode from '../Lib/react-native-barcode-builder';
import UserDetailActions, { UserDetailSelectors } from '../Redux/user/UserDetailRedux'
import { connect, useSelector, useDispatch } from 'react-redux';

const EmployeeInfo = (props) => {
  const [isShow, setIsShow] = React.useState(true);
  const [isTap, setIsTap] = React.useState(false);
  const _onPressButton = () => {
    if (!isTap) setIsTap(true);
    setIsShow(!isShow);
  }

  const user_detail = useSelector(state => UserDetailSelectors.selectPayload(state.user.userDetail));
  console.tron.log("TCL: EmployeeInfo -> user_detail", user_detail)

  const uuid = _.get(props, ['data', 'username'], null);
  const uuidText = uuid || "Không tìm thấy thông tin";
  const onLongPress = _.get(props, ['onLongPress'], Function());
  const company_email = _.get(props, ['data', 'email']) || 'Chưa cập nhật';
  // const mobile_number = _.get(props, ['data', 'phoneNumber']) || 'Chưa cập nhật';
  // const first_name = _.get(props, ['data', 'firstname'], '');
  // const last_name = _.get(props, ['data', 'lastName'], '');
  // const middle_name = _.get(props, ['data', 'middle_name'], '');

  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const middle_name = _.get(user_detail, ['data', 'middle_name'], '');
  const mobile_number = _.get(user_detail, ['userdetail', 'phoneNumber'], '');
  const email = _.get(user_detail, 'email', '');

  return (
    <TouchableOpacity onPress={_onPressButton}
      onLongPress={!isShow && onLongPress}
    >
      <View style={styles.barcodeblock}>
        {uuid && <Barcode value={uuid} format="CODE128" height={styles.barcode.height} />}
        <Text style={styles.barcodeText}>{uuidText}</Text>
      </View>
      {!!isShow && <View style={styles.infoblock}>
        <Text style={styles.jobposition}>{_.concat([last_name, middle_name ? ' ' : '', middle_name, first_name ? ' ' : '', first_name])}</Text>
        <Text style={styles.jobtitle}>Customer Implementation Executive</Text>
        <Text style={styles.email}>Email: {company_email}</Text>
        <Text style={styles.phone}>Phone: {mobile_number}</Text>
      </View>}

      {!isTap && <View style={styles.tapAnimationblock}>
        <View style={styles.tapAnimation}>
          <LottieView source={FingerTap} autoPlay loop />
        </View>
      </View>}
    </TouchableOpacity>
  )
}

export default EmployeeInfo;

