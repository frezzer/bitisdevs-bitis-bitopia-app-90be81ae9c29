import React, { Component } from 'react';

import FontAwesome from 'react-native-vector-icons/FontAwesome'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Feather from 'react-native-vector-icons/Feather'
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import { createIconSetFromFontello } from 'react-native-vector-icons';
import UniconsConfig from '../Themes/Icons/UniconsConfig.json';


const Unicons = createIconSetFromFontello(UniconsConfig);

export {
    FontAwesome,
    SimpleLineIcons,
    Ionicons,
    MaterialIcons,
    Feather,
    MaterialCommunityIcons,
    Unicons
}
