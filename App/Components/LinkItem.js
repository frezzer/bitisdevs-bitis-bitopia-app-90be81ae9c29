import React from 'react';
import _ from 'lodash';
import { TouchableOpacity, Text } from 'react-native';

import { Feather } from './Icons';
import styles from './Styles/AppDashboard.styles';
import { compareOmitState } from '../Services/utils';

function LinkItem(props) {

    return (
        <Text>abc</Text>
    );
}

export default React.memo(
    LinkItem,
    (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);
