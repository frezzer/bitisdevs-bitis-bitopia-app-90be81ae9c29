import React, { Component } from 'react'
import { View } from 'react-native';
import { useSelector } from 'react-redux'
import { loadingSagaSelectors } from '../Redux/LoadingSagaRedux';
import { compareOmitState } from '../Services/utils';
import styles from './Styles/LoadingIndicatorStyle';
import LottieView from 'lottie-react-native';
import logo from './loading.json'

function LoadingIndicator(props) {
  const show = useSelector(state => loadingSagaSelectors.selectIndicator(state.saga.loading));
  if (!show) return <View />
  return (
    <View style={styles.container}>
      <View style={styles.background} >
      </View>
      <View style={styles.content}>
        <View style={styles.boxIndicator}>
          <LottieView source={logo} autoPlay loop />
        </View>
      </View>
    </View>
  )
}

export default React.memo(
  LoadingIndicator,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);
