'use strict';
import React from 'react';
import { ActivityIndicator, View, SafeAreaView, Text } from 'react-native';

export default function LoadingInteractions(props) {
  const size = props.size || 'small';


  
  return <SafeAreaView style={{ flex: 1 }}>
    <View style={{ flex: 1 }}>
      <View style={{ flex: 1 }}></View>
      <View style={{ flex: 39 }}>
        <ActivityIndicator size={size} />
      </View>
    </View>
  </SafeAreaView>
}
