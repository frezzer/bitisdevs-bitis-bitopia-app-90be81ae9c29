import React from 'react';
import {
  View, Text, Button, TouchableOpacity,
  SafeAreaView, Linking
} from 'react-native';
import { connect } from 'react-redux'
import { compareOmitState } from '../../Services/utils';
import _ from 'lodash'
// Styles
import styles from './Styles/ButtonHandle.styles'

const ButtonHandle = (props) => {
  const onPressButton = _.get(props, 'onPressButton', () => { });
  const buttonText = _.get(props, 'buttonText', ' Nhấn vào đây');
  const _styles = _.get(props, 'styles', {});
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <View style={styles.buttonArea}>
          <TouchableOpacity style={[styles.Button, _styles]} onPress={onPressButton}>
            <Text style={styles.buttonText}>{buttonText}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  )

}

export default React.memo(
  ButtonHandle,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);

