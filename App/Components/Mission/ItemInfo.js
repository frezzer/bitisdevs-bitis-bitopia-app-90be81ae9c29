import React from 'react';
import { Image, View, Text,TouchableOpacity } from 'react-native';
import styles from './styles/ItemInfoStyles'
import { compareOmitState } from '../../Services/utils';
import { Feather } from '../../Components/Icons'

const ItemInfo = (props) => {
  const { data_item } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity>
      <View style={styles.block1}>
        <View style={styles.block_image}>
          <Image
            style={styles.image_info}
            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

          />
        </View>
        <View style={styles.block_info}>
          <Text style={styles.sub_text}>{data_item.title}</Text>
          <Text style={styles.Caption}>{data_item.date}</Text>
        </View>
        <View style={styles.block_check}>
        <View style={styles.button_check} >
          <Feather style={styles.icon} name='check' />
        </View>
        </View> 
     
      </View>
      </TouchableOpacity>
      <View style={styles.lineStyle} />
    </View>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ItemInfo, areEqual);
