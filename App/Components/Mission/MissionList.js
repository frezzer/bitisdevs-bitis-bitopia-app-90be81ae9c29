
import _ from 'lodash';
import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

// Components
import ItemInfo from './ItemInfo';
import styles from './styles/MissionListStyles';
import { compareOmitState } from '../../Services/utils';


const RenderItem = React.memo((props) => {
  const { item } = props;
  return (
    <View id={item.id} style={styles.element}>
      <ItemInfo data_item={item} />
    </View>
  );
}, areEqual);



const MissionList = (props) => {
  const data = _.get(props, 'data', dataSample);
  const { onPressButton } = props;
  // For render
  const renderItem = ({ item }) => <RenderItem item={item} />
  return (
    <View>
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={data}
        renderItem={renderItem}
        initialNumToRender={3}
        ListFooterComponent={
          <View style={styles.block2}>
            <TouchableOpacity onPress={onPressButton}>
              <Text style={styles.Text2}>Xem thêm</Text>
            </TouchableOpacity>
          </View>
        }
        ListHeaderComponent={
          <View>
            <View style={styles.block1}>
              <Text style={styles.Text1}>Nhiệm vụ</Text>
            </View>
            <View style={styles.lineStyle} />
          </View>
        }
      />
    </View>
  )
}
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(MissionList, areEqual);





const dataSample = [
  {
    id: 1,
    title: `Chụp ảnh check in`,
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
  },
  {
    id: 2,
    title: `Chụp ảnh check in`,
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
  },
  {
    id: 3,
    title: `Chụp ảnh check in`,
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
  },
  {
    id: 4,
    title: `Chụp ảnh check in`,
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
  },
];
