import _ from 'lodash';
import { StyleSheet, Platform } from 'react-native';
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },

  Text1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginLeft: Metrics.marginSmall
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,

  },
  image_info: {
    width: 50 / 375 * Metrics.width,
    height: 50 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 12,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    flexDirection: 'row',
    marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent:'center'
  },
  block_info: {
    paddingVertical: Metrics.paddingSmall,
    marginLeft: Metrics.marginXSmall,
    flex:4
  },
  block_image: {
    justifyContent: 'center',
    flex:1
    
  },
  sub_text: {
    fontFamily: Fonts.type.fontRegular,
    fontWeight: Platform.select({
      ios: '700',
      android: 'bold'
    }),
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginBottom: Metrics.marginXTiny
  },
  Caption: {
    
    textAlign: 'left',
    color: '#666666',
    fontSize: Fonts.size.textSmall,
    fontFamily: Fonts.type.fontRegular
    
  },
  block_check: {
    // height: 50 / 375 * Metrics.width,
    // width: 50 / 375 * Metrics.width,
    paddingTop: Metrics.paddingXSmall,
    // backgroundColor: 'red',
    justifyContent:'center',
    alignItems:'center',
    // ...ApplicationStyles.specialApp.alignCenter,
    // alignSelf: 'center',
    flex:1
    
  },
  button_check: {
    backgroundColor: Colors.greenMedium,
    ...ApplicationStyles.specialApp.dimensisons(26,26),
    ...ApplicationStyles.specialApp.alignCenter,
    // alignSelf:'center',
    borderRadius: wp(13 / 375 * 100),
    marginBottom:Metrics.marginXSmall
  },
  icon: {
    color: 'white',
    fontSize: 21 / 375 * Metrics.width,
    alignItems:'center',
    alignItems:'center',
    justifyContent:'center'
  },
  


})
