import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes/'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  block1: {
    height: wp(50 / 375 * 100),
    backgroundColor: Colors.white,
    // paddingTop: Metrics.paddingHuge,\
    // marginTop: Metrics.marginXSmall,
    justifyContent: 'center'

  },
  Text1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginLeft: Metrics.marginMedium
  },
  lineStyle: {
    borderWidth: 0.2,
    borderColor: Colors.greenLight,
    // margin: 10,
  },
  block2: {
    height: wp(50 / 375 * 100),
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    // marginBottom: Metrics.marginXSTiny

  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.blueOcean,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center'

    // margin: Metrics.marginXSmall
  }

})


