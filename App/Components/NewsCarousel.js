import React from 'react';
import { TouchableOpacity, Text, View, Image, InteractionManager } from 'react-native';
import styles from './Styles/NewsCarouselStyle';
import Carousel from 'react-native-snap-carousel';
import metrics from '../Themes/Metrics';
import { compareOmitState } from '../Services/utils';

// Components
import LoadingInteractions from '../Components/LoadingInteractions';


const renderItem = ({ item, index }) => {
  return (<NewsCarouselItem key={String(index)} item={item} index={index} />);
}

const NewsCarouselItem = React.memo((props) => {
  const { item, index } = props;
  return (
    <TouchableOpacity key={String(index)} style={styles.slider} onPress={this.onPressButton}>
      <Image
        style={styles.imgslider}
        source={{ uri: item.img }}
      />
    </TouchableOpacity>
  )
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}


export default function NewsCarousel(props) {
  const [initialRender, setInitialRender] = React.useState(true);
  const [entries, setEntries] = React.useState([]);
  const { entriesData } = props;

  React.useEffect(() => {
    if (initialRender) {
      setTimeout(() => {
        setInitialRender(false);
        setEntries(entriesData);
      }, 300);
    }
    return () => { };
  }, [])

  if (initialRender) return <LoadingInteractions size={'large'} />
  return (
    <View style={styles.container}>
      <Carousel
        initialNumToRender={1}
        removeClippedSubviews={true}
        data={entries}
        renderItem={renderItem}
        sliderWidth={metrics.width}
        itemWidth={295 / 375 * metrics.width}
        firstItem={1}
        loop={true}
        autoplay={true}
      />
    </View>
  );
}
