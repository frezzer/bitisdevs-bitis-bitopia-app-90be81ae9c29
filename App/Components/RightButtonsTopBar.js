import _ from 'lodash';
import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import { Feather } from './Icons';
import styles from './Styles/RightButtonsTopBarStyle';


const RightButtonsTopBar = (props) => {
  const { name, onPress } = props;
  const styleView = _.get(props, ['style'], {});
  const styleIcon = _.get(props, ['styleIcon'], {});

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={[styles.content, styleView]}>
        {/* <Feather name={name} style={[styles.icon, styleIcon]} /> */}
        <IconOutline name={name} style={[styles.icon, styleIcon]} />

      </TouchableOpacity>
    </View>
  );
};

export default RightButtonsTopBar;
