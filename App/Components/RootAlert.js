import React, { Component } from 'react';
import {
  ScrollView, Alert, View, Text,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { Feather } from './Icons';
import RootAlertActions, { RootAlertSelectors } from '../Redux/RootAlertRedux';
import { Metrics, Colors } from '../Themes';
// Styles
import styles from './Styles/RootAlertStyle';
import { compareOmitState } from '../Services/utils';

function RootAlert(props) {
  const dispatch = useDispatch();
  const [initiaRender, setInitiaRender] = React.useState(true);
  const [toggle, setToggle] = React.useState(true);
  const [timeoutId, settimeoutId] = React.useState(true);
  const [constants, setConstants] = React.useState({});

  const show = useSelector((state) => RootAlertSelectors.selectShow(state.saga.alert));
  const error = useSelector((state) => RootAlertSelectors.selectError(state.saga.alert));

  React.useEffect(() => {
    if (initiaRender) {
      setInitiaRender(false);
      setToggle(false);
    } else if (!!show && !toggle) {
      Alert.alert('THÔNG BÁO', error);
      dispatch(RootAlertActions.rootAlertHide());
      // clearTimeout(timeoutId);
      // settimeoutId(
      //   setTimeout(() => {
      //     dispatch(RootAlertActions.rootAlertHide());
      //     setToggle(false);
      //   }, 3000),
      // );
      // setToggle(true);
    }

    return () => {
    };
  }, [show]);

  const renderProps = {
    show,
    error,
  };
  return <Render {...renderProps} />;
}

const Render = React.memo((props) => {
  const { error, show } = props;
  // if (!show) return <View />;
  // return (
  //   <View style={styles.container}>
  //     <View style={styles.content}>

  //       <View style={styles.loadingBox}>
  //         <Feather name="alert-triangle" style={styles.spinner} />
  //       </View>

  //       <View style={styles.textBox}>
  //         <Text numberOfLines={3} style={styles.loadingText}>{error}</Text>
  //       </View>
  //     </View>
  //   </View>
  // );
  return <View />;
}, (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []));


export default React.memo(
  RootAlert,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
