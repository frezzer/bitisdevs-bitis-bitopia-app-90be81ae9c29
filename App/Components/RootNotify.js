import React, { Component } from 'react'
import { Alert, View, Text, SafeAreaView, AppState } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import NetInfo from "@react-native-community/netinfo";
import { Feather } from '../Components/Icons';
import NotifyActions, { NotifySelectors } from '../Redux/NotifyRedux'
import { Metrics, Colors } from '../Themes';
// Styles
import styles from './Styles/RootNotify.style';
import { compareOmitState } from '../Services/utils';

function RootNotify(props) {
  const dispatch = useDispatch();
  const [initiaRender, setInitiaRender] = React.useState(true);
  const [checkInternetInterval, setCheckInternetInterval] = React.useState(null);

  const show = useSelector(state => NotifySelectors.selectShow(state.saga.notify));
  const text = useSelector(state => NotifySelectors.selectText(state.saga.notify));
  const color = useSelector(state => NotifySelectors.selectColor(state.saga.notify));

  const netInfo = NetInfo.useNetInfo();

  React.useEffect(() => {
    if (initiaRender) setInitiaRender(false);
  }, []);

  React.useEffect(() => {
    if (!netInfo.isConnected) {
      dispatch(NotifyActions.notifyShow({
        show: true,
        text: 'No Internet Connection',
        color: Colors.red
      }));
    } else {
      dispatch(NotifyActions.notifyHide());
    }
  }, [netInfo])



  const renderProps = {
    show,
    color,
    text,
  }
  return <Render {...renderProps} />
}

const Render = React.memo((props) => {
  const { color, show, text } = props;
  if (!show) return <View />
  return (
    <View style={styles.container}>
      <View style={[styles.content, { backgroundColor: color }]}>
        <View style={styles.textBox}>
          <Text style={styles.loadingText}>{text}</Text>
        </View>
      </View>
    </View>
  );
}, (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []));


export default React.memo(
  RootNotify,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);

