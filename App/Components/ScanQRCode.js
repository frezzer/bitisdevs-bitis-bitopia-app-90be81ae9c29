
import _ from 'lodash';
import React from 'react';
import {
  View, Text, TouchableOpacity, Image, TextInput,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { Feather, Unicons } from './Icons';
import {
  Colors, Fonts, Images, Metrics,
} from '../Themes';
import styles from './Styles/ScanQRCode.styles';


const ScanQRCodeContainer = (props) => {
  const { isSearch } = props;
  // let scanner = null;
  const onScanSuccess = _.get(props, 'onScanSuccess', () => { });
  const infoName = _.get(props, 'info_name', '');
  const currentTime = _.get(props, 'current_time', '');
  const detailShiftText = _.get(props, 'detailShiftText', '');
  // const reactivate = _.get(props, 'reactivate', false);
  const _reactivate = _.get(props, 'reactivate', false);

  const onSuccess = (e) => {
    onScanSuccess(e);
  };

  if (_reactivate) {
    // scanner.reactivate();
    this.scanner.reactivate();
  }

  return (
    <QRCodeScanner
      // ref={(node) => { scanner = node; }}
      ref={(node) => { this.scanner = node; }}
      onRead={onSuccess}
      reactivate
      reactivateTimeout={3000}
      showMarker
      cameraStyle={{ height: Metrics.height }}
      customMarker={(
        <View style={styles.container}>
          <View style={styles.header_bar} />
          {isSearch
            ? <View style={styles.block_top_search} />
            : <View style={styles.block_top} />}
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.block_left_right} />
            <View style={styles.block_rectangle} />
            <View style={styles.block_left_right} />
          </View>

          <View style={styles.block_bottom}>
            <Text style={styles.text_alert}>
              Đưa mã vạch vào ô vuông để quét
            </Text>
          </View>

        </View>
      )}
    />
  );
};


export default ScanQRCodeContainer;
