'use strict';
import _ from 'lodash';
import React from 'react';
import { Image, View, TouchableOpacity, Text } from 'react-native';
import { compareOmitState } from '../Services/utils';
import styles from './Styles/ShiftItemStyles'
import { Feather } from './Icons';
// import {
// } from "../Navigation1/navScreens";
import moment from 'moment';
// import BlockSingle from '../Components/'



const ShiftItem = (props) => {
  const { data_item, onPressButton } = props;
  // console.tron.log("TCL: ShiftItem -> data_item", data_item)


  // const company_email = _.get(props, ['data', 'employee_id', 'company_email'], '');
  // const name = _.get(props, ['data_item', 'name'], '');
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPressButton}>
        <View style={styles.block1}>
          <View style={styles.info}>
            <View style={{flexDirection:'column'}}>
            <Text style={styles.sub_text}>abc</Text>
            <Text style={styles.Caption}> Ngày tạo: </Text>
            <Text style={styles.Caption}> Lượt điểm danh: </Text>
            </View>

          </View>
          <View style={styles.icon}>
            <Feather name="clipboard" style={styles.icon_feather} />
          </View>
        </View>
        <View style={styles.lineStyle} />
      </TouchableOpacity>
    </View>
  );
}


function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ShiftItem, areEqual);
