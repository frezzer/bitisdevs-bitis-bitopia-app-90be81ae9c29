
import React from 'react';
import {
  TouchableOpacity, Text, View,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import styles from './styles/RankTimeStyles';
import Switch from '../../Lib/react-native-switch/Switch';

import { Feather, AntDesign, Unicons } from '../Icons';
import { Colors } from '../../Themes';


const RankTime = (props) => {
  const [switchValue, setSwitchValue] = React.useState(true);
  const toggleSwitch = (value) => {
    setSwitchValue(value);
  };


  return (
    <View style={styles.block3}>
      {/* <Text style={styles.Text3}>Thống kê</Text> */}
      <View>
        <View style={styles.block3_2}>
          <View style={styles.block_rank}>
            <Text style={styles.Text4}>Doanh số</Text>
            {/* <Text style={styles.Text5}>{switchValue}</Text> */}
            <Text style={styles.Text5}>{switchValue ? '********' : '5.000.000'}</Text>
            <Text style={styles.Text6}>Doanh số mục tiêu 6.000.000 đ</Text>
            <View style={styles.rank_bar}>
              <View style={styles.bar_width} />
            </View>
          </View>
          <View style={styles.block_time}>
            {/* <View style={styles.button_time}> */}
            <Feather style={styles.icon} name="clock" />
            {/* </View> */}
            <Text style={styles.Text5_1}>{switchValue ? '****' : '30.3h'}</Text>
            <Text style={styles.Text7}>Số giờ làm việc / tháng</Text>
          </View>
        </View>
      </View>
      <View style={styles.block3_3}>
        <View style={styles.switch_button}>
          <Switch
            onValueChange={toggleSwitch}
            onChangeText={(switchValue) => setSwitchValue(switchValue)}
            value={switchValue}
            circleActiveColor="white"
            backgroundActive={Colors.green1}
            circleBorderWidth={1}
            // circleBorderActiveColor={Colors.grey5}
            // circleBorderInactiveColor={Colors.grey5}
            // circleInactiveBorderColor={Colors.grey5}
            // ircleActiveBorderColor={Colors.grey5}
            backgroundInactive={Colors.grey5}
            circleInActiveColor={Colors.white}
            circleSize={25}
            changeValueImmediately
            innerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
            outerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
            switchLeftPx={2}
            switchRightPx={2}
          />
        </View>
        <Text style={styles.textDes}>Ẩn số tiền và số giờ làm việc</Text>
      </View>
    </View>
  );
};

export default RankTime;
