import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    ...ApplicationStyles.specialApp.dimensisons(375, 213),
    // ...ApplicationStyles.specialApp.alignCenter,
  },
  boxStart: {
    ...ApplicationStyles.specialApp.dimensisons(83, 54),
    ...ApplicationStyles.specialApp.alignCenter,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.grey4,
    marginLeft: Metrics.marginHuge,

  },
  boxStart1: {
    ...ApplicationStyles.specialApp.dimensisons(110, 54),
      backgroundColor:'white',
    ...ApplicationStyles.specialApp.alignCenter,
    // marginLeft: Metrics.marginHuge,

    paddingLeft: Metrics.marginHuge,

    // borderBottomWidth: 0.3,
    // borderBottomColor: Colors.grey4,
  },
  blockRank: {
    flex: 2,
    ...ApplicationStyles.specialApp.alignCenter,
    ...ApplicationStyles.specialApp.dimensisons(83, 54),
    // marginLeft: Metrics.marginTiny,

  },
  blockEmp: {
    flex: 5,
    ...ApplicationStyles.specialApp.alignCenter,

  },
  title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
  },
  boxEmp: {
    ...ApplicationStyles.specialApp.dimensisons(292, 54),
    backgroundColor: 'white',
    ...ApplicationStyles.specialApp.alignCenter,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.grey4,
    marginLeft: Metrics.marginMedium,
  },
  boxEmp1: {
    ...ApplicationStyles.specialApp.dimensisons(292, 54),
    backgroundColor: 'white',
    ...ApplicationStyles.specialApp.alignCenter,
    marginLeft: Metrics.marginMedium,


  },
  boxTitle: {
    flex: 1,
    borderBottomWidth: 0.3,
    borderBottomColor: Colors.grey4,
  },
});
