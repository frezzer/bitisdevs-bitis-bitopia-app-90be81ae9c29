import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

const full_price = 10000000;
const current_price = 2000000;
const test_bar = current_price / full_price * 200;


export default StyleSheet.create({
  Text3: { //
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginBottom: Metrics.marginXSmall,

  },
  block3: { //
    height: wp(225 / 375 * 100),
    backgroundColor: Colors.white,
    paddingHorizontal: Metrics.marginLarge,
    // ...ApplicationStyles.specialApp.alignCenter
    paddingTop: Metrics.paddingMedium,
    marginTop: Metrics.marginXSTiny,
    borderBottomWidth: 0.3,
    borderBottomColor: '#eee',
  },
  block3_2: { //
    height: wp(153 / 375 * 100),
    flexDirection: 'row',
  },
  block_rank: {
    flex: 1,
    height: wp(153 / 375 * 100),
    backgroundColor: Colors.blue2,
    marginRight: Metrics.marginTiny,
    borderRadius: Metrics.boderLarge,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: Metrics.marginXLarge,
  },
  button_rank: {
    ...ApplicationStyles.specialApp.dimensisons(86, 24),
    backgroundColor: Colors.blueOcean,
    borderRadius: Metrics.boderNomarl,
    justifyContent: 'center',
    marginBottom: Metrics.marginXSmall,
  },
  Text4: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textMedium,
    // textAlign: 'center',
  },
  Text5: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textXXLarge,
    paddingTop: Metrics.paddingSmall,
  },
  Text5_1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textXXLarge,
    paddingTop: Metrics.paddingXTiny,
  },
  Text6: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.white,
    fontSize: Fonts.size.textSmall,
    // paddingTop: Metrics.paddingMedium,

    // marginLeft:Metrics.marginXSmall
  },

  rank_bar: {
    ...ApplicationStyles.specialApp.dimensisons(200, 7),
    borderRadius: Metrics.boderTiny,
    backgroundColor: Colors.bar,
    marginVertical: Metrics.marginXSTiny,
  },
  bar_info: {
    // width: wp(140 / 375 * 100),
    height: wp(7 / 375 * 100),
    backgroundColor: Colors.green1,
    borderRadius: Metrics.boderTiny,
  },
  bar_width: {
    height: wp(7 / 375 * 100),
    backgroundColor: Colors.green1,
    borderRadius: Metrics.boderTiny,
    width: wp(test_bar / 375 * 100),
  },
  block_time: {
    ...ApplicationStyles.specialApp.dimensisons(100, 153),
    // ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.blue2,
    borderRadius: Metrics.boderLarge,
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    paddingTop: Metrics.paddingXsLarge,
  },
  button_time: {
    backgroundColor: Colors.blue2,
    ...ApplicationStyles.specialApp.dimensisons(32, 32),
    // ...ApplicationStyles.specialApp.alignCenter,
    // alignSelf: 'center',
    borderRadius: wp(16 / 375 * 100),
    marginBottom: Metrics.marginXSmall,
  },
  icon: {
    color: 'white',
    fontSize: 24 / 375 * Metrics.width,
    alignItems: 'center',
    // alignItems: 'center',
    justifyContent: 'center',
  },
  Text7: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.white,
    fontSize: Fonts.size.textSmall,
    textAlign: 'center',
    paddingTop: wp(12 / 375 * 100),
  },
  block3_3: {
    height: wp(50 / 375 * 100),
    flexDirection: 'row',
    alignItems: 'center',
  },
  switch_button: {
    ...ApplicationStyles.specialApp.dimensisons(40, 22),
    ...ApplicationStyles.specialApp.alignCenter,
    marginRight: Metrics.marginXSmall,
  },
  animationSwicth: {
    transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }],
  },
  textDes: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grayLight,
    fontSize: Fonts.size.textMedium,
    marginVertical: Metrics.paddingTiny,

    // marginLeft:Metrics.marginXSmall
  },
});
