import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
  app: {
    ...ApplicationStyles.specialApp.padding(0, 0, 0, 0, 20, 8),
    ...ApplicationStyles.specialApp.dimensisons(109, 109),
    ...ApplicationStyles.specialApp.alignCenter,
    // marginVertical: Metrics.marginXTiny,
    backgroundColor: Colors.white,
    // borderRadius: Metrics.boderSmall,
  },
  appcircle: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    padding: 10 / 375 * Metrics.width,
    borderRadius: Metrics.boderMaxium,
  },
  appicon: {
    color: Colors.white,
    fontSize: 24 / 375 * Metrics.width
  },
  apptitle: {
    marginTop: Metrics.marginXTiny,
    ...ApplicationStyles.specialApp.typography.titleAppButton
  }
})
