import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({

    container: {
    },
    block1: {
        height: wp(118 / 375 * 100),
        flexDirection: 'row'

    },
    Text1: {
        fontFamily: Fonts.type.fontItalic,
        color: Colors.black,
        fontSize: Fonts.size.textMedium,
        textAlign: 'center'
    },
    Text2: {

        fontFamily: Fonts.type.fontBold,
        color: Colors.black,
        fontSize: Fonts.size.textHuge,
        textAlign: 'center'

    },
    lineStyle: {
        borderWidth: 0.3,

    },
    info1: {
        height: wp(59 / 375 * 100),
        flexDirection:'row'
    },
    info2: {
        height: wp(59 / 375 * 100),
    },
    block2: {
        flex: 1,
    },
    block_image: {
        width: wp(100 / 375 * 100),
        height: wp(118 / 375 * 100),
        justifyContent: 'center',
        alignItems: 'center'
    },

    shift1: {
        width: wp(137 / 375 * 100),
        height: wp(59 / 375 * 100),
        justifyContent:'center'

    },
    shift2: {
        flex:1,
        justifyContent:'center'

    },
    circle:{
        height:wp(6 / 375 * 100),
        width: wp(6 / 375 * 100),
        borderRadius: wp(3 / 375 * 100),
        backgroundColor:'blue',

    },
    block2_1:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    Text3: {

        fontFamily: Fonts.type.fontItalic,
        color: Colors.grayLight,
        fontSize: Fonts.size.textMedium,
        textAlign: 'center'

    },
    Text4: {

        fontFamily: Fonts.type.fontItalic,
        color: Colors.black,
        fontSize: Fonts.size.textMedium,
        textAlign: 'center'

    },
    Text5: {

        fontFamily: Fonts.type.fontBold,
        color: Colors.black,
        fontSize: Fonts.size.textMedium,
    },
    Text6:{
        fontFamily: Fonts.type.fontRegular,
        color: Colors.blueOcean,
        fontSize: Fonts.size.textMedium,  
    }, 
     lineStyle: {
        borderWidth: 0.5,
        borderColor: Colors.greenLight,
      },


})
