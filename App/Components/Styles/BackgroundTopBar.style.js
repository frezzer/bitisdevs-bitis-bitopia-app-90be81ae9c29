import { StyleSheet, Platform } from 'react-native';
import { Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    width: Metrics.width + 6,
    position: 'absolute',
    top: 0,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 15,
  }
});
