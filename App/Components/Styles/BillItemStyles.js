import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    marginBottom: Metrics.marginTiny,
    flexDirection: 'row',
    paddingHorizontal: Metrics.paddingXXSmall,
    paddingVertical: Metrics.paddingMedium
  },
  part1: {
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingXXSmall
  },
  part2: {
    flex: 1,
    paddingHorizontal: Metrics.paddingXXSmall,
    justifyContent: 'space-between'
  },
  total_block: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  icon: {
    fontSize: Fonts.size.textXHuge,
    color: Colors.blue,
    marginBottom: Metrics.marginXXTiny
  },
  // Text
  text1: {
    ...Fonts.style.subtitle,
    fontFamily: Fonts.type.fontBold,
    marginBottom: Metrics.marginXTiny
  },
  text2: {
    fontSize: Fonts.size.textXSmall,
    fontFamily: Fonts.type.fontBold,
    fontStyle: 'italic',
    color: Colors.blue
  },
  text3: {
    ...Fonts.style.caption,
    color: Colors.black
  },
  text5: {
    ...Fonts.style.subtitleExtra,
    fontFamily: Fonts.type.fontBold,
    fontStyle: 'italic',
    color: Colors.blue
  }
})
