import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Metrics, Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    height: wp(94 / 375 * 100),
    backgroundColor: Colors.white,
    // elevation: 5,
    // shadowColor: 'rgba(0, 0, 0, 0.2)',
    // shadowOpacity: 2,
    // shadowRadius: 6,
    flexDirection: 'row',
  },
  tab_item: {
    flex: 1,
    // backgroundColor: Colors.blue,
    // margin: 2,
    paddingTop: Metrics.paddingSmall,
  },
});
