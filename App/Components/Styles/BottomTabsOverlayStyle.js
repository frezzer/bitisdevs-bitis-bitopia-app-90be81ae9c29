import { StyleSheet, Platform } from 'react-native'
import { Metrics, Colors } from '../../Themes';
import { widthPercentageToDP } from 'react-native-responsive-screen';
// import DeviceInfo from 'react-native-device-info'
// import {} from 'react-native-device-info'

export default StyleSheet.create({
  container: {
    width: Metrics.width + 6,
    backgroundColor: Colors.white,
    position: 'absolute',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 10,
  },
})
