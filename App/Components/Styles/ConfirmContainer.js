import _ from 'lodash';
import React from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import styles from './Styles/ConfirmContainerStyles';

const ConfirmContainer = (props) => {
  const onPressConfirm = _.get(props, 'onPressConfirm', () => { });
  const titleText = _.get(props, 'titleText', 'Xác nhận đóng ca');
  const descriptionText = _.get(props, 'descriptionText', 'Sau khi bạn xác nhận đóng ca các nhân viên khác sẽ không thể tan ca');
  const store_name = _.get(props, 'store_name', 'Sau khi bạn xác nhận đóng ca các nhân viên khác sẽ không thể tan ca');
  const name_of_shift = _.get(props, 'name_of_shift', 'Sau khi bạn xác nhận đóng ca các nhân viên khác sẽ không thể tan ca');
  return (
    <View style={styles.container}>
      <View style={styles.block1}>
        <View style={styles.box} >
          <Text style={styles.Text1}>{titleText}</Text>
          <Text style={styles.Text2}>{descriptionText}</Text>
          <Text style={styles.Text2}>{store_name}</Text>
          <Text style={styles.Text2}>{name_of_shift}</Text>
          
        </View>
      </View>
      <View style={styles.button}>
        <TouchableOpacity
          onPress={onPressConfirm}
          style={styles.button_check_in}>
          <Text style={styles.Text12}>Xác Nhận</Text>
        </TouchableOpacity>
      </View>

    </View>
  );
}

export default ConfirmContainer;
