import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../Themes';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    infoblock: {
        paddingHorizontal: Metrics.paddingMedium,
        paddingVertical: Metrics.paddingXXLarge,
        marginHorizontal: Metrics.marginMedium,
        marginTop: Metrics.marginMedium,
        marginBottom: Metrics.marginTiny,
        backgroundColor: Colors.silver,
        height: 150 / 375 * Metrics.width,
        width: 343 / 375 * Metrics.width,
        borderRadius: Metrics.boderLarge,
        alignSelf: 'center',
        // justifyContent: 'center',
        position: 'absolute',
        justifyContent:'center',
    },
    barcodeblock: {
        paddingHorizontal: Metrics.paddingMedium,
        paddingVertical: Metrics.paddingXXLarge,
        marginHorizontal: Metrics.marginMedium,
        marginTop: Metrics.marginMedium,
        marginBottom: Metrics.marginTiny,
        backgroundColor: Colors.white,
        borderRadius: Metrics.boderLarge,
        borderColor: Colors.silver,
        borderWidth: 2,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        height: 150 / 375 * Metrics.width,
        width: 343 / 375 * Metrics.width,
    },
    barcode: {
        height: 93 / 375 * Metrics.width,
    },
    barcodeText: {
        color: Colors.grayMedium,
        fontSize: Fonts.size.textLarge,
        textAlign: 'center'
    },
    jobposition: {
        color: Colors.black,
        fontFamily: Fonts.type.fontBold,
        fontSize: Fonts.size.textXLarge,
    },
    jobtitle: {
        color: Colors.blue,
        fontFamily: Fonts.type.fontRegular,
        fontSize: Fonts.size.textLarge,
        marginBottom:Metrics.marginXTiny
        
    },
    email: {
        color: Colors.grayLight,
        fontFamily: Fonts.type.fontItalic,
        fontStyle: 'italic',
        fontSize: Fonts.size.textMedium,
    },
    phone: {
        color: Colors.grayLight,
        fontFamily: Fonts.type.fontItalic,
        fontStyle: 'italic',
        fontSize: Fonts.size.textMedium,
    },
    notice: {
        // position: 'absolute',
        color: Colors.grayLight,
        fontFamily: Fonts.type.fontItalic,
        fontStyle: 'italic',
        fontSize: Fonts.size.textSmall,
        textAlign: 'center',
    },
    tapAnimationblock: {
        marginHorizontal: Metrics.marginMedium,
        marginTop: Metrics.marginMedium,
        marginBottom: Metrics.marginTiny,
        borderRadius: Metrics.boderLarge,
        alignSelf: 'center',
        position: 'absolute',
        height: 150 / 375 * Metrics.width,
        width: 343 / 375 * Metrics.width,

    },
    tapAnimation: {
        height: 90 / 375 * Metrics.width,
        width: 90 / 375 * Metrics.width,
        marginTop: 70 / 375 * Metrics.width,
        marginLeft: 263 / 375 * Metrics.width,
    }

})
