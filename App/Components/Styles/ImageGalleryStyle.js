import { StyleSheet } from 'react-native'
import { Metrics, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    // flex: 1,
    height: 160,
    borderRadius: Metrics.boderNomarl,
    overflow: 'hidden'
  },
  Carousel: {
    borderRadius: 10,
    overflow: 'visible', // for custom animations
  },
  slide: {
    overflow: 'hidden', // for custom animations
    flex: 1
  },
  pagination : {

  },
  paginationContainer: {
    position : 'absolute',
    paddingVertical: 8,
    bottom : 0,
    alignSelf: 'center'
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    alignSelf: 'center'
    // marginHorizontal: 8
  }
})
