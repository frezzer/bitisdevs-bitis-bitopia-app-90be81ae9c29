import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex : -1,
    width : Metrics.width - Metrics.marginMedium * 2
  },
  Row: {
    height :40,
    paddingVertical: 0,
    borderRadius :20
  },
  TextInput : {
    backgroundColor : Colors.transparent_w,
    margin : 0,
    padding : 0
  }
})
