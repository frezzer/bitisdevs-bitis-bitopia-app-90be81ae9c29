import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor : 'blue',
    alignSelf : 'center',
    marginHorizontal: Metrics.paddingSmall,
    padding : 0
  }
})
