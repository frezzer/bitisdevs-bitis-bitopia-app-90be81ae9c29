import { StyleSheet } from 'react-native'
import { Metrics, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    position: 'absolute',
  },
  background: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    height: Metrics.height,
    width: Metrics.width,
    backgroundColor: Colors.graySPLight,
    opacity: 0.1
  },
  content: {
    height: Metrics.height,
    width: Metrics.width,
    backgroundColor: Colors.transparent,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxIndicator: {
    height: 100 / 375 * Metrics.width,
    width: 100 / 375 * Metrics.width,
    backgroundColor: Colors.white,
    borderRadius: 16,
    alignItems: "center",
    justifyContent: 'center'
  },

  loadingText: {
    color: '#888888',
    alignSelf: 'center',
    marginTop: 80,
    fontStyle: 'italic',
    // fontWeight: 'bold',
    fontSize: 12
  },
});
