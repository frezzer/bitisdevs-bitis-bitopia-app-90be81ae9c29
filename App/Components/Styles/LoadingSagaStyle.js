import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    right: 0,
    left: 0,
    bottom: 0,
    width: Metrics.screenWidth,
    // height: Metrics.screenHeight,
    // alignSelf: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'white',
  },
  spinner: { size: 1, color: 'white' },

  loadingText: {
    color: '#888888',
    alignSelf: 'center',
    marginTop: 80,
    fontStyle: 'italic',
    // fontWeight: 'bold',
    fontSize: 12
  },

  loadingblock: {
    width: 150,
    height: 165,
    // backgroundColor: 'red',
    paddingVertical: 40,
    flexDirection: 'column-reverse',
    // borderColor: '#eeeeee',
    // borderWidth: 1,
    // borderRadius: 12,
  },

  loadingblockBackground: {
    width: 150,
    height: 150,
    backgroundColor: 'white',
    paddingVertical: 24,
    flexDirection: 'column-reverse',
    borderColor: '#eeeeee',
    borderWidth: 1,
    borderRadius: 12,
    position: 'absolute',
    opacity: 0.95,
  }
});
