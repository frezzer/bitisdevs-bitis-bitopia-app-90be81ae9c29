import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    // flex: 1
  },
  Item: {
    height: 100,
    flex: 1,
    margin: Metrics.marginSmall,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0.75,
    shadowRadius: Metrics.boderNomarl,
    shadowColor: Colors.silverDark,
    shadowOffset: { height: 0, width: 0 },
    backgroundColor : Colors.white,
    elevation: 1,
    borderRadius : Metrics.boderNomarl,
    // overflow : 'hidden'
    padding : 0
  },
  ItemButton : {
    margin : 0,
    padding : 0
  },
  ItemText: {
    color: Colors.silverDark,
    margin : 0,
    padding : 0,
    marginTop : Metrics.marginMedium,
    fontStyle: 'normal',
    fontWeight: 'normal',
  },
  ItemIcon: {
    color: Colors.silverDark,
    fontSize: 47,
    margin : 0,
    padding : 0
  }
})
