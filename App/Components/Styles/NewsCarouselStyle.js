import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    //flex: 1
  },
  slider:{
    height: 'auto',
    width: '100%',
  },
  imgslider: {
    height: 158 / 375 * Metrics.width,
    width: 295 / 375 * Metrics.width,
    borderRadius : Metrics.boderNomarl,
    backgroundColor: Colors.grayDark
  }
})
export const BackgroundItemGradient = {
  colors: ['rgba(255,255,255,.15)', Colors.transparent],
  locations: [0.2, 1],
}
