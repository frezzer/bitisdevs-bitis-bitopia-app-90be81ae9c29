import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Metrics, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.transparent,
    // backgroundColor: 'red',
    // alignSelf : 'flex-start',
    marginRight: Platform.select({ ios: 0, android: Metrics.marginMedium }),

  },
  content: {
    // paddingRight: Metrics.paddingXLarge,
    // paddingBottom: Metrics.paddingXTiny,
  },
  icon: {
    fontSize: 21,
    color: 'white',
  },
});
