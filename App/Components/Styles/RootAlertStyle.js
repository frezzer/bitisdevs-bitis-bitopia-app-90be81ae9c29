import { StyleSheet, } from 'react-native'
import { Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    // flex: 1,
    position: 'absolute',
    // backgroundColor: 'red',
    width: Metrics.width,
    // height: Metrics.height,
    // opacity: 0.2,
    alignItems: 'flex-end'
  },

  content: {
    flexDirection: 'row',
    marginTop: Metrics.width * 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    width: Metrics.width - 60,
    backgroundColor: '#111',
    opacity: 0.8,
    marginRight: Metrics.marginTiny,
  },

  
  loadingBox: {
    paddingVertical: Metrics.paddingTiny,
    paddingHorizontal: Metrics.paddingMedium
  },
  spinner: { 
    color: 'white' 
  },
  textBox: {
    paddingVertical: Metrics.paddingXTiny,
  },
  loadingText: {
    paddingRight: Metrics.paddingSmall,
    color: 'white',
    fontSize: Fonts.size.textMedium,
    textAlign: 'right',
  },
});
