import { StyleSheet, } from 'react-native'
import { Metrics, Fonts, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    width: Metrics.width,
    height: 2*Metrics.paddingXTiny + Fonts.size.textMedium +2, 
  },

  content: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: Colors.greenMedium,
  },
  textBox: {
    paddingVertical: Metrics.paddingXTiny,
  },
  loadingText: {
    color: Colors.white,
    fontSize: Fonts.size.textMedium,
    lineHeight: Fonts.size.textMedium + 2
  },
});
