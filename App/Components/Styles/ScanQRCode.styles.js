
import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'

const backButtonWidth = 44;
const confirmButtonHeight = 44;

export default StyleSheet.create({
  // Khanh rewrite 
  container: {
    flex: 1,
    width: Metrics.width,
    backgroundColor: 'transparent'
  },
  header_bar: {
    // height: wp(88 / 375 * 100),
    height: Metrics.navigationBarHeight,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    // backgroundColor: 'red',
  },
  block_top: {
    height: wp(32 / 375 * 100),
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  block_top_search: {
    height: wp(92 / 375 * 100),
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  block_left_right: {
    height: Metrics.height < 670 ? wp(290 / 375 * 100) : wp(300 / 375 * 100),
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  block_rectangle: {
    height: Metrics.height < 670 ? wp(290 / 375 * 100) : wp(300 / 375 * 100),
    width: Metrics.height < 670 ? wp(290 / 375 * 100) : wp(300 / 375 * 100),
    borderWidth: 3,
    borderColor: Colors.white,
  },
  block_bottom: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  // Text
  text_alert: {
    marginTop: Metrics.marginTiny,
    margin: Metrics.marginMedium,
    textAlign: 'center',
    // fontStyle: 'italic',
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.white,
  },

})
