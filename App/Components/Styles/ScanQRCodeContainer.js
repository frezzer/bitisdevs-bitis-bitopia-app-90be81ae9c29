'use strict';
import _ from 'lodash';
import React from 'react';
import {
  View, Text, TouchableOpacity,
  // Modal, FlatList, Alert, Button,
  // Dimensions, ScrollView, TouchableHighlight,
  // RefreshControl, StatusBar, SafeAreaView, Linking
} from 'react-native';
import { Feather } from '../Components/Icons';
import { Colors, Fonts, Images, Metrics } from '../Themes';

import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './Styles/ScanQRCodeContainerStyles';

const ScanQRCodeContainer = props => {
  const onScanSuccess = _.get(props, 'onScanSuccess', () => { });
  const onPressBackButton = _.get(props, 'onPressBackButton', () => { });
  const onPressContinue = _.get(props, 'onPressContinue', () => { });
  const continueText = _.get(props, 'continueText', 'Tiếp tục');
  // const onPressContinue = () => Navigation.push(props.componentId, navConfirmScreen);
  const title = _.get(props, 'title', '');
  const info_name = _.get(props, 'info_name', '');
  const current_time = _.get(props, 'current_time', '');
  const reactivate = _.get(props, 'reactivate', false);
  return (
    <View style={styles.container}>
      <QRCodeScanner
        onRead={onScanSuccess}
        reactivate={reactivate}
        reactivateTimeout={5000}
        showMarker={true}
        cameraStyle={{ height: Metrics.height }}
        customMarker={
          <View style={styles.blackBackground}>
            <View style={styles.rectangleContainer}>
              <View style={styles.topOverlay} />
              <View style={{ flexDirection: "row" }}>
                <View style={styles.leftAndRightOverlay} />
                <View style={styles.rectangle} />
                <View style={styles.leftAndRightOverlay} />
              </View>
              <View style={styles.bottomOverlay}>
                <View style={styles.bottomOverlayBg} />
                <Text style={styles.TextAlert}> Đưa mã vạch vào ô vuông để quyét </Text>
                <View style={styles.infoblock}>
                  <View style={styles.infoBlockDetail}>
                    <View style={{
                      width: '40%',
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                      <Text style={{
                        margin: Metrics.marginXTiny,
                        textAlign: 'center',
                        fontSize: Fonts.size.textSmall,
                        fontStyle: 'italic',
                        color: Colors.white,
                      }}>
                        Giờ quét</Text>
                      <Text style={{
                        margin: Metrics.marginXTiny,
                        textAlign: 'center',
                        fontSize: Fonts.size.textHuge,
                        fontWeight: 'bold',
                        color: Colors.white,
                      }}>{current_time}</Text>
                    </View>
                    <View style={{ width: 1, height: '100%', backgroundColor: Colors.white }} />
                    <View style={{
                      width: '60%',
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                      <Text style={{
                        margin: Metrics.marginXTiny,
                        textAlign: 'center',
                        fontSize: Fonts.size.textMedium,
                        fontWeight: 'bold',
                        color: Colors.white,
                      }}>{info_name}</Text>
                      <Text style={{
                        margin: Metrics.marginXTiny,
                        textAlign: 'center',
                        fontSize: Fonts.size.textSmall,
                        fontStyle: 'italic',
                        color: Colors.white,
                      }}>Ca 1 - 8/9/2019</Text>
                    </View>
                  </View>
                  <View style={styles.infoblockbackground} />
                </View>
              </View>
            </View>
          </View>
        }
      />

      <View style={styles.implement}>
        <View style={styles.topBar}>
          <View style={styles.backButton}>
            <Feather style={styles.backButtonButtonText} name='arrow-left' />
            <TouchableOpacity
              style={styles.backButtonTouchable}
              onPress={onPressBackButton}
            />
          </View>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>{title}</Text>
          </View>
        </View>
        <View>
          <View style={styles.bottomTabs}>
            <View style={styles.confirmButton}>
              <Text style={styles.confirmButtonText}> {continueText} </Text>
              <TouchableOpacity style={styles.confirmButtonTouchable} onPress={onPressContinue} />
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}


export default ScanQRCodeContainer;
