import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({

    container: {
        // flex: 1,
        backgroundColor: Colors.white
    },
    Text1: {
        fontFamily: Fonts.type.fontBold,
        color: Colors.black,
        fontSize: Fonts.size.textLarge,
        // marginLeft: Metrics.marginSmall
    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: Colors.greenLight,

    },
    icon: {
        width: wp(50 / 375 * 100),
        // height: wp(50 / 375 * 100),
        // backgroundColor:'yellow',
        justifyContent: 'center',
        alignItems: 'center'


    },
    icon_feather: {
        color: 'black',
        fontSize: 20 / 375 * Metrics.width,

    },
    block1: {
        flexDirection: 'row',
        // marginLeft: Metrics.marginSmall,
        // backgroundColor:'red',
        height: wp(89 / 375 * 100),
    },
    block2: {
        padding: Metrics.paddingSmall,
        backgroundColor: 'blue'
    },
    info: {
        // alignItems: 'center',
        justifyContent: 'center',
        width: wp(325 / 375 * 100),
        // height: wp(50 / 375 * 100),
        // backgroundColor: Colors.greenDark,
        paddingHorizontal: Metrics.paddingTiny,
        marginVertical: Metrics.marginMedium

    },
    sub_text: {
        fontFamily: Fonts.type.fontBold,
        color: Colors.black,
        fontSize: Fonts.size.textMedium,
        marginLeft:Metrics.marginXXTiny,
        marginBottom:Metrics.marginXXTiny
    },
    Caption: {

        // textAlign: 'left',
        color: Colors.grayLight,
        fontSize: Fonts.size.textSmall,
        fontFamily: Fonts.type.fontLight,
        marginBottom:Metrics.marginXXTiny
        // top: -1,
        // lineHeight: 20,
        // fontWeight: 'bold',
    },

})
