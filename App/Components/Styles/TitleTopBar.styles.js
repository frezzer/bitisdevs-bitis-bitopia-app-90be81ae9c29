import { StyleSheet, Platform } from 'react-native'
import { Colors, Fonts, Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.transparent,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: Metrics.paddingXXTiny,
    flexDirection:'row',
    // backgroundColor:'red'
    // alignSelf: 'flex-start', 
    // paddingTop: Platform.select({
    //   ios: Metrics.marginTiny,
    //   android: 0
    // })
  },
  titleTopbarText: {
    color: Colors.white,
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    fontWeight: Platform.select({
      ios: '800',
      android: 'bold'
    })
  }
});
