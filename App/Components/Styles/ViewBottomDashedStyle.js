import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    borderStyle: 'dotted',
    borderWidth: 1,
    borderColor: Colors.theme.border,
    position: 'relative',
    overflow: 'hidden',
  },
  topMask: {
    height: 1,
    width: 9999,
    backgroundColor: 'white',
    position: 'absolute',
    top: -1,
    left: -1,
  },
  rightMask: {
    height: 9999,
    width: 1,
    backgroundColor: 'white',
    position: 'absolute',
    top: -1,
    right: -1,
  },
  leftMask: {
    height: 9999,
    width: 1,
    backgroundColor: 'white',
    position: 'absolute',
    top: -1,
    left: -1,
  },
})
