
import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default StyleSheet.create({
  container: {
    flex:1
  },
  block_infor:{
    height: wp(44 / 375 * 100),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth:0.3,
    borderBottomColor: Colors.grey4,
    marginHorizontal:Metrics.paddingMedium
  },
  Text1: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,  
    fontSize: Fonts.size.textMedium,
    // marginLeft: Metrics.marginLarge,
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey9,
    fontSize: Fonts.size.textMedium,
    // marginLeft: Metrics.marginLarge,
  },
  lineStyle: {
    borderWidth: 0.3,
    borderColor: Colors.grey4,
    // marginHorizontal: Metrics.marginMedium,
  },
  
}); 
