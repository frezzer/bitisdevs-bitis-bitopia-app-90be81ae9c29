import React from 'react';
import {
  View, Text, Button, TouchableOpacity,
  SafeAreaView, Linking, TextInput,
} from 'react-native';
import _ from 'lodash';
import { compareOmitState } from '../../Services/utils';
// Styles
import styles from './Styles/TextInfo.styles';
// import { TextInput } from 'react-native-gesture-handler';

const TextInfo = (props) => {
  //   const onPressButton1 = _.get(props, 'onPressButton1', () => { });
  //   const onPressButton2 = _.get(props, 'onPressButton2', () => { });
  const Text1 = _.get(props, 'Text1');
  const Text2 = _.get(props, 'Text2');
  const styles2 = _.get(props, ['stylesCustom']);
  const _disable = _.get(props, ['disable'], false);


  //   const _styles1 = _.get(props, 'styles1', {});
  //   const _styles2 = _.get(props, 'styles2', {});
  return (

    <View style={[styles.container]}>
      <View style={[styles2, styles.block_infor]}>
        <View>
          <Text style={styles.Text1}>{Text1}</Text>
        </View>
        <View>
          <Text style={styles.Text2}>{Text2}</Text>
        </View>
      </View>
      {/* <View style={styles.lineStyle} /> */}

    </View>

  );
};

export default React.memo(
  TextInfo,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
