import React from 'react';
import _ from 'lodash';
import { View, Text } from 'react-native';
import { useSelector } from 'react-redux';
import { MeSelectors } from '../Redux/MeRedux';
import UserDetailActions, { UserDetailSelectors } from '../Redux/user/UserDetailRedux'
import { getFullName } from '../Services/utils'
import styles from './Styles/TitleTopBar.styles';


const TitleTopBar = (props) => {
  // const fullName = getFullName(user_detail);
  const me_payload = useSelector(state => MeSelectors.selectPayload(state.me));
  const user_detail = useSelector( state => UserDetailSelectors.selectPayload(state.user.userDetail));
  const employee_id = _.get(me_payload, 'employee_id', {});
  const firstName = _.get(user_detail, 'firstName');
  const lastName = _.get(user_detail, 'lastName');
  const middleName = _.get(user_detail, ['data', 'middle_name']);
  const styleView = _.get(props, ['styleView'], {});
  const styleText = _.get(props, ['styleText'], {});
  const fullName=_.concat([lastName, middleName ? ' ' : '', middleName, firstName ? ' ' : '', firstName])
  console.tron.log("TCL: TitleTopBar -> fullName", fullName)
  

  return (
    <View style={[styles.container, styleView]}>
      <Text style={[styles.titleTopbarText, styleText]}>
        Chào 
      </Text>
  <Text style={[styles.titleTopbarText, styleText]}> {fullName} !</Text>
    </View >
  )
}

export default TitleTopBar;
