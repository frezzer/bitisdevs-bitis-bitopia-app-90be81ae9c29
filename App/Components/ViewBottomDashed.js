import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import styles from './Styles/ViewBottomDashedStyle'


export default class ViewBottomDashed extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render() {
    const { backgroundColor } = this.props.style
    return (
      <View {...this.props} style={{ ...styles.container, ...this.props.style, backgroundColor }}>
        <View style={styles.topMask} />
        <View style={styles.rightMask} />
        <View style={styles.leftMask} />
        {this.props.children}
      </View>
    )
  }
}
