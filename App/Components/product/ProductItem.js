import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import Swipeout from 'react-native-swipeout';
import styles from './styles/ProductItem.styles'
import { compareOmitState, formatMoney } from '../../Services/utils';
import { Unicons } from '../../Components/Icons'
import { Fonts, Colors } from '../../Themes'
import BlockDouble from '../../Components/Block/BlockDouble'

const ProductItem = (props) => {
  const { data_item,  } = props;

  // Buttons
  var swipeoutBtns = [
    {
      component: (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Unicons name='times-circle' style={{ fontSize: Fonts.size.textXHuge, color: Colors.white }} />
          <Text style={{ ...Fonts.style.caption, color: Colors.white, fontFamily: Fonts.type.fontBold }}>Xóa</Text>
        </View>
      ),
      backgroundColor: 'red',
      onPress: props.onPressDelete
    }
  ]

  return (
    <Swipeout
      right={swipeoutBtns}
    >
      <BlockDouble
        buttonText1={`x${data_item.quantity ? data_item.quantity : 0} - ${data_item.uuid}`}
        buttonText2={`Đơn giá : ${formatMoney(Number(data_item.price))} VND`}
        btnDisalble={true}
        iconName='receipt-alt'
        hasArrow={false}
        imageLink={data_item.image_link}
      />
    </Swipeout>
  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ProductItem, areEqual);
