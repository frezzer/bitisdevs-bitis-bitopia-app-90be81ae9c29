
import _ from 'lodash';
import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

// Components

import styles from './styles/ProductList.styles';
import { compareOmitState } from '../../Services/utils';
import ProductItem from './ProductItem';


const RenderItem = React.memo((props) => {
  const { item } = props;
  return (
    <View id={item.id}>
      <ProductItem data_item={item} />
    </View>
  );
}, areEqual);


const ProductList = (props) => {
  const data = _.get(props, 'data_product', null);
  const { onPressButton } = props;
  // For render
  const renderItem = ({ item }) => <RenderItem item={item} />;
  return (
    <View>
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={data}
        renderItem={renderItem}
        initialNumToRender={3}
        ListEmptyComponent={(
          <View style={styles.block_empty}>
            <Text>Bạn chưa có sản phẩm nào!</Text>
          </View>
        )}
      />
    </View>
  );
};
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ProductList, areEqual);
