import _ from 'lodash';
import { StyleSheet, Platform } from 'react-native';
import { Colors, Metrics, Fonts, ApplicationStyles } from '../../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },
  block_empty: {
    height: wp(48/375*100),
    backgroundColor: Colors.white,
    alignItems: 'center', 
    padding: Metrics.paddingSmall 
  },
  Text1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginLeft: Metrics.marginSmall
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,

  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375,69),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent:'center',
    paddingHorizontal:Metrics.paddingMedium
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent:'center',
    marginLeft: Metrics.marginXSmall,
    flex:6
  },
  block_image: {
    justifyContent: 'center',
    flex:1
    
  },
  sub_text: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // marginBottom: Metrics.marginXTiny
  },

  block_check: {

    paddingTop: Metrics.paddingXSmall,
    // backgroundColor: 'red',
    justifyContent:'center',
    alignItems:'center',
    // ...ApplicationStyles.specialApp.alignCenter,
    // alignSelf: 'center',
    // flex:1
    
  },
  button_check: {
    // backgroundColor: Colors.greenMedium,
    ...ApplicationStyles.specialApp.dimensisons(38,38),
    ...ApplicationStyles.specialApp.alignCenter,
    // alignSelf:'center',
    borderRadius: wp(1 / 375 * 100),
    marginBottom:Metrics.marginXSmall
  },
  icon: {
    color: Colors.theme.text,
    fontSize: 20 / 375 * Metrics.width,
    alignItems:'center',
    alignItems:'center',
    justifyContent:'center'
  },
  caption:{
    fontFamily:Fonts.type.fontItalic,
    fontSize:Fonts.size.textSmall,
    color: Colors.grayLight

  }
  


})
