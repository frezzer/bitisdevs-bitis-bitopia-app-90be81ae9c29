import React from 'react';
import _ from 'lodash';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity, Text } from 'react-native';

import { Feather } from './Icons';
import styles from './Styles/AppDashboard.styles';
import { compareOmitState } from '../Services/utils';

function AppDashboard(props) {

  const { colors, appicon, apptitle, onPressButton } = props;
  return (
    <TouchableOpacity style={styles.app} onPress={onPressButton}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={colors}
        style={styles.appcircle}>
        <Feather style={styles.appicon} name={appicon} />
      </LinearGradient>
      <Text style={styles.apptitle}>{apptitle}</Text>
    </TouchableOpacity >
  );
}

export default React.memo(
  AppDashboard,
  (prevProps, nextProps) => compareOmitState(nextProps, prevProps, [])
);


