// Simple React Native specific changes
// import url from 'url'
// import { NativeModules } from 'react-native'
// const { hostname } = url.parse(NativeModules.SourceCode.scriptURL)
import '../I18n/I18n';

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  service: {
    bitis: {
      baseURL: 'http://bitisvn.com:8096',
      // baseURL: 'http://192.168.1.6:8096',
    },
  },
  useCodePush: !__DEV__,
  version: '1.1.13',
};
