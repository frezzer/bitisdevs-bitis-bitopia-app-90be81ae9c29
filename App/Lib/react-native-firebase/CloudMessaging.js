'use strict';
import _ from 'lodash';
import React from 'react';
import { AsyncStorage, View } from 'react-native';
import firebase from 'react-native-firebase';
import type { RemoteMessage, Notification, NotificationOpen} from 'react-native-firebase';

export default class FirebaseCloudMessaging extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    this.notificationListener = null;
    this.notificationOpenedListener = null;
    this.displayNotification = null;
    this.createNotificationListeners = this.createNotificationListeners.bind(this);
    this.removeListener = this.removeListener.bind(this);
    // this.onNotificationActions = this.onNotificationActions.bind(this);
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission()
    if (!!enabled) {
      this.getToken()
    } else {
      this.requestPermission()
    }
  }

  //3
  async getToken() {
    const fcmToken = await AsyncStorage.getItem('fcmToken')
    if (!fcmToken) {
      const fcmToken1 = await firebase.messaging().getToken()
      if (fcmToken1) {
        await AsyncStorage.setItem('fcmToken', fcmToken1)
      }
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission()
      this.getToken()
    } catch (error) {

    }
  }

  async componentDidMount() {
    await this.checkPermission()
    this.createNotificationListeners()
    // this.notificationOpenedListener()
    // this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
    //   // Process your notification as required
    //   // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    //   console.tron.log("onNotificationDisplayed -> notification", notification)
    // });
    // this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {
    //   console.tron.log("onNotification -> notification", notification)
    //   // Process your notification as required
    // });

    // this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
    // console.tron.log("DebugLog: FirebaseCloudMessaging -> componentDidMount -> notificationOpen", notificationOpen)
    //   // Get the action triggered by the notification being opened
    //   const action = notificationOpen.action;
    //   // Get information about the notification that was opened
    //   const notification: Notification = notificationOpen.notification;
    // });
  }

  componentWillUnmount() {
    this.removeListener()
    // this.notificationOpenedListener()
    // this.removeNotificationDisplayedListener();
    // this.removeNotificationListener();
    // this.removeNotificationOpenedListener();
  }


  removeListener(){
    this.messageListener()
    // this.notificationListener()
    // this.notificationOpenedListener()
    // this.removeNotificationDisplayedListener()

  }


  createNotificationListeners() {
    /**
     * Triggered for data only payload in foreground
     */
    this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
      console.tron.log('onMessage', message);
    });

    // this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
    //   // Process your notification as required
    //   // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    //   console.tron.log("DebugLog: FirebaseCloudMessaging -> createNotificationListeners -> notification", notification)
    // });
    /**
    * Triggered when a particular notification has been received in foreground
    * */
    // this.notificationListener = firebase.notifications().onNotification((notification) => {
    //   console.tron.log('app is in foreground', notification)
    // })

    // /**
    // * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    // * */
    // this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   const { title, body, data } = notificationOpen.notification;
    //   console.tron.log('app is in background', title, body, data)
    // })
  }


  render() {
    return <View />
  }
}



