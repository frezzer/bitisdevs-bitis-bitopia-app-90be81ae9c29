'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native-webview';

import {
  View,
  Platform
} from 'react-native';

class Canvas extends React.Component {

  render() {
    let contextString = JSON.stringify(this.props.context);
    let renderString = this.props.render.toString();
    return (
      <View style={this.props.style}>
        <WebView
          useWebKit={true}
          automaticallyAdjustContentInsets={false}
          scalesPageToFit={Platform.OS === 'android'}
          contentInset={{ top: 0, right: 0, bottom: 0, left: 0 }}
          source={{ html: "<style>*{margin:0;padding:0;}canvas{transform:translateZ(0);}</style><canvas></canvas><script>var canvas = document.querySelector('canvas');(" + renderString + ").call(" + contextString + ", canvas);</script>" }}
          opaque={false}
          underlayColor={'transparent'}
          style={this.props.style}
          javaScriptEnabled={true}
          scrollEnabled={false}
          onLoad={this.props.onLoad}
          onLoadEnd={this.props.onLoadEnd}
          originWhitelist={['*']}
        />
      </View>
    );
  }
}


Canvas.propTypes = {
  style: PropTypes.object,
  context: PropTypes.object,
  render: PropTypes.func.isRequired,
  onLoad: PropTypes.func,
  onLoadEnd: PropTypes.func,
};

export default Canvas;
