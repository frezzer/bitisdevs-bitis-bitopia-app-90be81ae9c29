/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import {
  Image, View, Text, AsyncStorage,
} from 'react-native';
import moment from 'moment';
import { IconOutline } from '@ant-design/icons-react-native';
import _ from 'lodash';
// COMPONENTS
import styles from './Styles/CartItemNewVer.styles';
import { compareOmitState, formatMoney } from '../../../Services/utils';
import { Colors, Fonts } from '../../../Themes';

require('moment/locale/vi');

const CartItemNewVer = (props) => {
  const _dataBill = _.get(props, 'dataBill', {});
  const _status = _.get(props, 'status', {});
  return (
    <View style={styles.container}>
      <IconOutline name="snippets" color={Colors.blue} size={24} />
      <View style={styles.block1}>
        <Text style={styles.text1}>
          MĐH: #
          {(_dataBill.uuid) ? `${_dataBill.uuid}` : 'Đang cập nhật'}
        </Text>
        <Text style={styles.text2}>
          Nhân viên:
          {(_dataBill.createdByUser) ? ` ${_dataBill.createdByUser}` : 'Đang cập nhật'}
        </Text>
        <Text style={styles.text2}>
          Khách hàng:
          {(_dataBill.createdByUser) ? ` ${_dataBill.createdByUser}` : 'Đang cập nhật'}
        </Text>
      </View>
      <View style={styles.block2}>
        <View style={styles.blockStatus}>
          <Text style={styles.text3}>{_status}</Text>
        </View>
        <Text style={styles.text4}>{formatMoney(_dataBill.totalPrice || 0)} đ</Text>
        <Text style={styles.text5}>
          {moment(_dataBill.createdAt).format('HH:mm')}
          {' '}
          -
          {moment(_dataBill.createdAt).format('DD/MM/YYYY')}
        </Text>
      </View>
    </View>
  );
};

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(CartItemNewVer, areEqual);
