/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import {
  Image, View, Text, TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import { IconOutline } from '@ant-design/icons-react-native';
import _ from 'lodash';
// COMPONENTS
import styles from './Styles/ItemOrderNewVer.styles';
import { compareOmitState, formatMoney } from '../../../Services/utils';
import { Colors, Fonts } from '../../../Themes';

require('moment/locale/vi');

const ItemOrderNewVer = (props) => {
  const _dataOders = _.get(props, 'dataOders', {});
  const _blockType = _.get(_dataOders, 'block_type', 'justView');
  const _orderDetail = _.get(_dataOders, 'orderDetail', {});
  const _callbackFromChild = _.get(props, 'callbackFromChild', () => {});
  console.tron.log('_orderDetail', _orderDetail);
  const _renderChooseQuantity = (
    <View style={styles.blockChooseQuantityVariation}>
      <TouchableOpacity
        style={styles.blockCoverBtn}
        onPress={() => _callbackFromChild({ billId: _dataOders.billId, ..._orderDetail }, 'decrease')}
      >
        <IconOutline name="minus" size={16} color={Colors.grey7} />
      </TouchableOpacity>
      <Text style={styles.textQuantity}>{_orderDetail.quantity || 1}</Text>
      <TouchableOpacity
        style={styles.blockCoverBtn}
        onPress={() => _callbackFromChild({ billId: _dataOders.billId, ..._orderDetail }, 'increase')}
      >
        <IconOutline name="plus" size={16} color={Colors.grey7} />
      </TouchableOpacity>
    </View>
  );

  const _renderTextQuantity = (
    <View style={styles.blockViewQuantityVariation}>
      <Text style={styles.textQuantity}>x{_orderDetail.quantity || 0}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Image
          style={styles.imageShoes}
          source={{ uri: 'https://product.hstatic.net/1000230642/product/dsuh00100cam__4__fc1b8001ad264b6d8ed49c96fa3525c8_1024x1024.jpg' }}
        />

        <View style={styles.blockInfoVariation}>
          <Text style={styles.text1}>{_orderDetail.variation.name}</Text>
          <Text style={styles.text2}>Mã sản phẩm: {_orderDetail.variation.uuid}</Text>
          <Text style={styles.text3}>{formatMoney(_blockType === 'justView' ? _orderDetail.totalPrice : (_orderDetail.price * _orderDetail.quantity))} đ</Text>
        </View>

        {_blockType === 'justView' ? _renderTextQuantity : _renderChooseQuantity }
      </View>

      <View style={styles.line_style} />
    </View>

  );
};

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ItemOrderNewVer, areEqual);
