/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: Metrics.paddingMedium,
    backgroundColor: Colors.white,
    flexDirection: 'row',
  },
  block1: {
    flex: 1,
    marginLeft: Metrics.marginTiny,
  },
  block2: {
    alignItems: 'flex-end',
  },
  blockStatus: {
    backgroundColor: Colors.green3,
    borderRadius: Metrics.boderNomarl,
    paddingHorizontal: Metrics.paddingTiny,
    marginRight: Metrics.marginTiny,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // Text
  text1: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey9,
    fontSize: Fonts.size.textMedium,
    lineHeight: wp(22 / 375 * 100),
  },
  text2: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey7,
    fontSize: Fonts.size.textSmall,
    lineHeight: wp(20 / 375 * 100),
  },
  text3: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.green1,
    fontSize: Fonts.size.textSmall,
    lineHeight: wp(20 / 375 * 100),
  },
  text4: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.grey9,
    fontSize: Fonts.size.textMedium,
    lineHeight: wp(24 / 375 * 100),
  },
  text5: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.greybold,
    fontSize: Fonts.size.textSmall,
    lineHeight: wp(20 / 375 * 100),
  },
});
