/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    padding: Metrics.paddingMedium,
  },
  imageShoes: {
    height: wp(42 / 375 * 100),
    width: wp(42 / 375 * 100),
  },
  blockInfoVariation: {
    flex: 1,
    marginLeft: Metrics.marginTiny,
  },
  blockChooseQuantityVariation: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  blockViewQuantityVariation: {
    paddingHorizontal: Metrics.paddingHuge,
    paddingTop: Metrics.paddingTiny,
  },
  blockCoverBtn: {
    padding: Metrics.paddingTiny,
    borderRadius: Metrics.boderXTiny,
    borderColor: Colors.grey4,
    borderWidth: 1,
  },
  line_style: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
    marginHorizontal: Metrics.marginMedium,
  },
  // TEXT
  text1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey9,
    lineHeight: wp(20 / 375 * 100),
  },
  text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textXSmall,
    color: '#868686',
    lineHeight: wp(18 / 375 * 100),
  },
  text3: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    lineHeight: wp(22 / 375 * 100),
  },
  textQuantity: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    lineHeight: wp(32 / 375 * 100),
    paddingHorizontal: Metrics.paddingSmall,
  },
});
