/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    paddingVertical: Metrics.paddingMedium,
    marginHorizontal: Metrics.paddingLarge - 2,
    backgroundColor: Colors.white,
    flexDirection: 'row',
  },
  bottomBorder: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
  },
  // Text
  text1: {
    color: Colors.grey8,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    lineHeight: wp(22 / 375 * 100),
  },
  textInput: {
    flex: 1,
    color: Colors.grey9,
    textAlign: 'right',
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
  },
});
