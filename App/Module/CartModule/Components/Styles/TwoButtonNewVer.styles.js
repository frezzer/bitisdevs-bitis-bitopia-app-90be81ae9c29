/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    padding: Metrics.paddingMedium,
    paddingBottom: wp(28 / 375 * 100),
  },
  buttonArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonLeft: {
    flex: 1,
    backgroundColor: 'transparent',
    borderRadius: Metrics.boderLSmall,
    borderWidth: 1,
    borderColor: Colors.white,
    paddingVertical: Metrics.paddingXXSmall,
    alignItems: 'center',
  },
  buttonRight: {
    flex: 1,
    backgroundColor: Colors.white,
    marginLeft: Metrics.marginTiny,
    borderRadius: Metrics.boderLSmall,
    borderWidth: 1,
    borderColor: Colors.white,
    paddingVertical: Metrics.paddingXXSmall,
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,
    lineHeight: wp(24 / 375 * 100),
  },
});
