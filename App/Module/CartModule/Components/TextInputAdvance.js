import React from 'react';
import {
  Image, View, Text, TextInput, TouchableWithoutFeedback,
} from 'react-native';
import moment from 'moment';
import { IconOutline } from '@ant-design/icons-react-native';
import _ from 'lodash';
// COMPONENTS
import styles from './Styles/TextInputAdvance.styles';
import { compareOmitState, formatMoney } from '../../../Services/utils';
import { Colors, Fonts } from '../../../Themes';

require('moment/locale/vi');

function TextInputAdvance(props) {
  const _dataInput = _.get(props, ['data_input']);
  const _keyInput = _.get(_dataInput, 'key_input', 'key_name');
  const _title = _.get(_dataInput, 'title', 'Tiêu đề');
  const _placeholder = _.get(_dataInput, 'placeHolder', 'Nhập ở đây');
  const _value = _.get(_dataInput, 'value', null);
  const _disableInput = _.get(_dataInput, 'disableInput', false);
  const _bottomBorder = _.get(_dataInput, 'bottomBorder', false);

  // HANDLE
  let textInput = null;
  const [textValue, onChangeText] = React.useState(_value, '');

  // Focus to input when press hold block
  function _onFocus() {
    textInput.focus();
  }

  // Call back parent when submit text input or blur
  function _onSubmitOrBlurText() {
    const dataForCallback = {
      key_name: _keyInput,
      value: textValue,
    };
    props.callBackFromChild(dataForCallback);
  }

  return (
    <TouchableWithoutFeedback onPress={_onFocus}>
      <View style={[styles.container, _bottomBorder ? styles.bottomBorder : {}]}>
        <Text style={styles.text1}>{_title}</Text>
        <TextInput
          ref={(input) => { textInput = input; }}
          style={styles.textInput}
          placeholder={_placeholder}
          value={textValue}
          editable={!_disableInput}
          onChangeText={(text) => onChangeText(text)}
          onFocus={() => onChangeText('')}
          onBlur={_onSubmitOrBlurText}
        />
      </View>
    </TouchableWithoutFeedback>

  );
}

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(TextInputAdvance, areEqual);
