import React from 'react';
import {
  View, Text, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compareOmitState } from '../../../Services/utils';
import { Colors } from '../../../Themes';
// Styles
import styles from './Styles/TwoButtonNewVer.styles';

const TwoButtonNewVer = (props) => {
  const _onPressButton1 = _.get(props, 'onPressButton1', () => { });
  const _onPressButton2 = _.get(props, 'onPressButton2', () => { });
  const _buttonText1 = _.get(props, 'buttonText1', ' Nhấn vào đây');
  const _buttonText2 = _.get(props, 'buttonText2', ' Nhấn vào đây');
  const _stylesTextLeft = _.get(props, 'stylesTextLeft', { color: Colors.white });
  const _stylesTextRight = _.get(props, 'stylesTextLeft', { color: Colors.blue1 });
  const _styles1 = _.get(props, 'styles1', {});
  const _styles2 = _.get(props, 'styles2', {});
  const _styles = _.get(props, 'styles', {});
  return (

    <View style={styles.container}>
      <View style={[styles.buttonArea, _styles]}>
        <TouchableOpacity style={[styles.buttonLeft, _styles1]} onPress={_onPressButton1}>
          <Text style={[styles.buttonText, _stylesTextLeft]}>{_buttonText1}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonRight, _styles2]} onPress={_onPressButton2}>
          <Text style={[styles.buttonText, _stylesTextRight]}>{_buttonText2}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default React.memo(
  TwoButtonNewVer, (prevProps, nextProps) => compareOmitState(nextProps, prevProps, []),
);
