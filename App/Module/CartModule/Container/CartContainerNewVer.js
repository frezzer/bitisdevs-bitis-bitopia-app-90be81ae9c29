import _ from 'lodash';
import React from 'react';
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, FlatList,
  SafeAreaView, Platform, Alert, ScrollView, RefreshControl,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import Swipeout from 'react-native-swipeout';
// Components
import { IconOutline } from '@ant-design/icons-react-native';
import screensName from '../../../Navigation/Screens';
import ButtonHandle from '../../../Components/Button/ButtonHandle';
import {
  Images, Metrics, Colors, Fonts,
} from '../../../Themes';
import CartItemNewVer from '../Components/CartItemNewVer';
import { Unicons } from '../../../Components/Icons';
// Styles
import styles from './Styles/CartContainerNewVer.styles';
// Actions and Selectors Redux
import { MeSelectors } from '../../../Redux/MeRedux';
import BillCraftGetListActions, { BillCraftGetHistorySelectors } from '../Redux/BillCraftGetListRedux';
import BillTempClientActions, { CreateBillTempClientSelectors } from '../Redux/BillTempClientRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../../Redux/shift/ShiftFindRedux';
import { FindstoreSellerSelectors } from '../../../Redux/shift/FindStoreSellerRedux';
// utils
import { compareOmitState, formatMoney, checkStaffInShift } from '../../../Services/utils';


require('moment/locale/vi');

const navigationOptions = (props) => {
  const onPressGoBack = () => props.navigation.goBack();
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Bán hàng',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS === 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoBack}
        >
          <IconOutline name="left" color="white" style={styles.iconBack} />
        </TouchableOpacity>
      );
    },
  };
};

CartContainerNewVer.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CartContainerNewVer(props) {
  const dispatch = useDispatch();
  /** INTERNAL STATE */
  const [initialRender, setInitialRender] = React.useState(true);
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));

  /** REDUX STATE */
  const shiftFindPayload = useSelector((state) => ShiftFindSelectors.selectPayload(state.shift.find));
  const listBillCraft = useSelector((state) => BillCraftGetHistorySelectors.selectPayload(state.billCraftHistory));
  const listBillCraftFetching = useSelector((state) => BillCraftGetHistorySelectors.selectFetching(state.billCraftHistory));
  const listBillTempClientData = useSelector((state) => CreateBillTempClientSelectors.selectData(state.billTempClient));
  const currentIndex = useSelector((state) => CreateBillTempClientSelectors.selectActiveIndex(state.billTempClient));
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));

  /** VARIBLE GLOBAL */
  // const checkStaffAreOnShift = shiftFindPayload && shiftFindPayload.length > 0 ? checkStaffInShift(shiftFindPayload[0].shifttype.time_in, shiftFindPayload[0].shifttype.time_out) : false;
  const checkStaffAreOnShift = true;

  /** FUNCTION */
  /** Handle refresh list bill craft */
  const onRefresh = React.useCallback(() => {
    Actions.getListBill();
  }, [listBillCraftFetching]);

  /** LIFECYCLE */
  /** Didmount */
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });

      Actions.getListBill();

      Actions.getListShift({
        _limit: 1,
        'attendances.user_uuid': mePayload.username,
      });
    }
    // Clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {

    /** Get list bill craft */
    getListBill: (params = {}) => {
      dispatch(BillCraftGetListActions.billCraftGetHistoryRequest({
        ...params,
        status: 'draft',
        _populate: ['orders'],
        _limit: 10,
      }));
    },

    /** Get List Shift */
    getListShift: (params = {}) => {
      dispatch(ShiftFindActions.shiftFindRequest({
        ...params,
        _sort: 'created_at:desc',
        _populate: ['shifttype', 'attendances'],
      }));
    },

    /** Add one bill temp at client */
    onPressCreateBillTemp: () => {
      if (listBillTempClientData.length >= 3) return Alert.alert('THÔNG BÁO', 'Bạn chỉ được tạo tối đa 3 đơn nháp, vui lòng xóa bớt để có thể tạo đơn mới!');
      const dataBillTemp = {
        id: currentIndex + 1,
        status: 'draft',
        totalPrice: 0,
        savingPrice: 0,
        listingPrice: 0,
        storeId: findStoreSellerPayload ? findStoreSellerPayload.store_uuid : undefined,
        createdByUser: mePayload.username,
        firstName: mePayload.firstName,
        lastName: mePayload.lastName,
        storeName:  findStoreSellerPayload ? findStoreSellerPayload.store_name : undefined,
        createdAt: new Date(),
        orders: [],
      };
      dispatch(BillTempClientActions.addBillTempClientRequest(dataBillTemp));
      props.navigation.navigate(screensName.CartDetailNewVerScreen, { billDetail: dataBillTemp });
    },

    /** Onpress delete bill */
    onPressDeleteBillTemp: (params) => {
      dispatch(BillTempClientActions.deleteBillTempClientRequest(params));
    },
  };

  const controlerForState = {
    initialRender,
  };

  const controlerForProps = {
    listBillCraft,
    listBillCraftFetching,
    listBillTempClientData,
    checkStaffAreOnShift,
  };

  /**
   * RENDER
   *  - declare variables and function for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    ...props,
    Actions,
    controlerForState,
    controlerForProps,
    onRefresh,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  /** DECLARE EVERYTHING */
  const {
    Actions, controlerForState, controlerForProps,
    onRefresh,
  } = props;

  const { initialRender } = controlerForState;

  const {
    listBillCraft, listBillCraftFetching, listBillTempClientData,
    checkStaffAreOnShift,
  } = controlerForProps;

  /** RENDER CONTENT BILL CRAFT FETCH FROM SERVER */
  const _renderItemBillCraft = ({ item }) => {
    return (
      <TouchableOpacity id={item.id} onPress={() => props.navigation.navigate(screensName.CartDetailJustViewScreen, { billDetail: item })}>
        <CartItemNewVer dataBill={item} status="Đã gửi" />
      </TouchableOpacity>
    );
  };

  /** RENDER CONTENT BILL TEMP AT CLIENT */
  const _renderItemBillTemp = ({ item }) => {
    /** RENDER CONTENT OF SWIPE BLOCK */
    const swipeoutBtns = [
      {
        component: (
          <View style={styles.block_swiper}>
            <Unicons name="times-circle" style={styles.icon_delete} />
            <Text style={styles.text2}>Xóa</Text>
          </View>
        ),
        backgroundColor: Colors.orange1,
        onPress: () => Actions.onPressDeleteBillTemp({ id: item.id }),
      },
    ];

    return (
      <Swipeout right={swipeoutBtns}>
        <TouchableOpacity id={item.id} onPress={() => props.navigation.navigate(screensName.CartDetailNewVerScreen, { billDetail: item })}>
          <CartItemNewVer dataBill={item} status="Chờ gửi" />
        </TouchableOpacity>
      </Swipeout>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.content}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={listBillCraftFetching} onRefresh={onRefresh} />
        }
      >
        {/* List bill craft from service */}
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={listBillCraft}
          renderItem={_renderItemBillCraft}
          initialNumToRender={3}
          ListHeaderComponent={(
            <View style={styles.block_header_list}>
              <Text style={styles.text1}>Chờ thanh toán</Text>
            </View>
          )}
          ItemSeparatorComponent={() => (
            <View style={styles.block_separator}>
              <View style={styles.line_style} />
            </View>
          )}
          ListEmptyComponent={() => (
            <Text style={{ textAlign: 'center' }}>Không có đơn hàng nào!</Text>
          )}
        />

        {/* List bill temp client */}
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={listBillTempClientData}
          renderItem={_renderItemBillTemp}
          initialNumToRender={3}
          ListHeaderComponent={(
            <View style={styles.block_header_list}>
              <Text style={styles.text1}>Đơn nháp chờ gửi</Text>
            </View>
          )}
          ItemSeparatorComponent={() => (
            <View style={styles.block_separator}>
              <View style={styles.line_style} />
            </View>
          )}
          ListEmptyComponent={() => (
            <Text style={{ textAlign: 'center' }}>Không có đơn hàng nào!</Text>
          )}
        />

      </ScrollView>

      {/* Bottom button */}
      {checkStaffAreOnShift
        && (
        <View style={styles.block_bottom}>
          <ButtonHandle
            onPressButton={Actions.onPressCreateBillTemp}
            buttonText="Tạo đơn hàng"
            styles={{ backgroundColor: Colors.blue2 }}
          />
        </View>
        )}
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
