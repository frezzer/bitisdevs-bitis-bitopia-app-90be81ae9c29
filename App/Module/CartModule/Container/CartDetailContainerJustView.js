/* eslint-disable react/jsx-one-expression-per-line */
import _ from 'lodash';
import React, { useRef } from 'react';
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, FlatList,
  SafeAreaView, Platform, TextInput, AsyncStorage, Alert, ScrollView, Animated,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Swipeout from 'react-native-swipeout';
import { IconOutline } from '@ant-design/icons-react-native';
// Components
import screensName from '../../../Navigation/Screens';
import ButtonHandle from '../../../Components/Button/ButtonHandle';
import {
  Images, Metrics, Colors, Fonts,
} from '../../../Themes';
import TextInputAdvance from '../Components/TextInputAdvance';

import { Unicons } from '../../../Components/Icons';
// Container
import ListProductTempContainer from './ListProductTempContainer';
// Styles
import styles from './Styles/CartDetailContainerJustView.styles';
// Actions and Selectors Redux
// utils
import { compareOmitState, formatMoney } from '../../../Services/utils';

const INTIAL_HEIGHT_TOP_BAR = Metrics.navigationBarHeight;
const AnimatedIconOutline = Animated.createAnimatedComponent(IconOutline);

/**
 * Main
 */
export default function CartDetailContainerNewVer(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const animatedScrollYValue = useRef(new Animated.Value(0)).current;
  const [message, setmessage] = React.useState("");

  // useEffect Didmount
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }

  

    // listen event from input
    // Actions.actionsfunction

    // clear when Screen off
    return () => { };
  }, [/** input */]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
  };

  const state = {
    animatedScrollYValue,
  };

  // For animated header background color when scroll
  const headerBackgroundColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: ['rgba(0,0,0,0.0)', Colors.white],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For animated cover button background color when scroll
  const btnBackgroundColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: [Colors.grey7, 'rgba(0,0,0,0.0)'],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For animated color icon when scroll
  const headerIconColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: [Colors.white, Colors.black],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For distance from list item to top when scroll
  const contentViewTopPosition = animatedScrollYValue.interpolate({
    inputRange: [0, 200],
    outputRange: [16, 0],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    ...props,
    message,
    setmessage,
    initialRender,
    Actions,
    state,
    headerBackgroundColor,
    btnBackgroundColor,
    headerIconColor,
    contentViewTopPosition,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  // declare variables
  callbackFunction = (childData) => {
    setmessage(childData)
  }



  const {
    initialRender, Actions, state,message,
    headerBackgroundColor, btnBackgroundColor, headerIconColor,
    contentViewTopPosition,
  } = props;
  const { animatedScrollYValue } = state;

  const billDetail = props.navigation.getParam('billDetail', null);
  console.tron.log('billDetail', billDetail);

  const CONFIG_TEXT_INPUT = [
    {
      key_input: 'uuid',
      title: 'Mã đơn hàng',
      value: billDetail.uuid || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'customerName',
      title: 'Tên khách hàng',
      value: billDetail.customerName || 'Đang cập nhật',
      placeHolder: 'Nhập tên khách hàng',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'phoneNumber',
      title: 'Số điện thoại',
      value: billDetail.phoneNumber || 'Đang cập nhật',
      placeHolder: 'Nhập số điện thoại',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'createdByUser',
      title: 'Nhân viên bán',
      value: billDetail.saler || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'storeName',
      title: 'Cửa hàng',
      value: billDetail.storeName || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
  ];

  return (
    <View style={styles.container}>
      {/* Background setting */}
      <View style={styles.block_background}>
        <View style={styles.background_part1} />
        <View style={styles.background_part2} />
      </View>

      {/* Header bar custom */}
      <Animated.View
        style={[styles.block_header, {
          backgroundColor: headerBackgroundColor,
          height: INTIAL_HEIGHT_TOP_BAR,
        }]}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity
            onPress={() => 
Alert.alert(route.params?.post)
            //props.navigation.goBack()
            }
          >
            <Animated.View
              style={[styles.btn_header_cover, {
                padding: Metrics.paddingXXSmall,
                backgroundColor: btnBackgroundColor,
              }]}
            >
              <AnimatedIconOutline name="left" color={headerIconColor} size={24} />
            </Animated.View>
          </TouchableOpacity>

          <TouchableOpacity>
            <Animated.View
              style={[styles.btn_header_cover, {
                backgroundColor: btnBackgroundColor,
                padding: Metrics.paddingXTiny + 2,
              }]}
            >
              <AnimatedIconOutline name="camera" color={headerIconColor} size={14} />
            </Animated.View>
          </TouchableOpacity>
        </View>
      </Animated.View>

      <Animated.ScrollView
        style={styles.scroll_view}
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event([{
          nativeEvent: { contentOffset: { y: animatedScrollYValue } },
        }])}
      >
        <Animated.View style={{
          ...styles.block_content,
          transform: [{
            translateY: contentViewTopPosition, // change distance of list view from top when scroll
          }],
        }}
        >
          <Text style={styles.text1}>Thông tin chung</Text>

          {/* Bill Temp Info */}
          <View style={styles.block_bill_info}>
            {CONFIG_TEXT_INPUT.map((item, index) => {
              return (<TextInputAdvance data_input={item} callBackFromChild={Actions.callBackFromChild} />);
            })}
          </View>

          <Text style={styles.text1}>Sản phẩm</Text>

          {/* List Product Of Bill Temp */}
          <View style={styles.block_bill_info}>
            <ListProductTempContainer blockType="justView" billData={billDetail} />
          </View>

          {/* Total money */}
          <Text style={styles.text3}>
            Tổng tiền:
            <Text style={styles.text4}> {formatMoney(billDetail.totalPrice)} đ</Text>
          </Text>
        </Animated.View>
      </Animated.ScrollView>

    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
