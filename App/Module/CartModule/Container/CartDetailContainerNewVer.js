/* eslint-disable react/jsx-one-expression-per-line */
import _ from 'lodash';
import React, { useRef } from 'react';
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, FlatList,Image,
  SafeAreaView, Platform, TextInput, AsyncStorage, Alert, ScrollView, Animated,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Swipeout from 'react-native-swipeout';
import { IconOutline } from '@ant-design/icons-react-native';
// Components
import screensName from '../../../Navigation/Screens';
import ButtonHandle from '../../../Components/Button/ButtonHandle';
import {
  Images, Metrics, Colors, Fonts,
} from '../../../Themes';
import TextInputAdvance from '../Components/TextInputAdvance';

import { Unicons } from '../../../Components/Icons';
// Container
import ListProductTempContainer from './ListProductTempContainer';
// Styles
import styles from './Styles/CartDetailContainerNewVer.styles';
// Actions and Selectors Redux
import BillTempClientActions, { CreateBillTempClientSelectors } from '../Redux/BillTempClientRedux';
import CreateBillCraftActions, { CreateBillCraftSelectors } from '../Redux/CreateBillCraftRedux';

// utils
import { compareOmitState, formatMoney } from '../../../Services/utils';

const INTIAL_HEIGHT_TOP_BAR = Metrics.navigationBarHeight;
const AnimatedIconOutline = Animated.createAnimatedComponent(IconOutline);
const INTIAL_BILL_INFO = {
  customerName: '',
  phoneNumber: '',
};

/**
 * Main
 */
export default function CartDetailContainerNewVer(props) {
  const dispatch = useDispatch();
  /** INTERNAL STATE */
  const [initialRender, setInitialRender] = React.useState(true);
  const [billInfo, setBilInfo] = React.useState(INTIAL_BILL_INFO, {});
  const animatedScrollYValue = useRef(new Animated.Value(0)).current;

  /** PROPS FROM NAVIGATION */
  const { navigation } = props;
  const billDetail = navigation.getParam('billDetail', null);
  const tempTran= navigation.getParam('itemId',false)

  /** REDUX STATE */
  const BillTempClientPayload = useSelector((state) => CreateBillTempClientSelectors.selectData(state.billTempClient));
  const CreateBillCraftPayload = useSelector((state) => CreateBillCraftSelectors.selectPayload(state.createBillCraft));
  const CreateBillCraftSuccess = useSelector((state) => CreateBillCraftSelectors.selectSuccess(state.createBillCraft));
  const CreateBillCraftFetching = useSelector((state) => CreateBillCraftSelectors.selectFetching(state.createBillCraft));

  /** VARIBLE GLOBAL */
  const findBillTempFromRedux = _.find(BillTempClientPayload, (bill) => bill.id === billDetail.id) || {};


  /** LIFECYCLE */
  /** Didmount */
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }

    // listen event from input
    // Actions.actionsfunction

    // clear when Screen off
    return () => { };
  }, [/** input */]);

  React.useEffect(() => {
    if (initialRender === false && CreateBillCraftPayload && CreateBillCraftSuccess === true && CreateBillCraftFetching === false) {
      props.navigation.goBack();
      Alert.alert('THÔNG BÁO', 'Tạo hóa đơn thành công!');
    }
  }, [CreateBillCraftPayload]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    callBackFromChild: (dataCallBack) => {
      if (dataCallBack.value !== '') {
        const objKey = dataCallBack.key_name;
        billInfo[objKey] = dataCallBack.value;
        setBilInfo(billInfo);
      }
    },
    goScanProduct: () => {
      props.navigation.navigate(screensName.CartScanScreen, { billDetail: findBillTempFromRedux });
    },
    onPressCreateBillCraft: () => {
      if (billInfo.customerName === '' || billInfo.phoneNumber === '') return Alert.alert('THÔNG BÁO', 'Vui lòng nhập tên và SDT để có thể tạo đơn!');
      if (findBillTempFromRedux.orders.length === 0) return Alert.alert('THÔNG BÁO', 'Bạn chưa có sản phẩm nào trong giỏ hàng.');
      const handleDataForCreate = {
        bill: {
          ...findBillTempFromRedux,
          orders: _.map(findBillTempFromRedux.orders, (order) => {
            return {
              quantity: order.quantity,
              variation: {
                uuid: order.variation.uuid,
              },
            };
          }),
        },
      };
      dispatch(CreateBillCraftActions.createBillCraftRequest(handleDataForCreate));
    },
  };

  const state = {
    animatedScrollYValue,
  };

  // For animated header background color when scroll
  const headerBackgroundColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: ['rgba(0,0,0,0.0)', Colors.white],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For animated cover button background color when scroll
  const btnBackgroundColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: [Colors.grey7, 'rgba(0,0,0,0.0)'],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For animated color icon when scroll
  const headerIconColor = animatedScrollYValue.interpolate({
    inputRange: [0, 40],
    outputRange: [Colors.white, Colors.black],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  // For distance from list item to top when scroll
  const contentViewTopPosition = animatedScrollYValue.interpolate({
    inputRange: [0, 200],
    outputRange: [16, 0],
    extrapolate: 'clamp',
    // useNativeDriver: true,
  });

  const controlerForState = {
    initialRender,
    animatedScrollYValue,
    billInfo,
  };

  const controlerForProps = {
    billDetail,
    tempTran,
    findBillTempFromRedux,
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    ...props,
    Actions,
    controlerForState,
    controlerForProps,
    headerBackgroundColor,
    btnBackgroundColor,
    headerIconColor,
    contentViewTopPosition,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  /** DECLARE EVERYTHING */

   

  const {
    Actions, controlerForState, controlerForProps,
    headerBackgroundColor, btnBackgroundColor, headerIconColor,
    contentViewTopPosition,
  } = props;
  const {
    initialRender, billInfo, animatedScrollYValue,
  } = controlerForState;

  const {
    findBillTempFromRedux,
  } = controlerForProps;
  // const { animatedScrollYValue } = state;

  const CONFIG_TEXT_INPUT = [
    {
      key_input: 'uuid',
      title: 'Mã đơn hàng',
      value: findBillTempFromRedux.uuid || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'customerName',
      title: 'Tên khách hàng',
      value: findBillTempFromRedux.customerName || 'Đang cập nhật',
      placeHolder: 'Nhập tên khách hàng',
      bottomBorder: true,
    },
    {
      key_input: 'phoneNumber',
      title: 'Số điện thoại',
      value: findBillTempFromRedux.phoneNumber || 'Đang cập nhật',
      placeHolder: 'Nhập số điện thoại',
      bottomBorder: true,
    },
    {
      key_input: 'createdByUser',
      title: 'Nhân viên bán',
      value: (`${findBillTempFromRedux.lastName} ${findBillTempFromRedux.firstName}`) || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
    {
      key_input: 'storeName',
      title: 'Cửa hàng',
      value: findBillTempFromRedux.storeName || 'Đang cập nhật',
      disableInput: true,
      bottomBorder: true,
    },
  ];

  return (
    <View style={styles.container}>
      {/* Background setting */}
      <View style={styles.block_background}>
        <View style={styles.background_part1} />
        <View style={styles.background_part2} />
      </View>

      {/* Header bar custom */}
      <Animated.View
        style={[styles.block_header, {
          backgroundColor: headerBackgroundColor,
          height: INTIAL_HEIGHT_TOP_BAR,
        }]}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity
            onPress={() => 
           //Alert.alert(JSON.stringify(controlerForProps.tempTran))
           //  console.log(controlerForProps)
            props.navigation.goBack()
            }
          >
            <Animated.View
              style={[styles.btn_header_cover, {
                padding: Metrics.paddingXXSmall,
                backgroundColor: btnBackgroundColor,
              }]}
            >
              <AnimatedIconOutline name="left" color={headerIconColor} size={24} />
            </Animated.View>
          </TouchableOpacity>

          <TouchableOpacity onPress={Actions.goScanProduct}>
            <Animated.View
              style={[styles.btn_header_cover, {
                backgroundColor: btnBackgroundColor,
                padding: Metrics.paddingXTiny + 2,
              }]}
            >
              <AnimatedIconOutline name="camera" color={headerIconColor} size={32} />
            </Animated.View>
          </TouchableOpacity>
        </View>
      </Animated.View>

      <Animated.ScrollView
        style={styles.scroll_view}
        scrollEventThrottle={16}
        onScroll={Animated.event([{
          nativeEvent: { contentOffset: { y: animatedScrollYValue } },
        }])}
      >
        <Animated.View style={{
          ...styles.block_content,
          transform: [{
            translateY: contentViewTopPosition, // change distance of list view from top when scroll
          }],
        }}
        >
          <Text style={styles.text1}>Thông tin chung</Text>

          {/* Bill Temp Info */}
          <View style={styles.block_bill_info}>
            {CONFIG_TEXT_INPUT.map((item, index) => {
              return (<TextInputAdvance data_input={item} callBackFromChild={Actions.callBackFromChild} />);
            })}
          </View>

          <Text style={styles.text1}>Sản phẩm</Text>

          {/* List Product Of Bill Temp */}
          {/* <View style={styles.block_bill_info}> */}


{ controlerForProps.tempTran ? (
          <View style={{flexDirection: "row",  marginVertical: 8, borderRadius: 4
     
    }}>
    <View style={{backgroundColor: "white"}}>
     <Image style={{width: 100, height: 95}} source=
     {{ uri: "https://images.ctfassets.net/od02wyo8cgm5/4VXZrAobQXbBcEExdKqIHI/2a4bb16c771038d5edaad8ba33682d75/cloudswift-fw19-rock_slate-m-g1.png"}}
     >
     </Image></View>
<View style={{flexDirection: "column"}}>
       <Text style={{ 
  color:"#000",
  marginTop: 10, marginLeft: 10,
   }}>Bitis Hunter-size[39, 40, 41]</Text>
   <View style={{flexDirection: "row",  marginTop: 30, marginLeft: 10
   }}>
       <Text style={{ color:"#000",}}>
         Nam
       </Text>
       <Text style={{ color:"#000", marginLeft: 30}}>
       1.600.000 VNĐ
       </Text>

   </View>
   
   
   </View>
     </View> ) : ( <ListProductTempContainer blockType="chooseVariation" billData={findBillTempFromRedux} />)}
           
        {/* </View> */}

          {/* Total money */}
          <Text style={styles.text3}>
            Tổng tiền:
            <Text style={styles.text4}>
              {controlerForProps.tempTran ? "1.600.000" :  formatMoney(findBillTempFromRedux.totalPrice) }
            
             đ</Text>
          </Text>
        </Animated.View>
      </Animated.ScrollView>

      {/* Bottom button */}
      <View style={styles.block_bottom}>
        <ButtonHandle
          buttonText="Gửi đơn đến thu ngân"
          onPressButton={Actions.onPressCreateBillCraft}
          styles={{ backgroundColor: Colors.blue2 }}
        />
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
