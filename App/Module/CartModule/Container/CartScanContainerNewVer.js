/* eslint-disable react/jsx-one-expression-per-line */
import _ from 'lodash';
import React, { useRef } from 'react';
import {
  InteractionManager, View, Text, TouchableOpacity, TextInput, Image,
  Alert, ScrollView,FlatList
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { IconOutline } from '@ant-design/icons-react-native';
// Components
import screensName from '../../../Navigation/Screens';
import ScanQRCode from '../../../Components/ScanQRCode';
import TwoButtonNewVer from '../Components/TwoButtonNewVer';
import {
  Images, Metrics, Colors, Fonts,
} from '../../../Themes';
import { Unicons } from '../../../Components/Icons';
// Styles
import styles from './Styles/CartScanContainerNewVer.styles';
// Actions and Selectors Redux
import BillTempClientActions from '../Redux/BillTempClientRedux';
import ProductGetDetailActions, { ProductGetDetailSelectors } from '../../../Redux/product/ProductGetDetailRedux';
// utils
import { compareOmitState, formatMoney } from '../../../Services/utils';
import jwt from 'react-native-pure-jwt';
import CartDetailContainerNewVer from '../../../Screens/Cart/CartDetailNewVerScreen.js';
import CartDetailScreen from '../../../Screens/Cart/CartDetailScreen';

/**
 * Main
 */
export default function CartScanContainerNewVer(props) {
  const dispatch = useDispatch();
  /** INTERNAL STATE */
  const [initialRender, setInitialRender] = React.useState(true);
  const [rescanQRCode, setRescanQRCode] = React.useState(false);
  const [searchVariation, setSearchVariation] = React.useState('');
  const [dataVariation, setDataVariation] = React.useState([]);
  const [IsHide, setIsHide] = React.useState(false);
  const [code, setCode] = React.useState("1205")

  /** PROPS FROM NAVIGATION */
  const { navigation } = props;
  const billDetail = navigation.getParam('billDetail', {});
  /** REDUX STATE */
  const ProductGetDetailPayload = useSelector((state) => ProductGetDetailSelectors.selectPayload(state.product_detail));
  const ProductGetDetailFetching = useSelector((state) => ProductGetDetailSelectors.selectFetching(state.product_detail));

  
  /** VARIBLE GLOBAL */

  /** LIFECYCLE */
  /** Didmount */
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }

    // clear when Screen off
    return () => { };
  }, [/** input */]);

  /** Catching action get variation */
  React.useEffect(() => {
    if (ProductGetDetailFetching === false && ProductGetDetailPayload.length === 0) return Alert.alert('THÔNG BÁO', 'Mã SP không đúng hoặc không tồn tại!');
    if (!initialRender) setDataVariation(ProductGetDetailPayload);
  }, [ProductGetDetailPayload]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    /** Go back navigation */
    onPressGoBack: () => {
      props.navigation.goBack();
    },

    /** Add variation to cart
      1. Validate
        - if don't have bill id flowx fail => Alert
        - if ProductGetDetailPayload null or [] => dont have a variation => Alert
    */
    onPressAddProduct: () => {
      //props.navigation.goBack();
      props.navigation.navigate('CartDetailNewVerScreen', {
        itemId: true,
        otherParam: 'anything you want here',
      }
      // {
      //   itemId: 86,
      //   otherParam: 'anything you want here',
      // }
      )
      //console.log(props)

      // if (!billDetail.id) {
      //   Alert.alert('THÔNG BÁO', 'Đơn hàng tạm hiện đang bị lỗi! Vui lòng xóa đơn tạo đơn hàng mới');
      //   return props.navigation.goBack();
      // }
      // if (dataVariation && dataVariation.length === 0) return Alert.alert('THÔNG BÁO', 'Bạn chưa có sản phẩm nào! Vui lòng quét hoặc nhập mã SP vào ô tìm kiếm');
      // if (dataVariation.length > 1 || !dataVariation[0].variationId) return Alert.alert('THÔNG BÁO', 'Vui lòng nhập chính xác mã sản phẩm!');
      // const dataOrder = {
      //   billId: billDetail.id,
      //   quantity: 1,
      //   ...dataVariation[0],
      // };
      // dispatch(BillTempClientActions.updateVariationToBill(dataOrder));
      // setDataVariation([]);
      // Actions.onPressRescan();
    },
  //     ShowHideComponent :() => {
  //   if (IsHide == true) {
  //     setIsHide(false);
  //   } else {
  //     setIsHide(true);
  //   }
  // },  
    /** Handle with data call back from component scan */
    scanSuccess: (result) => {
      const dataFromScan = _.get(result, 'data', '');
      const { decode } = jwt;
      console.log("qrcode",jwt)



      Actions.getVariationWithParams(dataFromScan, 'scan');
    
    },
    /** Reactive scan */
    onPressRescan: () => {
      setSearchVariation('');
      setDataVariation([]);
      setRescanQRCode(true);
    },

    /** Find variation with variationId or _q */
    getVariationWithParams: (value, type = 'search') => {
      const paramsRequest = {
        _populate: ['variation', 'store'],
      };
      paramsRequest.variationId = (type === 'scan') ? value : undefined;
      
      paramsRequest.variationId_contains = (type === 'search') ? value : undefined;

      if(value === "1205")
      {
        if (IsHide == true) {
          setIsHide(false);
        } else {
          setIsHide(true);
        }
      }
      else if(value != "1205")
      {
        Alert.alert('THÔNG BÁO', 'Mã SP không đúng hoặc không tồn tại!')
      }
      dispatch(ProductGetDetailActions.productGetDetailRequest(paramsRequest));
    },
  };

  const controlerForState = {
    initialRender,
    IsHide,
    setIsHide,
    rescanQRCode,
    dataVariation,
    searchVariation,
    setSearchVariation,
  };

  const controlerForProps = {
    ProductGetDetailPayload,
  };

  /**
   * RENDER
   *  - declare variables and function for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    ...props,
    Actions,
    controlerForState,
    controlerForProps,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  /** DECLARE EVERYTHING */
  

  const {
    Actions, controlerForState, controlerForProps,
  } = props;
  const {
    initialRender, dataVariation, rescanQRCode,IsHide,setIsHide,
    searchVariation, setSearchVariation,
  } = controlerForState;

  const {
    ProductGetDetailPayload,
  } = controlerForProps;

  console.tron.log('catching render', dataVariation);
  const DATA = [
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Bitis Hunter-size[39, 40, 41]',
      imge: 'https://images.ctfassets.net/od02wyo8cgm5/4VXZrAobQXbBcEExdKqIHI/2a4bb16c771038d5edaad8ba33682d75/cloudswift-fw19-rock_slate-m-g1.png',
      price: "1.600.000 VNĐ"
    },
  ];
  function Item({ title, price}) {
    return (
      <View style={{flexDirection: "row",  marginVertical: 8, borderRadius: 4
     
     }}>
     <View style={{backgroundColor: "white"}}>
      <Image style={{width: 100, height: 95}} source=
      {{ uri: "https://images.ctfassets.net/od02wyo8cgm5/4VXZrAobQXbBcEExdKqIHI/2a4bb16c771038d5edaad8ba33682d75/cloudswift-fw19-rock_slate-m-g1.png"}}
      >
      </Image></View>
<View style={{flexDirection: "column"}}>
        <Text style={{ 
   color:"#fff",
   marginTop: 10, marginLeft: 10,
    }}>{title}</Text>
    <View style={{flexDirection: "row",  marginTop: 30, marginLeft: 10
    }}>
        <Text style={{ color:"#fff",}}>
          Nam
        </Text>
        <Text style={{ color:"#fff", marginLeft: 30}}>
          {price}
        </Text>

    </View>
    
    
    </View>
      </View>
    );
  }
  return (
    <View>
      <ScanQRCode

        onScanSuccess={Actions.scanSuccess}
        isSearch
        reactivate={rescanQRCode}
      />
      <View style={styles.container}>

        {/* Block header custom */}
        <View style={styles.blockHeader}>
          <TouchableOpacity
            onPress={Actions.onPressGoBack}
            style={{ padding: Metrics.paddingXXSmall }}
          >
            <IconOutline name="left" size={24} color={Colors.white} />
          </TouchableOpacity>
          <Text style={styles.textTitle}>Quét</Text>
        </View>

        {/* Block input search */}
        <View style={styles.blockSearch}>
          <TextInput
            style={styles.boxTextInput}
            placeholder="Nhập mã sản phẩm"
            placeholderTextColor="white"
            value={searchVariation}
            onChangeText={(text) => setSearchVariation(text)}
            onSubmitEditing={() => Actions.getVariationWithParams(searchVariation, 'search')}
          />
          <TouchableOpacity onPress={() => 
  
          Actions.getVariationWithParams(searchVariation, 'search')}>
            <Unicons style={styles.iconSearch} name="search" />
          </TouchableOpacity>
        </View>

        {/* Block input search */}
        <View style={{ flex: 1,  }}>
          { dataVariation && dataVariation.length === 1
          && (
          <View style={styles.blockProduct}>
            <Image
              style={styles.imageProduct}
              source={{ uri: 'https://product.hstatic.net/1000230642/product/dsuh00100cam__4__fc1b8001ad264b6d8ed49c96fa3525c8_1024x1024.jpg' }}
            />
            <View style={styles.productDetail}>
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              <Text style={styles.text1}>{dataVariation[0].variation.name}</Text>
              <Text style={styles.text2}>Mã sản phẩm: {dataVariation[0].variationId}</Text>
              <Text style={styles.text3}>{formatMoney(dataVariation[0].price)} đ</Text>
            </View>
          </View>
          )}
   {IsHide ? ( <View style={{
height: 100, marginTop: 350, 
borderLeftWidth: 2, 
borderBottomWidth: 2, 
borderTopWidth: 2, 
borderRightWidth: 2,
borderColor: "white",
marginLeft: 25,
marginRight: 25,
alignItems: "center"
 }}>
<FlatList
        data={DATA}
        renderItem={({ item }) => <Item title={item.title} price={item.price} />}
        keyExtractor={item => item.id}
      />
</View> ): null}
        </View>
        <View  style={{ marginHorizontal: -Metrics.marginMedium }}>
          <TwoButtonNewVer
            onPressButton1={Actions.onPressRescan}
            onPressButton2={Actions.onPressAddProduct}
            buttonText1="Quét lại"
            buttonText2="Thêm vào giỏ hàng"
          />
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
