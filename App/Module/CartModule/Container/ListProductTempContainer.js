import React from 'react';
import {
  Image, View, Text, FlatList, TouchableOpacity, Alert,
} from 'react-native';
import moment from 'moment';
import Swipeout from 'react-native-swipeout';
import { useSelector, useDispatch } from 'react-redux';
import { IconOutline } from '@ant-design/icons-react-native';
import _ from 'lodash';
// COMPONENTS
import styles from './Styles/ListProductTempContainer.styles';
import { compareOmitState, formatMoney } from '../../../Services/utils';
import { Colors, Metrics } from '../../../Themes';
import { Unicons } from '../../../Components/Icons';
import ItemOrderNewVer from '../Components/ItemOrderNewVer';
/** ACTIONS */
import BillTempClientActions from '../Redux/BillTempClientRedux';

require('moment/locale/vi');

const ListProductTempContainer = (props = {}) => {
  const dispatch = useDispatch();

  const onPressIncreaseOrDecrease = (dataHandle = {}, type = undefined) => {
    if (type === 'decrease' && dataHandle.quantity === 1) return Alert.alert('THÔNG BÁO', 'Số lượng sản phẩm chỉ còn 1. Nếu bạn muốn xóa sản phẩm vui lòng vuốt ngang');
    if (type === 'increase' && dataHandle.quantity >= (dataHandle.inventory - dataHandle.sold)) {
      return Alert.alert('THÔNG BÁO', 'Số lượng bạn chọn đã quá số lượng tồn kho');
    }
    dispatch(BillTempClientActions.increaseVariationToBill({ dataHandle, type }));
  };

  /** Delete variation from bill temp */
  const onPressDeleteVariation = (params = {}) => {
    dispatch(BillTempClientActions.deleteVariationRequest(params));
  };

  const { blockType, billData } = props;


  const _renderItemOrder = ({ item }) => {
    const dataOders = {
      block_type: blockType || 'justView',
      billId: billData.id,
      orderDetail: item,
    };
    console.tron.log('_renderItemOrder', item);
    const swipeoutBtns = [
      {
        component: (
          <View style={styles.block_swiper}>
            <Unicons name="times-circle" style={styles.icon_delete} />
            <Text style={styles.text2}>Xóa</Text>
          </View>
        ),
        backgroundColor: Colors.orange1,
        onPress: () => onPressDeleteVariation({ billId: billData.id, variationId: item.variationId }),
      },
    ];

    return (
      <Swipeout
        right={swipeoutBtns}
        disabled={blockType === 'justView'}
      >
        <ItemOrderNewVer dataOders={dataOders} callbackFromChild={onPressIncreaseOrDecrease} />
      </Swipeout>
    );
  };

  return (
    <FlatList
      keyExtractor={(item, index) => item.id}
      data={billData.orders}
      renderItem={_renderItemOrder}
      initialNumToRender={3}
      ListHeaderComponent={(
        <View style={styles.block_header_border} />
      )}
      ItemSeparatorComponent={() => (
        <View style={styles.block_separator}>
          <View style={styles.line_style} />
        </View>
      )}
      ListEmptyComponent={() => (
        <Text style={{ textAlign: 'center', marginTop: Metrics.marginMedium }}>Không có sản phẩm nào!</Text>
      )}
    />
  );
};

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
}

export default React.memo(ListProductTempContainer, areEqual);
