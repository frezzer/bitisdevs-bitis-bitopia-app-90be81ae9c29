import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  block_header_list: {
    padding: Metrics.paddingMedium,
    paddingTop: Metrics.paddingXXLarge,
  },
  block_separator: {
    paddingHorizontal: Metrics.marginLarge,
    backgroundColor: Colors.white,
  },
  line_style: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
  },
  block_swiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  block_bottom: {
    padding: Metrics.paddingMedium,
    paddingBottom: Metrics.paddingXXLarge,
    backgroundColor: Colors.background,
  },
  // ICON
  icon_delete: {
    fontSize: Fonts.size.textXHuge,
    color: Colors.white,
  },
  // TEXT
  text1: {
    color: Colors.grey8,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
  },
  text2: {
    color: Colors.white,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
