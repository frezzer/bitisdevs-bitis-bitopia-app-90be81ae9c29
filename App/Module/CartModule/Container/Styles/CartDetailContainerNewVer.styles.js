import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  block_background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'red',
  },
  background_part1: {
    flex: 1,
    backgroundColor: Colors.blue1,
  },
  background_part2: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  block_header: {
    justifyContent: 'flex-end',
    paddingHorizontal: Metrics.paddingMedium,
  },
  scroll_view: {
    zIndex: 10,
  },
  block_content: {
    height: Metrics.height,
    backgroundColor: Colors.white,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
    paddingTop: Metrics.paddingXXLarge,
    paddingHorizontal: Metrics.paddingMedium,
  },
  block_bill_info: {
    marginHorizontal: -Metrics.marginMedium,
    marginBottom: Metrics.marginHuge,
  },
  line_style: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
  },
  block_swiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  block_bottom: {
    padding: Metrics.paddingMedium,
    paddingBottom: Metrics.paddingXXLarge,
  },
  // BUTTON
  btn_header_cover: {
    borderRadius: Metrics.boderLSmall,
  },
  // ICON
  icon_delete: {
    fontSize: Fonts.size.textXHuge,
    color: Colors.white,
  },
  // TEXT
  text1: {
    color: Colors.grey9,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,
  },
  text2: {
    color: Colors.white,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
  },
  text3: {
    color: Colors.black,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textLarge,
    textAlign: 'right',
  },
  text4: {
    color: Colors.grey9,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,
    textAlign: 'right',
  },
});
