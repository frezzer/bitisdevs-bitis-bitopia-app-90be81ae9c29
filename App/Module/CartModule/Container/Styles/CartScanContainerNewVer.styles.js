/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: Metrics.paddingMedium,
    position: 'absolute',
    left: 0,
    right: 0,
    height: Metrics.height,
  },
  blockHeader: {
    height: Metrics.navigationBarHeight,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  blockSearch: {
    marginTop: Metrics.marginMedium,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    borderRadius: Metrics.boderLSmall,
    borderColor: Colors.white,
    borderWidth: 1,
  },
  boxTextInput: {
    height: wp(44 / 375 * 100),
    flex: 1,
    color: Colors.white,
  },
  blockProduct: {
    marginTop: Metrics.height < 670 ? wp(395 / 375 * 100) : wp(405 / 375 * 100),
    flexDirection: 'row',
    borderRadius: Metrics.boderLSmall,
    padding: Metrics.paddingMedium,
    borderWidth: 1,
    borderColor: Colors.white,
  },
  productDetail: {
    flex: 1,
    marginLeft: Metrics.marginTiny,
  },
  imageProduct: {
    height: wp(72 / 375 * 100),
    width: wp(72 / 375 * 100),
  },
  // Icon
  iconSearch: {
    fontSize: wp(20 / 375 * 100),
    color: Colors.white,
  },
  // Text
  textTitle: {
    textAlign: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: Metrics.marginXSmall,
    color: Colors.white,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,
  },
  text1: {
    color: Colors.white,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    lineHeight: wp(20 / 375 * 100),
  },
  text2: {
    color: Colors.white,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textXSmall,
    lineHeight: wp(18 / 375 * 100),
  },
  text3: {
    color: Colors.white,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    lineHeight: wp(22 / 375 * 100),
  },
});
