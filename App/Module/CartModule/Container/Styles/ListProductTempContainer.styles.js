/* eslint-disable no-mixed-operators */
import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../../Themes';

export default StyleSheet.create({
  block_header_border: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
    height: wp(8 / 375 * 100),
    marginHorizontal: Metrics.marginMedium,
  },
  block_swiper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  icon_delete: {
    fontSize: Fonts.size.textXHuge,
    color: Colors.white,
  },
  // Text
  text2: {
    color: Colors.white,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
  },
});
