import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  addBillTempClientRequest: ['dataAdd'],
  deleteBillTempClientRequest: ['deleteVariationParams'],
  updateVariationToBill: ['dataVariation'],
  increaseVariationToBill: ['increaseVariationData'],
  deleteVariationRequest: ['deleteVariationParams'],
});

export const CreateBillTempClientTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  activeIndex: 0,
  data: [],
});

/* ------------- Selectors ------------- */

const getData = (state) => state.data;
const getActiveIndex = (state) => state.activeIndex;

export const CreateBillTempClientSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectActiveIndex: createSelector([getActiveIndex], (activeIndex) => activeIndex),
};

/* ------------- Reducers ------------- */

/**
  Request the data from an api
*/
/** ADD NEW BILL TEMP CLIENT */
export const addBillTemp = (state, action) => {
  const { dataAdd } = action;
  const litsBillTemp = Immutable.asMutable(state.data);
  const nextActiveIndex = state.activeIndex + 1;
  dataAdd.totalPrice = 0;
  litsBillTemp.push(dataAdd);
  return state.merge({ data: litsBillTemp, activeIndex: nextActiveIndex });
};

/** DELETE BILL TEMMP CLIENT */
export const deleteBillTemp = (state, action) => {
  const { deleteVariationParams } = action;
  const litsBillTemp = Immutable.asMutable(state.data);
  _.remove(litsBillTemp, (bill) => bill.id === deleteVariationParams.id);
  return state.merge({ data: litsBillTemp });
};

/** UPDATE VARITION DATA TO BILL */
export const updateVariation = (state, action) => {
  const { dataVariation } = action;
  const litsBillTempOld = Immutable.asMutable(state.data);

  let billCurrent = _.clone(_.find(litsBillTempOld, (bill) => bill.id === dataVariation.billId));
  const newOrder = Immutable.asMutable(billCurrent.orders);
  const checkVaritionExist = _.findIndex(newOrder, (order) => order.variation.uuid === dataVariation.variation.uuid);

  if (checkVaritionExist === -1) {
    newOrder.push(_.omit(dataVariation, ['billId']));
  } else {
    const handleOrder = {
      ...newOrder[checkVaritionExist],
      quantity: newOrder[checkVaritionExist].quantity + 1,
    };
    newOrder[checkVaritionExist] = handleOrder;
  }
  billCurrent = {
    ...billCurrent,
    orders: newOrder,
    totalPrice: _.sumBy(newOrder, (order) => order.quantity * order.price),
  };

  _.remove(litsBillTempOld, (billTemp) => billTemp.id === dataVariation.billId);
  litsBillTempOld.push(billCurrent);
  return state.merge({ data: litsBillTempOld });
};

/** INCREASE OR DECREASE QUANTITY VARITION TO BILL */
export const increaseVariation = (state, action) => {
  const { increaseVariationData } = action;
  const { dataHandle, type } = increaseVariationData;
  const litsBillTempOld = Immutable.asMutable(state.data);

  let billCurrent = _.find(litsBillTempOld, (bill) => bill.id === dataHandle.billId);
  const newOrder = Immutable.asMutable(billCurrent.orders);
  const findVaritionIndex = _.findIndex(newOrder, (order) => order.variation.uuid === dataHandle.variation.uuid);
  const handleOrder = {
    ...newOrder[findVaritionIndex],
    quantity: (type === 'increase') ? (newOrder[findVaritionIndex].quantity + 1) : (newOrder[findVaritionIndex].quantity - 1),
  };
  newOrder[findVaritionIndex] = handleOrder;

  billCurrent = {
    ...billCurrent,
    orders: newOrder,
    totalPrice: _.sumBy(newOrder, (order) => order.quantity * order.price),
  };

  _.remove(litsBillTempOld, (billTemp) => billTemp.id === dataHandle.billId);
  litsBillTempOld.push(billCurrent);
  return state.merge({ data: litsBillTempOld });
};

/** DELETE VARITION TO BILL TEMP */
export const deleteVariation = (state, action) => {
  const { deleteVariationParams } = action;
  const { billId, variationId } = deleteVariationParams;
  const litsBillTempOld = Immutable.asMutable(state.data);

  let billCurrent = _.find(litsBillTempOld, (bill) => bill.id === billId);
  const newOrder = Immutable.asMutable(billCurrent.orders);
  _.remove(newOrder, (order) => order.variation.uuid === variationId);
  billCurrent = {
    ...billCurrent,
    orders: newOrder,
    totalPrice: _.sumBy(newOrder, (order) => order.quantity * order.price),
  };
  _.remove(litsBillTempOld, (billTemp) => billTemp.id === billId);
  litsBillTempOld.push(billCurrent);
  return state.merge({ data: litsBillTempOld });
};

/** DELETE VARITION TO BILL */


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_BILL_TEMP_CLIENT_REQUEST]: addBillTemp,
  [Types.DELETE_BILL_TEMP_CLIENT_REQUEST]: deleteBillTemp,
  [Types.UPDATE_VARIATION_TO_BILL]: updateVariation,
  [Types.INCREASE_VARIATION_TO_BILL]: increaseVariation,
  [Types.DELETE_VARIATION_REQUEST]: deleteVariation,
});
