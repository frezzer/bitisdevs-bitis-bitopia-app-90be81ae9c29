import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  createBillCraftRequest: ['data'],
  createBillCraftSuccess: ['payload'],
  createBillCraftFailure: ['error'],
  createBillCraftClear: null,
});

export const CreateBillCraftTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  success: null,
  error: null,
});

/* ------------- Selectors ------------- */

const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;
const getSuccess = (state) => state.success;

export const CreateBillCraftSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
  selectSuccess: createSelector([getSuccess], (success) => success),
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({
    success: true, fetching: false, error: null, payload,
  });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: null });
export const clear = (state) => state.merge({ ...INITIAL_STATE });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CREATE_BILL_CRAFT_REQUEST]: request,
  [Types.CREATE_BILL_CRAFT_SUCCESS]: success,
  [Types.CREATE_BILL_CRAFT_FAILURE]: failure,
  [Types.CREATE_BILL_CRAFT_CLEAR]: clear,
});
