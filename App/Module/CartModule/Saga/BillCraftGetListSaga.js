/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */
import { AsyncStorage } from 'react-native';
import { call, put, select } from 'redux-saga/effects';
import _ from 'lodash';
import { MeSelectors } from '../../../Redux/MeRedux';
import BillCraftGetListActions from '../Redux/BillCraftGetListRedux';

// eslint-disable-next-line import/prefer-default-export
export function* getBillCraftHistory(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.getBillCraftHistory, {
    Authorization,
    data,
  });
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(BillCraftGetListActions.billCraftGetHistorySuccess(response.data));
  } else {
    yield put(BillCraftGetListActions.billCraftGetHistoryFailure(response.error));
  }
}
