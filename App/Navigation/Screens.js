
export default {

  // initial
  SplashScreen: 'SplashScreen',
  // AUTH
  LoginScreen: 'LoginScreen',
  // HOME
  DashBoardScreen: 'DashBoardScreen',
  // Seller
  SaleStatisticScreen: 'SaleStatisticScreen',
  SellerCheckInScreen: 'SellerCheckInScreen',
  SellerConfirmScreen: 'SellerConfirmScreen',
  SellerCheckOutScreen: 'SellerCheckOutScreen',
  SellerListShiftScreen: 'SellerListShiftScreen',
  SellerInforShiftScreen: 'SellerInforShiftScreen',
  SellerStaffSummaryScreen: 'SellerStaffSummaryScreen',

  // Manager
  ManagerStatisticScreen: 'ManagerStatisticScreen',
  ManagerScanCreateShiftScreen: 'ManagerScanCreateShiftScreen',
  ManagerCreateShiftScreen: 'ManagerCreateShiftScreen',
  ManagerConfirmCreateScreen: 'ManagerConfirmCreateScreen',
  ManagerFinishShiftScreen: 'ManagerFinishShiftScreen',
  ManagerListShiftScreen: 'ManagerListShiftScreen',
  ManagerCloseShiftScreen: 'ManagerCloseShiftScreen',
  ManagerConfirmCloseShiftScreen: 'ManagerConfirmCloseShiftScreen',
  ManagerShiftSummaryScreen: 'ManagerShiftSummaryScreen',
  ManagerShiftSummaryOfSellerScreen: 'ManagerShiftSummaryOfSellerScreen',
  FilterScreen: 'FilterScreen',

  // Menu
  AttendanceListScreen: 'AttendanceListScreen',
  AttendanceInfoScreen: 'AttendanceInfoScreen',
  LeaveOffWorkListScreen: 'LeaveOffWorkListScreen',
  LeaveOffWorkInfoScreen: 'LeaveOffWorkInfoScreen',
  SummaryAttendanceScreen: 'SummaryAttendanceScreen',

  // Other
  MoreScreen: 'MoreScreen',
  BillScreen: 'BillScreen',
  SampleScreen: 'SampleScreen',
  TestScreen: 'TestScreen',
  NewScreen: 'NewScreen',
  HistoryScreen: 'HistoryScreen',
  NotificationScreen: 'NotificationScreen',
  StoreNewsScreen: 'StoreNewsScreen',
  HistoryNewsScreen: 'HistoryNewsScreen',
  NewsDetails: 'NewsDetails',
  PickDayScreen: 'PickDayScreen',

  // Information
  SettingScreen: 'SettingScreen',
  ProfileScreen: 'ProfileScreen',
  PassWordScreen: 'PassWordScreen',
  MissionScreen: 'MissionScreen',
  EmployeeManagementScreen: 'EmployeeManagementScreen',
  HelpScreen: 'HelpScreen',

  // / FOR DESIGN LAYOUT
  // Cart
  CartDetailScreen: 'CartDetailScreen',
  CartScreen: 'CartScreen',
  CartCustomerScreen: 'CartCustomerScreen',
  ScanProductScreen: 'ScanProductScreen',
  // Cart News Ver
  CartScreenNewVer: 'CartScreenNewVer',
  CartDetailNewVerScreen: 'CartDetailNewVerScreen',
  CartDetailJustViewScreen: 'CartDetailJustViewScreen',
  CartScanScreen: 'CartScanScreen',
  // Bottom Router
  BottomManagerRouter: 'BottomManagerRouter',
  BottomSalerRouter: 'BottomSalerRouter',
  BottomNewsRouter: 'BottomNewsRouter',
  //
  TestingScreen: 'TestingScreen',
  AppScreen: 'AppScreen',

};
