import {
    Dimensions,
    Platform,
    BackHandler, Animated, Easing
  } from 'react-native';
  
  let {width} = Dimensions.get('window');
  
  let IosTransition = (index, position) => {
    const inputRange = [index - 1, index, index + 0.99, index + 1];
    const outputRange = [width, 0, -10, -10];
  
    const translateY = 0;
    const translateX = position.interpolate({
      inputRange,
      outputRange,
    });
  
    const opacity = position.interpolate({
      inputRange,
      outputRange: [0, 1, 1, 0],
    });
    return {
      opacity,
      transform: [{translateX}, {translateY}],
    };
  };
  
  let DroidTransition = (index, position) => {
    const inputRange = [index - 1, index, index + 0.99, index + 1];
  
    const opacity = position.interpolate({
      inputRange,
      outputRange: [0, 1, 1, 0],
    });
  
    const translateX = 0;
    const translateY = position.interpolate({
      inputRange,
      outputRange: [50, 0, 0, 0],
    });
  
    return {
      opacity,
      transform: [{translateX}, {translateY}],
    };
  };
  
  const transitionConfig = () => {
    return {
      screenInterpolator: (sceneProps) => {
        const {position, scene} = sceneProps;
        const {index} = scene;
        if (Platform.OS === 'ios')
          return IosTransition(index, position);
        return DroidTransition(index, position);
      }
      // transitionSpec: {
      //   duration: 300,
      //   easing: Easing.out(Easing.poly(4)),
      //   timing: Animated.timing,
      // },
      // screenInterpolator: sceneProps => {
      //   const { layout, position, scene } = sceneProps
      //   const { index } = scene

      //   const height = layout.initHeight
      //   const translateY = position.interpolate({
      //     inputRange: [index - 1, index, index + 1],
      //     outputRange: [height, 0, 0],
      //   })

      //   const opacity = position.interpolate({
      //     inputRange: [index - 1, index - 0.99, index],
      //     outputRange: [0, 1, 1],
      //   })

      //   return { opacity, transform: [{ translateY }] }
      // },
    }
  }
  
  // const transitionConfig = () => {
  //   return {
  //     transitionSpec: {
  //       duration: 750,
  //       easing: Easing.out(Easing.poly(4)),
  //       timing: Animated.timing,
  //       useNativeDriver: true,
  //     },
  //     screenInterpolator: sceneProps => {
  //         const { position, layout, scene, index, scenes } = sceneProps
  //         const toIndex = index
  //         const thisSceneIndex = scene.index
  //         const height = layout.initHeight
  //         const width = layout.initWidth
    
  //         const translateX = position.interpolate({
  //           inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
  //           outputRange: [width, 0, 0]
  //         })
    
  //         // Since we want the card to take the same amount of time
  //         // to animate downwards no matter if it's 3rd on the stack
  //         // or 53rd, we interpolate over the entire range from 0 - thisSceneIndex
  //         const translateY = position.interpolate({
  //           inputRange: [0, thisSceneIndex],
  //           outputRange: [height, 0]
  //         })
    
  //         const slideFromRight = { transform: [{ translateX }] }
  //         const slideFromBottom = { transform: [{ translateY }] }
    
  //         const lastSceneIndex = scenes[scenes.length - 1].index
    
  //         // Test whether we're skipping back more than one screen
  //         if (lastSceneIndex - toIndex > 1) {
  //           // Do not transoform the screen being navigated to
  //           if (scene.index === toIndex) return
  //           // Hide all screens in between
  //           if (scene.index !== lastSceneIndex) return { opacity: 0 }
  //           // Slide top screen down
  //           return slideFromBottom
  //         }
    
  //         return slideFromRight
  //       },
  //   }}
  export const stackTransition = () => {
    return {
      screenInterpolator: (sceneProps) => {
        const { position, scene } = sceneProps;
        const { index } = scene;
        const inputRange = [index - 1, index, index + 1]; // [-1, 0 ,0.99, 1]
        const outputRange = [width, 0, 0];
        const translateY = 0;
        const translateX = position.interpolate({
          inputRange,
          outputRange,
        });
  
        const opacity = position.interpolate({
          inputRange,
          outputRange: [0, 1, 0],
        });
        return {
          opacity,
          transform: [{ translateX }, { translateY }],
        };
      },
      transitionSpec: {
        // duration: 300,
        // easing: Easing.out(Easing.poly(4)),
        // timing: Animated.timing,
      },
    }
  }
    
  export default transitionConfig;