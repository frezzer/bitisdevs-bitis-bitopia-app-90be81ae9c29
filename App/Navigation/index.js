
import React from 'react';
import {
  Platform, Text, Button, TouchableOpacity,
  Dimensions, View, Image,
} from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';


// COMPONENTS
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import {
  Colors, Images, Fonts, Metrics,
} from '../Themes';
import { Unicons } from '../Components/Icons';
import styles from './Styles/IndexStyles';

import SplashScreen from '../Screens/Initial/SplashScreen';
import LoginScreen from '../Screens/Auth/LoginScreen';
import DashBoardScreen from '../Screens/Other/DashBoardScreen';
// /  SELLER
import SaleStatisticScreen from '../Screens/Seller/StatisticScreen';
import SellerCheckInScreen from '../Screens/Seller/CheckInScreen';
import SellerCheckOutScreen from '../Screens/Seller/CheckOutScreen';
import SellerConfirmScreen from '../Screens/Seller/SellerConfirmScreen';
import SellerListShiftScreen from '../Screens/Seller/ListShiftScreen';
import SellerInforShiftScreen from '../Screens/Seller/SellerInforShiftScreen';
import SellerStaffSummaryScreen from '../Screens/Seller/SellerStaffSumary';
// MANAGER
import ManagerStatisticScreen from '../Screens/Manager/StatisticScreen';
import ManagerConfirmCreateScreen from '../Screens/Manager/ConfirmCreateScreen';
import ManagerScanCreateShiftScreen from '../Screens/Manager/ScanCreateShiftScreen';
import ManagerCreateShiftScreen from '../Screens/Manager/CreateShiftScreen';
import ManagerCloseShiftScreen from '../Screens/Manager/CloseShift';
import ManagerListShiftScreen from '../Screens/Manager/ListShiftScreen';
import ManagerConfirmCloseShiftScreen from '../Screens/Manager/ConfirmCloseShiftScreen';
import ManagerShiftSummaryScreen from '../Screens/Manager/ShiftSummaryScreen';
import ManagerShiftSummaryOfSellerScreen from '../Screens/Manager/ShiftSummaryScreenOfSeller';
import ManagerFinishShiftScreen from '../Screens/Manager/ManagerFinishShiftScreen';
import FilterScreen from '../Screens/Other/FilterScreen';

// OTHERS
import BillScreen from '../Screens/Other/BillScreen';
import MoreScreen from '../Screens/Other/MoreScreen';
import SettingScreen from '../Screens/Other/SettingScreen';
import HelpScreen from '../Screens/Other/HelpScreen';
import TestScreen from '../Screens/Other/TestScreen';
import PickDayScreen from '../Screens/Other/PickDayScreen';
// import SampleScreen from '../Screens/Sample/SampleScreen1';
import EmployeeManagementScreen from '../Screens/Other/EmployeeManagementScreen';
import TestingScreen from '../../_test_/test';
import NewScreen from '../Screens/News/NewScreen';
import HistoryScreen from '../Screens/Other/HistoryScreen';
import NotificationScreen from '../Screens/Other/NotificationScreen';
import StoreNewsScreen from '../Screens/News/StoreNewsScreen';
import HistoryNewsScreen from '../Screens/News/HistoryNewsScreen';
import NewsDetails from '../Screens/News/TestAnimation';


// CART
import CartDetailScreen from '../Screens/Cart/CartDetailScreen';
import CartScreen from '../Screens/Cart/CartScreen';
import CartCustomerScreen from '../Screens/Cart/CartCustomerScreen';
import ScanProductScreen from '../Screens/Sample/ScanProductScreen';

// CART NEW VER
import CartNewVerScreen from '../Screens/Cart/CartNewVerScreen';
import CartDetailNewVerScreen from '../Screens/Cart/CartDetailNewVerScreen';
import CartDetailJustViewScreen from '../Screens/Cart/CartDetailJustViewScreen';
import CartScanScreen from '../Screens/Cart/CartScanScreen';

// INFORMATION
import ProfileScreen from '../Screens/Information/ProfileScreen';
import PassWordScreen from '../Screens/Information/PassWordScreen';
import MissionScreen from '../Screens/Other/MissionScreen';
// MENU
import SummaryAttendanceScreen from '../Screens/Menu/SummaryAttendanceScreen';
import AttendanceListScreen from '../Screens/Menu/AttendanceListScreen';
import AttendanceInfoScreen from '../Screens/Menu/AttendanceInfoScreen';
import LeaveOffWorkListScreen from '../Screens/Menu/LeaveOffWorkListScreen';
import LeaveOffWorkInfoScreen from '../Screens/Menu/LeaveOffWorkInfoScreen';
// BOTTOM TAB
import BottomTabs from '../Components/BottomTabNavigator';

const defaultNavigationOptionsConfig = {
  gesturesEnabled: true,
  headerTitleAllowFontScaling: true,
  headerBackAllowFontScaling: true,
  headerTintColor: '#fff',
  headerBackTitle: null,
  headerBackImage: () => (
    <View style={styles.back_btn}>
      {/* <Image source={Images.buttonBack} /> */}
      <IconOutline name="left" color="white" style={styles.iconBack} />
    </View>
  ),
  headerStyle: {
    backgroundColor: Colors.blue2,
  },
  headerBackTitleStyle: {},
  headerTitleStyle: styles.header_title_text,
  headerRight: () => {
    return <View />;
  },
};

// Stack router for manager flow
const CartStack = createStackNavigator(
  {
    CartNewVerScreen,
    CartDetailNewVerScreen,
    CartDetailJustViewScreen,
    // CartScreen,
    CartScanScreen,
    CartDetailScreen,
    CartCustomerScreen,
    ScanProductScreen,
  },
  {
    initialRouteName: 'CartNewVerScreen',
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <IconOutline name="shopping" size={24} color={focused ? Colors.orange1 : Colors.greybold} />
            <Text
              style={[styles.textBottomTab, { color: focused ? Colors.blue2 : Colors.greybold }]}
            >
              Bán hàng
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const StatisticStack = createStackNavigator(
  {
    ManagerStatisticScreen,
    FilterScreen,
  },
  {
    initialRouteName: 'ManagerStatisticScreen',
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <IconOutline name="line-chart" size={24} color={focused ? Colors.orange1 : Colors.greybold} />
            <Text
              style={[styles.textBottomTab, { color: focused ? Colors.blue2 : Colors.greybold }]}
            >
              Tổng quan
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const BillStack = createStackNavigator(
  {
    BillScreen,
    FilterScreen,
    CartDetailJustViewScreen,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            {/* <Unicons
              name="receipt-alt"
              style={{
                ...styles.icon_tab,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            /> */}
            <IconOutline name="snippets" size={24} color={focused ? Colors.orange1 : Colors.greybold} />
            <Text
              style={[styles.textBottomTab, { color: focused ? Colors.blue2 : Colors.greybold }]}
            >
              Hóa đơn
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const MoreSalerStack = createStackNavigator(
  {
    // TestScreen,
    MoreScreen,
    // MANAGER
    EmployeeManagementScreen,

    // SELLER
    SellerCheckInScreen,
    SellerListShiftScreen,
    SellerCheckOutScreen,
    SellerConfirmScreen,
    SellerInforShiftScreen,
    SellerStaffSummaryScreen,
    FilterScreen,
    HistoryScreen,
    PickDayScreen,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <IconOutline name="calendar" size={24} color={focused ? Colors.orange1 : Colors.greybold} />
            <Text
              style={[styles.textBottomTab, { color: focused ? Colors.blue2 : Colors.greybold }]}
            >
              Chấm công
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const MoreManagerStack = createStackNavigator(
  {
    // TestScreen,
    MoreScreen,
    // MANAGER
    ManagerConfirmCloseShiftScreen,
    ManagerCloseShiftScreen,
    ManagerScanCreateShiftScreen,
    ManagerCreateShiftScreen,
    ManagerConfirmCreateScreen,
    ManagerListShiftScreen,
    ManagerShiftSummaryScreen,
    EmployeeManagementScreen,
    ManagerShiftSummaryOfSellerScreen,

    // SELLER
    SellerCheckInScreen,
    SellerListShiftScreen,
    SellerCheckOutScreen,
    SellerConfirmScreen,
    SellerInforShiftScreen,
    SellerStaffSummaryScreen,
    FilterScreen,
    HistoryScreen,
    PickDayScreen,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <IconOutline name="team" size={24} color={focused ? Colors.orange1 : Colors.greybold} />
            <Text
              style={[styles.textBottomTab, { color: focused ? Colors.blue2 : Colors.greybold }]}
            >
              Quản lý
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const BottomManagerRouter = createBottomTabNavigator(
  {
    Cart: CartStack,
    Statistic: StatisticStack,
    Bill: BillStack,
    More: MoreManagerStack,
  },
  {
    initialRouteName: 'Cart',
    tabBarComponent: (props) => <BottomTabs {...props} />,
    tabBarOptions: {
      allowFontScaling: true,
      activeTintColor: Colors.blue,
      inactiveTintColor: Colors.white,
    },
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const BottomSalerRouter = createBottomTabNavigator(
  {
    Cart: CartStack,
    Statistic: StatisticStack,
    Bill: BillStack,
    More: MoreSalerStack,
  },
  {
    initialRouteName: 'Cart',
    tabBarComponent: (props) => <BottomTabs {...props} />,
    tabBarOptions: {
      allowFontScaling: true,
      activeTintColor: Colors.blue,
      inactiveTintColor: Colors.white,
    },
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);


const NewsStack = createStackNavigator(
  {
    NewScreen,
    NewsDetails,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <Unicons
              name="list-ul"
              style={{
                ...styles.icon_tab,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            />
            <Text
              style={{
                ...Fonts.style.caption,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            >
              Tin tức
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const StorageStack = createStackNavigator(
  {
    StoreNewsScreen,
  },
  {
    headerMode: Platform.select({
      header: null,
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <Unicons
              name="bookmark"
              style={{
                ...styles.icon_tab,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            />
            <Text
              style={{
                ...Fonts.style.caption,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            >
              Lưu trữ
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const HisotryStack = createStackNavigator(
  {
    HistoryNewsScreen,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <Unicons
              name="clock-nine"
              style={{
                ...styles.icon_tab,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            />
            <Text
              style={{
                ...Fonts.style.caption,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            >
              Lịch sử
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const BottomNewsRouter = createBottomTabNavigator(
  {
    News: NewsStack,
    Storage: StorageStack,
    Hisotry: HisotryStack,
  },
  {
    initialRouteName: 'News',
    tabBarComponent: (props) => <BottomTabs {...props} />,
    tabBarOptions: {
      allowFontScaling: true,
      activeTintColor: Colors.blue,
      inactiveTintColor: Colors.white,
    },
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    navigationOptions: ({ navigation }) => {
      const tabBarVisible = !(navigation.state.index > 0);
      return {
        tabBarIcon: ({ focused, tintColor }) => (
          <View style={styles.tab_content}>
            <Unicons
              name="list-ul"
              style={{
                ...styles.icon_tab,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            />
            <Text
              style={{
                ...Fonts.style.caption,
                color: focused ? Colors.blue : Colors.grayLight,
              }}
            >
              Khác
            </Text>
          </View>
        ),
        tabBarVisible,
      };
    },
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      ...defaultNavigationOptionsConfig,
    }),
  },
);

const AppRouter = createStackNavigator(
  {
    DashBoardScreen,
    // NewScreen,
    // SellerCheckInScreen,
    ProfileScreen,
    PassWordScreen,
    ManagerScanCreateShiftScreen,
    SettingScreen,
    HelpScreen,
    NotificationScreen,
  },
  {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    initialRouteName: 'DashBoardScreen',
    defaultNavigationOptions: ({ navigation, screenProps }) => ({
      gesturesEnabled: true,
      headerTitleAllowFontScaling: true,
      headerBackAllowFontScaling: true,
      headerTintColor: '#fff',
      headerBackTitle: null,
      headerBackImage: () => {
        return (
          <View style={styles.back_btn}>
            {/* <Image source={Images.buttonBack} /> */}
            <IconOutline name="left" color="white" style={styles.iconBack} />

          </View>
        );
      },
      headerStyle: {
        backgroundColor: Colors.blue2,
      },
      headerBackTitleStyle: {},
      headerTitleStyle: {
        fontSize: (16 / 375) * Metrics.width,
        fontWeight: 'bold',
      },
    }),
  },
);

const AuthRouter = createStackNavigator(
  {
    LoginScreen: { screen: LoginScreen },
  },
  {
    headerMode: 'none',
    initialRouteName: 'LoginScreen',
    navigationOptions: {},
  },
);

export default createAppContainer(
  createSwitchNavigator(
    {
      Initial: SplashScreen,
      // Initial: CartScanScreen,
      AuthRouter,
      AppRouter,
      BottomManagerRouter,
      BottomSalerRouter,
      BottomNewsRouter,
    },
    {
      headerMode: 'none',
      initialRouteName: 'Initial',
    },
  ),
);
