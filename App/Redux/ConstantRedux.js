import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setNavigation: null,
})

export const ConstantTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
  navigation: {},
})

/* ------------- Selectors ------------- */

const getData = (state) => state.navigation;

export const ConstantSelectors = {
  selectData: createSelector([getData], (navigation) => navigation),
}

/* ------------- Reducers ------------- */

// increase
export const setNavigation = (state, action) => {
  const navigation = action.navigation;
  state.merge({ navigation });
}

export const reducer = createReducer(INITIAL_STATE, {
  [ConstantTypes.SET_NAVIGATION]: setNavigation,
})
