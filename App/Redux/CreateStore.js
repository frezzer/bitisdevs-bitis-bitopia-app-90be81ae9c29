import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import Reactotron from 'reactotron-react-native';

import devToolsEnhancer, { composeWithDevTools } from 'remote-redux-devtools';
import RehydrationServices from '../Services/Rehydration';
import ReduxPersist from '../Config/ReduxPersist';
import Config from '../Config/DebugConfig';
import ScreenTracking from './ScreenTrackingMiddleware';
// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = [];
  const enhancers = [];

  /* ------------- Analytics Middleware ------------- */
  // middleware.push(ScreenTracking)

  /* ------------- Saga Middleware ------------- */

  const sagaMonitor = Config.useReactotron ? Reactotron.createSagaMonitor() : null;
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
  middleware.push(sagaMiddleware);

  /* ------------- Assemble Middleware ------------- */


  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  if (Config.useReactotron) enhancers.push(Reactotron.createEnhancer());
  enhancers.push(applyMiddleware(...middleware));

  const composeHelper = Config.useRemoteDevtools ? composeWithDevTools(...enhancers) : compose(...enhancers);
  const store = createStore(rootReducer, composeHelper);

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
    RehydrationServices.updateReducers(store);
  }

  // kick off root saga
  const sagasManager = sagaMiddleware.run(rootSaga);

  return {
    store,
    sagasManager,
    sagaMiddleware,
  };
};
