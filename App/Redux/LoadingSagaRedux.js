import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'
import { createSelector } from 'reselect'
import { LoginSelectors } from './LoginRedux'
import { MeSelectors } from './MeRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loadingSagaShow: ['text'],
  loadingSagaHide: null,
  loadingIndicatorShow: null,
  loadingIndicatorHide: null,
})

export const LoadingSagaTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  text: 'Đang Tải Dữ Liệu...',
  show: false,
  indicator: false
})

/* ------------- Selectors ------------- */

const getShow = state => (
  state.saga.loading.show ||
  LoginSelectors.selectFetching(state.login) ||
  MeSelectors.selectFetching(state.me)
  // add more auto loading you want
)
const getText = state => (state.text);
const getIndicator = state => state.indicator;


export const loadingSagaSelectors = {
  selectShow: createSelector([getShow], (show) => show),
  selectText: createSelector([getText], (text) => text),
  selectIndicator: createSelector([getIndicator], (show) => show),
}

/* ------------- Reducers ------------- */


export const show = (state, { text }) => state.merge({ text: text ? text : '', show: true })
export const hide = (state) => state.merge(INITIAL_STATE)
export const indicatorShow = (state) => state.merge({ indicator: true })
export const indicatorHide = (state) => state.merge({ indicator: false })


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOADING_SAGA_SHOW]: show,
  [Types.LOADING_SAGA_HIDE]: hide,
  [Types.LOADING_INDICATOR_SHOW]: indicatorShow,
  [Types.LOADING_INDICATOR_HIDE]: indicatorHide,
})
