import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  login: ['data'],
  loginSuccess: ['payload'],
  loginFailure: ['error'],
  initial: null,
  logout: null,
});

export const LoginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  logout: null,
});

/* ------------- Selectors ------------- */
const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;
const getLogout = (state) => state.logout;

export const LoginSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
  selectLogout: createSelector([getLogout], (logout) => logout),
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  return state.merge({
    fetching: true, data, payload: null, logout: false,
  });
};

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({
    fetching: false, error: null, payload, logout: false,
  });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({
  fetching: false, error: true, payload: null, logout: true,
});
export const logout = (state) => state.merge({ logout: true });
export const clear = (state) => state.merge({ ...INITIAL_STATE });

// export const logout = (state) => { return state; };
// export const clear = (state) => {
//   console.tron.log(state);
//   return state.merge({
//     ...INITIAL_STATE,
//     data: {
//       ...state.data,
//       password: null,
//     },
//   });
// };

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.INITIAL]: clear,
});
