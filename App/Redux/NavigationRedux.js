import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  navigationSetRoot: ['data'],
})

export const NavigationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  rootName: null,
})

/* ------------- Selectors ------------- */


const getRoot = state => state.rootName


export const NavigationSelectors = {
  selectData: createSelector([getRoot], (data) => data),
}


/* ------------- Reducers ------------- */

// request the data from an api
export const setRoot = (state, { data }) => {
  return state.merge({ rootName: data })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NAVIGATION_SET_ROOT]: setRoot,
})
