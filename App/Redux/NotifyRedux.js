import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'
import { createSelector } from 'reselect'
import { LoginSelectors } from './LoginRedux'
import { MeSelectors } from './MeRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  notifyShow: ['data'],
  notifyHide: null,
})

export const NotifyTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  text: '',
  show: false,
  color: 'green'
})

/* ------------- Selectors ------------- */

const getText = state => state.text;
const getShow = state => state.show;
const getColor = state => state.color;

export const NotifySelectors = {
  selectShow: createSelector([getShow], (show) => show),
  selectText: createSelector([getText], (text) => text),
  selectColor: createSelector([getColor], (color) => color),
}

/* ------------- Reducers ------------- */


export const show = (state, { data }) => state.merge(data);
export const hide = (state) => state.merge(INITIAL_STATE)


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NOTIFY_SHOW]: show,
  [Types.NOTIFY_HIDE]: hide,
})
