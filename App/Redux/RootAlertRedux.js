import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rootAlertShow: ['error'],
  rootAlertHide: null,
});

export const RootAlertTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  show: false,
  error: 'Lỗi cập nhật dữ liệu, vui lòng thử lại sau !',
});

/* ------------- Selectors ------------- */

const getShow = (state) => state.show;
const getError = (state) => state.error;

export const RootAlertSelectors = {
  selectShow: createSelector([getShow], (show) => show),
  selectError: createSelector([getError], (error) => {
    if (typeof error == 'string') return error;
    if (error.message && typeof error.message == 'string') return error.message;
    if (_.isArray(error.message)) {
      const errorMes = _.head(error.message);
      const message = _.head(errorMes.messages);
      return message.id;
    }
    if (error.raw && error.raw.message) return error.raw.message;
    if (error.code) return error.code;
    if (error.raw && error.raw.code) return error.raw.code;
    return 'Lỗi cập nhật dữ liệu, vui lòng thử lại sau !';
  }),
};

/* ------------- Reducers ------------- */

// show
export const show = (state, action) => {
  const { error } = action;
  return state.merge({ show: true, error });
};

// hide
export const hide = (state, action) => {
  const { error } = action;
  return state.merge(INITIAL_STATE);
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ROOT_ALERT_SHOW]: show,
  [Types.ROOT_ALERT_HIDE]: hide,
});
