import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'

/* ------------- Types and Action Creators ------------- */
import { MeTypes } from './MeRedux'


const { Types, Creators } = createActions({
  startup: null,
  reset: null,
  
})

export const StartupTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
  progress: 0,
})

/* ------------- Selectors ------------- */

const getProgress = (state) => state.progress;

export const StartupSelectors = {
  selectProgress: createSelector(
    [getProgress], (progress) => {
      if (progress < 0.99) {
        return progress
      } else {
        return 1
      }
    }),
}

/* ------------- Reducers ------------- */

// increase
export const increase = (state, action) => {
  const scaled = parseFloat(1.0 / 1.0)
  // console.tron.log('SPASH SCREEN' ,scaled.toFixed(5))
  const progress = state.progress < 1 ? (state.progress + scaled) : 1;
  return state.merge({ progress })
}

export const reset = (state, action) => {
  const progress = 0;
  return state.merge({ progress })
}


export const reducer = createReducer(INITIAL_STATE, {
  // [Types.STARTUP]: increase,
  // [Types.STARTUP_INCREASE]: stayhere,
  // [MerchantTypes.MERCHANT_SUCCESS]: increase,
  [MeTypes.ME_SUCCESS]: increase,
  [StartupTypes.RESET]: reset
})
