import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'
import { createSelector } from 'reselect'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  attendanceCreateoneRequest: ['data'],
  attendanceCreateoneSuccess: ['payload'],
  attendanceCreateoneFailure: null,
  attendanceCreateoneInitial: null
})

export const AttendanceCreateoneTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */


const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const AttendanceCreateoneSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
}

/* ------------- Reducers ------------- */

export const initial = (state, { data }) => state.merge({
  data: null,
  fetching: null,
  payload: null,
  error: null
})
// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ATTENDANCE_CREATEONE_REQUEST]: request,
  [Types.ATTENDANCE_CREATEONE_SUCCESS]: success,
  [Types.ATTENDANCE_CREATEONE_FAILURE]: failure,
  [Types.ATTENDANCE_CREATEONE_INITIAL]: initial
})
