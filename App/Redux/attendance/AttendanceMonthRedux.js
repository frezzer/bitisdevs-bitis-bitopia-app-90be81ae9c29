import _ from 'lodash';
import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  AttendanceMonthRequest: ['data'],
  AttendanceMonthSuccess: ['payload'],
  AttendanceMonthFailure: null,
  AttendanceMonthInitial: null,
});

export const AttendanceMonthTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */


const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const AttendanceMonthSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });
export const clear = (state, { data }) => state.merge({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ATTENDANCE_MONTH_REQUEST]: request,
  [Types.ATTENDANCE_MONTH_SUCCESS]: success,
  [Types.ATTENDANCE_MONTH_FAILURE]: failure,
  [Types.ATTENDANCE_MONTH_INITIAL]: clear,
});
