import _ from 'lodash';
import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  AttendanceUserRequest: ['data'],
  AttendanceUserSuccess: ['payload'],
  AttendanceUserInitial: null,
  AttendanceUserFailure: null,
});

export const AttendanceUserTypes = Types;
export default Creators;


/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  currentPage: 0,
  pageSize: 20,
  data: null,
  fetching: null,
  payload: [],
  error: null,
});

/* ------------- Selectors ------------- */


const getFetching = (state) => state.fetching;
const getCurrentPage = (state) => state.currentPage;
const getPageSize = (state) => state.pageSize;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const AttendanceUserSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
  // getCurrentPage: (state) => state.shift.find.currentPage,
  // getPageSize: (state) => state.shift.find.pageSize,
};

/* ------------- Reducers ------------- */

export const initial = (state, { data }) => state.merge({ ...INITIAL_STATE });
// request the data from an api
export const request = (state, { data }) => {
  const { currentPage } = state;
  return state.merge({ fetching: true, data });
};

// successful api lookup
export const success = (state, action) => {
  const payloadNew = action.payload;
  // console.tron.log("TCL: success -> payloadNew", payloadNew)
  const { currentPage, payload } = state;
  const payloadEnd = _.concat(payload, payloadNew);
  // console.tron.log("TCL: success -> payloadEnd", payloadEnd)
  return state.merge({
    fetching: false, error: null, payload: payloadNew, currentPage: currentPage + 1,
  });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: [] });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ATTENDANCE_USER_REQUEST]: request,
  [Types.ATTENDANCE_USER_SUCCESS]: success,
  [Types.ATTENDANCE_USER_FAILURE]: failure,
  [Types.ATTENDANCE_USER_INITIAL]: initial,
});
