import _ from 'lodash';
import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  cartShoppingCreatedRequest: ['created_order'],
  cartShoppingDelRequest: ['delete_order'],
  cartShoppingAddItemRequest: ['add_product'],
  cartShoppingDelItemRequest: ['del_product'],
  cartShoppingAddCusInfoRequest: ['customer_info'],
  cartShoppingFailure: null,
  cartShoppingInitial: null,
});

export const CartShoppingTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: {
    current_id: 0,
  },
  fetching: null,
  payload: [],
  error: null,
});
/* ------------- Selectors ------------- */


const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const CartShoppingSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
};

/* ------------- Reducers ------------- */

// request the data from an api
// ORDER
export const created_order = (state, action) => {
  const { payload } = state;
  const { created_order } = action;
  let temp_payload = Immutable.asMutable(payload);
  temp_payload.push(created_order);
  console.tron.log('created_order', created_order, temp_payload);
  return state.merge({
 data: { current_id: created_order.id }, fetching: false, error: null, payload: temp_payload 
});
};

export const delete_order = (state, action) => {
  console.tron.log('delete_order actions', action);
  const { payload } = state;
  const { delete_order } = action;
  // console.tron.log('delete_order', delete_order, payload)
  let temp_payload = Immutable.asMutable(payload);
  _.remove(temp_payload, (item) => {
    return item.id == delete_order.id
  });
  return state.merge({ fetching: false, error: null, payload: temp_payload });
};

// LIST PRODUCT
// Use case : find product in list -> if had increase a quantity -> if not had push new product to list
export const add_product = (state, action) => {
  const { payload } = state;
  const { add_product } = action;
  let new_payload = _.map(payload, (item) => {
    let temp_list_product = Immutable.asMutable(item.list_product)
    if (item.id == add_product.cart_id) {
      const check_product_exist = _.find(temp_list_product, function (e) {
        return e.uuid == add_product.product_detail.uuid
      })
      if (!check_product_exist) {
        let prepare_new_product = {
          ...add_product.product_detail,
          quantity: 1,
          promotion_uuid: null
        }
        temp_list_product.push(prepare_new_product)
      } else {
        let prepare_new_product = {
          ...check_product_exist,
          quantity: check_product_exist.quantity + 1,
        }
        var new_arr = _.unionBy([prepare_new_product], temp_list_product, 'uuid');
        temp_list_product = new_arr
      }
    }
    return { ...item, list_product: temp_list_product }
  });
  // console.tron.log('new_payload', new_payload)
  return state.merge({ fetching: false, error: null, payload: new_payload });
};

// Use case : find product in list -> if had remove item
export const del_product = (state, action) => {
  const { payload } = state;
  const { del_product } = action;

  let new_payload = _.map(payload, (item) => {
    let temp_list_product = Immutable.asMutable(item.list_product)
    if (item.id == del_product.cart_id) {
      _.remove(temp_list_product, function (n) {
        return n.uuid == del_product.product_detail.uuid;
      });
    }
    return { ...item, list_product: temp_list_product }
  });
  return state.merge({ fetching: false, error: null, payload: new_payload });
};

// Use case : find order in list -> add customer info
export const add_customer_info = (state, action) => {
  const { payload } = state;
  const { customer_info } = action;

  let new_payload = _.map(payload, (item) => {
    let temp_order = Immutable.asMutable(item)
    if (item.id == customer_info.cart_id) {
      temp_order = {
        ...item,
        customer_name: customer_info.customer_name ? customer_info.customer_name : '',
        phone_number: customer_info.phone_number ? customer_info.phone_number : ''
      }
    }
    return temp_order
  });
  return state.merge({ fetching: false, error: null, payload: new_payload });
};

// clear all data
export const clear = (state, { data }) => state.merge({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CART_SHOPPING_CREATED_REQUEST]: created_order,
  [Types.CART_SHOPPING_DEL_REQUEST]: delete_order,
  [Types.CART_SHOPPING_ADD_ITEM_REQUEST]: add_product,
  [Types.CART_SHOPPING_DEL_ITEM_REQUEST]: del_product,
  [Types.CART_SHOPPING_ADD_CUS_INFO_REQUEST]: add_customer_info,
  [Types.CART_SHOPPING_FAILURE]: failure,
  [Types.CART_SHOPPING_INITIAL]: clear,
});
