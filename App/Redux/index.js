import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import NetInfo from '@react-native-community/netinfo';
import { AsyncStorage } from 'react-native';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';
import ReduxPersist from '../Config/ReduxPersist';

// export const RootAlertRedux = require('./RootAlertRedux');
// export const LoadingSagaRedux = require('./LoadingSagaRedux');
// export const MeRedux = require('./MeRedux');
export const LoginRedux = require('./LoginRedux');
export const StartupRedux = require('./StartupRedux');
export const ShiftRedux = require('./shift/ShiftCreateOneRedux');


/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  saga: combineReducers({
    loading: require('./LoadingSagaRedux').reducer,
    alert: require('./RootAlertRedux').reducer,
    notify: require('./NotifyRedux').reducer,
  }),
  login: require('./LoginRedux').reducer,
  startup: require('./StartupRedux').reducer,
  navigation: require('./NavigationRedux').reducer,
  constants: require('./ConstantRedux').reducer,

  store: combineReducers({
    stock: require('./store/StoreGetStockRedux').reducer,
  }),
  // me
  me: require('./MeRedux').reducer,
  user: combineReducers({
    userDetail: require('./user/UserDetailRedux').reducer,
    userFind: require('./user/FindUserRedux').reducer,
  }),
  // employee
  employee: combineReducers({
    me: require('./employee/EmployeeMeRedux').reducer,
    revenue: require('./employee/EmployeeRevenueRedux').reducer,
  }),
  // shift
  shift: combineReducers({
    find: require('./shift/ShiftFindRedux').reducer,
    createOne: require('./shift/ShiftCreateOneRedux').reducer,
    findStore: require('./shift/ShiftFindStoreRedux').reducer,
    findStorebyUser: require('./shift/FindStoreRedux').reducer,
    findStorebySeller: require('./shift/FindStoreSellerRedux').reducer,
    findOnlyOne: require('./shift/ShiftFindOnlyOneRedux').reducer,
    finishOne: require('./shift/ShiftFinishOneRedux').reducer,
    getTypes: require('./shift/ShiftGetTypesRedux').reducer,
  }),
  attendance: combineReducers({
    find: require('./attendance/AttendanceFindRedux').reducer,
    createOne: require('./attendance/AttendanceCreateOneRedux').reducer,
    finishOne: require('./attendance/AttendanceFinishOneRedux').reducer,
    attendanceMonth: require('./attendance/AttendanceMonthRedux').reducer,
    attendanceSubmit: require('./attendance/AttendanceSubmitRedux').reducer,
    attendanceUser: require('./attendance/AttendanceUserRedux').reducer,
    attendanceMonthSeller: require('./attendance/AttendanceMonthSellerRedux').reducer,

  }),
  // employee
  cart: combineReducers({
    cart_shopping: require('./cart/CartShoppingRedux').reducer,
  }),
  // product
  product_detail: require('./product/ProductGetDetailRedux').reducer,
  // bill
  bill: combineReducers({
    temp_bill: require('./bill/CreateTempBillRedux').reducer,
    bill_his: require('./bill/BillGetHistoryRedux').reducer,
  }),
  // bill craft
  billCraftHistory: require('../Module/CartModule/Redux/BillCraftGetListRedux').reducer,
  billTempClient: require('../Module/CartModule/Redux/BillTempClientRedux').reducer,
  createBillCraft: require('../Module/CartModule/Redux/CreateBillCraftRedux').reducer,
});

const rootReducer = (state, action) => {
  // console.tron.log('state, action',state, action)
  if (action.type === 'LOGOUT') {
    // for all keys defined in your persistConfig(s)
    AsyncStorage.removeItem('Authorization');
    AsyncStorage.removeItem('storeUuid');
    // state = undefined;
  }
  return reducers(state, action);
};

export default () => {
  let finalReducers = rootReducer;
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig;
    finalReducers = persistReducer(persistConfig, reducers);
    console.tron.log('reduxPersist', finalReducers);
  }
  const { store, _sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga);
  let sagasManager = _sagasManager;

  if (module.hot) {
    module.hot.accept(() => {
      // const nextRootReducer = require('./').reducers;
      // store.replaceReducer(nextRootReducer);

      const newYieldedSagas = require('../Sagas').default;
      sagasManager.cancel();
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas);
      });
    });
  }

  return store;
};
