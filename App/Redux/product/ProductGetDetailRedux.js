import _ from 'lodash';
import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  productGetDetailRequest: ['data'],
  productGetDetailSuccess: ['payload'],
  productGetDetailInitial: null,
  productGetDetailFailure: null,
});

export const ProductGetDetailTypes = Types;
export default Creators;


/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */
const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const ProductGetDetailSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
};

/* ------------- Reducers ------------- */

export const initial = (state, { data }) => state.merge({ ...INITIAL_STATE });
// request the data from an api
export const request = (state, { data }) => {
  return state.merge({ fetching: true, data });
};

// successful api lookup
export const success = (state, action) => {
  return state.merge({ fetching: false, error: null, payload: action.payload });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: [] });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PRODUCT_GET_DETAIL_REQUEST]: request,
  [Types.PRODUCT_GET_DETAIL_SUCCESS]: success,
  [Types.PRODUCT_GET_DETAIL_FAILURE]: failure,
  [Types.PRODUCT_GET_DETAIL_INITIAL]: initial,
});
