import _ from 'lodash';
import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  shiftGettypesRequest: ['data'],
  shiftGettypesSuccess: ['payload'],
  shiftGettypesFailure: null
})

export const ShiftGettypesTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */


const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const ShiftGettypesSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SHIFT_GETTYPES_REQUEST]: request,
  [Types.SHIFT_GETTYPES_SUCCESS]: success,
  [Types.SHIFT_GETTYPES_FAILURE]: failure
})
