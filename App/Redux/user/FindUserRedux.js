import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import _ from 'lodash';
import { createSelector } from 'reselect';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  UserFindRequest: ['data'],
  UserFindSuccess: ['payload'],
  UserFindFailure: ['error'],
  UserFindClear: null,

});

export const UserFindTypes = Types;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

const getFetching = (state) => state.fetching;
const getPayload = (state) => state.payload;
const getData = (state) => state.data;
const getError = (state) => state.error;

export const UserFindSelectors = {
  selectData: createSelector([getData], (data) => data),
  selectFetching: createSelector([getFetching], (fetching) => fetching),
  selectPayload: createSelector([getPayload], (payload) => payload),
  selectError: createSelector([getError], (error) => error),
  getAuthorization: (state) => (state.me.data ? state.me.data.Authorization : ''),
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  return state.merge({ fetching: false, error: null, payload });
};

// Something went wrong somewhere.
export const failure = (state) => state.merge({ fetching: false, error: true, payload: null });
export const clear = (state) => state.merge({ ...INITIAL_STATE });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_FIND_REQUEST]: request,
  [Types.USER_FIND_SUCCESS]: success,
  [Types.USER_FIND_FAILURE]: failure,
  [Types.USER_FIND_CLEAR]: clear,
});

export default Creators;
