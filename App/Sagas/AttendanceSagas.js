/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import { MeSelectors } from '../Redux/MeRedux';
import AttendanceCreateoneActions from '../Redux/attendance/AttendanceCreateOneRedux';
import AttendanceFinishoneActions from '../Redux/attendance/AttendanceFinishOneRedux';
import AttendanceFindActions, { attendanceFindSelectors } from '../Redux/attendance/AttendanceFindRedux';
import AttendanceUserActions, { AttendanceUserSelectors } from '../Redux/attendance/AttendanceUserRedux';
import AttendanceMonthActions from '../Redux/attendance/AttendanceMonthRedux';
import AttendanceMonthSellerActions from '../Redux/attendance/AttendanceMonthSellerRedux';
import AttendanceSubmitActions from '../Redux/attendance/AttendanceSubmitRedux';
import ShiftFindActions from '../Redux/shift/ShiftFindRedux';

import LoadingSagaActions from '../Redux/LoadingSagaRedux';
import RootAlertActions from '../Redux/RootAlertRedux';


export function* createOneAttendance(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  yield put(LoadingSagaActions.loadingIndicatorShow());
  const response = yield call(api.createOneAttendance, {
    Authorization,
    data,
  });


  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceCreateoneActions.attendanceCreateoneSuccess(response.data));
    const dataRequest = {
      'attendances.user_uuid': data.username,
      _populate: ['shifttype', 'attendances'],
      deleted: false,
      store_uuid: data.store_uuid,
    };
    yield put(ShiftFindActions.shiftFindRequest(dataRequest));
    yield put(LoadingSagaActions.loadingIndicatorHide());
  } else {
    yield put(AttendanceCreateoneActions.attendanceCreateoneFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
  }
}

export function* finishOneAttendance(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  yield put(LoadingSagaActions.loadingIndicatorShow());
  const response = yield call(api.finishOneAttendance, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceFinishoneActions.attendanceFinishoneSuccess(response.data));
    const dataRequest = {
      'attendances.user_uuid': data.username,
      _populate: ['shifttype', 'attendances'],
      deleted: false,
      store_uuid: data.store_uuid,

    };
    yield put(ShiftFindActions.shiftFindRequest(dataRequest));
    yield put(LoadingSagaActions.loadingIndicatorHide());
  } else {
    yield put(AttendanceFinishoneActions.attendanceFinishoneFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
  }
}

export function* findAttendance(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // get page info
  const currentPage = yield select(attendanceFindSelectors.getCurrentPage);
  const pageSize = yield select(attendanceFindSelectors.getPageSize);
  // make the call to the api
  const response = yield call(api.findAttendance, {
    Authorization,
    data: {
      ...data,
      _start: 0,
      _limit: 50,
    },
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceFindActions.attendanceFindSuccess(response.data));
  } else {
    yield put(AttendanceFindActions.attendanceFindFailure());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}
export function* userAttendance(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // get page info
  // const currentPage = yield select(attendanceFindSelectors.getCurrentPage);
  // const pageSize = yield select(attendanceFindSelectors.getPageSize);
  // make the call to the api
  const response = yield call(api.userAttendance, {
    Authorization,
    data: {
      ...data,
      _start: 0,
      _limit: 50,
    },
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceUserActions.AttendanceUserSuccess(response.data));
  } else {
    yield put(AttendanceUserActions.AttendanceUserFailure());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}


export function* attendanceMonth(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.attendanceMonth, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceMonthActions.AttendanceMonthSuccess(response.data));
  } else {
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
    yield put(AttendanceMonthActions.AttendanceMonthFailure());
  }
}
export function* attendanceMonthSeller(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.attendanceMonthSeller, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceMonthSellerActions.AttendanceMonthSellerSuccess(response.data));
  } else {
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
    yield put(AttendanceMonthSellerActions.AttendanceMonthSellerFailure());
  }
}
export function* attendanceSubmit(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.attendanceSubmit, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(AttendanceSubmitActions.attendanceSubmitSuccess(response.data));
  } else {
    yield put(AttendanceSubmitActions.attendanceSubmitFailure());
  }
}
