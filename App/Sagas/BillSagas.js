import { AsyncStorage } from 'react-native';
import { call, put, select } from 'redux-saga/effects';
import _ from 'lodash';
import RootAlertActions from '../Redux/RootAlertRedux';
import { MeSelectors } from '../Redux/MeRedux';
import CreateBillCraftActions from '../Module/CartModule/Redux/CreateBillCraftRedux';
import BillTempClientActions from '../Module/CartModule/Redux/BillTempClientRedux';
import BillGetHistoryActions from '../Redux/bill/BillGetHistoryRedux';
import CartShoppingActions from '../Redux/cart/CartShoppingRedux';

export function* createBillCraft(api, action) {
  const { data } = action;
  console.tron.log('createBillCraft', data);
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.createBillCraft, {
    Authorization,
    data,
  });
  // success?
  if (response.ok) {
    yield put(CreateBillCraftActions.createBillCraftSuccess(response.data));
    yield put(BillTempClientActions.deleteBillTempClientRequest({ id: data.bill.id }));
  } else {
    yield put(CreateBillCraftActions.createBillCraftFailure(response.error));
  }
}

export function* getBillHistory(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.getBillCraftHistory, {
    Authorization,
    data,
  });
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(BillGetHistoryActions.billGetHistorySuccess(response.data));
  } else {
    yield put(BillGetHistoryActions.billGetHistoryFailure(response.error));
  }
}
