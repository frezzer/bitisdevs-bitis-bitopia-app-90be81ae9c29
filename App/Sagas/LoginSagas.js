/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */

import { call, put } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import LoginActions, { LoginTypes } from '../Redux/LoginRedux';
import MeActions, { MeTypes } from '../Redux/MeRedux';
import EmployeeMeActions, { EmployeeMeTypes } from '../Redux/employee/EmployeeMeRedux';
import RootAlertActions from '../Redux/RootAlertRedux';

export function* login(api, action) {
  const { data } = action;
  const response = yield call(api.userLogin, data);

  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.

    AsyncStorage.setItem('Authorization', response.data.jwt);
    yield put(LoginActions.loginSuccess(response.data));
    yield put(MeActions.meRequest({ Authorization: response.data.jwt }));
  } else {
    const customMessageError = {
      ...response.data,
      message: response.data.statusCode === 400
        ? 'Sai tên đăng nhập hoặc mật khẩu vui lòng thử lại'
        : response.data.message,
    };
    yield put(RootAlertActions.rootAlertShow(response.data ? customMessageError : response.problem));
    yield put(LoginActions.loginFailure());
  }
}


export function* logout(api, action) {
  yield put(LoginActions.initial());
  yield put(MeActions.meClear());
  yield put(EmployeeMeActions.employeeMeClear());
}
