/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */
import { AsyncStorage } from 'react-native';
import { call, put, select } from 'redux-saga/effects';
import MeActions, { MeSelectors } from '../Redux/MeRedux';
import UserDetailActions from '../Redux/user/UserDetailRedux';
import UserFindActions from '../Redux/user/FindUserRedux';
import RootAlertActions from '../Redux/RootAlertRedux';
import LoginActions, { LoginTypes } from '../Redux/LoginRedux';

//


export function* getMe(api, action) {
  const { data } = action;
  // get current data from Store
  // const currentData = yield select(MeSelectors.getData)
  // make the call to the api
  const response = yield call(api.userGetMe, data);

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    // console.tron.log("renew token :)))", response.data.data)
    console.tron.log('getMe response', response);
    AsyncStorage.setItem('Authorization', data.Authorization);
    yield put(MeActions.meSuccess(response.data));
  } else {
    // if (response.data && response.data.statusCode == 401 && response.data.error == 'Unauthorized')
    //   yield put(LoginActions.logout())
    // else {
    yield put(MeActions.meFailure(response.error));
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
    // }
  }
}

export function* getUserDetail(api, action) {
  // const { data } = action
  // get current data from Store
  // const currentData = yield select(MeSelectors.getData)
  // make the call to the api
  // const Authorization = yield select(MeSelectors.getAuthorization);
  // // make the call to the api
  // const response = yield call(api.userGetDetail, {
  //   Authorization: Authorization,
  //   data: data
  // })

  // // success?
  // if (response.ok) {
  //   // You might need to change the response here - do this with a 'transform',
  //   // located in ../Services/. Otherwise, just pass the data back from the api.
  //   // console.tron.log("renew token :)))", response.data.data)
  //   yield put(UserDetailActions.meSuccess(response.data))
  // } else {
  //   // if (response.data && response.data.statusCode == 401 && response.data.error == 'Unauthorized')
  //   //   yield put(LoginActions.logout())
  //   // else {
  //   yield put(UserDetailActions.meFailure(response.error))
  //   yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem))
  //   // }
  // }

  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.userGetDetail, {
    Authorization,
    data,
  });

  // console.tron.log("TCL: function*getUserDetail -> data", data)
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    // console.tron.log("renew token :)))", response.data.data)
    yield put(UserDetailActions.UserDetailSuccess(response.data));
  } else {
    yield put(UserDetailActions.UserDetailFailure(response.error));
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
  }
}

export function* findUser(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.userFind, {
    Authorization,
    data,
  });
  if (response.ok) {
    yield put(UserFindActions.UserFindSuccess(response.data));
  } else {
    yield put(UserFindActions.UserFindFailure(response.error));
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
  }
}
