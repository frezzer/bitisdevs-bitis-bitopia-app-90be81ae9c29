/* eslint-disable import/prefer-default-export */
/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import { MeSelectors } from '../Redux/MeRedux';
import ProductGetDetailActions from '../Redux/product/ProductGetDetailRedux';
import LoadingSagaActions from '../Redux/LoadingSagaRedux';
import RootAlertActions from '../Redux/RootAlertRedux';

export function* getProductDetail(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  yield put(LoadingSagaActions.loadingIndicatorShow());
  const response = yield call(api.getProductDetail, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ProductGetDetailActions.productGetDetailSuccess(response.data));
    yield put(LoadingSagaActions.loadingIndicatorHide());
  } else {
    yield put(ProductGetDetailActions.productGetDetailFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data ? response.data : response.problem));
  }
}
