/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
************************************************************ */

import { call, put, select } from 'redux-saga/effects';
import { MeSelectors } from '../Redux/MeRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../Redux/shift/ShiftFindRedux';
import ShiftCreateoneActions from '../Redux/shift/ShiftCreateOneRedux';
import ShiftFindStoreActions from '../Redux/shift/ShiftFindStoreRedux';
// import ShiftFindStorebyUserActions from '../Redux/shift/ShiftFindStorebyUserRedux';
import ShiftFinishOneActions from '../Redux/shift/ShiftFinishOneRedux';
import FindStoreActions from '../Redux/shift/FindStoreRedux';
import FindStoreSellerActions from '../Redux/shift/FindStoreSellerRedux';
import ShiftFindOnlyOneActions from '../Redux/shift/ShiftFindOnlyOneRedux';
import ShiftGetTypesActions from '../Redux/shift/ShiftGetTypesRedux';
import LoadingSagaActions from '../Redux/LoadingSagaRedux';
import RootAlertActions from '../Redux/RootAlertRedux';

// function* consoleLogSaga(data) {
//   console.tron.log('Hello Sagas!', data);
// }


export function* findStoreShift(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.findStoreShift, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftFindStoreActions.shiftFindstoreSuccess(response.data));
  } else {
    yield put(ShiftFindStoreActions.shiftFindstoreFailure());
  }
}
export function* findStore(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.findStoreSeller, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(FindStoreActions.FindstoreSuccess(response.data));
  } else {
    yield put(FindStoreActions.FindstoreFailure());
  }
}
export function* findStoreSeller(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.findStoreSeller, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    console.tron.log('FindstoreSellerSuccess response', response);
    yield put(FindStoreSellerActions.FindstoreSellerSuccess(response.data.data));
  } else {
    yield put(FindStoreSellerActions.FindstoreSellerFailure());
  }
}

export function* createOneShift(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.createOneShift, {
    Authorization,
    data,
  });
  yield put(LoadingSagaActions.loadingIndicatorShow());

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftCreateoneActions.shiftCreateoneSuccess(response.data));
    const dataRequest = {
      // 'attendances.user_uuid': data.username,
      // _populate: ['shifttype', 'attendances'],
      // deleted: false,
      user_uuid: data.username,
      store_uuid: data.store_Uuid,
      _populate: ['shifttype', 'attendances'],
    };
    console.tron.log("debug: function*createOneShift -> dataRequest", dataRequest)
    yield put(ShiftFindActions.shiftFindRequest(dataRequest));
    yield put(LoadingSagaActions.loadingIndicatorHide());
  } else {
    yield put(ShiftCreateoneActions.shiftCreateoneFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}

export function* findOnlyOneShift(api, action) {
  const { data } = action;
  // console.tron.log("TCL: function*findOnlyOneShift -> data", data)

  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.findOnlyOneShift, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftFindOnlyOneActions.shiftFindonlyoneSuccess(response.data));
  } else {
    yield put(ShiftFindOnlyOneActions.shiftFindonlyoneFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}

export function* finishOneShift(api, action) {
  const { data } = action;
  // show loading
  yield put(LoadingSagaActions.loadingIndicatorShow());

  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.finishOneShift, {
    Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftFinishOneActions.shiftFinishoneSuccess(response.data));
    const dataRequest = {
      // 'attendances.user_uuid': data.username,
      // _populate: ['shifttype', 'attendances'],
      // deleted: false,
      user_uuid: data.username,
      store_uuid: data.store_Uuid,
      _populate: ['shifttype', 'attendances'],
    };
    console.tron.log("debug: function*createOneShift -> dataRequest", dataRequest)
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(ShiftFindActions.shiftFindRequest());
  } else {
    yield put(ShiftFinishOneActions.shiftFinishoneFailure());
    yield put(LoadingSagaActions.loadingIndicatorHide());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}

export function* getTypesShift(api, action) {
  const { data } = action;
  // get current data from Store
  // const Authorization = yield select(MeSelectors.getAuthorization);
  // make the call to the api
  const response = yield call(api.getTypesShift, {
    // Authorization: Authorization,
    data,
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftGetTypesActions.shiftGettypesSuccess(response.data));
  } else {
    yield put(ShiftGetTypesActions.shiftGettypesFailure());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}


export function* findShift(api, action) {
  const { data } = action;
  // get current data from Store
  const Authorization = yield select(MeSelectors.getAuthorization);
  // get page info
  const currentPage = yield select(ShiftFindSelectors.getCurrentPage);
  const pageSize = yield select(ShiftFindSelectors.getPageSize);
  // make the call to the api
  const response = yield call(api.findShift, {
    Authorization,
    data: {
      ...data,
      // _start: 0,
      // _limit: 50,
    },
  });

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Services/. Otherwise, just pass the data back from the api.
    yield put(ShiftFindActions.shiftFindSuccess(response.data));
  } else {
    yield put(ShiftFindActions.shiftFindFailure());
    yield put(RootAlertActions.rootAlertShow(response.data || response.problem));
  }
}
