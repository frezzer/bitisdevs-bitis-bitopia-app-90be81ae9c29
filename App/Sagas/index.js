import { takeLatest, all } from 'redux-saga/effects';
import API from '../Services/Api';

/* ------------- Types ------------- */
import { StartupTypes } from '../Redux/StartupRedux';
import { LoginTypes } from '../Redux/LoginRedux';
// user
import { MeTypes } from '../Redux/MeRedux';
import { UserDetailTypes } from '../Redux/user/UserDetailRedux';
import { UserFindTypes } from '../Redux/user/FindUserRedux';
// import { ShiftTypes } from '../Redux/shift/ShiftCreateOneRedux';

// shift
import { ShiftCreateoneTypes } from '../Redux/shift/ShiftCreateOneRedux';
import { ShiftFindstoreTypes } from '../Redux/shift/ShiftFindStoreRedux';
import { FindstoreTypes } from '../Redux/shift/FindStoreRedux';
import { FindstoreSellerTypes } from '../Redux/shift/FindStoreSellerRedux';


// import { ShiftFindStorebyUserTypes } from '../Redux/shift/ShiftFindStorebyUserRedux';
import { ShiftFindonlyoneTypes } from '../Redux/shift/ShiftFindOnlyOneRedux';
import { ShiftFinishoneTypes } from '../Redux/shift/ShiftFinishOneRedux';
import { ShiftFindTypes } from '../Redux/shift/ShiftFindRedux';
import { ShiftGettypesTypes } from '../Redux/shift/ShiftGetTypesRedux';

// attendance
import { AttendanceCreateoneTypes } from '../Redux/attendance/AttendanceCreateOneRedux';
import { AttendanceFinishoneTypes } from '../Redux/attendance/AttendanceFinishOneRedux';
import { AttendanceFindTypes } from '../Redux/attendance/AttendanceFindRedux';
import { AttendanceMonthTypes } from '../Redux/attendance/AttendanceMonthRedux';
import { AttendanceMonthSellerTypes } from '../Redux/attendance/AttendanceMonthSellerRedux';
import { attendanceSubmitTypes } from '../Redux/attendance/AttendanceSubmitRedux';
import { AttendanceUserTypes } from '../Redux/attendance/AttendanceUserRedux';


// employee
import { EmployeeMeTypes } from '../Redux/employee/EmployeeMeRedux';
import { EmployeeRevenueTypes } from '../Redux/employee/EmployeeRevenueRedux';

// store
import { StoreGetStockTypes } from '../Redux/store/StoreGetStockRedux';

// product
import { ProductGetDetailTypes } from '../Redux/product/ProductGetDetailRedux';

// bill
// import { CreateTempBillTypes } from '../Redux/bill/CreateTempBillRedux';
import { BillGetHistoryTypes } from '../Redux/bill/BillGetHistoryRedux';
// bill craft
import { BillCraftGetHistoryTypes } from '../Module/CartModule/Redux/BillCraftGetListRedux';
import { CreateBillCraftTypes } from '../Module/CartModule/Redux/CreateBillCraftRedux';

/* ------------- Sagas ------------- */
import { startup } from './StartupSagas';
import { login, logout } from './LoginSagas';

// user
import { getMe, getUserDetail, findUser } from './MeSagas';

// employee
import { getMeEmployee } from './EmployeeSagas';
import { getRevenueEmployee } from './EmployeeRevenueSagas';

// shift
import {
  createOneShift,
  findStoreShift,
  findOnlyOneShift,
  finishOneShift,
  getTypesShift,
  findShift,
  findStore,
  findStoreSeller,
} from './ShiftSagas';
import {
  createOneAttendance, finishOneAttendance, findAttendance, attendanceMonth, attendanceSubmit, userAttendance, attendanceMonthSeller,
} from './AttendanceSagas';

// store
import { getstockStore } from './StoreSaga';

// product
import { getProductDetail } from './ProductSagas';

// bill
import { createBillCraft, getBillHistory } from './BillSagas';
// bill craft
import { getBillCraftHistory } from '../Module/CartModule/Saga/BillCraftGetListSaga';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.createAPI();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(LoginTypes.LOGIN, login, api),
    takeLatest(LoginTypes.LOGOUT, logout, api),

    // user
    takeLatest(MeTypes.ME_REQUEST, getMe, api),
    takeLatest(UserDetailTypes.USER_DETAIL_REQUEST, getUserDetail, api),
    takeLatest(UserFindTypes.USER_FIND_REQUEST, findUser, api),


    // employee
    takeLatest(EmployeeMeTypes.EMPLOYEE_ME_REQUEST, getMeEmployee, api),
    takeLatest(EmployeeRevenueTypes.EMPLOYEE_REVENUE_REQUEST, getRevenueEmployee, api),

    // shift
    takeLatest(ShiftCreateoneTypes.SHIFT_CREATEONE_REQUEST, createOneShift, api),
    takeLatest(ShiftFindstoreTypes.SHIFT_FINDSTORE_REQUEST, findStoreShift, api),
    takeLatest(FindstoreTypes.FINDSTORE_REQUEST, findStore, api),
    takeLatest(ShiftFindonlyoneTypes.SHIFT_FINDONLYONE_REQUEST, findOnlyOneShift, api),
    takeLatest(ShiftFinishoneTypes.SHIFT_FINISHONE_REQUEST, finishOneShift, api),
    takeLatest(ShiftGettypesTypes.SHIFT_GETTYPES_REQUEST, getTypesShift, api),
    takeLatest(ShiftFindTypes.SHIFT_FIND_REQUEST, findShift, api),
    takeLatest(FindstoreSellerTypes.FINDSTORE_SELLER_REQUEST, findStoreSeller, api),

    // attendance
    takeLatest(AttendanceCreateoneTypes.ATTENDANCE_CREATEONE_REQUEST, createOneAttendance, api),
    takeLatest(AttendanceFinishoneTypes.ATTENDANCE_FINISHONE_REQUEST, finishOneAttendance, api),
    takeLatest(AttendanceFindTypes.ATTENDANCE_FIND_REQUEST, findAttendance, api),
    takeLatest(AttendanceMonthTypes.ATTENDANCE_MONTH_REQUEST, attendanceMonth, api),
    takeLatest(AttendanceMonthSellerTypes.ATTENDANCE_MONTH_SELLER_REQUEST, attendanceMonthSeller, api),
    takeLatest(attendanceSubmitTypes.ATTENDANCE_SUBMIT_REQUEST, attendanceSubmit, api),
    takeLatest(AttendanceUserTypes.ATTENDANCE_USER_REQUEST, userAttendance, api),

    // store
    takeLatest(StoreGetStockTypes.STORE_GETSTOCK_REQUEST, getstockStore, api),

    // product
    takeLatest(ProductGetDetailTypes.PRODUCT_GET_DETAIL_REQUEST, getProductDetail, api),

    // bill
    takeLatest(BillGetHistoryTypes.BILL_GET_HISTORY_REQUEST, getBillHistory, api),

    // bill craft
    takeLatest(BillCraftGetHistoryTypes.BILL_CRAFT_GET_HISTORY_REQUEST, getBillCraftHistory, api),
    takeLatest(CreateBillCraftTypes.CREATE_BILL_CRAFT_REQUEST, createBillCraft, api),
  ]);
}
