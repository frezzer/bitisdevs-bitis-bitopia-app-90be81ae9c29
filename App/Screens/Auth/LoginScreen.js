
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import ReactNativeBiometrics from 'react-native-biometrics';
// Components
import {
  InteractionManager,
  Text, View, TextInput, TouchableWithoutFeedback,
  TouchableOpacity, KeyboardAvoidingView, Keyboard,
  SafeAreaView, Platform, AsyncStorage, Linking,
} from 'react-native';
import LottieView from 'lottie-react-native';
import Modal from 'react-native-modal';
import { IconOutline } from '@ant-design/icons-react-native';
import ButtonHandle from '../../Components/Button/ButtonHandle';

// Styles
import styles from './Styles/LoginScreen.styles';
import { Feather } from '../../Components/Icons';
// Actions and Selectors Redux
import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';
import NavigationActions, { NavigationSelectors } from '../../Redux/NavigationRedux';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux';
import { Metrics, Colors } from '../../Themes';


// utils
import Logo from '../../Fixtures/Logo.json';
import { compareOmitState } from '../../Services/utils';


/**
 * Options
 *  - navigation options for Screens
 * */

/**
 * Main
 */
export default function LoginScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [showPassword, setShowPassword] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);


  // selector store
  const navRootName = useSelector((state) => NavigationSelectors.selectData(state.navigation));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const loginData = useSelector((state) => LoginSelectors.selectData(state.login));
  const meError = useSelector((state) => MeSelectors.selectError(state.me));
  const isBlock = !!navRootName && navRootName !== 'auth';

  // useStates
  const [timeOutId, setTimeOutId] = React.useState(null);
  // const [identifier, setIdentifier] = React.useState(_.get(loginData, 'identifier') || '');
  const [identifier, setIdentifier] = React.useState('');
  const [password, setPassword] = React.useState('');
  const passwordRef = React.useRef(0);
  // const isBlock = false;

  // effects
  React.useEffect(() => {
    InteractionManager.runAfterInteractions(() => setInitialRender(false));
  }, []);

  React.useEffect(() => {
    if (!isBlock && mePayload) {
      props.navigation.navigate('AppRouter');
    }
    dispatch(loadingSagaActions.loadingIndicatorHide());
    return () => {
      if (timeOutId) clearTimeout(timeOutId);
    };
  }, [mePayload, isBlock]);

  React.useEffect(() => {
    dispatch(loadingSagaActions.loadingIndicatorHide());
    return () => {
      if (timeOutId) clearTimeout(timeOutId);
    };
  }, [meError, isBlock]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onSubmitLoginUserNamePassword: (params) => {
      const { _identifier = '', _password = '' } = params;
      dispatch(LoginActions.login({ identifier, password }));
      // dispatch(LoginActions.login({ identifier: '190426960', password: '123456' }));

      dispatch(loadingSagaActions.loadingIndicatorShow());
      if (timeOutId) clearTimeout(timeOutId);
      setTimeOutId(
        setTimeout(() => {
          dispatch(loadingSagaActions.loadingIndicatorHide());
        }, 3000),
      );
    },
    onSubmitLoginBiometrics: (params) => {
      ReactNativeBiometrics.simplePrompt({ promptMessage: 'Confirm fingerprint' }).then(({ success }) => {
        if (success) {
          AsyncStorage.getItem('AuthorizationBiometric').then((jwt) => {
            if (jwt) {
              dispatch(MeActions.meRequest({ Authorization: jwt }));
            } else {
              alert('You need setting first');
            }
          });
        }
      });
    },
    togglePassword: () => {
      setShowPassword(!showPassword);
    },
    toggleModal: () => {
      setIsModalVisible(true);
    },
    hideModal: () => {
      setIsModalVisible(false);
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    passwordRef,
    identifier,
    setIdentifier,
    password,
    setPassword,
    Actions,
    showPassword,
    isModalVisible,
  };
  if (isBlock) return <View />;
  return <Render {...renderProps} />;
}


/**
 * Render
 *  - areEqual declare function should be update screen or not
 */
const Render = React.memo((props) => {
  // declare propTypes
  const {
    initialRender,
    passwordRef,
    identifier,
    password,
    setIdentifier,
    setPassword,
    Actions,
    showPassword,
    isModalVisible,
  } = props;

  console.tron.log('LoginScreen render');
  if (initialRender) return <View />;
  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.container} testID="welcome">
            <View style={{ alignItems: 'center' }}>
              <View style={styles.logo}>
                <LottieView source={Logo} autoPlay loop />
              </View>
            </View>
            <View>
              <Text style={styles.note}>Tên đăng nhập</Text>
            </View>
            <View style={styles.BlankRectangle}>
              {/* <Feather style={styles.icon} name="mail" /> */}
              <TextInput
                // ref={identifierRef}
                style={styles.textInput}
                testID="input_user"
                label="Tên đăng nhập"
                autoCapitalize="none"
                returnKeyType="next"
                blurOnSubmit={false}
                autoCorrect={false}
                autoCompleteType="off"
                onSubmitEditing={() => passwordRef.current.focus()}
                placeholder="abc@bitis.com.vn"
                onChangeText={(text) => setIdentifier(text)}
                value={identifier}
                placeholderTextColor="#888888"
              />
            </View>
            <View>
              <Text style={styles.note}>Mật khẩu</Text>
            </View>
            <View style={styles.BlankRectangle}>
              {/* <Feather style={styles.icon} name="lock" /> */}
              <TextInput
                ref={passwordRef}
                testID="input_password"
                style={styles.textInput}
                label="Password"
                returnKeyType="done"
                secureTextEntry={showPassword}
                autoCorrect={false}
                onSubmitEditing={() => Actions.onSubmitLoginUserNamePassword({
                  identifier,
                  password,
                })}
                onChangeText={(text) => setPassword(text)}
                value={password}
                placeholder="Nhập mật khẩu"
                placeholderTextColor="#888888"
              />
              <TouchableOpacity style={styles.blockCheck} onPress={Actions.togglePassword}>
                <Feather name="eye" style={styles.icon} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={Actions.toggleModal}>
              <Text style={styles.note1}>Quên mật khẩu ?</Text>
            </TouchableOpacity>
            <View style={styles.LoginButton}>
              <ButtonHandle
                testID="button_login"
                // onPressButton={Actions.onSubmitLoginUserNamePassword.bind(this, {
                //   identifier,
                //   password,
                // })}
                onPressButton={() => Actions.onSubmitLoginUserNamePassword({
                  identifier,
                  password,
                })}
                buttonText="Đăng Nhập"
                styles={{ backgroundColor: Colors.blue2 }}
              />
            </View>

            <Modal isVisible={isModalVisible} onBackdropPress={Actions.hideModal}>
              <View style={styles.Modal}>
                <Text style={styles.modal_title}>Quên mật khẩu?</Text>
                <Text style={styles.content}>Vui lòng liên hệ phòng Công nghệ thông tin</Text>
                <Text style={styles.content1}>theo để được cấp lại mật khẩu.</Text>
                <Text style={styles.content2} onPress={() => { Linking.openURL('tel:0906 676 781'); }}>Hotline: 0906 676 781</Text>
              </View>
            </Modal>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}, areEqual);


function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, [
    'Actions',
  ]);
}
