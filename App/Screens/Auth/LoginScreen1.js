import React, { Component } from 'react'
import {
  Text, View, TextInput, Animated,
  TouchableOpacity, KeyboardAvoidingView, Keyboard
} from 'react-native'
import { connect } from 'react-redux'
import { SafeAreaView } from 'react-native'
import { Navigation } from '../../Navigation/Navigation';
import Svg from 'react-native-svg';
import SvgUri from 'react-native-svg-uri';
// Redux
import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux'
import MeActions, { MeSelectors } from '../../Redux/MeRedux'

// Component
import styles from './Styles/LoginScreen.styles'
import { Feather } from '../../Components/Icons';
import { Fonts } from '../../Themes'

import { compareOmitState, comparePickedState } from '../../Services/Utils'
import LottieView from 'lottie-react-native';
import Logo from '../../Fixtures/Logo.json';


//#region [LoginScreen]
class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.identifier = null;
    this.password = null;
    this.state = {
      user_info: {
        identifier: null,
        password: null
      }
    };
  }


  shouldComponentUpdate(nextProps, nexState) {
    return !comparePickedState(nextProps, this.props, ['fetch_me_payload', 'fetch_login_payload'])
      || !compareOmitState(nexState, this.state, ['user_info']);
  }

  render() {
    console.tron.log('LoginScreen');
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <SafeAreaView style={styles.container}>
          <View style={styles.container} styleName="h-center v-center">
            <View style={styles.logo}>
              <LottieView source={Logo} autoPlay loop />
            </View>
            <View style={styles.BlankRectangle}>
              <Feather style={styles.icon} name='mail' />
              <TextInput
                style={styles.textInput}
                ref='identifier'
                label="Tên đăng nhập"
                autoCapitalize={'none'}
                returnKeyType={"next"}
                blurOnSubmit={false}
                onSubmitEditing={() => { this.refs.password.focus(); }}
                autoCompleteType={'off'}
                onChangeText={(text) => this.setState({ user_info: { ...this.state.user_info, identifier: text } })}
                value={this.state.user_info.identifier}
                placeholder={'Tên đăng nhập'} />
            </View>
            <View style={styles.BlankRectangle}>
              <Feather style={styles.icon} name='lock' />
              <TextInput
                style={styles.textInput}
                ref='password'
                label="Password"
                returnKeyType={"done"}
                onSubmitEditing={this.onSubmitLoginUserNamePassword.bind(this)}
                secureTextEntry={true}
                onChangeText={(text) => this.setState({ user_info: { ...this.state.user_info, password: text } })}
                value={this.state.user_info.password}
                placeholder={'Password'} />
            </View>
            <TouchableOpacity
              style={styles.LoginButton}
              onPress={this.onSubmitLoginUserNamePassword.bind(this)} >
              <Text style={styles.ButtonText}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView >

    );
  }
}
const mapStateToProps = (state) => {
  return {
    fetch_me_payload: MeSelectors.selectPayload(state.me),
    fetch_login_payload: LoginSelectors.selectPayload(state.login)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (identifier, password) => {
      dispatch(LoginActions.login({ identifier, password }))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

//#endregion 

