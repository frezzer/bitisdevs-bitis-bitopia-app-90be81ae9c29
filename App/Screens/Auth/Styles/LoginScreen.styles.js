import { StyleSheet } from 'react-native';
import { widthPercentageToDP, widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../../Themes';


export default StyleSheet.create({
  container: {
    flex: 1,
    // ...ApplicationStyles.specialApp.alignCenter,
    justifyContent: 'center',
  },
  logo: {
    ...ApplicationStyles.specialApp.alignCenter,
    ...ApplicationStyles.specialApp.margin(-48, 48),
    ...ApplicationStyles.specialApp.dimensisons(175, 175),
    // alignItems:'center',
    // backgroundColor:'red'
  },
  icon: {
    color: Colors.greybold,
    fontSize: 24 / 375 * Metrics.width,
    marginHorizontal: Metrics.marginMedium,
    // justifyContent:'flex-start'
  },
  BlankRectangle: {
    flexDirection: 'row',
    ...ApplicationStyles.specialApp.dimensisons(343, 50),
    ...ApplicationStyles.specialApp.border(1, '#eeeeee', 6),
    ...ApplicationStyles.specialApp.itemCenter,
    backgroundColor: Colors.white,
    margin: Metrics.marginXTiny,
  },
  textInput: {
    padding: Metrics.paddingMedium,
    ...ApplicationStyles.specialApp.dimensisonsPercentage('100%', '100%'),
    textAlign: 'left',
    fontSize: Fonts.size.textLarge,
    color: Colors.black,
  },
  LoginButton: {
    alignSelf: 'center',
    margin: Metrics.marginMedium,
  },
  ButtonText: {
    ...ApplicationStyles.specialApp.buttonText,
    fontSize: Fonts.size.textXLarge,
  },
  note: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
    // textAlign:'left'
    marginLeft: Metrics.marginMedium,
    marginBottom: Metrics.marginTiny,
    marginTop: Metrics.marginMedium,
  },
  note1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blue2,
    // textAlign:'left'
    marginLeft: Metrics.marginMedium,
    marginBottom: Metrics.marginTiny,
    marginTop: Metrics.marginMedium,
  },
  blockCheck: {
    position: 'absolute',
    // justifyContent:'flex-end',
    marginLeft: 295 / 375 * Metrics.width,
  },
  Modal: {
    ...ApplicationStyles.specialApp.dimensisons(343, 150),
    backgroundColor: Colors.white,
    borderRadius: widthPercentageToDP(16 / 375 * 100),
    marginLeft: -wp(2 / 375 * 100),
  },
  modal_title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign: 'center',
    marginTop: Metrics.marginXXLarge,
  },
  content: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
    paddingHorizontal: widthPercentageToDP(24 / 375 * 100),
    textAlign: 'center',
    marginTop: Metrics.marginTiny,
  },
  content1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
    paddingHorizontal: widthPercentageToDP(24 / 375 * 100),
    textAlign: 'center',
    marginTop: Metrics.marginXTiny,
  },
  content2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blue2,
    paddingHorizontal: widthPercentageToDP(24 / 375 * 100),
    textAlign: 'center',
    marginTop: Metrics.marginXTiny,
  },
});
