
import _ from 'lodash';
import React from 'react';
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, FlatList,
  SafeAreaView, Platform, Image, TextInput, Alert,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Swipeout from 'react-native-swipeout';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';


// Components
import LoadingInteractions from '../../Components/LoadingInteractions';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
// Styles
import styles from './Styles/CartCustomerScreen.styles';

// Actions and Selectors Redux
import { MeSelectors } from '../../Redux/MeRedux';
import CartShoppingActions, { CartShoppingSelectors } from '../../Redux/cart/CartShoppingRedux';
// utils
import { compareOmitState, formatMoney } from '../../Services/utils';

const { width } = Dimensions.get('window');


const navigationOptions = (props) => {
  const onPressGoBack = () => props.navigation.goBack();
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Thông tin khách hàng',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS === 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoBack}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack}/>

        </TouchableOpacity>
      );
    },
  };
};

CartCustomerScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CartCustomerScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);

  const [customerName, setCustomerName] = React.useState('');
  const [phoneNumber, setPhoneNumber] = React.useState('');

  const cart_detail = props.navigation.getParam('cart_detail', null);
  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  });

  React.useEffect(() => {
    if (cart_detail.username && cart_detail.username != '') {
      setCustomerName(cart_detail.customer_name);
    }
    if (cart_detail.phone_number && cart_detail.phone_number != '') {
      setPhoneNumber(cart_detail.phone_number);
    }
  }, [cart_detail]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onChangeTextInput: (key, value) => {
      switch (key) {
        case 'CustomerName':
          setCustomerName(value);
          break;
        case 'PhoneNumber':
          setPhoneNumber(value);
          break;
        default:
          break;
      }
    },
    onPressConfirm: () => {
      if (cart_detail == null) return Alert.alert('THÔNG BÁO', 'Thông tin đơn hàng hiện ko đúng. Xin vui lòng thử lại !');
      console.tron.log('onPressConfirm', cart_detail);
      const data_request = {
        cart_id: cart_detail.id,
        customer_name: customerName,
        phone_number: phoneNumber,
      };
      dispatch(CartShoppingActions.cartShoppingAddCusInfoRequest(data_request));
      props.navigation.goBack();
    },
  };

  const state = {
    customerName,
    phoneNumber,
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const { initialRender, Actions, state } = props;
  const { customerName, phoneNumber } = state;

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.block_cus_name}>
          <Text style={styles.text_title}>Tên khách hàng</Text>
          <TextInput
            placeholder="Ex: Quang Khánh"
            style={styles.element_text_input}
            onChangeText={(text) => Actions.onChangeTextInput('CustomerName', text)}
            value={customerName}
          />
        </View>
        <View style={styles.block_phone_number}>
          <Text style={styles.text_title}>Số điện thoại</Text>
          <TextInput
            placeholder="Ex: 098123123"
            onChangeText={(text) => Actions.onChangeTextInput('PhoneNumber', text)}
            style={styles.element_text_input}
            value={phoneNumber}
          />
        </View>
        <View style={styles.bottom_block}>
          <ButtonHandle
            buttonText="Xác nhận"
            onPressButton={() => Actions.onPressConfirm()}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
