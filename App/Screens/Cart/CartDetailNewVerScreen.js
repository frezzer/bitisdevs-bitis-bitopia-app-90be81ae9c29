import _ from 'lodash';
import React from 'react';
import {
  TouchableOpacity, Platform, Image,
} from 'react-native';
// Components
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
// Container
import CartDetailContainerNewVer from '../../Module/CartModule/Container/CartDetailContainerNewVer';
// Styles

// Actions and Selectors Redux
const navigationOptions = (props) => ({
  header: null,
});

CartDetailNewVerScreen.navigationOptions = navigationOptions;
/**
 * Main
 */
export default function CartDetailNewVerScreen(props) {
  return <CartDetailContainerNewVer {...props} />;
}
