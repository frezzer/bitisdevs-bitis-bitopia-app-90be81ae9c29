import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Barcode from 'react-native-barcode-builder';
import {
  Dimensions, InteractionManager, View, Text, FlatList, AsyncStorage, TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
// Components
import LoadingInteractions from '../../Components/LoadingInteractions';
import ProductItem from '../../Components/product/ProductItem';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import screensName from '../../Navigation/Screens';

// Styles
import styles from './Styles/CartDetailScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import CartShoppingActions, { CartShoppingSelectors } from '../../Redux/cart/CartShoppingRedux';
import CreateTempBillActions, { CreateTempBillSelectors } from '../../Redux/bill/CreateTempBillRedux';

// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');

const navigationOptions = (props) => {
  const cart_id = props.navigation.getParam('cart_id', null);
  const onPressRightButton = () => props.navigation.navigate(screensName.ScanProductScreen, { cart_id });

  return {
    // headerMode: Platform.select({
    //   android: 'screen',
    //   ios: 'float'
    // }),
    tabBarVisible: false,
    title: 'Chi tiết đơn hàng',
    headerRight: () => {
      return (
        <RightButtonsTopBar
          name="camera"
          styleView={styles.headerRightView}
          onPress={onPressRightButton}
        />
      );
    },

  };
};

CartDetailScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CartDetailScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  // pass params
  const cart_id = props.navigation.getParam('cart_id', null);

  // useStates
  const [initialRender, setInitialRender] = React.useState(true);

  // selector store
  const list_bill_temp = useSelector((state) => {
    console.tron.log('redux saga', state);
    return CartShoppingSelectors.selectPayload(state.cart.cart_shopping);
  });

  // defined varible
  const cart_detail = _.find(list_bill_temp, (item) => { return item.id == cart_id; });
  // effects
  React.useEffect(() => {
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    // listen event from input
    // Actions.actionsfunction

    // clear when Screen off
    return () => { };
  }, [/** input */]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    // dispatch action add bill -> del temp bill -> go back
    onPressPayment: async (cart_detail) => {
      const store_uuid = await AsyncStorage.getItem('storeUuid');
      const order_data = _.map(cart_detail.list_product, (item) => {
        return {
          uuid: item.uuid,
          quantity: item.quantity,
          promotion_uuid: item.promotion_uuid ? item.promotion_uuid : null,
        };
      });
      const data_order = {
        temp_id: cart_detail.id,
        username: cart_detail.username,
        customer_phone_number: cart_detail.phone_number ? cart_detail.phone_number : '',
        customer_name: cart_detail.customer_name ? cart_detail.customer_name : '',
        store_uuid,
        orders: order_data,
      };
      // dispatch(CreateTempBillActions.createTempBillRequest(data_order));
      props.navigation.goBack();
    },

    onPressCustomerDetail: () => {
      props.navigation.navigate(screensName.CartCustomerScreen, { cart_detail });
    },

    onPressDeleteProduct: (data_del) => {
      dispatch(CartShoppingActions.cartShoppingDelItemRequest({ cart_id, product_detail: data_del }));
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    cart_detail,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  // declare variables
  const {
    initialRender, Actions, state, cart_detail,
  } = props;

  const renderItem = ({ item }) => {
    return (
      <View id={item.id}>
        <ProductItem
          data_item={item}
          onPressDelete={() => Actions.onPressDeleteProduct(item)}
        />
      </View>
    );
  };

  if (cart_detail) {
    return (
      <View style={styles.content}>
        <View style={styles.barcode_block}>
          <Barcode
            value={cart_detail.username}
            format="CODE128"
            width={wp(2.5 / 375 * 100)}
            height={styles.barcode.height}
          />
          <Text style={styles.barcode_text}>{cart_detail.username}</Text>
        </View>

        <TouchableOpacity
          onPress={Actions.onPressCustomerDetail}
        >
          <View style={styles.block_customer}>
            <Text style={styles.text_title_cus}>Thông tin khách hàng</Text>
            <View style={styles.element_customer}>
              <Text style={styles.text_cus_name}>Tên khách hàng</Text>
              <Text>{cart_detail.customer_name ? cart_detail.customer_name : 'Chưa cập nhật'}</Text>
            </View>
          </View>
        </TouchableOpacity>

        <FlatList
          keyExtractor={(item, index) => item.id}
          data={cart_detail.list_product}
          renderItem={renderItem}
          initialNumToRender={3}
          ListHeaderComponent={(
            <View style={styles.block_title}>
              <Text style={styles.title}>Sản phẩm</Text>
            </View>
          )}
          ListEmptyComponent={(
            <View style={styles.block_empty}>
              <Text>Bạn chưa có sản phẩm nào!</Text>
            </View>
          )}
        />

        <View style={styles.bottom_block}>
          <ButtonHandle
            buttonText="Tiến hành thanh toán"
            onPressButton={() => Actions.onPressPayment(cart_detail)}
          />
        </View>
      </View>
    );
  }

  return null;
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
