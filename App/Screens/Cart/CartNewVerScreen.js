import _ from 'lodash';
import React from 'react';
import {
  TouchableOpacity, Platform, Image,
} from 'react-native';
// Components
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
// Container
import CartContainerNewVer from '../../Module/CartModule/Container/CartContainerNewVer';

// Styles
import styles from './Styles/CartCustomerScreen.styles';


// Actions and Selectors Redux

const navigationOptions = (props) => {
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  return {
    title: 'Bán hàng nè',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS === 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack} />

        </TouchableOpacity>
      );
    },
  };
};

CartNewVerScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CartNewVerScreen(props) {
  return <CartContainerNewVer {...props} />;
}
