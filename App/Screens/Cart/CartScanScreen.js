import _ from 'lodash';
import React from 'react';
// Components
import { Images, Metrics } from '../../Themes';
// Container
import CartScanContainerNewVer from '../../Module/CartModule/Container/CartScanContainerNewVer';
// Styles

// Actions and Selectors Redux
const navigationOptions = (props) => ({
  header: null,
});

CartScanScreen.navigationOptions = navigationOptions;
/**
 * Main
 */
export default function CartScanScreen(props) {
  return <CartScanContainerNewVer {...props} />;
}
