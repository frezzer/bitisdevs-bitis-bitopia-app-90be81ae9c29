
import _ from 'lodash';
import React from 'react';
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, FlatList,
  SafeAreaView, Platform, Image, AsyncStorage, Alert,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Swipeout from 'react-native-swipeout';

// Components
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import CartItem from '../../Components/CartItem';
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
// Styles
import styles from './Styles/CartScreen.styles';

// Actions and Selectors Redux
import { MeSelectors } from '../../Redux/MeRedux';
import CartShoppingActions, { CartShoppingSelectors } from '../../Redux/cart/CartShoppingRedux';
// utils
import { compareOmitState, formatMoney } from '../../Services/utils';

const { width } = Dimensions.get('window');


const navigationOptions = (props) => {
  console.tron.log('top bar', props.navigation);
  // const onPressGoDashboard = () => props.navigation.navigate('AppRouter');
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  return {
    // headerMode: Platform.select({
    //   android: 'screen',
    //   ios: 'float'
    // }),
    title: 'Bán hàng',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS == 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          <IconOutline name="left" color="white" style={styles.iconBack} />
        </TouchableOpacity>
      );
    },
  };
};

CartScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CartScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [storeUuid, setStoreUuid] = React.useState('');

  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const list_bill_temp = useSelector((state) => CartShoppingSelectors.selectPayload(state.cart.cart_shopping));
  const { current_id } = useSelector((state) => CartShoppingSelectors.selectData(state.cart.cart_shopping));

  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen will mount');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }

    // listen event from input
    // Actions.actionsfunction

    // clear when Screen off
    return () => { };
  }, [/** input */]);

  React.useEffect(() => {
    /**
      Do not using async - await for useEffect beacause it make hell loop
      Link research: https://medium.com/javascript-in-plain-english/how-to-use-async-function-in-react-hook-useeffect-typescript-js-6204a788a435
      Using a IIFE for handle asynchronous instructions
    */
    (async function setStoreUuidFunction() {
      const temp = await AsyncStorage.getItem('storeUuid');
      setStoreUuid(temp);
    }());
  }, []);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressCartItem: (type, id) => {
      console.tron.log('onPressCartItem', type);
      const cart_detail_data = {};
      switch (type) {
        case 'create':
          const handle_add_data = {
            id: current_id + 1,
            username: me_payload ? me_payload.username : '',
            price: 0,
            list_product: [],
          };
          console.tron.log('onPressCartItem create', handle_add_data);
          dispatch(CartShoppingActions.cartShoppingCreatedRequest(handle_add_data));
          props.navigation.navigate(screensName.CartDetailScreen, { cart_id: handle_add_data.id });
          break;
        case 'get':
          props.navigation.navigate(screensName.CartDetailScreen, { cart_id: id });
          break;
        default:
          break;
      }
    },
    onPressDelItem: (cart_data) => {
      dispatch(CartShoppingActions.cartShoppingDelRequest(cart_data));
    },
  };

  const state = {
    storeUuid,
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    list_bill_temp,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  // declare variables
  const {
    initialRender, Actions, state, list_bill_temp,
  } = props;
  const { storeUuid } = state;


  const renderItem = ({ item }) => {
    return (
      <View id={item.id}>
        <CartItem
          store_uuid={storeUuid}
          data_item={item}
          onPressDetail={() => Actions.onPressCartItem('get', item.id)}
          onPressDelete={() => Actions.onPressDelItem(item)}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <TouchableOpacity style={styles.button_create} onPress={() => Actions.onPressCartItem('create')}>
          <Text style={styles.buton_title}>+ Tạo hóa đơn mới</Text>
        </TouchableOpacity>

        <FlatList
          keyExtractor={(item, index) => item.id}
          data={list_bill_temp}
          renderItem={renderItem}
          initialNumToRender={10}
          ListHeaderComponent={(
            <View style={styles.block_title}>
              <Text style={styles.title}>Chờ thanh toán </Text>
            </View>
          )}
          ListEmptyComponent={(
            <View style={styles.block_empty}>
              <Text>Bạn chưa có đơn hàng nào!</Text>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
