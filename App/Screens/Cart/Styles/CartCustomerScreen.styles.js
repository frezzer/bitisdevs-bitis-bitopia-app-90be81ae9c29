import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.theme.block,
    paddingTop: Metrics.paddingMedium,
  },
  block_cus_name: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    paddingVertical: Platform.OS == 'ios' ? Metrics.paddingMedium : 0,
    borderBottomColor: Colors.greenLight,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  element_text_input: {
    flex: 1,
    textAlign: 'right',
  },
  block_phone_number: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    paddingVertical: Platform.OS == 'ios' ? Metrics.paddingMedium : 0,
    alignItems: 'center',
  },
  bottom_block: {
    ...ApplicationStyles.specialApp.coverBottomTab,
  },
  // Text
  text_title: {
    color: Colors.grayLight,
    fontFamily: Fonts.type.fontBlack,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
