import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.block
  },
  barcode_block: {
    // paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingXXLarge,
    margin: Metrics.marginMedium,
    // marginBottom: Metrics.marginTiny,
    backgroundColor: Colors.white,
    borderRadius: Metrics.boderLarge,
    borderColor: Colors.silver,
    borderWidth: 2,
    alignContent: 'center',
    justifyContent: 'center',
    height: wp(150 / 375 * 100),
    width: wp(343 / 375 * 100),
  },
  barcode: {
    height: wp(93 / 375 * 100),
  },
  block_customer: {
    backgroundColor: Colors.white,
    marginVertical: Metrics.marginMedium
  },
  element_customer: {
    flexDirection: 'row',
    padding: Metrics.paddingMedium,
    borderTopColor: Colors.greenLight,
    borderTopWidth: 1,
    justifyContent: 'space-between'
  },
  bottom_block: {
    ...ApplicationStyles.specialApp.coverBottomTab
  },
  barcode_text: {
    color: Colors.black,
    fontSize: Fonts.size.textSmall,
    textAlign: 'center'
  },
  title: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginMedium,
  },
  block_title: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: Colors.white,
    justifyContent: 'center',
    borderBottomColor: Colors.block,
    borderBottomWidth: 1
  },
  block_empty: {
    height: wp(48/375*100),
    backgroundColor: Colors.white,
    alignItems: 'center', 
    padding: Metrics.paddingSmall 
  },
  // Text
  text_title_cus: {
    fontFamily: Fonts.type.fontBlack,
    padding: Metrics.paddingMedium
  },
  text_cus_name: {
    fontFamily: Fonts.type.fontBlack,
    color: Colors.grayLight
  }
});
