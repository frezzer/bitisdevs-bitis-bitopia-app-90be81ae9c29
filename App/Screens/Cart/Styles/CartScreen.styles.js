import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.block
  },
  content: {
    flex: 1,
    // backgroundColor: 'green'
  },
  block_empty: {
    height: wp(48 / 375 * 100),
    backgroundColor: Colors.white,
    alignItems: 'center',
    padding: Metrics.paddingSmall
  },
  button_create: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: Colors.white,
    marginTop: Metrics.marginMedium,
    ...ApplicationStyles.specialApp.alignCenter
  },
  buton_title: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blueOcean
  },
  title: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginMedium,
  },
  block_title: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: Colors.white,
    justifyContent: 'center',
    marginTop: Metrics.marginMedium,
    borderBottomColor: Colors.block,
    borderBottomWidth: 1
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },

});
