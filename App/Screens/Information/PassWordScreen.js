
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, TextInput,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import TextInfo from '../../Components/TextInput/TextInfo';
import ButtonHandle from '../../Components/Button/ButtonHandle';

// Styles
import styles from './Styles/PasswordScreen.styles';
import { Colors } from '../../Themes';

// Actions and Selectors Redux

// utils
import { compareOmitState } from '../../Services/utils';
import screenName from '../../Navigation/Screens';


const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Thay đổi mật khẩu',
  };
};

PassWordScreen.navigationOptions = navigationOptions;
/**
 * Main
 */
export default function PassWordScreen(props) {
  const { onPressButton } = props;
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [oldPass, setOldPass] = React.useState('');
  const [newPass, setNewPass] = React.useState('');
  const [changePass, setChangePass] = React.useState('');
  const btnOldPass = React.useRef(null);
  const btnNewPass = React.useRef(null);
  const btnChangePass = React.useRef(null);

  const onPressBtnOldPass = () => {
    btnOldPass.current.focus();
  };
  const onPressBtnNewPass = () => {
    btnNewPass.current.focus();
  };
  const onPressBtnChangePass = () => {
    btnChangePass.current.focus();
  };


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
     * Actions
     *  - for flows and onPress attribute button
     * */

  const Actions = {
    onPressBtnOldPass,
    onPressBtnNewPass,
    setOldPass,
    setNewPass,
    setChangePass,
    onPressBtnChangePass,
    onPressButton,

    onPressPopToTop: () => props.navigation.navigate(screenName.DashBoardScreen),

  };
  const state = {
    oldPass, newPass, btnOldPass, btnNewPass, changePass, btnChangePass,
  };


  /**
     * render
     *  - declare variables for render
     *  - render loading when initialRender is true
     */
  const renderProps = {
    initialRender,
    Actions,
    state,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const { initialRender, Actions, state } = props;
  const {
    btnOldPass, btnNewPass, oldPass, newPass, changePass, btnChangePass,
  } = state;
  const {
    onPressBtnOldPass, onPressBtnNewPass, setOldPass, setNewPass, setChangePass, onPressBtnChangePass, onPressButton,
  } = Actions;

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={styles.Block_Detail}
        onPress={onPressBtnOldPass}
      >
        <View style={styles.part1}>
          <Text style={styles.Text5}>Mật khẩu cũ</Text>
        </View>
        <View style={styles.part2}>
          <TextInput
                        // style={{backgroundColor:'red'}}
            ref={btnOldPass}
            style={styles.Text6}
            onChangeText={(oldPass) => setOldPass(oldPass)}
            value={oldPass}
            placeholderTextColor={Colors.black}
            editable
            keyboardType="default"
            maxLength={6}
            secureTextEntry
          />
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.Block_Detail1}
        onPress={onPressBtnNewPass}
      >
        <View style={styles.part1}>
          <Text style={styles.Text5}>Mật khẩu mới</Text>
        </View>
        <View style={styles.part2}>
          <TextInput
                        // style={{backgroundColor:'red'}}
            ref={btnNewPass}
            style={styles.Text6}
            onChangeText={(newPass) => setNewPass(newPass)}
            value={newPass}
            placeholderTextColor={Colors.black}
            editable
            keyboardType="default"
                        // placeholder={'chưa cập nhật'}
            maxLength={6}
            secureTextEntry
          />
        </View>
      </TouchableOpacity>
      <View style={styles.lineStyle} />
      <TouchableOpacity
        style={styles.Block_Detail2}
        onPress={onPressBtnChangePass}
      >
        <View style={styles.part1}>
          <Text style={styles.Text5}>Nhập lại mật khẩu</Text>
        </View>
        <View style={styles.part2}>
          <TextInput
                        // style={{backgroundColor:'red'}}
            ref={btnChangePass}
            style={styles.Text6}
            onChangeText={(changePass) => setChangePass(changePass)}
            value={changePass}
            placeholderTextColor={Colors.black}
            editable
            keyboardType="default"
                        // placeholder={'chưa cập nhật'}
            maxLength={6}
            secureTextEntry
          />
        </View>
      </TouchableOpacity>
      <View style={styles.bottomTab}>
        <ButtonHandle
          onPressButton={onPressButton || Actions.onPressPopToTop}
          buttonText="Cập Nhật"
          styles={{ backgroundColor: 'red' }}
        />
      </View>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
