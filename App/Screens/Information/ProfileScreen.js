'user strict';

import _ from 'lodash';
import React, { Component, useState, useRef } from 'react';
import {
  SafeAreaView, Text, Image, TextInput,
  AsyncStorage, View, InteractionManager,
  KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, TouchableOpacity,
} from 'react-native';
import {
  Svg, Circle, Polygon, Polyline, Path, Rect, G,
} from 'react-native-svg';

// Components
import moment from 'moment';
import { connect, useSelector, useDispatch } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import DoubleButton from '../../Components/Button/DoubleButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import Switch from '../../Lib/react-native-switch/Switch';
import TextInfo from '../../Components/TextInput/TextInfo';


// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux';

// import DateTimePicker from '../../Components/DateTimePicker';
import LoadingInteractions from '../../Components/LoadingInteractions';
import 'moment/locale/vi';
import { turnOnLoginBiometric, turnOffLoginBiometric } from '../../Services/Biometric';

// Redux
import EmployeeMeActions, { EmployeeMeSelectors } from '../../Redux/employee/EmployeeMeRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';
import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';

// Utils
import { compareOmitState } from '../../Services/utils';

// Styles
import { Colors, Metrics } from '../../Themes';
import styles from './Styles/ProfileScreen.styles';

// Navigation
import screenName from '../../Navigation/Screens';


const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Thông tin cá nhân',
  };
};

ProfileScreen.navigationOptions = navigationOptions;

export default function ProfileScreen(props) {
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [switchValue, setSwitchValue] = React.useState(false);

  const [dateOfBirth, setDateOfBirth] = useState(null);
  const [btnStatus, setBtnStatus] = useState(false);
  const [name, setName] = useState('');
  const [pwd, setPwd] = useState('');
  const [number, setNumber] = useState('');
  const [gender, setGender] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');
  const date_time_picker = useRef(null);
  const btnName = useRef(null);
  const btnPwd = useRef(null);
  const btnNum = useRef(null);
  const btnDob = useRef(null);
  const btnGender = useRef(null);
  const btnEmail = useRef(null);
  const btnAddress = useRef(null);

  // NEW
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));


  const _onShowDateTimePicker = () => {
    date_time_picker.current._show();
  };
  const _changeBtnStatus = () => {
    setBtnStatus(false);
  };
  const onPressLogout = async () => {
    dispatch(LoginActions.logout());
    await AsyncStorage.removeItem('Authorization');
    await props.navigation.navigate('Initial');
  };
  const onPressBtnName = () => {
    btnName.current.focus();
  };
  const onPressBtnPwd = () => {
    btnPwd.current.focus();
  };
  const onPressBtnDob = () => {
    btnDob.current.focus();
  };
  const onPressBtnNum = () => {
    btnNum.current.focus();
  };
  const onPressBtnGender = () => {
    btnGender.current.focus();
  };
  const onPressBtnEmail = () => {
    btnEmail.current.focus();
  };
  const onPressBtnAddress = () => {
    btnAddress.current.focus();
  };
  const onPressChangePassWord = () => {
    props.navigation.navigate(screenName.PassWordScreen);
  };

  const onSwitchValueChange = (value) => {
    if (value) {
      // turnOnLoginTouchId
      turnOnLoginBiometric();
    } else {
      // turnOffLoginTouchID
      turnOffLoginBiometric();
    }
    setSwitchValue(value);
  };


  React.useEffect(() => {
    console.tron.log('EmployeeMeActions dispatch');
    InteractionManager.runAfterInteractions(() => setInitialRender(false));
    dispatch(EmployeeMeActions.employeeMeRequest());
    AsyncStorage.getItem('AuthorizationBiometric').then((token) => {
      setSwitchValue(!!token);
    });
  }, []);


  const Actions = {
    setDateOfBirth,
    setBtnStatus,
    setName,
    setPwd,
    setNumber,
    setSwitchValue,
    _onShowDateTimePicker,
    _changeBtnStatus,
    onPressLogout,
    onPressBtnName,
    onPressBtnPwd,
    onPressBtnDob,
    onPressBtnNum,
    onPressBtnEmail,
    onPressBtnGender,
    onPressBtnAddress,
    onPressChangePassWord,
    onSwitchValueChange,
  };
  const state = {
    dateOfBirth,
    btnStatus,
    name,
    pwd,
    number,
    date_time_picker,
    btnName,
    btnPwd,
    btnNum,
    btnDob,
    switchValue,
  };
  /**
     * render
     *  - declare variables for render
     *  - render loading when initialRender is true
     */
  const renderProps = {
    initialRender,
    // employee_me,
    user_detail,
    state,
    Actions,
  };

  return (
    <Render {...renderProps} />
  );
}

const Render = React.memo((props) => {
  console.tron.log('DashBoardScreen render');
  const {
    initialRender,
    // employee_me,
    user_detail,
    state,
    Actions,
  } = props;

  const {
    dateOfBirth, btnStatus, name, pwd, number, date_time_picker,
    btnPwd, btnNum, btnDob, btnName, btnAddress, btnEmail, btnGender,
    switchValue,
  } = state;
  const {
    setDateOfBirth, setBtnStatus, setName, setPwd, setNumber,
    setSwitchValue,
    onSwitchValueChange,
    _onShowDateTimePicker, _changeBtnStatus, onPressLogout, onPressBtnName,
    onPressBtnPwd, onPressBtnDob, onPressBtnNum, onPressBtnEmail, onPressBtnGender, onPressBtnAddress, onPressChangePassWord,
  } = Actions;

  const first_name = _.get(user_detail.userdetail, 'firstName', '');
  const last_name = _.get(user_detail.userdetail, 'lastName', '');
  const middle_name = _.get(user_detail.userdetail, ['data', 'middle_name'], '');
  const mobile_number = _.get(user_detail.userdetail, 'phoneNumber', '');
  const email = _.get(user_detail.userdetail, 'email', '');
  const dob = _.get(user_detail.userdetail, 'dateOfBirth', '');
  const dob_emp = moment(dob).format('DD/MM/YYYY');
  const gender_emp = _.get(user_detail.userdetail, 'gender', '');
  const address_emp = _.get(user_detail.userdetail, 'address', '');

  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeName = _.get(findStoreSellerPayload, ['store_name']);
  const department = _.get(user_detail.userdetail, 'department', '');
  const position = _.get(user_detail.userdetail, 'position', '');
  // const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));

  // const dataRoles = _.get(user_detail, ['roles'], '');
  // console.tron.log("debug: user_detail", user_detail)

  // // take position
  // const position = _.find(dataRoles, (item) => { return item.type === 'shiftleader' || item.type === 'seller'; });


  if (initialRender) return <LoadingInteractions size="large" />;

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        enabled
        style={styles.container}
        behavior="padding"
      >
        {/* <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}> */}
        <View style={styles.container}>

          <ScrollView style={styles.Block_Textinput}>
            <View style={{ backgroundColor: 'white' }}>
              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Họ Và Tên</Text>
                </View>
                <View> */}
              {/* <TextInput
                  // style={{backgroundColor:'red'}}
                  ref={btnName}
                  style={styles.Text6}
                  onChangeText={name => setName(name)}
                  value={name}
                  placeholderTextColor={Colors.black}
                  editable={true}
                  keyboardType='default'
                  placeholder={'chưa cập nhật'}
                /> */}
              {/* <Text style={styles.Text6}>{_.concat([last_name, middle_name ? ' ' : '', middle_name, first_name ? ' ' : '', first_name])}</Text>
                </View>
              </View> */}
              <TextInfo
                Text1="Họ Và Tên"
                Text2={_.concat([last_name, middle_name ? ' ' : '', middle_name, first_name ? ' ' : '', first_name])}
              />
              {/* <View style={styles.lineStyle} /> */}

              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Ngày Sinh</Text>
                </View>
                <View> */}
              {/* <DateTimePicker
                  ref={date_time_picker}
                  // ref={(el) => { date_time_picker = el }}
                  callBack={(date) => { setDateOfBirth({ dob: moment(date) }); _changeBtnStatus() }}
                /> */}
              {/* <View> */}
              {/* <TextInput
                    style={styles.Text6}
                    // ref={date_time_picker}
                    returnKeyType='next'
                    editable={false}
                    pointerEvents='none'
                    placeholder={dob != null ? moment(dob).format('DD/MM/YYYY') : 'chưa cập nhật'}
                    placeholderTextColor={Colors.black}
                    onChangeText={dob => setDateOfBirth(dob)}
                  /> */}
              {/* <Text style={styles.Text6}>{dob_emp == 'Invalid date' || dob_emp == null ? 'chưa cập nhật' : dob_emp}</Text>
                  </View>
                </View>
              </View> */}
              <TextInfo
                Text1="Ngày Sinh"
                Text2={dob_emp == 'Invalid date' || dob_emp == null ? 'chưa cập nhật' : dob_emp}
              />
              <TextInfo
                Text1="Giới Tính"
                Text2={gender_emp ? 'Nam' : 'Nữ' || 'chưa cập nhật'}
              />
              <TextInfo
                Text1="Số Điện Thoại"
                Text2={mobile_number || 'chưa cập nhật'}
              />
              <TextInfo
                Text1="Email"
                Text2={email || 'chưa cập nhật'}
              />
              <TextInfo
                Text1="Địa chỉ "
                Text2={address_emp || 'chưa cập nhật'}
              />
              {/* <View style={styles.lineStyle} /> */}
              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Giới Tính</Text>
                </View>
                <View> */}
              {/* <TextInput
                  ref={btnGender}
                  style={styles.Text6}
                  onChangeText={gender => setName(gender)}
                  // value={gender}
                  placeholderTextColor={Colors.black}
                  editable={true}
                  keyboardType='default'
                  placeholder={'chưa cập nhật'}
                /> */}
              {/* <Text style={styles.Text6}>{gender_emp ? 'Nam' : 'Nữ' || 'chưa cập nhật'}</Text>
                </View>
              </View> */}

              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Số Điện Thoại</Text>
                </View>
                <View> */}
              {/* <TextInput
                  ref={btnNum}
                  style={styles.Text6}
                  onChangeText={number => setNumber(number)}
                  value={number}
                  placeholderTextColor={Colors.black}
                  editable={true}
                  maxLength={12}
                  // keyboardType='number-pad'
                  placeholder={mobile_number || 'chưa cập nhật'}
                  keyboardType='decimal-pad'
                /> */}
              {/* <Text style={styles.Text6}>{mobile_number || 'chưa cập nhật'}</Text>
                </View>
              </View> */}
              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Email</Text>
                </View>
                <View> */}
              {/* <TextInput
                  ref={btnEmail}
                  style={styles.Text6}
                  onChangeText={email => setName(email)}
                  // value={email}
                  placeholderTextColor={Colors.black}
                  editable={true}
                  keyboardType='default'
                  placeholder={'chưa cập nhật'}
                /> */}
              {/* <Text style={styles.Text6}>{email || 'chưa cập nhật'}</Text>
                </View>
              </View> */}
              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Địa chỉ </Text>
                </View>
                <View> */}
              {/* <TextInput
                  ref={btnAddress}
                  style={styles.Text6}
                  onChangeText={address => setName(address)}
                  // value={address}
                  placeholderTextColor={Colors.black}
                  editable={true}
                  keyboardType='default'
                  placeholder={'chưa cập nhật'}
                /> */}
              {/* <Text numberOfLines={1} style={styles.Text6_1}>{address_emp || 'chưa cập nhật'}</Text>
                </View>
              </View> */}
            </View>
            {/* <View style={styles.lineStyle} /> */}

            <View style={styles.block_addition}>

              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Cửa hàng</Text>
                </View>
                <View> */}
              {/* <DateTimePicker
                  ref={date_time_picker}
                  // ref={(el) => { date_time_picker = el }}
                  callBack={(date) => { setDateOfBirth({ dob: moment(date) }); _changeBtnStatus() }}
                /> */}
              {/* <View> */}
              {/* <TextInput
                    style={styles.Text6}
                    // ref={date_time_picker}
                    returnKeyType='next'
                    editable={false}
                    pointerEvents='none'
                    placeholder={dob != null ? moment(dob).format('DD/MM/YYYY') : 'chưa cập nhật'}
                    placeholderTextColor={Colors.black}
                    onChangeText={dob => setDateOfBirth(dob)}
                  /> */}
              {/* <Text style={styles.Text6}>{storeName || 'chưa cập nhật'}</Text>
                  </View>
                </View>
              </View> */}
              <TextInfo
                Text1="Cửa hàng"
                Text2={storeName || 'chưa cập nhật'}
              />
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Bộ phận"
                Text2={department || 'chưa cập nhật'}
              />
              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Bộ phận</Text>
                </View>
                <View> */}
              {/* <DateTimePicker
                  ref={date_time_picker}
                  // ref={(el) => { date_time_picker = el }}
                  callBack={(date) => { setDateOfBirth({ dob: moment(date) }); _changeBtnStatus() }}
                /> */}
              {/* <View> */}
              {/* <TextInput
                    style={styles.Text6}
                    // ref={date_time_picker}
                    returnKeyType='next'
                    editable={false}
                    pointerEvents='none'
                    placeholder={dob != null ? moment(dob).format('DD/MM/YYYY') : 'chưa cập nhật'}
                    placeholderTextColor={Colors.black}
                    onChangeText={dob => setDateOfBirth(dob)}
                  /> */}
              {/* <Text style={styles.Text6}>{department || 'chưa cập nhật'}</Text>
                  </View>
                </View>
              </View> */}
              {/* <View style={styles.lineStyle} />
              <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Vị trí"
                Text2={position || 'chưa cập nhật'}
              />

              {/* <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text5}>Vị trí</Text>
                </View>
                <View> */}
              {/* <DateTimePicker
                  ref={date_time_picker}
                  // ref={(el) => { date_time_picker = el }}
                  callBack={(date) => { setDateOfBirth({ dob: moment(date) }); _changeBtnStatus() }}
                /> */}
              {/* <View> */}
              {/* <TextInput
                    style={styles.Text6}
                    // ref={date_time_picker}
                    returnKeyType='next'
                    editable={false}
                    pointerEvents='none'
                    placeholder={dob != null ? moment(dob).format('DD/MM/YYYY') : 'chưa cập nhật'}
                    placeholderTextColor={Colors.black}
                    onChangeText={dob => setDateOfBirth(dob)}
                  /> */}
              {/* <Text style={styles.Text6}>{position || 'chưa cập nhật'}</Text>
                  </View>
                </View>
              </View> */}
              {/* <View style={styles.lineStyle} /> */}
            </View>

            <View style={{ backgroundColor: 'white', marginTop: Metrics.marginHuge }}>
              {/* <View style={styles.Block_Detail2}>
                <View>
                  <Text style={styles.Text5}>Mật khẩu</Text>
                </View>
                <View> */}
              {/* <TextInput
                    style={styles.Text6}
                    // ref={date_time_picker}
                    returnKeyType='next'
                    editable={false}
                    pointerEvents='none'
                    placeholder={dob != null ? moment(dob).format('DD/MM/YYYY') : 'chưa cập nhật'}
                    placeholderTextColor={Colors.black}
                    onChangeText={dob => setDateOfBirth(dob)}
                  /> */}
              {/* <Text style={styles.Text6}>******</Text>
                </View>
              </View> */}
              <TextInfo
                Text1="Mật khẩu"
                Text2={position || '******'}
              />
              {/* <View style={styles.lineStyle} /> */}
              <View style={styles.Block_Detail}>
                <TouchableOpacity style={styles.block_Change_Info} onPress={onPressChangePassWord}>
                  <Text style={styles.Text9}>Đổi mật khẩu</Text>
                </TouchableOpacity>

              </View>
            </View>

            {/* <View style={styles.Block_Detail3}>
              <Text style={styles.Text5}>Xác thực vân tay</Text>
              <View style={styles.block_Change_Info}>
                <Switch
                  // onValueChange={toggleSwitch}
                  onValueChange={Actions.onSwitchValueChange}
                  value={switchValue}
                  circleActiveColor="#45CF04"
                  backgroundActive={Colors.theme.block}
                  circleBorderWidth={0.1}
                  backgroundInactive={Colors.theme.block}
                  circleInActiveColor={Colors.blueOcean}
                  circleSize={25}
                />
              </View>
            </View> */}
            {/* <View style={styles.lineStyle} /> */}
            <View style={{ marginBottom: 15 }} />
          </ScrollView>
          <View style={styles.bottomTab}>
            <ButtonHandle
              // onPressButton={onPressLogout}
              buttonText="Cập nhật"
              styles={{ backgroundColor: Colors.blue2 }}
            />
          </View>
        </View>
        {/* </TouchableWithoutFeedback> */}
      </KeyboardAvoidingView>
    </View>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
