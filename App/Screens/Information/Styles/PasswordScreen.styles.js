import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Fonts, Metrics,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.block,
  },
  content: {
    flex: 1,

  },
  block_pass: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: 'white',
  },
  Block_Detail: {
    height: wp(52 / 375 * 100),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    marginTop: Metrics.marginMedium,
  },
  Block_Detail1: {
    height: wp(52 / 375 * 100),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    marginTop: Metrics.marginTiny,
  },
  Block_Detail2: {
    height: wp(52 / 375 * 100),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Metrics.paddingMedium,
  },
  Text5: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grayLight,
    fontSize: Fonts.size.textMedium,
  },
  Text6: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // backgroundColor:'red'
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.grey4,
    marginHorizontal:Metrics.marginMedium,

  },
  bottomTab: {
    // ...ApplicationStyles.specialApp.coverBottomTab,
    position: 'absolute',
    backgroundColor: 'white',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100 / 375 * Metrics.width,

    shadowColor: '#0000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 8,
    // android
    elevation: 50,
  },
  part1: {
    ...Platform.select({
      ios: {
        flex: 5,
      },
      android: {
        flex: 6,
      },

    }),
  },
  part2: {
    flex: 1,
    textAlign: 'right',
  },
});
