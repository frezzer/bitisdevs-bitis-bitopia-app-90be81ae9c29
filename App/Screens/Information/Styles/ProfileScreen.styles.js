import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.silver,
  },
  Block_Infor: {
    ...ApplicationStyles.specialApp.dimensisons(375, 148),
    ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.white,
    marginBottom: Metrics.marginTiny,
    flexDirection: 'row',
  },
  Name: {
    ...ApplicationStyles.specialApp.dimensisons(226, 95),
    marginLeft: Metrics.marginSmall,
    // backgroundColor:'red',
    // ...ApplicationStyles.specialApp.alignCenter,
    justifyContent: 'center',
  },
  image_logo: {
    ...ApplicationStyles.specialApp.dimensisons(74, 74),
    borderRadius: wp(37 / 375 * 100),
  },
  Text1: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginLeft: Metrics.marginXXTiny,
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.blueOcean,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginXXTiny,
  },
  Text3: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textSmall,
    marginLeft: Metrics.marginXXTiny,
    marginBottom: Metrics.marginXTiny,
  },
  Text4: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grayLight,
    fontSize: Fonts.size.textSmall,
    marginLeft: Metrics.marginXXTiny,
  },
  Block_Textinput: {
    marginBottom: (100 / 375) * Metrics.width,
    // backgroundColor:'red',
    // ...ApplicationStyles.specialApp.dimensisons(375,208),
  },
  Block_Detail: {
    height: wp((44 / 375) * 100),
    backgroundColor: Colors.white,
    // marginBottom: Metrics.marginXXTiny,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  Text5: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grayLight,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginLarge,
  },
  Text6: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginRight: Metrics.marginLarge,
    textAlign: 'left',
    // backgroundColor:'red'
  },
  Text6_1: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginRight: Metrics.marginLarge,
    textAlign: 'left',
    paddingLeft: wp(120 / 375 * 100),
    // paddingRight: wp(50/375*100),
    // backgroundColor:'red'

    // backgroundColor:'red'
  },
  block_Change_Info: {
    // marginRight: Metrics.marginMedium,
    marginLeft: Metrics.marginMedium,
  },
  Text9: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.blueOcean,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
  },
  lineStyle: {
    borderWidth: 0.3,
    borderColor: Colors.grey4,
    marginHorizontal: Metrics.marginMedium,
  },
  buttomtab: {
    // ...ApplicationStyles.specialApp.coverBottomTab
  },
  block6: {
    marginTop: Metrics.marginTiny,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  Block_Detail1: {
    height: wp(44 / 375 * 100),
    backgroundColor: Colors.white,
    // marginBottom: Metrics.marginXXTiny,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: Metrics.marginTiny,
  },

  bottomTab: {
    ...ApplicationStyles.specialApp.coverBottomTab,
  },
  block_addition: {
    marginTop: Metrics.marginHuge,
    backgroundColor:'white'
  },
  Block_Detail3: {
    height: wp(44 / 375 * 100),
    backgroundColor: Colors.white,
    // marginBottom: Metrics.marginXXTiny,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // marginTop: Metrics.marginTiny,
  },
  Block_Detail2: {
    height: wp(44 / 375 * 100),
    backgroundColor: Colors.white,
    // marginBottom: Metrics.marginXXTiny,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
