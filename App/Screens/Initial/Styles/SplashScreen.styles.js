import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors,Fonts } from '../../../Themes'

export default StyleSheet.create({
  screen:{
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },
  value: {
    backgroundColor: "red",
    height: 3
  },
  progress: {
    marginTop: Metrics.baseMargin,
    alignSelf: 'center',
    marginBottom: 50,
    backgroundColor: '#e5e5e5',
  },
  LogoBanner: {
    justifyContent: 'center',
    marginTop: (Metrics.height * 0.3),
  },
  Logo: {
    fontSize: 160,
    color: 'white',
    //marginBottom: (Metrics.height * 0.05),
  },
  Caption : {
    textAlign: 'center',
    color:'#666666',
    fontSize: Fonts.size.textSmall,
    marginBottom:Metrics.marginTiny
},
});
