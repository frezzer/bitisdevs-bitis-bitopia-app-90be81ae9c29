import React, { useState, useEffect } from 'react';
import {
  ScrollView, View, Text, FlatList, SafeAreaView,
} from 'react-native';
import { Divider } from '@shoutem/ui';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/BillScreenStyles';

// Components
import BillItem from '../Components/BillItem';

// Utils
import { compareOmitState } from '../Transforms/utils';


const BillScreen = (props) => {
  const [data, setdata] = useState([
    {
      id: 1,
      title: 'Chụp ảnh check in',
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    },
    {
      id: 2,
      title: 'Chụp ảnh check in',
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    },
    {
      id: 3,
      title: 'Chụp ảnh check in',
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    },
  ]);

  const renderItem = ({ item }) => {
    return (
      <View id={item.id}>
        <BillItem data_item={item} />
      </View>
    );
  };
  return (
    <SafeAreaView>
      <View>
        {/* <Screen styleName='block' > */}
        <ScrollView>
          <View>
            <View style={styles.block1}>
              <Text style={styles.Text1}> Hôm nay</Text>
            </View>
            <Divider styleName="line" />
            <View>
              <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={(item, index) => item.id}
              />
            </View>
          </View>
          <View style={styles.block1}><Text style={styles.Text1}> Hôm qua</Text></View>
          <Divider styleName="line" />
          <View>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item, index) => item.id}
            />
          </View>
          <View style={styles.block1}><Text style={styles.Text1}> 12/8/2019</Text></View>
          <Divider styleName="line" />
          <View>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item, index) => item.id}
            />
          </View>
        </ScrollView>
        {/* </Screen> */}
      </View>
    </SafeAreaView>
  );
};


function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, []);
  // return false
}
export default React.memo(BillScreen, areEqual);
