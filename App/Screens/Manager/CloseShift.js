
// basics
import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import {
  Dimensions, InteractionManager, View, Text,
  TouchableOpacity, ScrollView, RefreshControl, Animated, Platform,
} from 'react-native';

// Components
import { widthPercentageToDP } from 'react-native-responsive-screen';
import QRCode from 'react-native-qrcode-svg';
import Modal from 'react-native-modal';
import { Metrics, Colors } from '../../Themes';
import { Feather } from '../../Components/Icons';
import BackButtonsTopBar from '../../Components/Button/BackButtonsTopBar';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import DoubleButton from '../../Components/Button/DoubleButton';
import TextInfo from '../../Components/TextInput/TextInfo';
import EmployeeStatus from '../../Components/Employee/EmployeeStatus';

// import { Ionicons as Icon } from '@expo/vector-icons';

// Actions and Selectors Redux
import ShiftFindstoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../Redux/shift/ShiftFindRedux';
import UserFindActions, { UserFindSelectors } from '../../Redux/user/FindUserRedux';
import ShiftFinishoneActions, { ShiftFinishoneSelectors } from '../../Redux/shift/ShiftFinishOneRedux';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';


// const AnimatedIcon = Animated.createAnimatedComponent(Icon);

// Styles
import styles from './Styles/CloseShiftScreen.styles';

// Navigation
import screensName from '../../Navigation/Screens';

// utils
import { compareOmitState, getFullName } from '../../Services/utils';


// Declare Animated
const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
ManagerCloseShiftScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function ManagerCloseShiftScreen(props) {
  const data = props.navigation.state.params;
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [refreshing, setRefreshing] = React.useState(false);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me)) || {};
  const employee_uuid = _.get(mePayload, 'username', null);
  // const findStorePayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  // const store_name = _.get(findStorePayload, ['data', 'store_name'], 'Chưa cập nhật');
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const store_name = _.get(findStoreSellerPayload, ['store_name']);
  const store_uuid = _.get(findStoreSellerPayload, ['store_uuid']);
  const employeeFinishShift = _.filter(data.attendances, (attendance) => { return attendance.status == 'DONE'; });
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const middle_name = _.get(user_detail, ['data', 'middle_name'], '');
  const shift_leader_name = _.concat([last_name, middle_name ? ' ' : '', middle_name, first_name ? ' ' : '', first_name]);
  const listUserAttendance = useSelector((state) => UserFindSelectors.selectPayload(state.user.userFind)) || [];
  const listAttendance = _.map(data.attendances, (attendance) => {
    return {
      ...attendance,
      user: _.find(listUserAttendance, (user) => { return user.username === attendance.user_uuid; }),
    };
  });

  const finishPayload = useSelector((state) => ShiftFinishoneSelectors.selectPayload(state.shift.finishOne));
  const finishFetching = useSelector((state) => ShiftFinishoneSelectors.selectFetching(state.shift.finishOne));

  // console.tron.log('debug: ManagerCloseShiftScreen -> finishPayload', finishPayload);
  // Pull to refresh
  function wait(timeout) {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  }
  React.useEffect(() => {
    dispatch(UserFindActions.UserFindRequest({
      username_in: _.map(data.attendances, (i) => i.user_uuid),
    }));
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(ShiftFindstoreActions.shiftFindstoreRequest({ store_uuid: data.store_uuid }));
    }
    return () => { };
  }, []);

  React.useEffect(() => {
    if (finishPayload != null && finishFetching === false) {
      setIsModalVisible(false);
      props.navigation.navigate(screensName.MoreScreen);
    }
  });
  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressFinish: () => {
      props.navigation.navigate(screensName.ManagerConfirmCloseShiftScreen, { listAttendance });
    },
    onPressGoBack: () => {
      props.navigation.goBack();
    },
    toggleModal: () => {
      setIsModalVisible(!isModalVisible);
    },
    onPressConfirm: () => {
      const close_time = new Date();
      dispatch(ShiftFinishoneActions.shiftFinishoneRequest({
        username: employee_uuid,
        store_Uuid: store_uuid,
        uuid: data.uuid,
        close_time: close_time.toISOString(),
      }));
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */

  const base64Logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAA..';
  const renderProps = {
    initialRender,
    Actions,
    base64Logo,
    data,
    employeeFinishShift,
    store_name,
    refreshing,
    // onRefresh,
    shift_leader_name,
    scrollY,
    setScrollY,
    // animatedValue,
    isModalVisible,
    listAttendance,

  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender,
    Actions,
    data,
    base64Logo,
    store_name,
    employeeFinishShift,
    shift_leader_name,
    scrollY,
    // animatedValue,
    isModalVisible,
    listAttendance,
  } = props;

  // ANIMATION
  const animateColor = scrollY.interpolate({
    inputRange: [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT)], // 0 - 4
    outputRange: [Colors.blue1, Colors.white], // animateColor blue-white
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT)],
    outputRange: [Colors.white, Colors.black],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };

  //
  if (initialRender) return <LoadingInteractions size="large" />;
  if (!data.uuid) {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.warningBox}>
            <Text style={styles.warningText}>Không tìm thấy thông tin ca.</Text>
          </View>
        </View>
      </View>
    );
  }


  const time_in = moment(data.shifttype.time_in, 'HH:mm:ss').format('HH:mm');
  const time_out = moment(data.shifttype.time_out, 'HH:mm:ss').format('HH:mm');

  console.tron.log('TCL: time_in', time_in);
  return (


    <Animated.View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <Animated.View style={{
          height: Metrics.navigationBarHeight,
          justifyContent: 'space-between',
          paddingTop: widthPercentageToDP(40 / 375 * 100),
          flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressFinish}>
            <View>
              <AnimatedFeather
                name="clock"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>

        </Animated.View>
      </Animated.View>

      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={
          Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }])
      }
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: widthPercentageToDP(24 / 375 * 100), borderTopRightRadius: widthPercentageToDP(24 / 375 * 100),
        }}
        >
          <View style={{ alignItems: 'center' }}>
            <View style={{ marginTop: widthPercentageToDP(40 / 375 * 100) }}>
              <QRCode
                value={data.uuid}
                logo={{ uri: base64Logo }}
                logoSize={50}
                logoBackgroundColor="transparent"
                size={247}
              />
            </View>
            <View style={styles.valueQrCodeView}>
              <View style={styles.info}>
                <Text style={styles.valueQrCodeText}>{data.shifttype.name}</Text>
                <Text style={styles.shiftName}>
                  {time_in}
                  {' '}
                  -
                  {' '}
                  {time_out}
                </Text>
              </View>

              <View style={styles.block_date}>
                <Text style={styles.shiftNote}>{moment(data.created_at).format('DD/MM/YYYY')}</Text>
              </View>
            </View>
            <View style={styles.block_hidden}>
              <Text>_______</Text>
            </View>
            <View style={styles.info_shift}>
              <Text style={styles.name_shift}>{data.name}</Text>
              <Text style={styles.note_shift}>{data.note}</Text>
            </View>
          </View>
          <View style={styles.block_info}>
            <TextInfo
              Text1="Người tạo ca"
              Text2={shift_leader_name}
            />
            <TextInfo
              Text1="Cửa hàng"
              Text2={store_name}
            />
            <TextInfo
              Text1="Ngày tạo ca"
              Text2={moment(data.created_at).format('DD/MM/YYYY')}
            />
            <TextInfo
              Text1="Thời gian tạo ca"
              Text2={moment(data.open_time).format('HH:mm')}
            />
            {data.status ? (
              <View>
                <TextInfo
                  Text1="Thời gian kết ca"
                  Text2={data.close_time ? moment(data.close_time).format('HH:mm') : 'Chưa cập nhật'}
                />
              </View>
            )
              : null}
            <TextInfo
              Text1="Nhân viên đã điểm danh"
              Text2={data.attendances.length}
            />
            <TextInfo
              Text1="Nhân viên đã tan ca"
              Text2={employeeFinishShift.length}
            />
          </View>
        </View>
        {data.status === false
          ? (
            <View style={styles.btn_bottom}>
              <ButtonHandle
                buttonText="Kết ca"
                stylesText={styles.Text12}
                onPressButton={Actions.toggleModal}
                styles={{ backgroundColor: Colors.orange1 }}
              />
            </View>
          )
          : (
            <View style={styles.btn_bottom}>
              <ButtonHandle
                buttonText="Đã kết ca"
                stylesText={{ color: Colors.greybold }}
                // onPressButton={Actions.toggleModal}
                styles={{ backgroundColor: Colors.grey4 }}
              />
            </View>
          )}
        <Modal isVisible={isModalVisible}>
          <View style={styles.Modal}>

            <View style={styles.containerModal}>
              <View style={styles.block_confirm}>
                <View style={styles.inside}>
                  <Text style={styles.ques}>Bạn muốn kết ca ? </Text>
                  <View style={{ alignItems: 'center' }}>
                    <Text styles={styles.note}>Sau khi kết ca tất cả các nhân viên khác sẽ </Text>
                    <Text styles={styles.note}>không thể tan ca.</Text>
                  </View>
                </View>

              </View>
              <View style={styles.block_info}>
                <View style={styles.block_hidden1}>
                  <Text>______</Text>
                </View>
                <View>
                  <EmployeeStatus dataEmployee={listAttendance} />
                </View>
              </View>

              <DoubleButton
                styles={styles.cover}
                onPressButton1={Actions.toggleModal}
                buttonText1="Hủy"
                onPressButton2={Actions.onPressConfirm}
                buttonText2="Kết ca"
              />

            </View>

          </View>
        </Modal>
      </ScrollView>
    </Animated.View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
