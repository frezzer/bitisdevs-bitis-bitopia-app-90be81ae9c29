
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, ScrollView, Animated, Platform, TouchableOpacity,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import LoadingInteractions from '../../Components/LoadingInteractions';
import EmployeeStatus from '../../Components/Employee/EmployeeStatus';
import DoubleButton from '../../Components/Button/DoubleButton';
import screensName from '../../Navigation/Screens';
import { Feather } from '../../Components/Icons';


// import { StackActions, NavigationActions } from 'react-navigation';

// Styles
import styles from './Styles/ConfirmCloseShiftScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindonlyoneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import ShiftFinishoneActions, { ShiftFinishoneSelectors } from '../../Redux/shift/ShiftFinishOneRedux';

// utils
import { compareOmitState } from '../../Services/utils';
import { Metrics, Colors } from '../../Themes';


// Declare Animated
const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
ManagerConfirmCloseShiftScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function ManagerConfirmCloseShiftScreen(props) {
  // maybe you need
  const listAttendance = props.navigation.state.params;
  console.tron.log('debug: ManagerConfirmCloseShiftScreen -> props.navigation.state.params', props.navigation.state.params);
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));


  // effects
  React.useEffect(() => {
    console.tron.log('ManagerConfirmCloseShiftScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction
    dispatch(ShiftFinishoneActions.shiftFinishoneInitial());
    dispatch(ShiftFindonlyoneActions.shiftFindonlyoneInitial());

    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressCancel: () => {
      props.navigation.goBack();
    },
    onPressGoBack: () => {
      props.navigation.goBack();
    },

  };
  const state = {

  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    // data,
    scrollY,
    setScrollY,
    listAttendance,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const {
    initialRender, Actions, state, scrollY, listAttendance,
    setScrollY,
  } = props;
  const { } = state;
  const { } = Actions;
  console.tron.log('debug: data_attendance', listAttendance);

  // ANIMATION
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.black],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };

  //

  return (

    <View style={{ flex: 1 }}>

      <Animated.View style={[{ height: widthPercentageToDP(130 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>


        </View>
      </Animated.View>

      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0, height: Metrics.height,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: widthPercentageToDP(24 / 375 * 100), borderTopRightRadius: widthPercentageToDP(24 / 375 * 100), paddingTop: widthPercentageToDP(20 / 375 * 100),
        }}
        >
          <View style={styles.container}>
            <Text style={styles.ques}>Danh sách nhân viên kết ca</Text>
            <View style={styles.block_info}>
              <View>
                <EmployeeStatus dataEmployee={listAttendance.listAttendance} btnDisalble />
              </View>
            </View>

          </View>
        </View>
      </ScrollView>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
