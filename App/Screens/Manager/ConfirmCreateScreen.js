
// basics
import _ from 'lodash';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, TextInput, TouchableOpacity, View, Text, Image, Animated, Platform,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { ScrollView } from 'react-native-gesture-handler';
// import { Colors } from 'react-native/Libraries/NewAppScreen';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import { Metrics, Images, Colors } from '../../Themes';
import TextInfo from '../../Components/TextInput/TextInfo';
import { Feather } from '../../Components/Icons';


// Styles
import styles from './Styles/ConfirmCreateScreen.styles';
import screenName from '../../Navigation/Screens';


// utils
import { compareOmitState } from '../../Services/utils';


const onPressBackButton = (navigation) => {
  navigation.popToTop();
};

const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 40;
const navigationOptions = (props) => ({
  header: null,
});
ManagerConfirmCreateScreen.navigationOptions = navigationOptions;


/**
 * Main
 */
export default function ManagerConfirmCreateScreen(props) {
  const { onPressButton } = props;
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));

  const {
    shiftCode,
    store_name,
    scan_time,
    fullName,
    timeInText,
    timeOutText,
    nameOfShift,
    descriptionShift,
    pickShiftTypes,
  } = props.navigation.state.params;

  // effects
  React.useEffect(() => {
    console.tron.log('ManagerConfirmScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressPopToTop: () => props.navigation.navigate(screenName.ManagerListShiftScreen),
    onPressGoBack: () => {
      props.navigation.goBack();
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const base64Logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAA..';
  const renderProps = {
    initialRender,
    onPressButton,
    Actions,
    shiftCode,
    store_name,
    scan_time,
    fullName,
    timeInText,
    timeOutText,
    base64Logo,
    nameOfShift,
    descriptionShift,
    scrollY,
    pickShiftTypes,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerConfirmScreen render');
  // declare variables
  const {
    base64Logo,
    initialRender,
    onPressButton,
    Actions,
    shiftCode,
    store_name,
    scan_time,
    fullName,
    timeInText,
    timeOutText,
    nameOfShift,
    descriptionShift,
    scrollY,
    pickShiftTypes,

  } = props;
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.black],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };


  if (initialRender) return <LoadingInteractions size="large" />;
  return (


    <View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        />
      </Animated.View>
      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: 24, paddingTop: widthPercentageToDP(20 / 375 * 100),
        }}
        >
          <Text style={styles.title}> Tạo ca thành công</Text>
          <View style={{ alignItems: 'center' }}>
            <QRCode
              value={shiftCode}
              logo={{ uri: base64Logo }}
              logoSize={50}
              logoBackgroundColor="transparent"
              size={250}
            />
            <View style={styles.valueQrCodeView}>
              <View style={styles.info}>
                <Text style={styles.valueQrCodeText}>{pickShiftTypes.name}</Text>
                <Text style={styles.shiftName}>
                  {timeInText}
                  -
                  {timeOutText}
                </Text>
              </View>

              <View style={styles.block_date}>
                <Text style={styles.shiftNote}>{moment(scan_time).format('DD/MM/YYYY')}</Text>
              </View>
            </View>
            <View style={styles.block_hidden}>
              <Text>_______</Text>
            </View>
            <View style={styles.info_shift}>
              <Text style={styles.name_shift}>{nameOfShift}</Text>
              <Text style={styles.note_shift}>{descriptionShift}</Text>
            </View>
          </View>
          <View style={styles.block_info}>
            <TextInfo
              Text1="Người tạo ca"
              Text2={fullName}
            />
            <TextInfo
              Text1="Cửa hàng"
              Text2={store_name}
            />
            <TextInfo
              Text1="Thời điểm tạo ca"
              Text2={moment(scan_time).format('DD/MM/YYYY')}
            />
            <TextInfo
              Text1="Thời gian tạo ca"
              Text2={moment(scan_time).format('HH:mm')}
            />
          </View>
          <View style={styles.btn_bottom}>
            <ButtonHandle
              onPressButton={onPressButton || Actions.onPressPopToTop}
              buttonText="Trở về danh sách ca"
              styles={{ backgroundColor: Colors.blue2 }}
            />
          </View>
        </View>

      </ScrollView>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
