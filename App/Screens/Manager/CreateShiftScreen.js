
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, View, Text, TouchableOpacity, TextInput,
  TouchableWithoutFeedback, Keyboard,
  SafeAreaView, InteractionManager,
  KeyboardAvoidingView, ScrollView,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Colors, Metrics } from '../../Themes';
import TextInfo from '../../Components/TextInput/TextInfo';
import ButtonHandle from '../../Components/Button/ButtonHandle';

// Styles
import styles, { pickerSelectStyles } from './Styles/CreateShiftScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftGettypesActions, { ShiftGettypesSelectors } from '../../Redux/shift/ShiftGetTypesRedux';
import ShiftFindstoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';
import ShiftCreateoneActions, { ShiftCreateoneSelectors } from '../../Redux/shift/ShiftCreateOneRedux';
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';


import screensName from '../../Navigation/Screens';


// utils
import { compareOmitState, getFullName, validate } from '../../Services/utils';
import 'moment/locale/vi';


const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Tạo ca',
  };
};

ManagerCreateShiftScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function ManagerCreateShiftScreen(props) {
  // const  currentTime  = props.navigation.state.params
  // console.tron.log("TCL: ManagerCreateShiftScreen -> props.navigation.state.params", currentTime)
  const open_time = moment().toISOString();
  // console.tron.log("TCL: ManagerCreateShiftScreen ->/ openTime", openTime)
  const currentDate = moment().format('DD/MM/YYYY');

  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [time_in, settime_in] = React.useState(null);
  const descriptionInputRef = React.useRef(null);
  const [time_out, settime_out] = React.useState(null);
  const [nameOfShift, setNameOfShift] = React.useState('');
  const [descriptionShift, setDescriptionShift] = React.useState('');
  const [pickShiftTypes, setPickShiftTypes] = React.useState(null);
  const findStorePayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));


  const [message, setMessage] = useState('')


  // selector store
  const listShiftTypes = useSelector((state) => ShiftGettypesSelectors.selectPayload(state.shift.getTypes)) || [];
  const createShiftPayload = useSelector((state) => ShiftCreateoneSelectors.selectPayload(state.shift.createOne));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me)) || {};
  const employee_uuid = _.get(mePayload, 'username', null);
  const timeInText = time_in ? moment(time_in, 'HH:mm:ss').format('HH:mm') : '--';
  const timeOutText = time_out ? moment(time_out, 'HH:mm:ss').format('HH:mm') : '--';
  const store_name = _.get(findStorePayload, ['data', 'store_name'], 'Tên cửa hàng');
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const middle_name = _.get(user_detail, ['data', 'middle_name'], '');
  const shift_leader_name = _.concat([last_name, middle_name ? ' ' : '', middle_name, first_name ? ' ' : '', first_name]);


  // effects
  React.useEffect(() => {
    dispatch(ShiftCreateoneActions.shiftCreateoneInitial());
    dispatch(ShiftGettypesActions.shiftGettypesRequest());
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
    }
  }, []);

  React.useEffect(() => {
    if (findStorePayload && createShiftPayload) {
      const { shiftCode } = createShiftPayload;
      // console.tron.log("TCL: ManagerCreateShiftScreen -> shiftCode", shiftCode)
      props.navigation.navigate(screensName.ManagerConfirmCreateScreen, {
        shiftCode,
        store_name,
        scan_time: open_time,
        fullName: shift_leader_name,
        timeInText,
        timeOutText,
        nameOfShift,
        descriptionShift,
        pickShiftTypes,
      });
      dispatch(ShiftCreateoneActions.shiftCreateoneInitial());
    }
  }, [createShiftPayload, findStorePayload]);


  /** cu
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSubmit: () => {
      // validate
      if (!pickShiftTypes) return alert('Vui lòng chọn loại ca');
      if (_.isString(nameOfShift) && nameOfShift.length < 3) return alert('Tên ca tối thiểu 3 ký tự');

      // console.tron.log("TCL: ManagerCreateShiftScreen -> open_time", moment(createTime).toISOString())
      dispatch(ShiftCreateoneActions.shiftCreateoneRequest({
        username: employee_uuid,
        name: nameOfShift,
        store_uuid: _.get(findStorePayload, ['data', 'store_uuid'], ''),
        open_time,
        shifttype: {
          id: _.get(pickShiftTypes, 'id'),
        },
        note: descriptionShift,
      }));
    },

  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    message,
    setMessage,

    initialRender,
    findStorePayload,
    mePayload,
    listShiftTypes,
    time_in,
    time_out,
    nameOfShift,
    descriptionShift,
    descriptionInputRef,
    Actions,
    settime_in,
    settime_out,
    setNameOfShift,
    setPickShiftTypes,
    setDescriptionShift,
    // Scan_time,
    currentDate,
    // currentTime,
    timeInText,
    timeOutText,
    store_name,
    shift_leader_name,
    open_time,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerCreateShiftScreen render');
  // declare variables
  const {
    initialRender,
    listShiftTypes,
    nameOfShift,
    descriptionShift,
    descriptionInputRef,
    Actions,
    settime_in,
    settime_out,
    setNameOfShift,
    setPickShiftTypes,
    setDescriptionShift,
    // currentTime,
    // Scan_time,
    currentDate,
    timeInText,
    timeOutText,
    store_name,
    shift_leader_name,
    open_time,
  } = props;
  if (initialRender) return <LoadingInteractions size="large" />;


  const _listShiftTypes = _.map(listShiftTypes, (i) => ({ value: i, label: i.name }));
  console.tron.log('TCL: _listShiftTypes', _listShiftTypes);

  // console.tron.log("TCL: createTime", createTime)
  callbackFunction = (childData) => {
    setMessage(childData)
  };

  return (
    <View style={styles.container}>
      {/* <ScrollView> */}
      <KeyboardAwareScrollView
        enabled
        style={styles.container}
        behavior="padding"
      >
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}>
          <View style={styles.container}>
            <View style={styles.part1}>
              <View style={styles.title}>
                <Text style={styles.text1_1}>Thông tin chung</Text>
              </View>
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Người tạo ca"
                Text2={shift_leader_name}
              />
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                // disable
                Text1="Cửa hàng"
                Text2={store_name}
              />
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Ngày tạo ca"
                Text2={currentDate}
              />
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Thời gian tạo ca"
                Text2={moment(open_time).format('HH:mm')}
              />

            </View>
            <View style={styles.part2}>
              <View style={styles.title}>
                <Text style={styles.text1_1}>Chi tiết</Text>
              </View>
              {/* <View style={styles.lineStyle} /> */}
              <View style={styles.Block_Detail}>
                <View>
                  <Text style={styles.Text2}>Chọn loại ca</Text>
                </View>
                <View style={styles.picker}>
                  <RNPickerSelect
                    placeholder={{
                      label: 'Loại ca...',
                      value: {},

                    }}
                    items={_listShiftTypes}
                    onValueChange={(value) => {
                      console.tron.log('TCL: value', value.time_out);
                      settime_in(value.time_in);
                      settime_out(value.time_out);
                      setPickShiftTypes(value);
                    }}
                    onUpArrow={() => { }}
                    onDownArrow={() => { }}
                    style={{ ...pickerSelectStyles }}
                  />
                </View>
              </View>
              {/* <View style={styles.lineStyle} /> */}

              <TextInfo
                Text1="Giờ bắt đầu"
                Text2={timeInText}
                stylesCustom={styles.textInfo}
              />
              {/* <View style={styles.lineStyle} /> */}
              <TextInfo
                Text1="Giờ kết thúc"
                Text2={timeOutText}
                stylesCustom={styles.textInfo}

              />
              {/* <View style={styles.lineStyle} /> */}
              {/* <View style={styles.lineStyle} /> */}
              <View style={styles.Block_Detail}>
                <View style={{flex:3}}>
                  <Text style={styles.Text2}>Tên ca</Text>
                </View>
                <View style={{flex:2}}>
                  <TextInput
                    style={styles.descriptionInput}
                    autoCapitalize="none"
                    returnKeyType="next"
                    autoCompleteType="off"
                    blurOnSubmit={false}
                    autoCorrect={false}
                    onSubmitEditing={() => descriptionInputRef.current.focus()}
                    onChangeText={(text) => setNameOfShift(text)}
                    value={nameOfShift}
                    placeholder="Nhập tên ca"
                    placeholderTextColor="#D9D9D9"
                  />
                </View>
              </View>
              {/* <View style={styles.lineStyle} /> */}
              <View style={styles.Block_Detail}>
                <View style={{flex:2}}>
                  <Text style={styles.Text2}>Mô tả</Text>
                </View>
                <View style={{flex:2}}>
                  <TextInput
                    style={styles.descriptionInput}
                    autoCapitalize="none"
                    returnKeyType="done"
                    ref={descriptionInputRef}
                    autoCompleteType="off"
                    autoCorect={false}
                    blurOnSubmit={false}
                    onSubmitEditing={Actions.onPressSubmit}
                    placeholder="Nhập mô tả ( nếu có )"
                    onChangeText={(text) => setDescriptionShift(text)}
                    value={descriptionShift}
                    placeholderTextColor="#D9D9D9"
                  />
                </View>
              </View>
              {/* <View style={styles.lineStyle} /> */}

            </View>
          </View>

        </TouchableWithoutFeedback>
      </KeyboardAwareScrollView>
      {/* </ScrollView> */}
      <View style={styles.bottomTab}>
        <ButtonHandle
          onPressButton={Actions.onPressSubmit}
          buttonText="Xác nhận tạo ca"
          styles={{ backgroundColor: Colors.blue2 }}
        />
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
