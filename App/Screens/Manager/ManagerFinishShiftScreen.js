'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import { Dimensions, InteractionManager, View, Text, TouchableOpacity } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ScanQRCodeComponent from '../../Components/ScanQRCode';
import CircleBackButton from '../../Components/Button/CircleBackButton';
// Styles
import styles from './Styles/ManagerFinishShiftScreen.styles';
// Actions and Selectors Redux
import ShiftFindonlyoneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import AttendanceCreateoneActions, { AttendanceCreateoneSelectors } from '../../Redux/attendance/AttendanceCreateOneRedux';
// utils
import { compareOmitState } from "../../Services/utils";
import moment from 'moment';



/**
 * Main
 */
export default function CheckInScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);

  // useStates
  const [createTime, setCreateTime] = React.useState(moment().format('HH:mm'));

  // selector store
  const find_store_name = useSelector(state => ShiftFindonlyoneSelectors.selectPayload(state.shift.findOnlyOne));
  const AttendanceCreateonePayload = useSelector(state => AttendanceCreateoneSelectors.selectPayload(state.attendance.createOne));

  // effects
  React.useEffect(() => {
    console.tron.log('SellerCheckInScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      dispatch(AttendanceCreateoneActions.attendanceCreateoneInitial());
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneInitial());
    }
    if (AttendanceCreateonePayload) {
      setTimeout(() => {
        Navigation.push(props.componentId, navComponentScreen({
          name: screensName.SellerConfirmCheckInScreen,
        }));
      }, 500);
    }
    return () => { };
  }, [AttendanceCreateonePayload]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */

  const Actions = {
    onScanSuccess: (e) => {
      const data = _.get(e, 'data', '');
      setCreateTime(moment().format('HH:mm'));
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneRequest({
        uuid: data
      }));
    },
    onPressContinue: () => {
      props.navigation.navigate('SellerConfirmScreen')
    },
    onPressBackButton: () => Navigation.pop(props.componentId)

  }

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const store_name = _.get(find_store_name, ['merchant', 'store_name'], '');
  const renderProps = {
    initialRender,
    store_name,
    info_name: store_name,
    reactivate: true,
    current_time: createTime,
    title: 'Tan Ca',
    continueText: 'Xác nhận',
    Actions
  }

  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SellerCheckInScreen render');
  // declare variables
  const {
    initialRender,
    store_name,
    reactivate,
    current_time,
    title,
    continueText,
    Actions
  } = props;
  if (initialRender) return <LoadingInteractions size={'large'} />

  return (
    <View style={styles.container}>
      <ScanQRCodeComponent
        onScanSuccess={Actions.onScanSuccess}
        info_name={store_name}
        current_time={current_time}
        reactivate={false}
      />
      <View style={styles.implement}>
        <View style={styles.topBar}>
          <CircleBackButton onPress={Actions.onPressBackButton} />
          <View style={styles.titleView}>
            <Text style={styles.titleText}>{title}</Text>
          </View>
        </View>
        <View>
          <View style={styles.bottomTabs}>
            <View style={styles.confirmButton}>
              <Text style={styles.confirmButtonText}>{continueText}</Text>
              <TouchableOpacity style={styles.confirmButtonTouchable} onPress={Actions.onPressContinue} />
            </View>
          </View>
        </View>
      </View>
    </View >
  )


}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
