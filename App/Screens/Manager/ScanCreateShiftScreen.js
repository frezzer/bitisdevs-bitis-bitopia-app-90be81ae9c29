
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, AsyncStorage, Alert,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import moment from 'moment';
import jwt from 'react-native-pure-jwt';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ScanQRCodeComponent from '../../Components/ScanQRCode';
import CircleBackButton from '../../Components/Button/CircleBackButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import { Colors } from '../../Themes';

// Styles
import styles from './Styles/ScanCreateShiftScreen.styles';

// Actions and Selectors Redux
import ShiftFindstoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';
import FindstoreActions, { FindstoreSelectors } from '../../Redux/shift/FindStoreRedux';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../Redux/shift/ShiftFindRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';


// Navigation
import screensName from '../../Navigation/Screens';

// utils

console.log(jwt);

/**
 * Options
 * */
const navigationOptions = ({ navigation }) => ({
  header: null,
});
ManagerCreateShiftScreen.navigationOptions = navigationOptions;

/**
 * Main
 */

export default function ManagerCreateShiftScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);

  // useStates
  const title = 'Thông tin cửa hàng';
  const [currentTime, setCurrentTime] = React.useState(null);
  const [reactivate, setReactivate] = React.useState(true);
  const [storeDetail, setStoreDetail] = React.useState(null);
  const [dataDecoded, setdataDecoded] = React.useState(null);

  // selector store
  // const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me)) || {};
  const findStorePayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  const findStoreFetching = useSelector((state) => ShiftFindstoreSelectors.selectFetching(state.shift.findStore));
  const findStoreError = useSelector((state) => ShiftFindstoreSelectors.selectError(state.shift.findStore));
  // const findStorebyUser = useSelector((state) => FindstoreSelectors.selectPayload(state.shift.findStorebyUser));
  // const storeUuidDefault = _.get(findStorebyUser, ['data', 'store_uuid'], '');
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeUuidDefault = _.get(findStoreSellerPayload, ['store_uuid']);
  // const from_dashboard = props.navigation.getParam('from_dashboard', false);
  // const employee_uuid = _.get(mePayload, ['username'], '');
  // const findShifts = useSelector((state) => ShiftFindSelectors.selectPayload(state.shift.find)) || [];
  // console.tron.log('debug: ManagerCreateShiftScreen -> FindShifts', findShifts);

  // console.tron.log('debug: DashBoardScreen -> findStorebyUserPayload', storeUuidDefault);

  // effects
  React.useEffect(() => {
    if (initialRender) {
      console.tron.log('ManagerCreateShiftScreen didmount');
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(ShiftFindstoreActions.shiftFindstoreInitial());
    }
    return () => {
      console.tron.log('ManagerCreateShiftScreen unmount');
    };
  });

  React.useEffect(() => {
    if (findStoreFetching === false && findStoreError == null) {
      setStoreDetail(findStorePayload.data);
      // console.tron.log("TCL: ManagerCreateShiftScreen -> findStorePayload.data", findStorePayload.data)
    }
  }, [findStorePayload]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  console.log(jwt);
  const Actions = {
    // TODO : Quan check 2 function again
    onScanSuccess: (params) => {
      const data = _.get(params, 'data', null);
      console.tron.log('debug: ManagerCreateShiftScreen -> data', data);
      // const data = '1805';
      if (!data) return Alert.alert('THÔNG BÁO', 'Không có dữ liệu của hàng');
      setCurrentTime(moment().toISOString());
      // console.log(jwt)
      const { decode } = jwt;
      // console.log(decode)
      decode(data, 'Bitis@123', {
        skipValidation: false, // to skip signature and exp verification
      }).then((decoded) => {
        console.log(decoded);
        const { payload: { data } } = decoded;
        setdataDecoded(data);
        dispatch(ShiftFindstoreActions.shiftFindstoreRequest({ store_uuid: data }));
      }).catch((error) => {
        alert(error);
      });
    },

    /**
     Use case :
     1. First check data from scan has exist
     2. Check store_uuid in AsyncStoragem, if null => first time user login , setItem store_uuid
     3. Compare store_uuid in AsyncStorage with store_uuid in reponse from service
     => if not equal => login flow again
     => if equal => navigation to next screen
     */
    onPressContinue: (params) => {
      // console.tron.log('TCL: DataDecoded -> data', dataDecoded);
      // if (_.isEmpty(storeDetail)) return alert('Không có thông tin cửa hàng, vui lòng quét lại mã');
      // const current_store_uuid = await AsyncStorage.getItem('storeUuid');
      // if (current_store_uuid == null) {
      //   await AsyncStorage.setItem('storeUuid', storeDetail.store_uuid);
      // } else
      if (storeUuidDefault !== dataDecoded) {
        return Alert.alert('THÔNG BÁO', 'Mã cửa hàng mà bạn đang quét khác với mã cửa hàng mà bạn đăng nhập, vui lòng đăng nhập lại để cập nhật thông tin mới nhất!');
      }
      props.navigation.navigate(screensName.ManagerCreateShiftScreen, { currentTime, storeDetail });
      console.tron.log('TCL: ManagerCreateShiftScreen -> storeDetail', storeDetail);
      // if (from_dashboard) props.navigation.navigate(screensName.BottomManagerRouter);
    },
    onPressBackButton: () => props.navigation.goBack(),
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const store_name = _.get(findStorePayload, ['data', 'store_name'], 'Tên cửa hàng');
  // const scan_time = currentTime ? moment(currentTime).format('HH:mm') : '';
  const renderProps = {
    initialRender,
    store_name,
    info_name: store_name,
    reactivate,
    current_time: currentTime,
    title,
    setReactivate,
    continueText: 'Xác nhận thông tin',
    Actions,
  };

  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerCreateShiftScreen render');
  // declare variables
  const {
    initialRender,
    info_name,
    reactivate,
    current_time,
    title,
    setReactivate,
    continueText,
    Actions,
  } = props;
  if (initialRender) return <LoadingInteractions size="large" />;

  return (

    <View style={styles.container}>
      <NavigationEvents
        onWillFocus={(payload) => setReactivate(true)}
        onDidBlur={(payload) => setReactivate(false)}
      />
      {reactivate && (
      <ScanQRCodeComponent
        onScanSuccess={Actions.onScanSuccess}
        reactivate={false}
        detailShiftText={null}
        visibleStore
      />
      )}

      <View style={styles.content}>
        <View style={styles.block_top}>
          <View style={styles.btn_back}>
            <CircleBackButton onPress={Actions.onPressBackButton} />
          </View>
          <Text style={styles.text_title}>{title}</Text>
        </View>
        <View style={styles.block_bottom}>
          <View style={styles.block_shift}>
            <View style={styles.block_shift_time}>
              <Text style={styles.text_scan_time}>Giờ quét</Text>
              <Text style={styles.text_time}>{current_time ? moment(current_time).format('HH:mm') : '--:--'}</Text>
            </View>
            <View style={styles.block_store}>
              <Text style={styles.text_store}>{info_name}</Text>
            </View>
          </View>

          <View style={styles.block_btn_bottom}>
            <ButtonHandle
              buttonText={continueText}
              onPressButton={Actions.onPressContinue}
              styles={{ backgroundColor: Colors.orange1 }}

            />
          </View>
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  // return compareOmitState(nextProps, prevProps, ['Actions']);
  return false;
}


// Sample data
const scanSuccess = {
  data: '1805',
};
