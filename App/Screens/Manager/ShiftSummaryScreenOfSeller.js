/* eslint-disable no-mixed-operators */

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, Image, Alert, ScrollView, ImageBackground, Button, Animated, Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import moment from 'moment';

import { widthPercentageToDP } from 'react-native-responsive-screen';
import screensName from '../../Navigation/Screens';
import { Feather } from '../../Components/Icons';
import { Metrics, Images, Colors } from '../../Themes';
import DoubleButton from '../../Components/Button/DoubleButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import TextInfo from '../../Components/TextInput/TextInfo';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';
import BackButtonsTopBar from '../../Components/Button/BackButtonsTopBar';

// Styles
import styles from './Styles/SummaryAttendanceScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import AttendanceSubmitActions, { attendanceSubmitSelectors } from '../../Redux/attendance/AttendanceSubmitRedux';
import AttendanceMonthSellerActions, { AttendanceMonthSellerSelectors } from '../../Redux/attendance/AttendanceMonthSellerRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';

// utils
import { compareOmitState } from '../../Services/utils';
// import { ScrollView } from 'react-native-gesture-handler';
const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
ShiftSummaryScreenOfSeller.navigationOptions = navigationOptions;

/**
 * Main
 */

export default function ShiftSummaryScreenOfSeller(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [isModalFlag, setIsModalFlag] = React.useState(false);
  const dataEmployee = props.navigation.getParam('dataInfo', {});
  console.tron.log('debug: dataEmployee', dataEmployee);


  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const roleTypeUser = _.get(mePayload, ['roles']);
  const roleseller = _.find(roleTypeUser, (i) => i === 'seller');
  const attendanceSubmitFetching = useSelector((state) => attendanceSubmitSelectors.selectFetching(state.attendance.attendanceSubmit));
  const attendanceSubmitError = useSelector((state) => attendanceSubmitSelectors.selectError(state.attendance.attendanceSubmit));
  const attendanceSubmitPayload = useSelector((state) => attendanceSubmitSelectors.selectPayload(state.attendance.attendanceSubmit));
  const attendanceSellerPayload = useSelector((state) => AttendanceMonthSellerSelectors.selectPayload(state.attendance.attendanceMonthSeller), {});
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeName = _.get(findStoreSellerPayload, ['store_name'], '');
  const storeUuid = _.get(findStoreSellerPayload, ['store_uuid'], undefined);

  const firstName = _.get(dataEmployee, 'firstName', '');
  const lastName = _.get(dataEmployee, 'lastName', '');
  const name = _.concat([lastName, firstName ? ' ' : '', firstName]);


  // effects
  React.useEffect(() => {
    console.tron.log('SCREEN KHÁC effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    return () => { };
  }, [/** input */]);
  React.useEffect(() => {
    if (attendanceSubmitFetching === false && attendanceSubmitError == null && isModalFlag === true) {
      setIsModalVisible(!isModalVisible);
    }
  }, [attendanceSubmitPayload]);
  React.useEffect(() => {
    dispatch(AttendanceMonthSellerActions.AttendanceMonthSellerRequest({
      userUuid: dataEmployee.username,
      storeUuid,
    }));
  }, []);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSendSummary: () => {
      if (!attendanceSellerPayload) {
        return Alert.alert('Hệ thống đang cập nhật vui lòng thử lại sau ít phút!');
      }
      if (!attendanceSellerPayload.fromDate || !attendanceSellerPayload.toDate) {
        return Alert.alert('Hệ thống đang cập nhật vui lòng thử lại sau ít phút!');
      }
      const dataAtendancePut = {
        userUuid: dataEmployee.username,
        fromDate: attendanceSellerPayload.fromDate,
        toDate: attendanceSellerPayload.toDate,
        storeUuid,
      };
      console.tron.log('debug: SellerStaffSummaryScreen -> dataAtendancePut', dataAtendancePut);
      dispatch(AttendanceSubmitActions.attendanceSubmitRequest(dataAtendancePut));
    },
    toggleModal: () => {
      setIsModalVisible(!isModalVisible);
      setIsModalFlag(true);
    },
    onPressGoBack: () => {
      props.navigation.goBack();
    },
    onPressHistory: () => {
      props.navigation.navigate(screensName.HistoryScreen, { staffData: dataEmployee });
    },

  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    isModalVisible,
    scrollY,
    setScrollY,
    ...props,
    attendanceSubmitPayload,
    attendanceSellerPayload,
    storeName,
    dataEmployee,
    name,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SummaryAttendanceScreen render');
  const {
    Actions,
    isModalVisible,
    scrollY,
    attendanceSubmitPayload,
    attendanceSellerPayload,
    storeName,
    dataEmployee,
    name,
  } = props;
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.blue1],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };

  return (
    <View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressHistory}>
            <View>
              <AnimatedFeather
                name="clock"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>

        </View>
      </Animated.View>
      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: 24,
        }}
        >
          {/* <View> */}
          <View style={styles.part1}>
            <Text style={styles.title}>Bảng chấm công tháng</Text>
          </View>

          <View style={styles.part2}>
            <TextInfo
              Text1="Nhân viên"
              Text2={name}
            />
            <TextInfo
              Text1="Tên cửa hàng"
              Text2={storeName || 'Đang cập nhật'}
            />
            <TextInfo
              Text1="Ngày bắt đầu"
              Text2={(attendanceSellerPayload && moment(attendanceSellerPayload.fromDate).format('DD/MM/YYYY')) || 'Chưa cập nhật'}
            />
            <TextInfo
              Text1="Ngày kết thúc"
              Text2={attendanceSellerPayload && moment(attendanceSellerPayload.toDate).format('DD/MM/YYYY') || 'Chưa cập nhật'}
            />
            <TextInfo
              Text1="Số ca làm việc"
              Text2={attendanceSellerPayload && attendanceSellerPayload.numberOfShifts || 'Chưa cập nhật'}
            />
            <TextInfo
              Text1="Số giờ làm việc"
              Text2={attendanceSellerPayload ? Math.round(attendanceSellerPayload.numberOfTime * 10) / 10 : 'Chưa cập nhật'}
            />
            <TextInfo
              Text1="Số ca đi trễ"
              Text2={attendanceSellerPayload && attendanceSellerPayload.numberOfShiftsLate || 0}
            />
            <TextInfo
              Text1="Tổng lương dự kiến"
              Text2={attendanceSellerPayload && attendanceSellerPayload.sumSalaryReference || 'Chưa cập nhật'}
            />
          </View>
        </View>
        <Modal isVisible={isModalVisible}>
          <View style={styles.Modal}>
            <Text style={styles.modal_title}>Bạn muốn gửi chấm công ?</Text>
            <Text style={styles.modal_content}>Thông tin sau khi gửi sẽ không thể điều chỉnh.</Text>
            <Text style={styles.modal_content1}>Nếu có sai sót, bạn phải báo cho HR điều chỉnh trước khi gửi chấm công</Text>
            <DoubleButton
              styles={styles.cover}
              onPressButton1={Actions.toggleModal}
              buttonText1="Hủy"
              onPressButton2={Actions.onPressSendSummary}
              buttonText2="Gửi thông tin"
            />

          </View>
        </Modal>
        {/* </View> */}
      </ScrollView>
      <View style={styles.btn_bottom}>
        <ButtonHandle
          buttonText="Gửi thông tin"
          onPressButton={Actions.toggleModal}
          disable={attendanceSubmitPayload != null && dataEmployee.username === attendanceSubmitPayload.userUuid}
          styles={{ backgroundColor: (attendanceSubmitPayload && dataEmployee.username === attendanceSubmitPayload.userUuid) ? Colors.greybold : Colors.orange1 }}
        />
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
