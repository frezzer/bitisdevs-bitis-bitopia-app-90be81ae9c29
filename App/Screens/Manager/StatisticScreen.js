
// basics
import _ from 'lodash';
import React, { useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import {
  ScrollView, TouchableOpacity, Text, InteractionManager, View, Image,
} from 'react-native';

// Components
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import MissionList from '../../Components/Mission/MissionList';
import RankTime from '../../Components/Statistic/RankTime';
import LoadingInteractions from '../../Components/LoadingInteractions';
import DoubleButton from '../../Components/Button/DoubleButton';
import RevenueChart from '../../Components/Charts/RevenueChart';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';
import RankEmployee from '../../Components/Statistic/RankEmployee';
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
import image1 from '../../Images/Svgs/1.svg';
import SVGicon from '../../Images/Svgs/Icon';


// Styles
import styles from './Styles/StatisticScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import EmployeeRevenueActions, { EmployeeRevenueSelectors } from '../../Redux/employee/EmployeeRevenueRedux';
import StoreGetStockActions, { StoreGetStockSelectors } from '../../Redux/store/StoreGetStockRedux';
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux';

// utils
import { compareOmitState } from '../../Services/utils';

const navigationOptions = (props) => {
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Tổng quan',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS == 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack} />

        </TouchableOpacity>
      );
    },
  };
};

ManagerStatisticScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function ManagerStatisticScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [DateInPickerVisible, setDateInPickerVisible] = useState(false);
  const [DateOutPickerVisible, setDateOutnPickerVisible] = useState(false);
  const [DatePickerVisible, setDatePickerVisible] = useState(false);
  // const date_time_picker = useRef(null);


  // const _onShowDateTimePicker = (key) => {
  //   setDatePickerVisible(true);
  // };
  // const _hideDateTimePicker = (key) => {
  //   const state = {};
  //   state[`is${key}PickerVisible`] = false;
  //   this.setState({ ...state });
  // };

  // const _handleDatePicked = (val, key) => {
  //   console.log('A date has been picked: ', val);
  //   this.hideDateTimePicker(key);
  // };


  // selector store
  const employee_revenue = useSelector((state) => EmployeeRevenueSelectors.selectPayload(state.employee.revenue));
  const storeStock = useSelector((state) => StoreGetStockSelectors.selectPayload(state.store.stock));
  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const dataChart = _.get(employee_revenue, ['dataCharts']);
  const data = _.map(dataChart, (i) => {
    return (parseInt(i.totalBill) / 1000000);
  });
  const datetime = _.map(dataChart, (item) => {
    return (moment(item.date).format('DD/MM/YYYY')).substr(0, 5);
  });
  const total_Quantity = _.sum(_.map(dataChart, 'totalBillQuantity'));
  const total_Cash = _.sum(_.map(dataChart, 'totalBillCash'));
  const store_uuid = _.get(me_payload, ['employee_id', 'merchant_uuid'], null);

  // effects
  React.useEffect(() => {
    if (initialRender) {
      dispatch(EmployeeRevenueActions.employeeRevenueRequest({
        startTime: moment().subtract(30, 'days').toISOString(),
        endTime: moment().toISOString(),
      }));
      dispatch(loadingSagaActions.loadingIndicatorHide());
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(StoreGetStockActions.storeGetstockRequest({ store_uuid }));
      setTimeout(() => {
        // Navigation.mergeOptions(props.componentId, { ...options({ ...props, onPressRightButton: Actions.onPressRightButton }) });
      }, 100);
    }
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressRightButton: () => {
      // Navigation.push(props.componentId, navComponentScreen({
      // name: screensName.DashBoardScreen
      props.navigation.navigate('MoreScreen');
    },

    onPressCreate: () => {
      props.navigation.navigate(screensName.ManagerScanCreateShiftScreen);
    },
    onPressGetList: () => {
      props.navigation.navigate(screensName.ManagerListShiftScreen);
    },
    onPressButton: () => {
      props.navigation.navigate(screensName.MissionScreen);
    },
    onPressFillter: () => {
      props.navigation.navigate(screensName.FilterScreen);
    },

  };
  // const state = {
  //   date_time_picker,
  //   DatePickerVisible,
  //   DateInPickerVisible,
  //   DateOutPickerVisible,
  // };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */

  const renderProps = {
    initialRender,
    Actions,
    // state,
    data,
    datetime,
    total_Quantity,
    total_Cash,
    storeStock,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerStatisticScreen render');
  // declare variables

  // PROPS
  const {
    initialRender,
    data,
    datetime,
    Actions,
    // state,
    storeStock,
  } = props;

  if (initialRender) return <LoadingInteractions size="large" />;

  // STATE
  // const {
  //   DatePickerVisible,
  //   DateInPickerVisible,
  //   DateOutPickerVisible,
  // } = state;

  // ACTIONS
  // const {
  //   _onShowDateTimePicker,
  //   _hideDateTimePicker,
  //   _handleDatePicked,
  // } = Actions;

  const totalQuantity = _.get(storeStock, '[0].totalquantity', 0);
  const totalValue = _.get(storeStock, '[0].totalamount', 0);


  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.block}>
          <View style={styles.container}>
            {/* <MissionList onPressButton={() => Actions.onPressButton()} /> */}
            <Text style={styles.title1_1}>Thống kê</Text>
            <RankTime />
            <RankEmployee />
            <Text style={styles.title1}>Doanh thu</Text>
            <RevenueChart onPressFillter={Actions.onPressFillter} />
            <Text style={styles.title1}>Lợi nhuận</Text>
            <RevenueChart onPressFillter={Actions.onPressFillter} />
            {/* <Text style={styles.title1}>Tồn Kho</Text> */}
            <View style={styles.block5}>
              {/* <Text style={styles.Text3}>Tồn Kho</Text> */}
              <View style={styles.stockArea}>
                <View style={styles.block_quantity}>
                  <Text style={styles.Text11}>Số lượng tồn kho </Text>
                  <Text style={styles.Text10}>
                    {' '}
                    {Number(totalQuantity).toLocaleString()}
                  </Text>
                  <Text style={styles.Text2}> Sản phẩm </Text>
                </View>
                <View style={styles.block_quantity1}>
                  <Text style={styles.Text11}>Giá trị tồn</Text>
                  <Text style={styles.Text10}>{Number(totalValue).toLocaleString()}</Text>
                  <Text style={styles.Text2}> VNĐ</Text>
                </View>
              </View>
            </View>
            {/* <View style={styles.block6}>

              <DoubleButton
                onPressButton1={Actions.onPressCreate}
                buttonText1='Tạo ca'
                styles2={{ backgroundColor: '#4BB67D' }}
                _styles1={styles.buttonBlueOcean}
                onPressButton2={Actions.onPressGetList}
                buttonText2='QR Code'
              />

            </View> */}

          </View>
        </View>
      </ScrollView>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
// const svgarr = [
//   {
//     svg: require('../../Images/Svgs/1.svg'),
//     type: 'Henu',
//   },

// ];
