import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.silver
  },
  /// BLOCK
  block1: {
    height: wp(40 / 375 * 100),
    justifyContent: 'center',
    backgroundColor: Colors.white,
    marginTop: Metrics.marginTiny
  },

  // TEXT
  Text1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.grayLight,
    fontSize: Fonts.size.textMedium,
    marginLeft: Metrics.marginSmall
  },


})
