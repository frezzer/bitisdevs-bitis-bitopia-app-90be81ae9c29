import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Fonts, Metrics,
} from '../../../Themes';


export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.theme.white,
  },
  block_confirm: {
    ...ApplicationStyles.specialApp.dimensisons(375, 76),
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Metrics.marginXLarge,
    // alignSelf:'center'
  },
  inside: {
    margin: Metrics.marginXLarge,
    // marginHo
  },
  ques: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign: 'center',
    marginBottom: wp(16 / 375 * 100),
    marginTop: Metrics.marginXSmall,

  },
  note: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center',
    // justifyContent:'center'
    alignSelf: 'center',
    color: Colors.graySPLight,
  },
  block_info: {
    // marginTop: Metrics.marginTiny
    // height: Metrics.width,
  },
  title: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: Colors.white,
    justifyContent: 'center',
    paddingHorizontal: Metrics.marginMedium,
  },
  text1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,
  },
  button: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  // ...ApplicationStyles.specialApp.coverBottomTab
  },
  block_hidden: {
  // marginTop:Metrics.marginXSmall,
    ...ApplicationStyles.specialApp.alignCenter,
    marginTop: Metrics.marginTiny,

  // backgroundColor:'red'
  },
  btn_bottom: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100 / 375 * Metrics.width,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.05,
    shadowRadius: 8,
    // android
    elevation: 30,
  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium,
  },
});
