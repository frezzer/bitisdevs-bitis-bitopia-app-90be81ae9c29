import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


export default StyleSheet.create({

  container: {
    // flex: 1,
    // backgroundColor: Colors.blue1,
    width: '100%',
    height: '100%',
  },
  blocked: {
    backgroundColor: 'white',
    flex: 1,
    // marginTop: wp(104 / 375 * 100),
    borderRadius: Metrics.boderLLLarge,
  },
  block_title: {
    ...ApplicationStyles.specialApp.alignCenter,
    marginTop: Metrics.marginXXLarge,

  },
  title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign:'center',
    marginBottom: wp(32 / 375 * 100),

  },
  content: {
    // flex: 1,
    // borderRadius: Metrics.boderLLLarge,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
    width: '100%',
    height: '100%',
  },
  ScrollView: {
    // borderTopLeftRadius: Metrics.boderLLLarge,
    // borderTopRightRadius: Metrics.boderLLLarge,

    flex: 1,
    position: 'absolute',
    width: '100%',
    // justifyContent:'center'
  },
  qrcode: {
    flex: 4,
    // ...ApplicationStyles.specialApp.alignCenter,
    alignItems: 'center',
    paddingTop: wp(32 / 375 * 100),
    backgroundColor: Colors.white,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
  },
  button: {
    // ...ApplicationStyles.specialApp.coverBottomTab
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',

  },
  button_check_in: {
    ...ApplicationStyles.specialApp.dimensisons(343, 44),
    backgroundColor: Colors.blueOcean,
    borderRadius: Metrics.boderNomarl,
    justifyContent: 'center',

  },
  Text12: {
    fontSize: Fonts.size.textXLarge,
  },
  warningBox: {
    ...ApplicationStyles.specialApp.dimensisons(343, 0),
  },

  warningText: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.grayDark,
    fontSize: Fonts.size.textHuge,
    textAlign: 'center',
  },
  valueQrCodeView: {
    ...ApplicationStyles.specialApp.dimensisons(247, 46),
    // backgroundColor:'blue',
    flexDirection: 'row',
    // justifyContent:'space-between',
    marginTop: wp(32 / 375 * 100),
  },
  info: {
    flex: 1,
    // backgroundColor: 'red',
    ...ApplicationStyles.specialApp.alignCenter,
  },
  block_date: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-end',
    // backgroundColor: 'blue',
    // paddingLeft: 1


  },
  valueQrCodeText: {

    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,

  },
  shiftName: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
    fontSize: Fonts.size.textMedium,
    marginTop: Metrics.marginXXTiny,
  },
  shiftNote: {
    fontFamily: Fonts.type.fontBold,
    textAlign: 'right',
    color: Colors.grey9,
    marginTop: Metrics.marginXXTiny,
    fontSize: Fonts.size.textLarge,
    // marginLeft:Metrics.marginXSmall
    // justifyContent:'flex-sta'

    // paddingBottom: wp(32 / 375 * 100)
  },
  block_info: {
    backgroundColor: Colors.white,

  },
  lineStyle: {
    borderWidth: 0.3,
    borderColor: Colors.grey4,
    marginHorizontal: Metrics.marginMedium,
  },
  btn_bottom: {
    backgroundColor: 'white',
    // bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 108 / 375 * Metrics.width,

  },
  block_hidden: {
    marginTop: Metrics.marginXSmall,
  },
  info_shift: {
    ...ApplicationStyles.specialApp.margin(0, 0, 0, 0, 10, 32),
  },
  name_shift: {
    fontFamily: Fonts.type.fontBold,
    textAlign: 'center',
    color: Colors.black,
    marginTop: Metrics.marginTiny,
    fontSize: Fonts.size.textLarge,
  },
  note_shift: {
    fontFamily: Fonts.type.fontRegular,
    textAlign: 'center',
    color: Colors.grey8,
    marginTop: Metrics.marginTiny,
    fontSize: Fonts.size.textMedium,
    paddingBottom: wp(32 / 375 * 100),
  },
  btn_back: {
    position: 'absolute',
    left: Metrics.marginMedium,
    marginTop: wp(44 / 375 * 100),
  },
  Modal: {
    // ...ApplicationStyles.specialApp.dimensisons(343, 686),
    // width: wp(470 / 375 * 100),
    // width:'50%',
    backgroundColor: Colors.white,
    borderRadius: wp(24 / 375 * 100),
  },
  cover: {
    ...ApplicationStyles.specialApp.dimensisons(335, 90),
    borderBottomLeftRadius: wp(24 / 375 * 100),
    borderBottomRightRadius: wp(24 / 375 * 100),
    // bottom:0

  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium,
  },
  // Modal

  block_confirm: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 76),
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Metrics.marginXXLarge,
    // alignSelf:'center'
  },
  inside: {
    margin: Metrics.marginMedium,
    // marginHo
  },
  ques: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXLarge,
    textAlign: 'center',
    marginBottom: Metrics.marginTiny,
  },
  block_hidden1: {
    // marginTop:Metrics.marginXSmall,
    ...ApplicationStyles.specialApp.alignCenter,
    // marginTop: Metrics.marginTiny,

    // backgroundColor:'red'
  },
  note: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center',
    // justifyContent:'center'
    alignSelf: 'center',
    color: Colors.graySPLight,
  },
  containerModal: {
    // flex: 1,
    // backgroundColor: Colors.blue1,

    borderRadius: wp(24 / 375 * 100),

  },
});
