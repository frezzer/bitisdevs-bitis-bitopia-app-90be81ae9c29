import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.theme.block,
  },
  content: {
    flex: 1,
    ...ApplicationStyles.specialApp.alignCenter,
  },

  part1: {
    marginTop: Metrics.marginTiny,
    backgroundColor: Colors.white,

  },
  title: {
    ...ApplicationStyles.specialApp.dimensisons(375, 44),
    backgroundColor: Colors.block,
    justifyContent: 'center',
    paddingHorizontal: Metrics.marginMedium,
  },
  text1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    marginTop: 18,
    // marginBottom: 16

  },
  text1_1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    marginTop: 16,
    paddingBottom: Metrics.marginSmall,

    // marginBottom: 16

  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,
  },
  part2: {
    marginTop: Metrics.marginTiny,
    backgroundColor: Colors.white,
    // marginBottom:Metrics.marginTiny
  },
  Block_Detail: {
    height: wp(44 / 375 * 100),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.white,
    // paddingHorizontal: Metrics.paddingMedium,
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 0.3,
    marginHorizontal: Metrics.marginMedium,
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
    fontSize: Fonts.size.textMedium,
  },
  Text3: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    marginRight: Metrics.marginLarge,
    textAlign: 'left',

  },
  descriptionInput: {
    fontSize: Fonts.size.textLarge,
    color: 'black',
    textAlign: 'right',
  },
  bottomTab: {
    ...ApplicationStyles.specialApp.coverBottomTab,
  },
  confirm_button: {
    backgroundColor: 'white',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100 / 375 * Metrics.width,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 8,
    // android
    elevation: 30,
  },
  picker: {
    ...Platform.select({
      ios: {
        // backgroundColor: 'red',
        marginRight: -wp( 8/ 375 * 100),
      },
      android: {
        width: '100%',
        paddingLeft: wp(193 / 375 * 100),
      },
    }),
  },
  textInfo: {
    height: wp(44 / 375 * 100),
    backgroundColor: Colors.red,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderTopWidth: 0.3,
    // borderBottomWidth: 0.3,
    // borderBottomColor: Colors.grey4,
    // borderTopColor: Colors.grey4,
    marginHorizontal: Metrics.paddingMedium,
  },


});

export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    ...ApplicationStyles.specialApp.dimensisons(130, 44),
    fontSize: Fonts.size.textLarge,
    ...ApplicationStyles.specialApp.padding(0, 0, 62, 0),
    // textAlign:'right',
    justifyContent: 'center',
    color: 'black',

  },
  inputAndroid: {
    // ...ApplicationStyles.specialApp.dimensisons(200, 44),
    fontSize: Fonts.size.textLarge,
    // backgroundColor:'red',
    color: 'black',
    // justifyContent:'center',
    width: '100%',
    // textAlign:'left',
    // alignItems:'center',
    // paddingRight:Metrics.paddingXLarge
    paddingRight: 100,


  },

});
