/* eslint-disable no-mixed-operators */
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  header: {
    // backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    // marginTop: Metrics.marginTiny,
    borderBottomColor: Colors.background,
    borderBottomWidth: 1,
  },
  btn_bottom: {
    backgroundColor: 'white',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: wp(88 / 375 * 100),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    // android
    elevation: 10,
  },
  // Text

  footer: {
    backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    marginTop: Metrics.marginTiny,
    borderBottomColor: Colors.block,
    borderBottomWidth: 1,
    ...ApplicationStyles.specialApp.alignCenter,
    marginBottom: Metrics.marginTiny,
  },
  title: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.grey8,
  },
  title_footer: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.blueOcean,

  },
  item: {
    // marginLeft: -Metrics.marginXSmall
    // ...ApplicationStyles.specialApp.dimensisons(375,62),
    // marginVertical:Metrics.paddingXXSmall


    // paddingHorizontal : 16,


  },
  divider: {
    // borderBottomColor: Colors.black,
    // borderBottomWidth: 1,
    // backgroundColor:'red',
  },
  status: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.green1,
    fontStyle: 'normal',
    marginTop: Metrics.marginXTiny,
  },
  status1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.orange1,
    fontStyle: 'normal',
    marginTop: Metrics.marginXTiny,
  },
  // block_footer: {
  //   backgroundColor: Colors.white,
  //   padding: Metrics.paddingMedium,
  //   marginTop: Metrics.marginTiny,
  //   borderBottomColor: Colors.block,
  //   borderBottomWidth: 1
  // },
  // content:{

  // }
  headerRightView: {
    backgroundColor: 'red',
    // paddingRight: 100
  },
  noShift:{
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.textMedium,
    color: Colors.grey9,
    marginTop: wp(20/375*100),
    textAlign:'center'
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },

});
