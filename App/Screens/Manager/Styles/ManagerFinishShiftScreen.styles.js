
import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes';

const backButtonWidth = 44;
const confirmButtonHeight = 44;

export default StyleSheet.create({
  container: {
    backgroundColor: 'black'
  },

  QRcodeview: {
    position: 'absolute'
  },

  blackBackground: {
...ApplicationStyles.specialApp.dimensisonsPercentage(),
    position: 'absolute'
  },

  implement: {
    ...ApplicationStyles.specialApp.dimensisonsPercentage(),
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },

  topBar: {
    width: '100%',
    height: (Metrics.navigationBarHeight + 16) / 375 * Metrics.width,
    flexWrap: 'wrap-reverse',
    flexDirection: "row",
    justifyContent: 'flex-start',
    // backgroundColor: 'red'
  },

  bottomTabs: {
    width: '100%',
    height: 108 / 375 * Metrics.width,
    alignSelf: 'flex-end',
    ...ApplicationStyles.specialApp.alignCenter,
  },

  backButton: {
    ...ApplicationStyles.specialApp.dimensisons(44,44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderMaxium,
    margin: Metrics.marginSmall,
  },

  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44,44),
    ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.white,
    borderRadius: Metrics.boderMaxium,
    opacity: 0.1,
    alignSelf: 'center',
    position: 'absolute'
  },

  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },

  titleView: {
    width: (375 - (backButtonWidth + Metrics.marginSmall * 2) * 2) / 375 * Metrics.width,
    height: 44 / 375 * Metrics.width,
    marginVertical: Metrics.marginSmall,
    ...ApplicationStyles.specialApp.alignCenter,
  },
  titleText: {
    paddingTop: Metrics.paddingXTiny,
    textAlign: 'center',
    fontSize: Fonts.size.textXXLarge,
    fontWeight: 'bold',
    color: Colors.white,
  },



  confirmButton: {
    ...ApplicationStyles.specialApp.dimensisons(343,44),
    ...ApplicationStyles.specialApp.alignCenter,
    alignSelf: 'center',
    margin: Metrics.marginSmall,
  },

  confirmButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(343,44),
    ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.white,
    borderRadius: Metrics.boderSmall,
    opacity: 0.1,
    alignSelf: 'center',
    position: 'absolute'
  },
  confirmButtonText: {
    textAlign: 'center',
    fontSize: Fonts.size.textLarge,
    fontWeight: 'bold',
    color: Colors.white,
  },

  infoblock: {
    ...ApplicationStyles.specialApp.dimensisons(343,100),
    ...ApplicationStyles.specialApp.alignCenter,
    alignSelf: 'center',
  },

  infoBlockDetail: {
    flexDirection: 'row',
  },

  infoblockbackground: {
    ...ApplicationStyles.specialApp.dimensisons(343,100),
    ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.white,
    borderRadius: Metrics.boderSmall,
    opacity: 0.1,
    alignSelf: 'center',
    position: 'absolute',
  },

  rectangleContainer: {
    flex: 1,
    ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: "transparent"
  },

  rectangle: {
    ...ApplicationStyles.specialApp.dimensisons(303,303),
    ...ApplicationStyles.specialApp.alignCenter,
    borderWidth: 3,
    borderColor: Colors.white,
    backgroundColor: "transparent"
  },

  topOverlay: {
    ...ApplicationStyles.specialApp.dimensisons(375,375),
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.7,
  },

  bottomOverlay: {
    ...ApplicationStyles.specialApp.dimensisons(375,375),
    flex: 3, // 6
  },

  bottomOverlayBg: {
...ApplicationStyles.specialApp.dimensisonsPercentage(),
    backgroundColor: 'black',
    opacity: 0.7,
    position: 'absolute',
  },

  leftAndRightOverlay: {
    height: 303 / 375 * Metrics.width,
    width: (Metrics.width - 303 / 375 * Metrics.width) / 2,
    backgroundColor: 'black',
    opacity: 0.7,
  },

  TextAlert: {
    margin: Metrics.marginMedium,
    textAlign: 'center',
    fontSize: Fonts.size.textSmall,
    fontStyle: 'italic',
    color: Colors.white,
  },

})
