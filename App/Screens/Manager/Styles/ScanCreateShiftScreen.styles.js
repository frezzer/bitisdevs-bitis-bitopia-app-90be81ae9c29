
import { StyleSheet, Platform } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
    
const backButtonWidth = 44;
const confirmButtonHeight = 44;

export default StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1
  },

  content: {
    ...ApplicationStyles.specialApp.dimensisonsPercentage(),
  },

  // topBar: {
  //   width: '100%',
  //   height: (Metrics.navigationBarHeight + 16) / 375 * Metrics.width,
  //   flexWrap: 'wrap-reverse',
  //   flexDirection: "row",
  //   justifyContent: 'flex-start',
  // },

  // bottomTabs: {
  //   width: '100%',
  //   height: 108 / 375 * Metrics.width,
  //   alignSelf: 'flex-end',
  //   ...ApplicationStyles.specialApp.alignCenter
  // },

  // backButton: {
  //   ...ApplicationStyles.specialApp.dimensisons(44, 44),
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   borderRadius: Metrics.boderMaxium,
  //   margin: Metrics.marginSmall,
  // },

  // backButtonTouchable: {
  //   ...ApplicationStyles.specialApp.dimensisons(44, 44),
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderMaxium,
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   position: 'absolute'
  // },

  // backButtonButtonText: {
  //   paddingTop: Metrics.paddingXTiny,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textHuge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  //   // position: 'absolute',
  // },

  // titleView: {
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   width: (375 - (backButtonWidth + Metrics.marginSmall * 2) * 2) / 375 * Metrics.width,
  //   height: 44 / 375 * Metrics.width,
  //   marginVertical: Metrics.marginSmall,
  // },
  // titleText: {
  //   paddingTop: Metrics.paddingXTiny,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textXXLarge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  // },



  // confirmButton: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 44),
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   margin: Metrics.marginSmall,
  // },

  // confirmButtonTouchable: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 44),
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderSmall,
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   position: 'absolute'
  // },
  // confirmButtonText: {
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textLarge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  // },

  // infoblock: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 100),
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter
  // },

  // infoBlockDetail: {
  //   flexDirection: 'row',
  // },

  // infoblockbackground: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 100),
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderSmall,
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   position: 'absolute',
  // },

  // rectangleContainer: {
  //   flex: 1,
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: "transparent"
  // },

  // rectangle: {
  //   ...ApplicationStyles.specialApp.dimensisons(303, 303),
  //   borderWidth: 3,
  //   borderColor: Colors.white,
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: "transparent"
  // },

  // topOverlay: {
  //   flex: 1,
  //   ...ApplicationStyles.specialApp.dimensisons(375, 375),
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  // },

  // bottomOverlay: {
  //   flex: 3, // 6
  //   ...ApplicationStyles.specialApp.dimensisons(375, 375),
  // },

  // bottomOverlayBg: {
  //   ...ApplicationStyles.specialApp.dimensisonsPercentage(),
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  //   position: 'absolute',
  // },

  // leftAndRightOverlay: {
  //   height: 303 / 375 * Metrics.width,
  //   width: (Metrics.width - 303 / 375 * Metrics.width) / 2,
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  // },

  // TextAlert: {
  //   margin: Metrics.marginMedium,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textSmall,
  //   fontStyle: 'italic',
  //   color: Colors.white,
  // },


  // // styles
  // block_product: {
  //   backgroundColor: 'blue',
  //   // position: 'absolute',
  //   // top: 50,
  //   width: '100%',
  //   // bottom: 120,
  //   padding: 16,
  //   marginBottom: Metrics.marginLarge
  // }

  block_top: {
    width: '100%',
    // height: wp(88 / 375 * 100),
    height: Metrics.navigationBarHeight,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingVertical: Metrics.paddingXSmall,
  },
  btn_back: {
    position: 'absolute',
    left: Metrics.marginMedium,
  },

  block_bottom: {
    width: Metrics.width,
    position: 'absolute',
    bottom: 0
  },

  block_shift: {
    backgroundColor: 'green',
    marginHorizontal: Metrics.marginMedium,
    marginBottom: Metrics.height < 670 ? 0 : Metrics.marginXHuge,
    paddingHorizontal: Metrics.paddingHuge,
    padding: Metrics.width < 330 ? Metrics.paddingLarge : Metrics.paddingHuge,
    borderRadius: Metrics.boderNomarl,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    flexDirection: 'row'
  },

  block_shift_time: {
    alignItems: 'center'
  },

  block_store: {
    justifyContent: 'center',
    flex: 1,
    paddingHorizontal: Metrics.paddingSmall,
  },

  block_btn_bottom: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: Metrics.marginXXLarge,
  },

  btn_bottom: {
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
  },

  // Text
  text_title: {
    fontSize: Fonts.size.textXXLarge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  text_scan_time: {
    fontSize: Fonts.size.textSmall,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.white,
  },
  text_time: {
    fontSize: Fonts.size.textHuge,
    color: Colors.white,
  },
  text_store: {
    fontSize: Fonts.size.textMedium,
    color: Colors.white,
    fontFamily: Fonts.type.fontBold,
    textAlign: 'center',
  },
  text_detail_shift: {
    marginTop: Metrics.marginXTiny,
    textAlign: 'center',
    fontSize: Fonts.size.textSmall,
    fontStyle: 'italic',
    color: Colors.white,
  },
  text_confirm_btn: {
    textAlign: 'center',
    fontSize: Fonts.size.textLarge,
    fontWeight: 'bold',
    color: Colors.white,
  },
})
