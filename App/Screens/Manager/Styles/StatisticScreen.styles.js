import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

const quantity_area = {
  backgroundColor: Colors.theme.block,
  borderRadius: Metrics.boderNomarl,
  borderColor: Colors.blue2,
  borderWidth: 1,

};


export default StyleSheet.create({
  block: {
    backgroundColor: Colors.theme.block,
  },

  container: {
    flex: 1,
    backgroundColor: Colors.block,
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blue2,
    textAlign: 'center',
  },
  Text3: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginBottom: Metrics.marginXSmall,

  },
  block5: {
    ...ApplicationStyles.specialApp.padding(16, 0, 0, 0, 0, 18),
    height: wp(146 / 375 * 100),
    backgroundColor: Colors.white,
    // marginBottom: Metrics.marginXSTiny,


  },
  block_quantity: {
    ...ApplicationStyles.specialApp.dimensisons(168, 92),
    ...quantity_area,
    ...ApplicationStyles.specialApp.alignCenter,
    marginRight: Metrics.marginTiny,
    justifyContent: 'center',
    backgroundColor:'white',
    // margin: 5,
  },
  block_quantity1: {
    ...ApplicationStyles.specialApp.dimensisons(168, 92),
    ...ApplicationStyles.specialApp.alignCenter,
    ...quantity_area,
    justifyContent: 'center',
    backgroundColor:'white'

  },
  Text10: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.blue2,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center',
    marginVertical: wp(5/375*100)
    
  },
  Text11: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center',
  },
  block6: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonBlueOcean: {
    backgroundColor: Colors.blueOcean,
  },

  stockArea: {
    flexDirection: 'row',
    ...ApplicationStyles.specialApp.alignCenter,
    paddingHorizontal: Metrics.paddingXLarge,
  },
  block4_1: {
    alignItems: 'center',
    // marginTop: Metrics.marginSmall,
    position: 'absolute',
  },
  Text2_1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blueOcean,
    textAlign: 'center',
  },
  // new
  title1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    color: Colors.grey8,
    paddingTop: Metrics.paddingHuge,
    paddingLeft: Metrics.paddingMedium,
    paddingBottom: Metrics.paddingMedium,
  },
  title1_1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    color: Colors.grey8,
    paddingTop: Metrics.paddingHuge,
    paddingLeft: Metrics.paddingMedium,
    paddingBottom: Metrics.paddingXSmall,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
  // profit:{
  //   backgroundColor:'red',
  //   marginTop : 50
  // }
});
