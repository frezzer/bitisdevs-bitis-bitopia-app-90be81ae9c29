import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { margin } from 'polished';
import {
  Metrics, Fonts, Colors, ApplicationStyles,
} from '../../../Themes';

export default StyleSheet.create({
  title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign: 'center',
    marginTop: wp(12 / 375 * 100),
    marginBottom: wp(16 / 375 * 100),
  },
  container: {
    flex: 1,
  },
  cover: {

    // ...ApplicationStyles.specialApp.dimensisons(323, 100),
    ...ApplicationStyles.specialApp.dimensisons(343, 100),
    borderBottomLeftRadius: Metrics.boderLLLarge,
    borderBottomRightRadius: Metrics.boderLLLarge,
    // backgroundColor:'red',
    // paddingHorizontal: 10

  },

  content: {
    flex: 1,
    backgroundColor: Colors.red,
    position: 'absolute',
    // marginTop: wp(16 / 375 * 100),
    // width:'100%',
    // height:'100%',
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
    // zIndex: 2
  },
  part1: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
    // marginBottom: Metrics.marginTiny,
    marginTop: Metrics.paddingXsLarge
  },
  part2: {
    backgroundColor: Colors.white,
  },
  block_element: {
    paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingSmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderBottomColor: Colors.block,
    // borderBottomWidth: 1,
  },
  // btn_bottom: {
  //   // position:'absolute',
  //   // bottom:0,
  //   backgroundColor: Colors.white,
  //   height: wp(108 / 375 * 100),
  //   width: Metrics.width,
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   shadowColor: '#000',
  //   elevation: 1,
  //   position: 'absolute',
  //   bottom: 0,
  //   shadowOffset: {
  //     width: 0,
  //     height: 0,
  //   },
  //   shadowOpacity: 0.15,
  //   shadowRadius: 8,
  //   elevation: 30,
  // },
  // Text
  text1: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
  },
  text2: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
  },
  blocked: {
    width: '100%',
    height: wp(300 / 375 * 100),
    // height: '100%',
    backgroundColor: Colors.blue1,
    // zIndex: 1

  },
  Modal: {
    // ...ApplicationStyles.specialApp.dimensisons(343, 234),
    width:  wp(343 / 375 * 100),
    backgroundColor: Colors.white,
    borderRadius: wp(16 / 375 * 100),
    marginLeft: -wp(2 / 375 * 100),

  },
  modal_title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign: 'center',
    marginTop: Metrics.marginXXLarge,
  },
  modal_content: {
    textAlign: 'center',
    marginTop: Metrics.marginTiny,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
  },
  modal_content1: {
    textAlign: 'center',
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
  },
  btn_bottom: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 108 / 375 * Metrics.width,

  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium,
  },
  lineStyle: {
    borderBottomWidth: 0.3,
    borderColor: Colors.grey4,
    // width:311
    marginHorizontal: Metrics.marginMedium,
    // backgroundColor: 'white',
    // flex: 1,
    // paddingVertical: Metrics.paddingXXSmall,
  },
  // lineStyle1: {
  //   borderWidth: 0.3,
  //   borderColor: Colors.grey4,
  //   // width:311
  //   marginHorizontal: Metrics.marginMedium,
  //   backgroundColor: Colors.silver,
  //   // flex: 1,
  //   // paddingVertical: Metrics.paddingXXSmall,
  // },
});
