'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import { Dimensions, InteractionManager, View, Text, Platform, TouchableOpacity, Image, SectionList } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens'
import { Metrics, Images } from '../../Themes'
import ButtonHandle from '../../Components/Button/ButtonHandle'
// Styles
import styles from './Styles/AttendanceInfoScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux'

// utils
import { compareOmitState } from "../../Services/utils";
import moment from 'moment'
/**
 * Main
 */

const onPressBackButton = (navigation) => {
  console.tron.log("TCL: onPressBackButton -> nav111111igation", navigation)
  navigation.goBack()
}

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float'
    }),
    title: 'Thông tin điểm danh',
    headerLeft: () => {
      return <TouchableOpacity
        style={{
          paddingLeft: Platform.OS == 'android' ? 18 : 5,
          paddingBottom: Metrics.paddingXTiny,
        }}
        onPress={() => onPressBackButton(props.navigation)}
      >
        <Image source={Images.buttonBack} />
      </TouchableOpacity>
    },
  };
}

AttendanceInfoScreen.navigationOptions = navigationOptions;

export default function AttendanceInfoScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [selectedStartDate, setselectedStartDate] = React.useState(null)
  const [selectedEndDate, setselectedEndDate] = React.useState(null)


  // effects
  React.useEffect(() => {
    console.tron.log('AttendanceInfoScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressConfirmAttendance: () => props.navigation.navigate(screensName.SaleStatisticScreen)
  }
  const state = {
    selectedStartDate,
    selectedEndDate
  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
  }
  return <Render {...renderProps} />
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('AttendanceInfoScreen render');
  const {
    initialRender,
    Actions
  } = props;

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.part1}>
          <Text style={styles.text1}>Ca sáng: 12/8/2019</Text>
          <Text style={styles.text2}>Tên ca đã tạo ở đây</Text>
          <Text style={styles.text3}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
            </Text>
        </View>

        <View style={styles.part2}>
          <View style={styles.block_element}>
            <Text style={styles.text4}>Người tạo ca</Text>
            <Text style={styles.text4}>Trần Xuân Hoàng</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text4}>Tên cửa hàng</Text>
            <Text style={styles.text4}>Trần Xuân Hoàng</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text4}>Thời điểm điểm danh</Text>
            <Text style={styles.text4}>Trần Xuân Hoàng</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text4}>Giờ bắt đầu</Text>
            <Text style={styles.text4}>Trần Xuân Hoàng</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text4}>Giờ kết thúc</Text>
            <Text style={styles.text4}>Trần Xuân Hoàng</Text>
          </View>
        </View>

        <View style={styles.btn_bottom}>
          <ButtonHandle
            buttonText="Xác nhận điểm danh"
            onPressButton={Actions.onPressConfirmAttendance}
          />
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}