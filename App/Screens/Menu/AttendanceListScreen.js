'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import { Dimensions, InteractionManager, View, Text, Platform, 
  TouchableOpacity, Image, SectionList, Alert } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens'
import { Metrics, Images } from '../../Themes'
import BlockSingle from '../../Components/Block/BlockSingle'
import ButtonHandle from '../../Components/Button/ButtonHandle'
// Styles
import styles from './Styles/AttendanceListScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux'
// Navigation
// import { Navigation } from '../../Navigation/Navigation'
// import { hideBottomTabsOptions, hideTopbarOptions } from '../../Navigation/Options';

// utils
import { compareOmitState } from "../../Services/utils";
import { SafeAreaView } from 'react-navigation';
const { width } = Dimensions.get('window');
import moment from 'moment'
/**
 * Main
 */

const onPressBackButton = (navigation) => {
  navigation.popToTop()
}

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float'
    }),
    title: 'Danh sách điểm danh',
    headerLeft: () => {
      return <TouchableOpacity style={{ paddingLeft: 5, paddingBottom: Metrics.paddingXTiny }}
        onPress={() => onPressBackButton(props.navigation)}
      >
        <Image source={Images.buttonBack} />

      </TouchableOpacity>
    },
  };
}

AttendanceListScreen.navigationOptions = navigationOptions;

export default function AttendanceListScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [selectedStartDate, setselectedStartDate] = React.useState(null)
  const [selectedEndDate, setselectedEndDate] = React.useState(null)


  // effects
  React.useEffect(() => {
    console.tron.log('AttendanceListScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSummaryAttendance: () => props.navigation.navigate(screensName.SummaryAttendanceScreen),
    onPressGoInfo: (data) => props.navigation.navigate(screensName.AttendanceInfoScreen)
  }
  const state = {
    selectedStartDate,
    selectedEndDate
  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    sample_data
  }
  return <Render {...renderProps} />
}

function Item({ data, actions }) {
  return (
    <View style={styles.item}>
      <BlockSingle data_item={data} onPress={actions.onPressGoInfo} />
    </View>
  );
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('AttendanceListScreen render');
  const {
    initialRender,
    Actions,
    sample_data
  } = props;

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <SectionList
          sections={sample_data}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item }) => <Item data={item} actions={Actions} />}
          renderSectionHeader={({ section: { title } }) => (
            <View style={styles.header}>
              <Text style={styles.title}>{title}</Text>
            </View>
          )}
        />
        <View style={styles.btn_bottom}>
          <ButtonHandle 
            buttonText="Chốt điểm danh trong tháng"
            onPressButton={Actions.onPressSummaryAttendance}
          />
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}

// Sample data
const sample_data = [
  {
    title: 'Hôm nay',
    data: ['Pizza', 'Burger'],
  },
  {
    title: 'Hôm qua',
    data: ['French Fries', 'Onion Rings'],
  },
  {
    title: '02/11/2019',
    data: ['Water', 'Coke'],
  },
  {
    title: '01/11/2019',
    data: ['Cheese Cake', 'Ice Cream'],
  },
];