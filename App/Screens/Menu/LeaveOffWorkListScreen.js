'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import { InteractionManager, View, Text, Platform, TouchableOpacity, Image, SectionList } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens'
import { Metrics, Images } from '../../Themes'
import BlockSingle from '../../Components/Block/BlockSingle'
import ButtonHandle from '../../Components/Button/ButtonHandle'
// Styles
import styles from './Styles/LeaveOffWorkListScreen.styles';
// Actions and Selectors Redux

// utils
import { compareOmitState } from "../../Services/utils";
import moment from 'moment'
/**
 * Main
 */

const onPressBackButton = (navigation) => {
  navigation.popToTop()
}

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float'
    }),
    title: 'Danh sách tan ca',
    headerLeft: () => {
      return <TouchableOpacity
        style={{
          paddingLeft: Platform.OS == 'android' ? 18 : 5,
          paddingBottom: Metrics.paddingXTiny,
        }}
        onPress={() => onPressBackButton(props.navigation)}
      >
        <Image source={Images.buttonBack} />

      </TouchableOpacity>
    },
  };
}

LeaveOffWorkListScreen.navigationOptions = navigationOptions;

export default function LeaveOffWorkListScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [selectedStartDate, setselectedStartDate] = React.useState(null)
  const [selectedEndDate, setselectedEndDate] = React.useState(null)


  // effects
  React.useEffect(() => {
    console.tron.log('LeaveOffWorkListScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressGoInfo: (data) => props.navigation.navigate(screensName.LeaveOffWorkInfoScreen)
  }
  const state = {
    selectedStartDate,
    selectedEndDate
  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    sample_data
  }
  return <Render {...renderProps} />
}

function Item({ data, actions }) {
  return (
    <View style={styles.item}>
      <BlockSingle data_item={data} onPress={actions.onPressGoInfo} />
    </View>
  );
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('LeaveOffWorkListScreen render');
  const {
    initialRender,
    Actions,
    sample_data
  } = props;

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <SectionList
          sections={sample_data}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item }) => <Item data={item} actions={Actions} />}
          renderSectionHeader={({ section: { title } }) => (
            <View style={styles.header}>
              <Text style={styles.title}>{title}</Text>
            </View>
          )}
        />
        {/* <View style={styles.btn_bottom}>
          <ButtonHandle 
            buttonText="Chốt tan ca trong tháng"
            onPressButton={Actions.onPressSummaryAttendance}
          />
        </View> */}
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}

// Sample data
const sample_data = [
  {
    title: 'Hôm nay',
    data: ['Pizza', 'Burger'],
  },
  {
    title: 'Hôm qua',
    data: ['French Fries', 'Onion Rings'],
  },
  {
    title: '02/11/2019',
    data: ['Water', 'Coke'],
  },
  {
    title: '01/11/2019',
    data: ['Cheese Cake', 'Ice Cream'],
  },
  {
    title: '01/11/2019',
    data: ['Cheese Cake', 'Ice Cream'],
  },


];