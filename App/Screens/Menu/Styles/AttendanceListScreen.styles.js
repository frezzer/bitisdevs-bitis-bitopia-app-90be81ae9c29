import { StyleSheet } from 'react-native';
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.theme.block
  },
  header: {
    backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    marginTop: Metrics.marginTiny,
    borderBottomColor: Colors.block,
    borderBottomWidth: 1
  },
  btn_bottom: {
    backgroundColor: Colors.white,
    height: wp(100 / 375 * 100),
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    elevation: 1
  },
  // Texxt
  title: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold
  },
});
