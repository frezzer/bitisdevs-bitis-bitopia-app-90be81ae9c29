import { StyleSheet } from 'react-native';
import { Metrics, Fonts, Colors } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.theme.block
  },
  part1: {
    padding: Metrics.paddingHuge,
    alignItems: 'center',
    backgroundColor: Colors.white,
    marginBottom: Metrics.marginTiny
  },
  part2: {
    backgroundColor: Colors.white,
  },
  block_element: {
    paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingSmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: Colors.block,
    borderBottomWidth: 1
  },
  btn_bottom: {
    backgroundColor: Colors.white,
    height: wp(100 / 375 * 100),
    width: Metrics.width,
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    elevation: 1,
    position: 'absolute',
    bottom: 0,
  },
  // Text
  text1: {
    fontSize: Fonts.size.textXLarge,
    fontFamily: Fonts.type.fontBold,
    lineHeight: wp(24 / 375 * 100)
  },
  text2: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.blue,
    marginVertical: Metrics.marginTiny
  },
  text3: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.grayLight,
    fontStyle: 'italic',
    textAlign: 'center'
  },
  text4: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.grayLight,
  }
});
