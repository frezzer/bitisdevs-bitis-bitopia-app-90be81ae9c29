
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  Dimensions, InteractionManager, View, Text, Platform, TouchableOpacity, Image, Alert,
} from 'react-native';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens';
import { Metrics, Images, Colors } from '../../Themes';
import ButtonHandle from '../../Components/Button/ButtonHandle';
// Styles
import styles from './Styles/SummaryAttendanceScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';
/**
 * Main
 */

const onPressBackButton = (navigation) => {
  navigation.goBack();
};

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    // headerMode: Platform.select({
    //   android: 'screen',
    //   ios: 'float',
    // }),
    // title: 'Tổng kết điểm danh',
    headerStyle: {
      backgroundColor: Colors.blue2,
      // elevation:0,
      // shadowColor: Colors.blue1,
      borderBottomWidth: 0,
    },
    title: 'Danh sách nhân viên',
    // headerLeft: () => {
    //   return (
    //     <TouchableOpacity
    //       style={{
    //         paddingLeft: Platform.OS == 'android' ? 18 : 5,
    //         paddingBottom: Metrics.paddingXTiny,
    //       }}
    //       onPress={() => onPressBackButton(props.navigation)}
    //     >
    //       <Image source={Images.buttonBack} />
    //     </TouchableOpacity>
    //   );
    // },
  };
};

SummaryAttendanceScreen.navigationOptions = navigationOptions;

export default function SummaryAttendanceScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [selectedStartDate, setselectedStartDate] = React.useState(null);
  const [selectedEndDate, setselectedEndDate] = React.useState(null);


  // effects
  React.useEffect(() => {
    console.tron.log('SummaryAttendanceScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSendSummary: () => Alert.alert('Gửi tổng kết'),
  };
  const state = {
    selectedStartDate,
    selectedEndDate,
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SummaryAttendanceScreen render');
  const {
    initialRender,
    Actions,
  } = props;

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.part1}>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Tiêu đề</Text>
            <Text style={styles.text1}>Điểm danh tháng 12</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Ngày bắt đầu</Text>
            <Text style={styles.text1}>1/12/2019</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Ngày kết thúc</Text>
            <Text style={styles.text1}>31/12/2019</Text>
          </View>
        </View>

        <View style={styles.part2}>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Nhân viên</Text>
            <Text style={styles.text2}>Trần Xuân Hoàng</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Tên cửa hàng</Text>
            <Text style={styles.text2}>Bitis hunter</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Số ca làm việc</Text>
            <Text style={styles.text2}>5</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Số giờ làm việc</Text>
            <Text style={styles.text2}>20:30</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Số ngày đi trễ</Text>
            <Text style={styles.text2}>4</Text>
          </View>
          <View style={styles.block_element}>
            <Text style={styles.text2}>Tổng lương dự kiến</Text>
            <Text style={{ ...styles.text2, color: Colors.blue }}>2.000.000 VND</Text>
          </View>
        </View>

        <View style={styles.btn_bottom}>
          <ButtonHandle
            buttonText="Gửi thông tin"
            onPressButton={Actions.onPressSendSummary}
          />
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
