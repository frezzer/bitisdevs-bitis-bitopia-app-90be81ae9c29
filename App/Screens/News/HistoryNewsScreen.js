
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, Image, ScrollView, FlatList,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Colors } from '../../Themes';

// Styles
import styles from './Styles/StoreNewsScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';
// import { ScrollView } from 'react-native-gesture-handler';
import BlockNews from '../../Components/Block/BlockNews';
// import { FlatList } from 'react-native-gesture-handler';
import BlockSaveNews from '../../Components/Block/BlockSaveNews';

const { width } = Dimensions.get('window');


/**
 * Main
 */

// const navigationOptions = (props) => ({
//   headerStyle: {
//     backgroundColor: Colors.blue2,
//     // elevation:0,
//     // shadowColor: Colors.blue1,
//     borderBottomWidth: 0,
//   },
//   // header: null,
//   title: 'Tin tức',

// });

// StoreNewsScreen.navigationOptions = navigationOptions;

export default function HistoryNewsScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {

  };
  const state = {

  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const RenderItem = React.memo((props) => {
  const { item } = props;
  const onPressButton = _.get(props, ['onPressButton'], () => {});
  return (
    <View style={styles.container}>
      <BlockSaveNews dataNews={item} bookmark />
    </View>
  );
}, areEqual);
const Render = React.memo((props) => {
  console.tron.log('StoreNewsScreen render');
  // declare variables
  const { initialRender, Actions, state } = props;
  const { } = state;
  const { } = Actions;
  const renderItem = ({ item }) => <RenderItem item={item} />;


  return (
    <View style={styles.container}>
      <ScrollView>
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={items}
          renderItem={renderItem}
          // initialNumToRender={3}
        />
      </ScrollView>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}


const items = [
  {
    id: 1,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',
  },
  {
    id: 2,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
  {
    id: 3,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
  {
    id: 4,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
];
