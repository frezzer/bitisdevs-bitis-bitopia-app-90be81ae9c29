
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {
  Dimensions, InteractionManager, View, Text, FlatList, Image, TouchableOpacity, ScrollView, Platform,
} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import { SafeAreaView } from 'react-navigation';
// Components
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import {
  Images, Colors, Metrics, Fonts,
} from '../../Themes';
import LoadingInteractions from '../../Components/LoadingInteractions';
import BlockNews from '../../Components/Block/BlockNews';
import screensName from '../../Navigation/Screens';


// Styles
import styles from './Styles/NewScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';

const navigationOptions = (props) => {
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  return {
    title: 'Tin tức',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS == 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack} />

        </TouchableOpacity>
      );
    },
  };
};


NewScreen.navigationOptions = navigationOptions;


const RenderItem = React.memo((props) => {
  // const onPressInfo = props;
  const { item, onPressInfo } = props;
  return (
    <BlockNews dataNews={item} onPress={onPressInfo} />
  );
}, areEqual);


/**
 * Main
 */
export default function NewScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
     * Actions
     *  - for flows and onPress attribute button
     * */
  const onPressInfo = () => {
    props.navigation.navigate(screensName.NewsDetails);
  };
  const Actions = {
  };
  const state = {

  };


  /**
     * render
     *  - declare variables for render
     *  - render loading when initialRender is true
     */
  const renderProps = {
    initialRender,
    Actions,
    state,
    onPressInfo,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const {
    initialRender, Actions, state, onPressInfo,
  } = props;
  const { } = state;
  const { } = Actions;
  const renderItem = ({ item }) => <RenderItem item={item} onPressInfo={onPressInfo} />;

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.content}>
        <View style={styles.part1}>
          <Text style={styles.text1}>Tin tức chung</Text>
          <TouchableOpacity>
            <Text style={styles.text2}>Xem tất cả</Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.text3}>Tin tức phổ biến cho toàn bộ nhân viên trong công ty.</Text>

        <View style={styles.list_news}>
          <FlatList
            keyExtractor={(item, index) => item.id}
            data={items}
            renderItem={renderItem}
            horizontal
            ItemSeparatorComponent={() => {
              return <View style={{ width: 8 }} />;
            }}
            ListHeaderComponent={() => {
              return <View style={{ width: 16 }} />;
            }}
            ListFooterComponent={() => {
              return <View style={{ width: 16 }} />;
            }}
                        // keyExtractor={(item, index) => String(index)}
            showsHorizontalScrollIndicator={false}
            decelerationRate={0}
            snapToAlignment="center"
            snapToInterval={148}
            // decelerationRate="fast"
          />
        </View>

        <View style={styles.part1}>
          <Text style={styles.text1}>Tin tức của bạn</Text>
          <TouchableOpacity>
            <Text style={styles.text2}>Xem thêm</Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.text3}>Tin tức riêng thuộc chi nhánh, bộ phận, phòng ban và trách nhiệm của bạn.</Text>

        <View style={styles.list_news}>
          <FlatList
            keyExtractor={(item, index) => item.id}
            data={items}
            renderItem={renderItem}
            horizontal
            ItemSeparatorComponent={() => {
              return <View style={{ width: 8 }} />;
            }}
            ListHeaderComponent={() => {
              return <View style={{ width: 16 }} />;
            }}
            ListFooterComponent={() => {
              return <View style={{ width: 16 }} />;
            }}
            showsHorizontalScrollIndicator={false}
            decelerationRate="fast"
          />
        </View>

      </ScrollView>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}


const items = [
  {
    id: 1,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',
  },
  {
    id: 2,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
  {
    id: 3,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
  {
    id: 4,
    title: 'Chụp ảnh check in',
    date: '1/1/2019-22/2/2019',
    image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg',
    despcription: 'Thông báo chính sách ưu đãi mua sản phẩm Biti\'s dành cho nhân viên...',
    content: 'Gửi đến cả nhà chương trình "Tết đong đầy từ Biti\'s" với chiết khấu ưu đãi hấp dẫn dành cho nhân viên công ty...',

  },
];
