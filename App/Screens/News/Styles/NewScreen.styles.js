import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.block,
    paddingHorizontal: Metrics.paddingMedium,
  },
  part1: {
    marginTop: Metrics.marginXXLarge,
    marginBottom: Metrics.marginTiny,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  list_news: {
    paddingTop: Metrics.paddingMedium,
    paddingBottom: 15,
    marginHorizontal: -Metrics.marginMedium,
    // width: wp(296 / 375 * 100),
    // height: wp(326/ 375 * 100),
    // backgroundColor:'red',


  },
  // Text
  text1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    color: Colors.black,
  },
  text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.black,
    textDecorationLine: 'underline',
  },
  text3: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey7,
  },
  text: {
    color: Colors.black,
    fontSize: 30,
    fontWeight: 'bold',
  },

  element: {

    backgroundColor: 'red',
    borderRadius: Metrics.marginMedium,
  },
  image_info: {
    width: wp(296 / 375 * 100),
    height: wp(158 / 375 * 100),
  },
  despcription: {
    marginTop: Metrics.marginTiny,
    paddingHorizontal: Metrics.marginMedium,
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,

  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
