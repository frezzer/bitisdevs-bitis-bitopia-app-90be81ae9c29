import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    // flex: 1,
    // backgroundColor: Colors.white,
    // paddingHorizontal: Metrics.paddingMedium,
    zIndex: 2,
  },
  image: {
    ...ApplicationStyles.specialApp.dimensisons(375, 320),
    // zIndex:2
  },
  blockContent: {
    //   flex:1,
    backgroundColor: 'yellow',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    position: 'absolute',
    zIndex: 1,
    top: 100,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
