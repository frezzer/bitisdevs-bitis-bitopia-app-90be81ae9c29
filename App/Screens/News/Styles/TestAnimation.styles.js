import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';
// const MAX_WIDTH_HEIGHT_IMG = 90,
// const MIN_WIDTH_HEIGHT_IMG = 30,
const INTIAL_HEIGHT_TOP_BAR = Platform.OS === 'ios' ? 88 : 82;
const INTIAL_HEIGHT_BACKGROUND_IMG = 350;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8E8E8',
  },
  row: {
    padding: 10,
    marginHorizontal: 16,
    marginBottom: 8,
    backgroundColor: 'white',
    borderRadius: 6,
  },
  headerStyle: {
    height: INTIAL_HEIGHT_TOP_BAR,
    // padding: 10,
    paddingHorizontal: 16,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',

  },
  headerLeftIcon: {
    // backgroundColor: 'yellow'
    fontSize: 25,
    color: 'black',
  },
  headerTitle: {
    flex: 1,
    textAlign: 'center',
    // backgroundColor: 'blue',
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: 'black',
    marginBottom: wp(10 / 375 * 100),
    marginLeft: -wp(50 / 375 * 100),
    // justifyContent:'center'
  },
  headerRightIcon: {
    // backgroundColor: 'yellow'
  },
  // Image
  headerImageStyle: {
    height: INTIAL_HEIGHT_BACKGROUND_IMG,
    width: '100%',
    position: 'absolute',
    top: 0,
  },
  profileImage: {
    // position: 'absolute',
    zIndex: 2,
  },
  // Text
  text1: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 6,
  },
  text3: {
    fontSize: 12,
    color: 'grey',
  },
  scrollViewContent: {
    marginTop: Metrics.marginHuge,
    paddingHorizontal: Metrics.paddingXXLarge,
  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.black,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    // marginHorizontal: Metrics.marginMedium,
  },
  Title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textHuge,
    color: Colors.grey9,
  },
  imageUser: {
    width: wp(16 / 375 * 100),
    height: wp(16 / 375 * 100),
    borderRadius: Metrics.boderLSmall,
    marginRight: Metrics.marginTiny,
  },
  block2: {
    flexDirection: 'column',
    // paddingHorizontal: Metrics.paddingMedium,
    // paddingTop: Metrics.paddingXXSmall,
    // justifyContent: 'space-between',
    // backgroundColor:'yellow',
    // alignItems: 'center',
    marginTop: Metrics.marginTiny,
  },
  name: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.greybold,
  },
  content: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
    paddingVertical: Metrics.marginXSmall,
    // textAlign:'center',
    textAlignVertical: 'center',
  },
});
