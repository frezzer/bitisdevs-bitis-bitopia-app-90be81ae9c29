import React, { useState, useEffect } from 'react';
import {
  Animated, Text, View, StyleSheet, Image, SafeAreaView, Dimensions, Platform, TouchableOpacity,
} from 'react-native';
// import { Ionicons } from '@expo/vector-icons';
import styles from './Styles/TestAnimation.styles';
import { Colors, Metrics } from '../../Themes';
import { Feather } from '../../Components/Icons';
import BLockNewsLike from '../../Components/Block/BLockNewsLike';
import CircleButton from '../../Components/Button/CircleButton';

const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const navigationOptions = ({ navigation }) => ({
  header: null,
});


MapScreen.navigationOptions = navigationOptions;
const { width } = Dimensions.get('window');
const MAX_WIDTH_HEIGHT_IMG = 30;
const MIN_WIDTH_HEIGHT_IMG = 30;
const INTIAL_HEIGHT_BACKGROUND_IMG = 250;
const INTIAL_HEIGHT_TOP_BAR = Platform.OS === 'ios' ? 88 : 100;
const RenderScrollViewContent = () => {
//   const data = Array.from({ length: 10 });
  return (

    <View style={styles.scrollViewContent}>

      <Text style={styles.Title}>Đi để trở về 4 – Khi bạn trở về chính là lúc Tết của ba mẹ bắt đầu</Text>
      <View style={styles.block2}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Image
              style={styles.imageUser}
              source={require('../../Images/news.png')}
            />
          </View>
          <Text style={styles.name}>HCNS Biti’s</Text>
          <View
            style={{
              //   height:15,
              borderLeftWidth: 1,
              borderLeftColor: Colors.grey4,
              marginHorizontal: Metrics.marginTiny,
            }}
          />
          <Text style={styles.name}>12/01/2020</Text>

        </View>
        <View>
          <Text style={styles.content} adjustsFontSizeToFit>
            Đến mỗi độ Tết về, người trẻ lại mong đợi về một hành trình Đi Để Trở Về với những thông điệp ý nghĩa. Năm nay, Biti's Hunter cùng Đi Để Trở Về mùa 4 bất ngờ trở lại với một "diện mạo mới": phim ngắn Tết đầu tiên - một hành trình đặc biệt từ Biti’s Hunter.

            Như đã thành một thói quen, mỗi năm cứ đến độ Tết về là người trẻ lại nôn nao đón chờ một mùa "Đi để trở về" mới, với những giai điệu được cất lên thay tiếng lòng của người con xa quê. Trở lại trong năm thứ tư, thương hiệu của "Đi và Trải nghiệm" quyết định mang Đi Để Trở Về quay lại đầy bất ngờ với phim ngắn "Hành trình đặc biệt của Tết" - phim ngắn Tết đầu tiên của thương hiệu sneaker Việt sau thành công của ba mùa MV ca nhạc cùng Soobin Hoàng Sơn.

            Lần này, thông điệp tưởng chừng như đã quen thuộc sẽ một lần nữa vang lên, nhưng theo một cách thật đặc biệt. Đi để trở về không chỉ dừng lại ở một chuyến đi đầy cảm xúc, mà còn trở nên mãnh liệt hơn, thôi thúc những người trẻ xa quê trở về sớm hơn Tết này.

            "Đặt vé chưa con? Quên quên vài bữa đến Tết sát ngày khó mua…"
            Mở đầu phim ngắn là hình ảnh chàng trai trẻ năng động, hết mình với công việc sáng tạo âm thanh. Giữa lúc bận rộn với những đam mê thường ngày, cuộc gọi của mẹ bỗng vang lên như một khoảng lặng. Và rồi như có điều gì đó thôi thúc, hành trình đi tìm về những âm thanh quen thuộc của Tết được bắt đầu.
            Đến mỗi độ Tết về, người trẻ lại mong đợi về một hành trình Đi Để Trở Về với những thông điệp ý nghĩa. Năm nay, Biti's Hunter cùng Đi Để Trở Về mùa 4 bất ngờ trở lại với một "diện mạo mới": phim ngắn Tết đầu tiên - một hành trình đặc biệt từ Biti’s Hunter.

            Như đã thành một thói quen, mỗi năm cứ đến độ Tết về là người trẻ lại nôn nao đón chờ một mùa "Đi để trở về" mới, với những giai điệu được cất lên thay tiếng lòng của người con xa quê. Trở lại trong năm thứ tư, thương hiệu của "Đi và Trải nghiệm" quyết định mang Đi Để Trở Về quay lại đầy bất ngờ với phim ngắn "Hành trình đặc biệt của Tết" - phim ngắn Tết đầu tiên của thương hiệu sneaker Việt sau thành công của ba mùa MV ca nhạc cùng Soobin Hoàng Sơn.

            Lần này, thông điệp tưởng chừng như đã quen thuộc sẽ một lần nữa vang lên, nhưng theo một cách thật đặc biệt. Đi để trở về không chỉ dừng lại ở một chuyến đi đầy cảm xúc, mà còn trở nên mãnh liệt hơn, thôi thúc những người trẻ xa quê trở về sớm hơn Tết này.

            "Đặt vé chưa con? Quên quên vài bữa đến Tết sát ngày khó mua…"

            Mở đầu phim ngắn là hình ảnh chàng trai trẻ năng động, hết mình với công việc sáng tạo âm thanh. Giữa lúc bận rộn với những đam mê thường ngày, cuộc gọi của mẹ bỗng vang lên như một khoảng lặng. Và rồi như có điều gì đó thôi thúc, hành trình đi tìm về những âm thanh quen thuộc của Tết được bắt đầu.¸
          </Text>
          <BLockNewsLike />
        </View>
      </View>
    </View>
  );
};
export default function MapScreen(props) {
  const [scrollY, setScrollY] = React.useState(new Animated.Value(0));
  const [iconColor, setIconColor] = React.useState(false);
  const onPressChangeColor = () => {};
  const onPressGoBack = () => {
    props.navigation.goBack();
  };


  // For animated header image opacity when scroll
  const _getHeaderImageOpacity = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: [1, 0],
      extrapolate: 'clamp', // => to be sure all of extrapolate value out of range will be inside outputRange
      useNativeDriver: true, // => always using this for animation can be smooth because dont need bringde js and native
    });
  };
  // For animated header background color when scroll
  const _getHeaderBackgroundColor = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: ['rgba(0,0,0,0.0)', 'white'],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  const _getHeaderButtonColor = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: ['white', 'black'],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // For header title opacity when scroll
  const _getHeaderTitleOpacity = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 20, 50],
      outputRange: [0, 0.5, 1],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  const _getButtonOpacity = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 20, 50],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // For animated image profile
  const _getImageHeight = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: [MAX_WIDTH_HEIGHT_IMG, MIN_WIDTH_HEIGHT_IMG],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // artist profile image width
  const _getImageWidth = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: [MAX_WIDTH_HEIGHT_IMG, MIN_WIDTH_HEIGHT_IMG],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // artist profile image position from left
  const _getImageLeftPosition = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 80, 140],
      outputRange: [(width - MAX_WIDTH_HEIGHT_IMG) / 2, (width - 45) / 2, 16],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // artist profile image border width
  const _getImageBorderWidth = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: [StyleSheet.hairlineWidth * 3, StyleSheet.hairlineWidth],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // artist profile image border color
  const _getImageBorderColor = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: ['green', 'rgba(0,0,0,0.0)'],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // artist profile image position from top
  const _getImageTopPosition = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 200],
      outputRange: [222, 80],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  // For distance from list item to top when scroll
  const _getListViewTopPosition = () => {
    // const { scrollY } = this.state;
    return scrollY.interpolate({
      inputRange: [0, 300],
      outputRange: [212, 0],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };
  //   render() {
  const headerImageOpacity = _getHeaderImageOpacity();
  // animated for second header
  const headerTitleOpacity = _getHeaderTitleOpacity();
  const headerBackgroundColor = _getHeaderBackgroundColor();
  const headerButtonColor = _getHeaderButtonColor();
  const ButtonOpacity = _getButtonOpacity();

  // animated for body
  const listViewTop = _getListViewTopPosition();
  // animated for image profile
  const profileImageHeight = _getImageHeight();
  const profileImageWidth = _getImageWidth();
  const profileImageTop = _getImageTopPosition();
  const profileImageLeft = _getImageLeftPosition();
  const profileImageBorderWidth = _getImageBorderWidth();
  const profileImageBorderColor = _getImageBorderColor();
  return (
    <View style={styles.container}>
      <Animated.Image
        style={[styles.headerImageStyle, {
          opacity: headerImageOpacity,
        }]}
        source={require('../../Images/Newslarge.png')}
      />
      <Animated.View style={[styles.headerStyle, {
        backgroundColor: headerBackgroundColor,
      }]}
      >
        {/* <View style={styles.headerLeftIcon}> */}
        {/* <AnimatedFeather name="chevron-left" style={styles.headerLeftIcon} /> */}
        {/* </View> */}
        <TouchableOpacity style={styles.backButtonTouchable} onPress={onPressGoBack}>
          <View>
            <AnimatedFeather
              name="chevron-left"
              style={[styles.backButtonButtonText, { color: headerButtonColor }]}
            />
          </View>
        </TouchableOpacity>
        {/* <View style={{backgroundColor:'red'}}> */}
        <Animated.Text
          style={[styles.headerTitle, {
            opacity: headerTitleOpacity,
            color: headerButtonColor,
          }]}
        >
          Tin tức
        </Animated.Text>
        {/* </View> */}
      </Animated.View>


      <Animated.ScrollView
        overScrollMode="never"
        style={{ zIndex: 1 }}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [
            {
              nativeEvent: { contentOffset: { y: scrollY } },
            },
          ],
        )}
      >
        <Animated.View
          style={
            [styles.profileImage, {
            //   borderWidth: profileImageBorderWidth,
            //   borderColor: profileImageBorderColor,
              height: profileImageHeight,
              width: profileImageWidth,
              borderRadius: MIN_WIDTH_HEIGHT_IMG / 2,
              transform: [
                { translateY: profileImageTop },
                // { translateX: profileImageLeft },
              ],
              flexDirection: 'row',
              alignSelf: 'flex-end',
              marginRight: 110,


              //   justifyContent: 'flex-end',
              //   backgroundColor:'yellow'
              //   marginTop: 80,
              //   opacity: ButtonOpacity,
              // marginTop: -10

            }]
}
        >
          <View style={{
            // backgroundColor: 'red',
            marginRight: Metrics.marginMedium,
          }}
          >
            <CircleButton name="bookmark" />
          </View>
          <View style={{
            // backgroundColor:'white',
          }}
          >
            <CircleButton name="share" />
          </View>
        </Animated.View>

        <Animated.ScrollView style={{
          transform: [{
            translateY: listViewTop, // change distance of list view from top when scroll
          }],
          //   marginTop: 100,
          borderTopLeftRadius: Metrics.boderLLLarge,
          borderTopRightRadius: Metrics.boderLLLarge,
          backgroundColor: Colors.white,
          zIndex: 1,
        }}
        >

          <RenderScrollViewContent />
        </Animated.ScrollView>
      </Animated.ScrollView>
    </View>
  );
//   }
}
// MapScreen.navigationOptions = {
//   header: null,
// };
// const styles = StyleSheet.create({

// });
