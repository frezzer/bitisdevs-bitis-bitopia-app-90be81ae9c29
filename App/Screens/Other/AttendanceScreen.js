'use strict';
import React, {useState, useEffect } from 'react';
import { ScrollView, View, FlatList, SafeAreaView } from 'react-native';

// redux

// Components
import AttendanceBlock from '../Components/AttendanceBlock';

// Styles

// Utils
import { compareOmitState } from '../Transforms/utils';

const Attendance = (props) => {
  const [data, setdata] = useState([
    {
      id: 1,
      title: `Chụp ảnh check in`,
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
    },
    {
      id: 2,
      title: `Chụp ảnh check in`,
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
    },
    {
      id: 3,
      title: `Chụp ảnh check in`,
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
    },
    {
      id: 4,
      title: `Chụp ảnh check in`,
      date: '1/1/2019-22/2/2019',
      image_link: 'https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg'
    },
  ])

  const _renderItem = ({ item }) => {
    return (
      <View id={item.id}>
        <AttendanceBlock data_item={item} />
      </View>
    );
  };
  return (
    <SafeAreaView>
    <View>
      <ScrollView>
        <FlatList
          data={data}
          renderItem={_renderItem}
          keyExtractor={(item, index) => item.id} />
      </ScrollView>
    </View>
    </SafeAreaView>
  )
}
function areEqual(prevProps, nextProps) {
  // console.tron.log("DebugLog: areEqual -> prevProps", prevProps, nextProps)
  return false
  return compareOmitState(nextProps, prevProps, []);
}
export default React.memo(Attendance, areEqual);

