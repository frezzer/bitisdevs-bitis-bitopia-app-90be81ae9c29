import React, { useState, useEffect } from 'react';
import {
  Platform, View, Text, FlatList, SafeAreaView, InteractionManager, TouchableOpacity, Image,
} from 'react-native';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux';
import BillGetHistoryActions, { BillGetHistorySelectors } from '../../Redux/bill/BillGetHistoryRedux';
// Styles
import styles from './Styles/BillScreenStyles';

// Components
// import BillItem from '../../Components/BillItem';
import CartItemNewVer from '../../Module/CartModule/Components/CartItemNewVer';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens';
import { Images, Metrics } from '../../Themes';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';
// Utils
import { compareOmitState } from '../../Services/utils';


const navigationOptions = (props) => {
  const onPressRightButton = () => props.navigation.navigate(screensName.FilterScreen, {
    defineActionWillUse: BillGetHistoryActions.billGetHistoryRequest({
      status: 'draft',
      _limit: 10,
    }),
  });
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Hóa đơn',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS === 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          <IconOutline name="left" color="white" style={styles.iconBack} />

        </TouchableOpacity>
      );
    },
    headerRight: () => {
      return (
        <RightButtonsTopBar
          name="filter"
          style={styles.headerRightView}
          onPress={onPressRightButton}
        />
      );
    },
  };
};

BillScreen.navigationOptions = navigationOptions;

export default function BillScreen(props) {
  const dispatch = useDispatch();
  /** INTERNAL STATE */
  const [initialRender, setInitialRender] = React.useState(true);
  const [fetchBillFirstTime, setFetchBillFirstTime] = React.useState(true);

  /** REDUX STATE */
  const billHis = useSelector((state) => BillGetHistorySelectors.selectPayload(state.bill.bill_his));
  const billFetching = useSelector((state) => BillGetHistorySelectors.selectFetching(state.bill.bill_his));

  /** FUNCTION */
  /** Handle refresh list bill craft */
  const onRefresh = React.useCallback(() => {
    Actions.getBillHistory();
  }, [billFetching]);

  /** LIFECYCLE */
  /** Didmount */
  React.useEffect(() => {
    if (initialRender) {
      dispatch(loadingSagaActions.loadingIndicatorHide());
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      setTimeout(() => {
      }, 100);
    }
  }, [/** input */]);

  React.useEffect(() => {
    if (!billFetching && fetchBillFirstTime) {
      setFetchBillFirstTime(false);
      Actions.getBillHistory();
    }
  });

  /**
     * Actions
     *  - for flows and onPress attribute button
     * */
  const Actions = {

    /** Get list bill */
    getBillHistory: () => {
      dispatch(BillGetHistoryActions.billGetHistoryRequest({
        status: 'draft',
        _limit: 10,
      }));
    },
  };

  const controlerForState = {
    initialRender,
  };

  const controlerForProps = {
    billHis,
    billFetching,
  };


  /**
     * render
     *  - declare variables for render
     *  - render loading when initialRender is true
     */

  const renderProps = {
    ...props,
    Actions,
    controlerForState,
    controlerForProps,
    initialRender,
    onRefresh,
  };
  return <Render {...renderProps} />;
}


const Render = React.memo((props) => {
  /** DECLARE EVERYTHING */
  const {
    Actions, controlerForState, controlerForProps,
    onRefresh,
  } = props;

  const { initialRender } = controlerForState;

  const {
    billHis, billFetching,
  } = controlerForProps;

  if (initialRender) return <LoadingInteractions size="large" />;

  /** RENDER ITEM  */
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity id={item.id} onPress={() => props.navigation.navigate(screensName.CartDetailJustViewScreen, { billDetail: item })}>
        <CartItemNewVer dataBill={item} status="Đã thanh toán" />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* <View style={styles.part1}> */}
      <FlatList
        refreshing={billFetching}
        data={billHis || []}
        renderItem={renderItem}
        keyExtractor={(item, index) => item.id}
        ListEmptyComponent={() => (
          <Text style={{ textAlign: 'center' }}>Bạn chưa có đơn hàng nào!</Text>
        )}
        ItemSeparatorComponent={() => (
          <View style={styles.block_separator}>
            <View style={styles.line_style} />
          </View>
        )}
        onRefresh={onRefresh}
      />
      {/* </View> */}
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
