
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, AsyncStorage, SafeAreaView, View, Text, ScrollView, TouchableOpacity, Alert, Image,
} from 'react-native';
// import ReactNativeBiometrics from 'react-native-biometrics';
import { widthPercentageToDP, widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {
  Svg, Circle, Polygon, Polyline, Path, Rect, G,
} from 'react-native-svg';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Modal from 'react-native-modal';
import LoadingInteractions from '../../Components/LoadingInteractions';
import RootNotify from '../../Components/RootNotify';
import DashboardMore from '../../Components/DashBoardMore';
import Barcode from '../../Lib/react-native-barcode-builder';


import {
  Colors, Images, Fonts, Metrics,
} from '../../Themes';

// Styles
import styles from './Styles/DashBoardScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';

// utils
import screensName from '../../Navigation/Screens';
import { compareOmitState, getFullName } from '../../Services/utils';

const { width } = Dimensions.get('window');


/**
 * Options
 * */
const navigationOptions = (props) => ({
  headerStyle: {
    backgroundColor: Colors.blue2,
    borderBottomWidth: 0,
  },
  header: null,

});

DashBoardScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function DashBoardScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);


  // useStates
  // selector store
  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  const roleTypeUser = _.get(me_payload, ['roles']);
  const data = _.get(me_payload, ['username']);

  // Role
  const roleshiftleader = _.find(roleTypeUser, (i) => i === 'shiftleader');
  const roleseller = _.find(roleTypeUser, (i) => i === 'seller');
  // uuid User
  const uuid = _.get(me_payload, ['username'], {});
  // Name
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const name = _.concat([last_name, first_name ? ' ' : '', first_name]);
  // Seller
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeName = _.get(findStoreSellerPayload, ['store_name']);
  const dataRoles = _.get(user_detail, ['roles'], '');

  // take position
  const position = _.find(dataRoles, (item) => { return item.type === 'shiftleader' || item.type === 'seller'; });
  console.tron.log('debug: DashBoardScreen -> positionSeller', position);


  // effects
  React.useEffect(() => {
    console.tron.log('DashBoardScreen effect');
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(UserDetailActions.UserDetailRequest({
        username: data,
      }));
      dispatch(FindstoreSellerActions.FindstoreSellerRequest({}));
      dispatch(loadingSagaActions.loadingIndicatorHide());
    }
  }, [/** no input */]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */

  const Actions = {
    onPressButton: () => alert('Danh mục đang phát triển. Vui lòng chờ trong bản cập nhật sau.'),
    onPressSaleButton: (params) => {
      if (roleseller !== 'seller') return alert('Bạn không thể vào danh mục này');
      requestAnimationFrame(() => {
        props.navigation.navigate(screensName.BottomManagerRouter);
      });
    },
    onPressShiftLeaderButton: (params) => {
      if (roleshiftleader !== 'shiftleader') return alert('Bạn không thể vào danh mục này');
      props.navigation.navigate(screensName.BottomManagerRouter);
    },
    onPressExtendBarCode: () => {
      props.navigation.navigate(screensName.EmployeeBarCodeScreen);
    },
    onPressBarcode: () => {
      props.navigation.navigate(screensName.EmployeeBarCodeScreen);
    },
    // Khanh rewrite
    // TODO : Quân ,you need to fix all function above like this function fo shorter and easier to read !
    onPressItemButton: async (key, params) => {
      switch (key) {
        case 'ShiftLeader':
          if (!roleshiftleader) return Alert.alert('Thông báo', 'Bạn không thể vào danh mục này');
          props.navigation.navigate(screensName.BottomManagerRouter);
          break;
        case 'Saller':
          if (!roleseller) return Alert.alert('Thông báo', 'Bạn không thể vào danh mục này');
          props.navigation.navigate(screensName.BottomSalerRouter, { from_dashboard: true });
          break;
        case 'News':
          props.navigation.navigate(screensName.BottomNewsRouter);
          break;
        default:
          alert('Danh mục đang phát triển. Vui lòng chờ trong bản cập nhật sau.');
          break;
      }
    },
    toggleModal: () => {
      setIsModalVisible(true);
    },
    hideModal: () => {
      setIsModalVisible(false);
    },
    onPressSetting: () => {
      props.navigation.navigate(screensName.SettingScreen);
    },
    onPressHelp: () => {
      props.navigation.navigate(screensName.HelpScreen);
    },
    onPressToNews: () => {
      props.navigation.navigate(screensName.NewScreen);
    },

  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    entriesData,
    Actions,
    isModalVisible,
    uuid,
    name,
    storeName,
    position,


  };
  return <Render {...renderProps} />;
}

const RenderItem = React.memo((props) => {
  const { item, onPress } = props;
  return (
    <View id={item.id} style={styles.child}>
      <View>
        <View style={{ top: wp(180 / 375 * 100), alignItems: 'center', zIndex: 2 }}>
          <TouchableOpacity style={styles.textMore} onPress={onPress}>
            <Text style={styles.textImage}>Xem thêm</Text>
          </TouchableOpacity>
        </View>
        <Image
          style={styles.imageNews}
          source={item.img}
        />
      </View>
    </View>
  );
}, areEqual);


/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender,
    entriesData,
    Actions,
    isModalVisible,
    uuid,
    name,
    storeName,
    position,
  } = props;
  const renderItem = ({ item }) => <RenderItem item={item} onPress={Actions.onPressToNews} />;
  if (initialRender) return <LoadingInteractions size="large" />;
  return (
    <View style={styles.container}>
      <View style={styles.blockHidden} />
      <View style={styles.account}>
        <View style={styles.area1}>
          <View style={{ justifyContent: 'center' }}>
            <Text style={styles.hello}>Xin chào,</Text>
          </View>
          <View style={styles.area2}>
            <TouchableOpacity style={styles.buttonBarcode} onPress={Actions.toggleModal}>
              <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path d="M2.8125 3.75H1.6875C1.58437 3.75 1.5 3.83437 1.5 3.9375V20.0625C1.5 20.1656 1.58437 20.25 1.6875 20.25H2.8125C2.91562 20.25 3 20.1656 3 20.0625V3.9375C3 3.83437 2.91562 3.75 2.8125 3.75ZM22.3359 3.75H21.2109C21.1078 3.75 21.0234 3.83437 21.0234 3.9375V20.0625C21.0234 20.1656 21.1078 20.25 21.2109 20.25H22.3359C22.4391 20.25 22.5234 20.1656 22.5234 20.0625V3.9375C22.5234 3.83437 22.4391 3.75 22.3359 3.75ZM4.6875 17.25H7.3125C7.41563 17.25 7.5 17.1656 7.5 17.0625V3.9375C7.5 3.83437 7.41563 3.75 7.3125 3.75H4.6875C4.58437 3.75 4.5 3.83437 4.5 3.9375V17.0625C4.5 17.1656 4.58437 17.25 4.6875 17.25ZM12.2109 17.25H13.3359C13.4391 17.25 13.5234 17.1656 13.5234 17.0625V3.9375C13.5234 3.83437 13.4391 3.75 13.3359 3.75H12.2109C12.1078 3.75 12.0234 3.83437 12.0234 3.9375V17.0625C12.0234 17.1656 12.1078 17.25 12.2109 17.25ZM15.1641 17.25H19.3359C19.4391 17.25 19.5234 17.1656 19.5234 17.0625V3.9375C19.5234 3.83437 19.4391 3.75 19.3359 3.75H15.1641C15.0609 3.75 14.9766 3.83437 14.9766 3.9375V17.0625C14.9766 17.1656 15.0609 17.25 15.1641 17.25ZM9.1875 17.25H10.3125C10.4156 17.25 10.5 17.1656 10.5 17.0625V3.9375C10.5 3.83437 10.4156 3.75 10.3125 3.75H9.1875C9.08437 3.75 9 3.83437 9 3.9375V17.0625C9 17.1656 9.08437 17.25 9.1875 17.25ZM7.33594 18.75H4.71094C4.60781 18.75 4.52344 18.8344 4.52344 18.9375V20.0625C4.52344 20.1656 4.60781 20.25 4.71094 20.25H7.33594C7.43906 20.25 7.52344 20.1656 7.52344 20.0625V18.9375C7.52344 18.8344 7.43906 18.75 7.33594 18.75ZM13.3594 18.75H12.2344C12.1313 18.75 12.0469 18.8344 12.0469 18.9375V20.0625C12.0469 20.1656 12.1313 20.25 12.2344 20.25H13.3594C13.4625 20.25 13.5469 20.1656 13.5469 20.0625V18.9375C13.5469 18.8344 13.4625 18.75 13.3594 18.75ZM19.3594 18.75H15.1875C15.0844 18.75 15 18.8344 15 18.9375V20.0625C15 20.1656 15.0844 20.25 15.1875 20.25H19.3594C19.4625 20.25 19.5469 20.1656 19.5469 20.0625V18.9375C19.5469 18.8344 19.4625 18.75 19.3594 18.75ZM10.3359 18.75H9.21094C9.10781 18.75 9.02344 18.8344 9.02344 18.9375V20.0625C9.02344 20.1656 9.10781 20.25 9.21094 20.25H10.3359C10.4391 20.25 10.5234 20.1656 10.5234 20.0625V18.9375C10.5234 18.8344 10.4391 18.75 10.3359 18.75Z" fill="white" />
              </Svg>
            </TouchableOpacity>
            <View style={{ flexDirection: 'column', justifyContent: 'center', top: -8 }}>
              <View style={styles.circleNoti}>
                <Text style={styles.textNoti}>1</Text>
              </View>

              <View style={styles.button_check} onPress={Actions.onPressBarcode}>
                <Svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <Path d="M25.5 24H24.75V13.375C24.75 8.96562 21.4906 5.32187 17.25 4.71562V3.5C17.25 2.80937 16.6906 2.25 16 2.25C15.3094 2.25 14.75 2.80937 14.75 3.5V4.71562C10.5094 5.32187 7.25 8.96562 7.25 13.375V24H6.5C5.94687 24 5.5 24.4469 5.5 25V26C5.5 26.1375 5.6125 26.25 5.75 26.25H12.5C12.5 28.1812 14.0688 29.75 16 29.75C17.9312 29.75 19.5 28.1812 19.5 26.25H26.25C26.3875 26.25 26.5 26.1375 26.5 26V25C26.5 24.4469 26.0531 24 25.5 24ZM16 27.75C15.1719 27.75 14.5 27.0781 14.5 26.25H17.5C17.5 27.0781 16.8281 27.75 16 27.75ZM9.5 24V13.375C9.5 11.6375 10.175 10.0062 11.4031 8.77812C12.6312 7.55 14.2625 6.875 16 6.875C17.7375 6.875 19.3687 7.55 20.5969 8.77812C21.825 10.0062 22.5 11.6375 22.5 13.375V24H9.5Z" fill="white" />
                </Svg>
              </View>
            </View>
          </View>
        </View>

        <View />
        <View style={styles.block_boss}>
          <View>
            <View style={styles.block1}>
              <View style={styles.block_image}>
                <Image
                  style={styles.image_info}
                  source={require('../../Images/news1.png')}
                />
              </View>
              <View style={styles.block_info}>
                <View style={styles.block2}>
                  <Text style={styles.sub_text}>{name}</Text>
                  <View
                    style={{
                      borderLeftWidth: 1,
                      borderLeftColor: Colors.grey7,
                      marginHorizontal: Metrics.marginTiny,
                    }}
                  />
                  <Text style={styles.position}>{position && position.type === 'seller' ? 'N.V bán hàng' : 'Trưởng cửa hàng' }</Text>
                </View>

                <Text style={styles.caption}>
                  {storeName || 'chưa cập nhật' }
                </Text>

              </View>
              <View />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.ListPaper}>
        <SwiperFlatList
          showPagination
          paginationStyleItem={styles.panigation}
          data={entriesData}
          renderItem={renderItem}
        />

      </View>
      <View style={styles.blockButton}>
        <View style={styles.appblock}>
          <DashboardMore
            appicon="clock"
            _styles={styles.blockBorder}
            _style2={styles.text_block}
            manager_dashboard
            apptitle="Trưởng cửa hàng"
            onPressButton={() => Actions.onPressItemButton('ShiftLeader')}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Bán hàng"
            _styles={styles.block}
            _style2={styles.text_block}
            SaleDashboard
            onPressButton={() => Actions.onPressItemButton('Saller')}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Tin tức"
            _styles={styles.blockBorder1}
            _style2={styles.text_block}
            NewsDashboard
            onPressButton={() => Actions.onPressItemButton('News')}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Cài đặt"
            _styles={styles.blockBorder}
            _style2={styles.text_block}
            SettingDashboard
            onPressButton={() => Actions.onPressSetting()}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Hướng dẫn"
            _styles={styles.block}
            _style2={styles.text_block}
            IntroDashboard
            onPressButton={() => Actions.onPressButton()}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Trợ giúp"
            _styles={styles.blockBorder1}
            _style2={styles.text_block}
            HelpDashboard
            onPressButton={() => Actions.onPressHelp()}
          />
        </View>
      </View>
      <RootNotify />
      <ScrollView />
      <Modal isVisible={isModalVisible} onBackdropPress={Actions.hideModal}>
        <View style={styles.Modal}>
          <View style={styles.contentBarcode}>
            <View style={styles.barcodeblock}>
              <Barcode value={uuid} format="CODE128" height={styles.barcode.height} width={styles.barcode.width} />
            </View>
            <Text style={styles.barcodeText}>{uuid}</Text>
          </View>
        </View>
      </Modal>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps);
  // return false;
}


// Sample Data

const entriesData = [
  {
    title: 'Tin số 1',
    img: require('../../Images/news1.png'),
  },
  {
    title: 'Tin số 1',
    img: require('../../Images/news1.png'),
  },
  {
    title: 'Tin số 1',
    img: require('../../Images/news1.png'),
  },
  {
    title: 'Tin số 1',
    img: require('../../Images/news1.png'),
  },
];
