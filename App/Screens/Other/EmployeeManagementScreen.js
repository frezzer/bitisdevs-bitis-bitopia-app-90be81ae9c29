
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, ScrollView, FlatList, TouchableOpacity, Image, Alert,
} from 'react-native';
import moment from 'moment';
import screensName from '../../Navigation/Screens';
import { Colors, Metrics } from '../../Themes';
import { Unicons, Feather } from '../../Components/Icons';


// Styles
import styles from './Styles/MissionScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';
// import EmployeeInfo from '../../Components/Employee/EmployeeInfo';
import EmployeeStatus from '../../Components/Employee/EmployeeStatus';
import LoadingInteractions from '../../Components/LoadingInteractions';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import AttendanceUserActions, { AttendanceUserSelectors } from '../../Redux/attendance/AttendanceUserRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';
import UserFindActions, { UserFindSelectors } from '../../Redux/user/FindUserRedux';


const navigationOptions = (props) => ({
  headerStyle: {
    backgroundColor: Colors.blue2,
    borderBottomWidth: 0,
  },
  title: 'Danh sách nhân viên',

});


EmployeeManagementScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function EmployeeManagementScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  // useStates

  // selector store
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me)) || {};
  const employee_uuid = _.get(mePayload, 'username', null);
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const name = _.concat([last_name, first_name ? ' ' : '', first_name]);
  // const name = _.concat([last_name, first_name]);
  //
  console.tron.log('debug: EmployeeManagementScreen -> name', name);
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeName = _.get(findStoreSellerPayload, ['store_name']);
  const storeUuid = _.get(findStoreSellerPayload, ['store_uuid']);
  const attendanceUsers = useSelector((state) => AttendanceUserSelectors.selectPayload(state.attendance.attendanceUser));
  console.tron.log('debug: EmployeeManagementScreen -> attendanceUsers', attendanceUsers);
  const userDetails = useSelector((state) => UserFindSelectors.selectPayload(state.user.userFind));
  const usersNoLeader = _.filter(userDetails, (i) => { return i.username !== employee_uuid; });
  console.tron.log('debug: EmployeeManagementScreen -> usersNoLeader', usersNoLeader);
  const usersStatusOfSeller = _.map(usersNoLeader, (i) => {
    const statusUser = _.find(attendanceUsers.users, (ii) => { return ii.userUuid === i.username; });
    return {
      ...i,
      status: statusUser ? statusUser.status : undefined,
    };
  });
  console.tron.log('debug: EmployeeManagementScreen -> usersStatusOfSeller', usersStatusOfSeller);

  // const AttendanceUserPayload
  // map user

  const position = 'chưa cập nhật';
  const dateNow = new Date();

  // effects
  React.useEffect(() => {
    console.tron.log('EmployeeManagementScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // dispatch(AttendanceMonthSellerActions.AttendanceMonthSellerRequest({
      if (storeUuid) {
        dispatch(AttendanceUserActions.AttendanceUserRequest({
          dateNow,
          storeUuid,
        }));
      }

      // }));
    }
    return () => { };
  }, [/** input */]);
  React.useEffect(() => {
    if (!_.isEmpty(attendanceUsers.users)) {
      dispatch(UserFindActions.UserFindRequest({
        username_in: attendanceUsers.users.map((i) => i.userUuid),
      }));
    }
  }, [attendanceUsers]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */

  const Actions = {
    actionsfunction: (params) => { },
    onPressToInfo: (roleType, dataEmployee) => {
      console.tron.log('debug: EmployeeManagementScreen -> data', roleType, dataEmployee);
      roleType === 'shiftleader'
        ? props.navigation.navigate(screensName.PickDayScreen)
        : (dataEmployee.status ? props.navigation.navigate(screensName.ManagerShiftSummaryOfSellerScreen, { dataInfo: dataEmployee }) : Alert.alert('Thông báo', 'Nhân viên chưa gửi bản chấm công tháng'));
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    name,
    storeName,
    position,
    usersStatusOfSeller,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    name,
    storeName,
    position,
    usersStatusOfSeller,
  } = props;


  if (initialRender) return <LoadingInteractions size="large" />;
  return (
    <View style={styles.container}>
      <View style={styles.block_boss}>
        <TouchableOpacity onPress={() => Actions.onPressToInfo('shiftleader')}>
          <View style={styles.block1}>
            <View style={styles.block_image}>
              <Image
                style={styles.image_info}
                source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png' }}

              />
            </View>
            <View style={styles.block_info}>
              <View style={styles.block2}>
                <Text style={styles.sub_text}>{name}</Text>
                <View
                  style={{
                    borderLeftWidth: 1,
                    borderLeftColor: Colors.white,
                    marginHorizontal: Metrics.marginTiny,
                  }}
                />
                <Text style={styles.position}>{position || 'chưa cập nhật'}</Text>
              </View>
              <Text style={styles.caption}>{storeName || 'chưa cập nhật'}</Text>
            </View>
            <View style={styles.icon}>
              <Feather style={styles.icon_arrow} name="chevron-right" />
            </View>
          </View>
        </TouchableOpacity>

      </View>
      <ScrollView style={styles.container}>
        <View>
          <EmployeeStatus dataEmployee={usersStatusOfSeller} hasArrow callBackParent={Actions.onPressToInfo} />
        </View>
      </ScrollView>
    </View>
  );
}, areEqual);


function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
