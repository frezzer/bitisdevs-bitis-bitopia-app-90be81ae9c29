/* eslint-disable no-mixed-operators */

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, TextInput, Alert,
} from 'react-native';
import CheckBox from 'react-native-check-box';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment, { now } from 'moment';
import { SafeAreaView } from 'react-navigation';
import DatePicker from 'react-native-datepicker';
import { widthPercentageToDP, widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Colors, Fonts } from '../../Themes';


// import { CheckBox } from 'react-native-elements'
// import { Colors } from '../../Themes';

// Styles
import styles from './Styles/FilterScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../Redux/shift/ShiftFindRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';


// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');

/** Quân đọc lại nhé */
// const initialState = {
//   isCheckedMonth: true,
//   isCheckedOption: false,
// };

/**
 * Main
 */
export default function FilterScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [isChecked, setIsChecked] = React.useState(false);
  const [isCheckedMonth, setIsCheckedMonth] = React.useState(false);
  const [isCheckedOption, setIsCheckedOption] = React.useState(false);
  const [fromDate, setFromDate] = React.useState('');
  const [toDate, setToDate] = React.useState('');
  const [disablePicker, setDisablePicker] = React.useState(true);

  /** Quân đọc lại nhé */
  // xử lý
  // const [stateDefine, setStateDefine] = React.useState(initialState, {});
  // setStateDefine({ ...stateDefine, isCheckedMonth: false, isCheckedOption: true });
  // setIsCheckedMonth(false);
  // reset
  // setStateDefine(initialState);

  const time7daysAgo = moment().startOf('week');
  const time30daysAgo = moment().startOf('month');

  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const employeeUuid = _.get(mePayload, 'username', null);
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const roleTypeUser = _.get(mePayload, ['roles']);
  const roleshiftleader = _.find(roleTypeUser, (role) => role === 'shiftleader');

  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);
  // React.


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressButton: () => {
      if (moment(fromDate).isAfter(moment(toDate))) return Alert.alert('THÔNG BÁO', 'Vui lòng chọn lại ngày kết thúc');

      const { navigation } = props;
      const actionWillUse = navigation.getParam('defineActionWillUse', () => {});
      const moreOption = navigation.getParam('type', undefined);

      /**
       * Handle data filter for case has more filter
       */
      const handleParamsRequest = {
        ...actionWillUse.data,
        user_uuid: roleshiftleader ? employeeUuid : undefined,
        store_uuid: findStoreSellerPayload.store_uuid,
        'attendances.user_uuid': !roleshiftleader ? employeeUuid : undefined,
        created_at_gte: fromDate ? moment(moment(fromDate).format('YYYY-MM-DD 00:00:00')).toISOString() : undefined,
        created_at_lte: toDate ? moment(moment(toDate).format('YYYY-MM-DD 23:59:59')).toISOString() : undefined,
      };
      if (moreOption) {
        actionWillUse.data = handleParamsRequest;
      } else {
        actionWillUse.data.created_at_gte = fromDate ? moment(moment(fromDate).format('YYYY-MM-DD 00:00:00')).toISOString() : undefined;
        actionWillUse.data.created_at_lte = toDate ? moment(moment(toDate).format('YYYY-MM-DD 23:59:59')).toISOString() : undefined;
      }
      dispatch(actionWillUse);
      props.navigation.goBack();
    },


  };
  const state = {
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    isChecked,
    setIsChecked,
    isCheckedMonth,
    setIsCheckedMonth,
    isCheckedOption,
    setIsCheckedOption,
    fromDate,
    toDate,
    setFromDate,
    setToDate,
    time30daysAgo,
    time7daysAgo,
    disablePicker,
    setDisablePicker,
  };

  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  // console.tron.log('SampleScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    state,
    isChecked,
    setIsChecked,
    isCheckedMonth,
    setIsCheckedMonth,
    isCheckedOption,
    setIsCheckedOption,
    fromDate,
    toDate,
    setFromDate,
    setToDate,
    time7daysAgo,
    time30daysAgo,
    disablePicker,
    setDisablePicker,

  } = props;
  // eslint-disable-next-line no-empty-pattern
  const { } = state;


  // console.tron.log("TCL: isChecked1", isChecked1)
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.title}>Bộ lọc</Text>

        <View style={styles.block_check1}>

          <CheckBox
            style={styles.check}
            onClick={() => {
              setIsChecked(!isChecked);
              setIsCheckedMonth(false);
              setIsCheckedOption(false);
              setFromDate(time7daysAgo);
              setToDate(new Date());
              setDisablePicker(true);
            }}
            isChecked={isChecked}
            rightText="Tuần này"
            checkBoxColor={Colors.block}
            uncheckedCheckBoxColor={Colors.block}
            checkedCheckBoxColor={Colors.blue5}
          />
        </View>
        <View style={styles.block_check1}>

          <CheckBox
            style={styles.check}
            onClick={() => {
              setIsCheckedMonth(!isCheckedMonth);
              setIsChecked(false);
              setIsCheckedOption(false);
              setFromDate(time30daysAgo);
              setToDate(new Date());
              setDisablePicker(true);
            }}
            isChecked={isCheckedMonth}
            rightText="Tháng này"
            checkBoxColor={Colors.block}
            uncheckedCheckBoxColor={Colors.block}
            checkedCheckBoxColor={Colors.blue5}
            // disabled={disableMonth}
          />
        </View>
        <View style={styles.block_check1}>

          <CheckBox
            style={styles.check}
            onClick={() => {
              setIsCheckedOption(!isCheckedOption);
              setIsChecked(false);
              setIsCheckedMonth(false);
              setDisablePicker(false);
            }}
            isChecked={isCheckedOption}
            rightText="Tùy chỉnh"
            checkBoxColor={Colors.block}
            uncheckedCheckBoxColor={Colors.block}
            checkedCheckBoxColor={Colors.blue5}
          />
        </View>

        <View style={styles.block_element1}>
          <Text style={styles.text3_1}>Ngày bắt đầu</Text>
          <TextInput
            value={fromDate === '' ? 'Chọn ngày/tháng/năm' : moment(fromDate).format('DD/MM/YYYY')}
            editable={false}
            style={{
              position: 'absolute', right: wp(16 / 375 * 100), top: wp(10 / 375 * 100), color: toDate === '' ? Colors.grey5 : Colors.grey9, fontSize: Fonts.size.textMedium,
            }}
          />
          <DatePicker
            style={{ width: 200 }}
            date={fromDate}
            mode="date"
            format="MM/DD/YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            hideText
            onDateChange={(date) => { return setFromDate(date); }}
            disabled={disablePicker}
          />
        </View>
        <View style={styles.block_element1}>
          <Text style={styles.text3_1}>Ngày kết thúc</Text>
          <TextInput
            value={toDate === '' ? 'Chọn ngày/tháng/năm' : moment(toDate).format('DD/MM/YYYY')}
            editable={false}
            style={{
              position: 'absolute', right: wp(16 / 375 * 100), top: wp(10 / 375 * 100), color: toDate === '' ? Colors.grey5 : Colors.grey9, fontSize: Fonts.size.textMedium,
            }}
          />
          <DatePicker
            style={{ width: 200 }}
            date={toDate}
            mode="date"
            format="MM/DD/YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            hideText
            onDateChange={(date) => {
              return setToDate(date);
            }}
            disabled={disablePicker}
          />
        </View>

        <View style={styles.block_check3}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => Actions.onPressButton()}
          >
            <Text style={styles.text_button}> Áp dụng </Text>
          </TouchableOpacity>


        </View>
      </View>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
