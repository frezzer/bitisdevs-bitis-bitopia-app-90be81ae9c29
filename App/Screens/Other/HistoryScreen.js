// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions,
  InteractionManager,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { FlatList } from 'react-native-gesture-handler';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Feather, Unicons } from '../../Components/Icons';


// Styles
import styles from './Styles/HistoryScreen.styles';
import { Colors, Metrics } from '../../Themes';


// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import FindstoreActions, { FindstoreSelectors } from '../../Redux/shift/FindStoreRedux';
import AttendanceFindActions, { attendanceFindSelectors } from '../../Redux/attendance/AttendanceFindRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';


// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');

const navigationOptions = (props) => {
  return {
    backgroundColor: Colors.red,
    title: 'Lịch sử chấm công',
    screenProps: { headerPurple: true },
  };
};

HistoryScreen.navigationOptions = navigationOptions;

/**
 * Main
 */

const RenderItem = React.memo((props) => {
  const { item } = props;
  console.tron.log('debug: item', item);
  const renderTimeDayShift = _.find(item.data, (atendance) => {
    return atendance.shift ? atendance.shift.shifttype : atendance.shift === 1;
  }) || {};
  const renderTimeNightShift = _.find(item.data, (atendance) => {
    return atendance.shift ? atendance.shift.shifttype : atendance.shift === 2;
  }) || {};
  console.tron.log('renderTimeNightShift.login_out', renderTimeNightShift, renderTimeNightShift.logout_time);
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.data}>
        <Grid>
          <Col style={styles.column1}>
            <Text style={styles.tableData}>{item.created_at}</Text>
          </Col>
          <Col style={styles.column2}>
            {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
            <Text style={styles.tableData}>
              {(renderTimeDayShift.login_time && moment(renderTimeDayShift.login_time).format('HH:mm')) || ''}
              {(renderTimeDayShift.login_time || renderTimeDayShift.login_out) ? ' - ' : '--'}
              {(renderTimeDayShift.logout_time && moment(renderTimeDayShift.logout_time).format('HH:mm')) || ''}
            </Text>
          </Col>
          <Col style={styles.column3}>
            {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
            <Text style={styles.tableData}>
              {(renderTimeNightShift.login_time && moment(renderTimeNightShift.login_time).format('HH:mm')) || ''}
              {(renderTimeNightShift.login_time || renderTimeNightShift.logout_time) ? ' - ' : '--'}
              {(renderTimeNightShift.logout_time && moment(renderTimeNightShift.logout_time).format('HH:mm')) || ''}
            </Text>
          </Col>
        </Grid>
        {/* <Text>ABC</Text> */}
      </View>
      <View style={styles.lineStyle} />
    </View>
  );
}, areEqual);


export default function HistoryScreen(props) {
  const staffData = props.navigation.getParam('staffData', {});
  console.tron.log('debug: HistoryScreen -> staffData', staffData);
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const userDetail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));

  const userUuid = _.get(staffData, 'username', null);
  const checkLeaderExist = _.find(staffData.roles, (item) => {
    return item === 'shiftleader';
  });
  console.tron.log('debug: HistoryScreen -> checkLeaderExist', checkLeaderExist);

  const firstName = _.get(staffData, 'firstName', '');
  const lastName = _.get(staffData, 'lastName', '');
  const middleName = _.get(userDetail, ['data', 'middleName'], '');
  const name = _.concat([lastName, firstName ? ' ' : '', firstName]);
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeName = _.get(findStoreSellerPayload, ['store_name']);
  const storeUuid = _.get(findStoreSellerPayload, ['store_uuid']);
  const position = checkLeaderExist ? 'Trưởng cửa hàng' : 'Nhân viên bán hàng';
  const dataTable = useSelector((state) => attendanceFindSelectors.selectPayload(state.attendance.find));
  console.tron.log('debug: HistoryScreen -> dataTable', dataTable);
  const dataIn = _.groupBy(dataTable, (item) => {
    return moment(item.created_at).format('DD/MM/YYYY');
  });
  console.tron.log('debug: HistoryScreen -> dataIn', dataIn);
  const dataOut = _.reduce(dataIn, (result = [], val, key) => {
    const temp = {
      created_at: key,
      data: val,
    };
    result.push(temp);
    return result;
  }, []);
  console.tron.log('debug: HistoryScreen -> dataOut', dataOut);

  // effects
  React.useEffect(
    () => {
      // didmount
      if (initialRender) {
        InteractionManager.runAfterInteractions(() => {
          setInitialRender(false);
        });
        dispatch(AttendanceFindActions.attendanceFindRequest({
          user_uuid: userUuid,
          'shift.store_uuid': !checkLeaderExist ? storeUuid : undefined,
          _populate: ['shift'],
          deleted: false,
          confirmedSeller: checkLeaderExist ? undefined : 1,
          confirmedShiftLeader: checkLeaderExist ? 1 : undefined,
          // fromdate
          // to date
        }));
        // ...
      }

      // clear when Screen off
      return () => {};
    },
    [
      /** input */
    ],
  );

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {};
  const state = {};

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    storeName,
    firstName,
    position,
    dataOut,
    name,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  // declare variables
  const {
    initialRender,
    Actions,
    state,
    storeName,
    firstName,
    position,
    dataOut,
    name,
  } = props;
  const renderItem = ({ item }) => <RenderItem item={item} />;
  // const { } = state;
  // const { } = Actions;
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.block_boss}>
          <TouchableOpacity>
            <View style={styles.block1}>
              <View style={styles.block_image}>
                <Image
                  style={styles.image_info}
                  source={{
                    uri:
                      'https://shoutem.github.io/img/ui-toolkit/examples/image-6.png',
                  }}
                />
              </View>
              <View style={styles.block_info}>
                <View style={styles.block2}>
                  <Text style={styles.sub_text}>{name}</Text>
                  <View
                    style={{
                      borderLeftWidth: 1,
                      borderLeftColor: Colors.grey7,
                      marginHorizontal: Metrics.marginTiny,
                    }}
                  />
                  <Text style={styles.position}>{position}</Text>
                </View>
                <Text style={styles.caption}>
                  {storeName}
                </Text>
              </View>
              <View />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <View style={styles.table}>
            <Grid>
              <Col style={styles.column1}>
                <Text style={styles.titleTable}>Ngày/tháng</Text>
              </Col>
              <Col style={styles.column2}>
                <Text style={styles.titleTable}>Điểm danh</Text>
                <Text style={styles.titleTable}>ca sáng</Text>
              </Col>
              <Col style={styles.column3}>
                <Text style={styles.titleTable}>Điểm danh </Text>
                <Text style={styles.titleTable}>ca chiều</Text>
              </Col>
            </Grid>
          </View>
          <View style={{ flex: 1 }}>
            <FlatList
              keyExtractor={(item, index) => item.id}
              data={dataOut}
              renderItem={renderItem}
              initialNumToRender={3}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
