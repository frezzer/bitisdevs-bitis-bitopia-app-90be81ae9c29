'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import { Dimensions, InteractionManager, View, Text, ScrollView } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import MissionList from '../../Components/Mission/MissionList'

// Styles
import styles from './Styles/MissionScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux'

// Navigation
// import { Navigation } from '../../Navigation/Navigation'
// import { hideBottomTabsOptions, hideTopbarOptions } from '../../Navigation/Options';


// utils
import { compareOmitState } from "../../Services/utils";
const { width } = Dimensions.get('window');

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Nhiệm vụ',
  };
}

MissionScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function MissionScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  // useStates

  // selector store
  const me_error = useSelector(state => MeSelectors.selectError(state.me));

  // effects
  React.useEffect(() => {
    console.tron.log('MissionScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction

    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    actionsfunction: (params) => { },
  }
  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions
  }
  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const { initialRender, Actions } = props;


  // if (initialRender) return <LoadingInteractions size={'large'} />
  return (
    <ScrollView style={styles.container}>
      <View style={styles.content}>
        <MissionList />
      </View>
    </ScrollView>
  );


}, areEqual);


function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
