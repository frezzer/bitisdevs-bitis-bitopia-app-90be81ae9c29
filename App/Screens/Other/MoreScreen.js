
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, SafeAreaView, TouchableOpacity, Alert, Image,
} from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Feather } from '../../Components/Icons';
import BlockDouble from '../../Components/Block/BlockDouble';
import screensName from '../../Navigation/Screens';
import { Images, Metrics, Colors } from '../../Themes';
import DashboardMore from '../../Components/DashBoardMore';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';

// Styles
import styles from './Styles/MoreScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// Navigation


// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');


/**
 * Options
 *  - navigation options for Screens
 * */
const navigationOptions = (props) => {
  const onPressGoDashboard = () => props.navigation.navigate(screensName.DashBoardScreen);
  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Quản lý cửa hàng',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS == 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={onPressGoDashboard}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack} />

        </TouchableOpacity>
      );
    },
  };
};


MoreScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function MoreScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);

  // useState
  // selector store
  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const me_error = useSelector((state) => MeSelectors.selectError(state.me));
  const employee_id = _.get(me_payload, 'employee_id', {});
  const roleTypeUser = _.get(me_payload, ['roles']);
  const roleshiftleader = _.find(roleTypeUser, (i) => i == 'shiftleader');
  const roleseller = _.find(roleTypeUser, (i) => i == 'seller');

  // effects
  React.useEffect(() => {
    console.tron.log('MoreScreen effect');
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressItem: (screensName = '') => {
      console.tron.log('screensName', screensName);
      if (_.isObject(screensName)) {
        Alert.alert('THÔNG BÁO', 'Chức năng hiện đang phát triển');
      }
      props.navigation.navigate(screensName);
    },
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    roleshiftleader,
    roleseller,
    Actions,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('MoreScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    roleshiftleader,
    roleseller,
  } = props;


  if (initialRender) return <LoadingInteractions size="large" />;
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {roleseller == 'seller' && (
        // <View style={styles.part1}>
        //   <BlockDouble
        //     buttonText1="Danh sách điểm danh"
        //     buttonText2="Thông tin điểm danh cá nhân"
        //     iconName="clock-three"
        //     onPressButton={() => Actions.onPressItem(screensName.SellerListShiftScreen)}
        //   />
        // </View>
        <View style={styles.appblock}>
          <DashboardMore
            appicon="clock"
            _styles={styles.block}
            _styles2={styles.text_block}
            manager_shift
            apptitle="Điểm danh ca"
            onPressButton={() => Actions.onPressItem(screensName.SellerListShiftScreen)}
          />
          <View
            style={{
              borderLeftWidth: 1,
              borderLeftColor: Colors.blue4,
              marginVertical: Metrics.marginXHuge,
            }}
          />
          <DashboardMore
            appicon="clock"
            apptitle="Bảng chấm công"
            _styles={styles.block}
            _styles2={styles.text_block}
            manager_emp
            onPressButton={() => Actions.onPressItem(screensName.PickDayScreen)}
          />
          <View style={styles.apphidden} />
        </View>
        )}

        {roleshiftleader == 'shiftleader'
          && (
          <View>
            {/* <BlockDouble
            buttonText1="Quản lý ca"
            buttonText2="Danh sách ca đã tạo"
            iconName="calender"
            onPressButton={() => Actions.onPressItem(screensName.ManagerListShiftScreen)}
          />

          <BlockDouble
            buttonText1="Quản lý nhân viên"
            buttonText2="Tổng hợp phiếu điểm danh"
            iconName="users-alt"
            onPressButton={() => Actions.onPressItem(screensName.EmployeeManagementScreen)}
          /> */}


            <View style={styles.appblock}>
              <DashboardMore
                appicon="clock"
                _styles={styles.block}
                _styles2={styles.text_block}
                manager_shift
                apptitle="Quản lí ca"
                onPressButton={() => Actions.onPressItem(screensName.ManagerListShiftScreen)}
              />
              <View
                style={{
                  borderLeftWidth: 1,
                  borderLeftColor: Colors.grey4,
                  marginVertical: Metrics.marginXHuge,
                  // backgroundColor: Colors.
                }}
              />
              <DashboardMore
                appicon="clock"
                apptitle="Quản lí nhân viên"
                _styles={styles.block}
                _styles2={styles.text_block}
                manager_emp
                onPressButton={() => Actions.onPressItem(screensName.EmployeeManagementScreen)}
              />
              <View style={styles.apphidden} />
            </View>
          </View>
          )}
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
