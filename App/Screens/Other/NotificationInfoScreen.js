
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  Dimensions, InteractionManager, View, Text, SafeAreaView, ScrollView, TouchableOpacity, Animated, Platform,
} from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Colors, Metrics } from '../../Themes';
// Styles
import styles from './Styles/SampleScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 80;
const HEADER_MIN_HEIGHT = 40;

const navigationOptions = (props) => ({
  headerStyle: {
    backgroundColor: Colors.blue1,
    borderBottomWidth: 0,
    // height: 30,
  },
});


NotificationInfoScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function NotificationInfoScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {};
  const state = {};


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender, Actions, state, scrollY, setScrollY,
  } = props;
  // this will set a height for topbar
  const renderItem = ({ item }) => {
    return (
      <View>
        <Text> abc </Text>
      </View>
    );
  };

  return (
    <View>

    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}

const data = [

];
