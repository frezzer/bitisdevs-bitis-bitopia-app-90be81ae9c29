
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, AsyncStorage,
} from 'react-native';
import {
  Svg, Circle, Polygon, Polyline, Path, Rect, G,
} from 'react-native-svg';
import { Colors } from '../../Themes';
import LoadingInteractions from '../../Components/LoadingInteractions';
import RectangleButton from '../../Components/Button/RectangleButton';
import screensName from '../../Navigation/Screens';
import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';
import Switch from '../../Lib/react-native-switch/Switch';


// Styles
import styles from './Styles/NotificationScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// Navigation


// utils
import { compareOmitState } from '../../Services/utils';
import { Feather } from '../../Components/Icons';

const { width } = Dimensions.get('window');


/**
 * Options
 *  - navigation options for Screens
 * */


const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Thông báo',
  };
};

NotificationScreen.navigationOptions = navigationOptions;

// EmployeeBarCodeScreen.options = options;
/**
 * Main
 */
export default function NotificationScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [switchValue, setSwitchValue] = React.useState(true);
  const [switchValue1, setSwitchValue1] = React.useState(true);
  const [switchValue2, setSwitchValue2] = React.useState(true);

  const toggleSwitch = (value) => {
    setSwitchValue(value);
  };
  const toggleSwitch1 = (value) => {
    setSwitchValue1(value);
  };
  const toggleSwitch2 = (value) => {
    setSwitchValue2(value);
  };


  // useStates

  // selector store
  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));

  // effects
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
    }
  }, []);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressInfo: () => {
      props.navigation.navigate(screensName.ProfileScreen);
    },
  };
  const onPressLogout = async () => {
    dispatch(LoginActions.logout());
    await AsyncStorage.removeItem('Authorization');
    await props.navigation.navigate('Initial');
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    me_payload,
    Actions,
    onPressLogout,
    toggleSwitch,
    switchValue,
    setSwitchValue,
    toggleSwitch1,
    switchValue1,
    setSwitchValue1,
    toggleSwitch2,
    switchValue2,
    setSwitchValue2,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('EmployeeBarCodeScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    onPressLogout,
    toggleSwitch,
    switchValue,
    setSwitchValue,
    toggleSwitch1,
    switchValue1,
    setSwitchValue1,
    toggleSwitch2,
    switchValue2,
    setSwitchValue2,
  } = props;
  if (initialRender) return <LoadingInteractions size="large" />;


  return (
    <View style={styles.container}>
      <View style={{backgroundColor:'white'}}>
        <View style={styles.blockSwitch}>
          <View style={styles.block}>
            <Text style={styles.text1}>Cho phép thông báo</Text>
            <View style={styles.switch_button}>
              <Switch
                onValueChange={toggleSwitch}
                onChangeText={(switchValue) => setSwitchValue(switchValue)}
                value={switchValue}
                circleActiveColor="white"
                backgroundActive={Colors.green1}
                circleBorderWidth={0.1}
                backgroundInactive={Colors.greybold}
                circleInActiveColor={Colors.white}
                circleSize={25}
                changeValueImmediately
                innerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                outerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                switchLeftPx={2.2}
                switchRightPx={2.2}
              />
            </View>
          </View>
        </View>
        <View style={styles.lineStyle} />
        <View style={styles.blockSwitch}>
          <View style={styles.block}>
            <Text style={styles.text1}>Âm thanh</Text>
            <View style={styles.switch_button}>
              <Switch
                onValueChange={toggleSwitch1}
                onChangeText={(switchValue1) => setSwitchValue1(switchValue1)}
                value={switchValue1}
                circleActiveColor="white"
                backgroundActive={Colors.green1}
                circleBorderWidth={0.1}
                backgroundInactive={Colors.greybold}
                circleInActiveColor={Colors.white}
                circleSize={25}
                changeValueImmediately
                innerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                outerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                switchLeftPx={2.2}
                switchRightPx={2.2}
              />
            </View>
          </View>
        </View>
        <View style={styles.lineStyle} />
        <View style={styles.blockSwitch}>
          <View style={styles.block}>
            <Text style={styles.text1}>Rung khi có thông báo</Text>
            <View style={styles.switch_button}>
              <Switch
                onValueChange={toggleSwitch2}
                onChangeText={(switchValue2) => setSwitchValue2(switchValue2)}
                value={switchValue2}
                circleActiveColor="white"
                backgroundActive={Colors.green1}
                circleBorderWidth={0.1}
                backgroundInactive={Colors.greybold}
                circleInActiveColor={Colors.white}
                circleSize={25}
                changeValueImmediately
                innerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                outerCircleStyle={{ alignItems: 'center', justifyContent: 'center' }}
                switchLeftPx={2.2}
                switchRightPx={2.2}
              />
            </View>
          </View>
        </View>

      </View>

    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
