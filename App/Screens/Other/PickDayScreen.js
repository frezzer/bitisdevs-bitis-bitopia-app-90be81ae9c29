/* eslint-disable no-mixed-operators */

// basics
import _ from 'lodash';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, TextInput, TouchableOpacity, View, Text, Image, Animated, Platform, Alert,
} from 'react-native';
import moment from 'moment';
import { widthPercentageToDP, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import DatePicker from 'react-native-datepicker';
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Feather } from '../../Components/Icons';
import ButtonHandle from '../../Components/Button/ButtonHandle';

import {
  Metrics, Images, Colors, Fonts,
} from '../../Themes';
import AttendanceFindActions, { attendanceFindSelectors } from '../../Redux/attendance/AttendanceFindRedux';
import AttendanceMonthActions, { AttendanceMonthSelectors } from '../../Redux/attendance/AttendanceMonthRedux';
import FindstoreActions, { FindstoreSelectors } from '../../Redux/shift/FindStoreRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';

// Styles
import styles from './Styles/PickDayScreen.styles';

// Actions and Selectors Redux

// utils
import { compareOmitState } from '../../Services/utils';
import screenName from '../../Navigation/Screens';


/**
 * Options
 *  - navigation options for Screens
 * */

const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
PickDayScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function PickDayScreen(props) {
  const { navigation } = props;
  const { onPressButton } = props;
  // console.tron.log("TCL: SellerConfirmScreen -> ShiftFindonlyonePayload", data)


  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [data, setdata] = React.useState([]);
  const [toDate, setToDate] = React.useState('');
  const [navigateFlat, setNavigateFlat] = React.useState(false);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  const storeUuid = _.get(findStoreSellerPayload, ['store_uuid']);
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  console.tron.log('debug: PickDayScreen -> mePayload', mePayload.roles[0]);
  const attendanceFindPayload = useSelector((state) => attendanceFindSelectors.selectPayload(state.attendance.find));
  const attendanceFindFetching = useSelector((state) => attendanceFindSelectors.selectFetching(state.attendance.find));
  const attendanceFindError = useSelector((state) => attendanceFindSelectors.selectError(state.attendance.find));
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));

  const attendanceMonthPayload = useSelector((state) => AttendanceMonthSelectors.selectPayload(state.attendance.attendanceMonth));
  const attendanceMonthFetching = useSelector((state) => AttendanceMonthSelectors.selectFetching(state.attendance.attendanceMonth));
  // set Store UUID

  const userUuid = _.get(mePayload, 'username');
  const startDate = _.get(_.head(data), 'created_at', new Date());

  console.tron.log('findStorePayload', attendanceFindPayload);
  const dataRequest = {
    user_uuid: userUuid,
    _populate: ['shifttype', 'attendances'],
    deleted: false,
    _sort: 'created_at:asc',
    confirmedSeller: (mePayload.roles[1] !== 'shiftLeader') ? 0 : null,
    confirmedShiftLeader: (mePayload.roles[1] === 'shiftLeader') ? 0 : null,
  };

  // effects
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
        dispatch(AttendanceFindActions.attendanceFindRequest(dataRequest));
      });
      // ...
    }
    // clear when Screen off
    return () => { };
  }, [/** input */]);

  React.useEffect(() => {
    if (attendanceFindFetching === false && attendanceFindError == null) {
      setdata(attendanceFindPayload);
    //   const pickDate = attendanceFindPayload[0].created_at;
    }
  }, [attendanceFindPayload]);

  React.useEffect(() => {
    if (attendanceFindFetching === false && attendanceFindError == null) {
      setdata(attendanceFindPayload);
    //   const pickDate = attendanceFindPayload[0].created_at;
    }
  }, [attendanceFindPayload]);

  React.useEffect(() => {
    if (attendanceMonthPayload !== null && attendanceMonthFetching === false && navigateFlat === true) {
      const dataAtendance = {
        ...attendanceMonthPayload,
        meData: mePayload,
        storeData: findStoreSellerPayload,
      };
      // console.tron.log('debug: PickDayScreen -> mePayload.roles[0]', mePayload);
      if (mePayload.roles[1] !== 'shiftleader') props.navigation.navigate(screenName.SellerStaffSummaryScreen, { dataAtendance });
      else props.navigation.navigate(screenName.ManagerShiftSummaryScreen, { dataAtendance });
    }
  }, [attendanceMonthPayload]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSummary: () => {
      props.navigation.navigate(screenName.ManagerShiftSummaryScreen);
    },
    onPressGoBack: () => {
      props.navigation.goBack();
    },
    onPressSendDate: () => {
      if (startDate === null || toDate === '') {
        return Alert.alert('THÔNG BÁO', 'Vui lòng chọn ngày kết thúc!');
      }
      if (moment(startDate).isAfter(moment(toDate))) {
        return Alert.alert('THÔNG BÁO', 'Vui lòng chọn lại ngày kết thúc !');
      }
      setNavigateFlat(true);
      console.tron.log('debug: PickDayScreen -> toDate', toDate, moment(toDate).endOf('day').toISOString());
      const dataAtendanceRequest = {
        userUuid,
        history: 0,
        fromDate: moment(moment(startDate).format('YYYY-MM-DD 00:00:00')).toISOString(),
        toDate: moment(moment(toDate).format('YYYY-MM-DD 23:59:59')).toISOString(),
        storeUuid,
        // confirmedSeller: 1,
      };
      console.tron.log("debug: PickDayScreen -> dataAtendanceRequest", dataAtendanceRequest)
      dispatch(AttendanceMonthActions.AttendanceMonthRequest(dataAtendanceRequest));
    },

  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    onPressButton,
    Actions,
    scrollY,
    toDate,
    setToDate,
    startDate,
    // date_time_picker,
    // fullName

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SellerConfirmScreen render');
  // declare variables
  const {
    initialRender,
    onPressButton,
    Actions,
    scrollY,
    toDate,
    setToDate,
    startDate,
  } = props;
  // ANIMATION
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.blue1],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };

  if (initialRender) return <LoadingInteractions size="large" />;
  return (


    <View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>


        </View>
      </Animated.View>
      {/* </Animated.View> */}
      <View
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: widthPercentageToDP(24 / 375 * 100), flex: 1,
        }}
        >
          <Text style={styles.Title}>Chọn ngày chấm công</Text>
          <View style={styles.block_element}>
            <Text style={styles.text3}>Ngày bắt đầu</Text>
            <Text style={styles.text4}>{moment(startDate).format('DD/MM/YYYY')}</Text>
          </View>
          <View style={styles.block_element1}>
            <Text style={styles.text3_1}>Ngày kết thúc</Text>
            <TextInput
              value={toDate === '' ? 'Chọn ngày/tháng/năm' : moment(toDate).format('DD/MM/YYYY')}
              editable={false}
              style={{
                position: 'absolute', right: wp(16 / 375 * 100), top: wp(10 / 375 * 100), color: toDate === '' ? Colors.grey5 : Colors.grey9, fontSize: Fonts.size.textMedium,
              }}
            />
            <DatePicker
              style={{ width: 200 }}
              date={toDate}
              mode="date"
              format="MM/DD/YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              hideText
              onDateChange={(date) => { return setToDate(date); }}
            />
          </View>
        </View>
      </View>
      <View style={styles.btn_bottom}>
        <ButtonHandle
          buttonText=" Xác nhận"
          stylesText={styles.Text12}
          onPressButton={Actions.onPressSendDate}
          styles={{ backgroundColor: Colors.orange1 }}
        />
      </View>
      {/* </View> */}
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
