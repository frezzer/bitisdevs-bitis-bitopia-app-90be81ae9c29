
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, AsyncStorage, Linking,
} from 'react-native';
import Modal from 'react-native-modal';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';
import { Colors } from '../../Themes';
import LoadingInteractions from '../../Components/LoadingInteractions';
import RectangleButton from '../../Components/Button/RectangleButton';
import screensName from '../../Navigation/Screens';
import LoginActions, { LoginSelectors } from '../../Redux/LoginRedux';
import DoubleButton from '../../Components/Button/DoubleButton';


// Styles
import styles from './Styles/EmployeeBarCodeScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// Navigation


// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');


/**
 * Options
 *  - navigation options for Screens
 * */


const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Cài đặt',
  };
};

SettingScreen.navigationOptions = navigationOptions;

// EmployeeBarCodeScreen.options = options;
/**
 * Main
 */
export default function SettingScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);


  // useStates

  // selector store
  const me_payload = useSelector((state) => MeSelectors.selectPayload(state.me));

  // effects
  React.useEffect(() => {
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
    }
  }, []);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressInfo: () => {
      props.navigation.navigate(screensName.ProfileScreen);
    },
    toggleModal: () => {
      setIsModalVisible(!isModalVisible);
    },
    onPressNotification: () => {
      props.navigation.navigate(screensName.NotificationScreen);
    },
  };
  const onPressLogout = async () => {
    dispatch(LoginActions.logout());
    await AsyncStorage.removeItem('Authorization');
    // await AsyncStorage.removeItem('storeUuid');
    await props.navigation.navigate('Initial');
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    me_payload,
    Actions,
    onPressLogout,
    isModalVisible,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('EmployeeBarCodeScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    onPressLogout,
    isModalVisible,
  } = props;
  if (initialRender) return <LoadingInteractions size="large" />;
  return (
    <View style={styles.container}>
      <View style={{ backgroundColor: 'white' }}>
        <RectangleButton
          name="lock"
          title="Thông tin tài khoản"
          _style={{ color: Colors.orange1 }}
          onPress={Actions.onPressInfo}
        />
        {/* <View style={styles.lineStyle} /> */}
        <RectangleButton
          name="bell"
          title="Thông báo"
          onPress={Actions.onPressNotification}

        />
      </View>
      <View style={styles.blockLogout}>
        <RectangleButton
          name="export"
          title="Đăng xuất"
          _style={{ color: Colors.greybold }}
          onPress={Actions.toggleModal}
        />
        {/* <View style={styles.lineStyle} />

        <RectangleButton
          name="help-circle"
          title="Thông tin về Bitopia"
          _style={{ color: Colors.green1 }}
          // onPress={onPress}
        /> */}
      </View>
      <Modal isVisible={isModalVisible}>
        <View style={styles.Modal}>
          <Text style={styles.modal_title}>Bạn muốn đăng xuất ?</Text>
          {/* <Button title="Hide modal" onPress={Actions.toggleModal} /> */}
          <DoubleButton
            styles={styles.cover}
            onPressButton1={Actions.toggleModal}
            buttonText1="Hủy"
            onPressButton2={onPressLogout}
            buttonText2="Đăng xuất"
          />

        </View>
      </Modal>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
