import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.silver,
    flex: 1,
  },
  part1: {
    flex: 1,
    marginTop: Metrics.marginMedium,
  },
  block_empty: {
    height: wp(48 / 375 * 100),
    backgroundColor: Colors.white,
    alignItems: 'center',
    padding: Metrics.paddingSmall,
  },
  block_separator: {
    paddingHorizontal: Metrics.marginLarge,
    backgroundColor: Colors.white,
  },
  line_style: {
    borderBottomColor: Colors.grey4,
    borderBottomWidth: 1,
  },
  headerRightView: {
    marginRight: Metrics.marginMedium,
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },
});
