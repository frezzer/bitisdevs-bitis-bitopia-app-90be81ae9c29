import { StyleSheet,Dimensions } from 'react-native';
import { widthPercentageToDP, widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  Metrics, ApplicationStyles, Colors, Fonts,
} from '../../../Themes';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
  // heightTitle: heightTitle,
  container: {
    flex: 1,
    backgroundColor: Colors.block,
    // paddingTop: 88
  },
  blockHidden: {
    height: 44 /375 * Metrics.width,
    width: Metrics.width,
    backgroundColor: Colors.blue2,
  },
  content: {
    flex: 1,
    ...ApplicationStyles.specialApp.alignCenter,
  },
  titleView: {
    paddingLeft: Metrics.paddingXLarge,
    paddingTop: Metrics.paddingTiny,
  },
  titleText: {
    fontFamily: Fonts.type.fontExtraBold,
  },
  SearchBar: {
    borderRadius: 18,
    height: 36,
    flex: 1,
  },
  ButtonSearchOverlay: {
    backgroundColor: Colors.greenLight,
    padding: Metrics.paddingMedium,
    borderRadius: Metrics.paddingMedium + 24,
  },
  ButtonSearch: {
    backgroundColor: Colors.greenMedium,
    borderColor: Colors.transparent,
    borderRadius: 24,
    height: 48,
    width: 190,
  },
  BackgroundOverlay: {
    position: 'absolute',
    width: Metrics.height,
    height: Metrics.height,
    borderRadius: Metrics.height / 2,
    top: -(Metrics.height - 76 - 180 - Metrics.marginMedium * 3),
  },
  CalendarTextStyle: {
    lineHeight: 24,
  },

  employeeInfoView: {
    height: 174 / 375 * Metrics.width,
    width: 343 / 375 * Metrics.width,
  },

  carousel: {
    marginVertical: Metrics.marginTiny,
    alignSelf: 'center',
    alignItems: 'center',
    height: 158 / 375 * Metrics.width,
    width: '100%',
  },
  // appblock: {
  //   marginHorizontal: Metrics.marginMedium,
  //   marginVertical: Metrics.marginTiny,
  //   borderRadius: Metrics.boderNomarl,
  //   flexDirection: 'row',
  //   flexWrap: 'wrap',
  //   justifyContent: 'space-between',
  //   // justifyContent: 'flex-start',
  //   height: 'auto',
  //   width: 343 / 375 * Metrics.width,
  // },
  // apphidden: {
  //   paddingVertical: Metrics.paddingXLarge,
  //   paddingHorizontal: Metrics.paddingTiny,
  //   marginVertical: Metrics.marginXTiny,
  //   backgroundColor: 'red',
  //   opacity: 0,
  //   borderRadius: Metrics.boderSmall,
  //   alignSelf: 'center',
  //   alignItems: 'center',
  //   width: 109 / 375 * Metrics.width,
  //   height: 10 / 375 * Metrics.width,
  // },

  titleTopbarText: {
    fontSize: Fonts.size.textXLarge,
    fontStyle: 'italic',
    fontWeight: '800',
  },
  account: {
    ...ApplicationStyles.specialApp.dimensisons(375, 60),
    // mả: 20,
    backgroundColor: Colors.blue2,
    // borderBottomLeftRadius: 24 / 375 * Metrics.width,
    // borderBottomRightRadius: 24 / 375 * Metrics.width,
    zIndex: 2,
    // position: 'absolute',
    // top:200
  },
  hello: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textHuge,
    color: Colors.white,
    // marginTop: wp( 10/375*100)
  },
  area1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: Metrics.marginMedium,
    // backgroundColor:'yellow'
    // paddingTop: wp( 5/375*100),
  },
  area2: {
    flexDirection: 'row',
    // backgroundColor:'blue'

  },
  buttonBarcode: {
    justifyContent: 'center',
    marginRight: 10,
    // paddingTop: 10,
  },
  circleNoti: {
    width: 20 / 375 * Metrics.width,
    height: 20 / 375 * Metrics.width,
    borderRadius: 10 / 375 * Metrics.width,
    backgroundColor: Colors.orange1,
    marginLeft: wp( 16/375*100),
    // top: wp( 5/375*100),
    ...ApplicationStyles.specialApp.alignCenter,
  },
  textNoti: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.white,
  },


  // //////////////
  block_boss: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 74),
    backgroundColor: Colors.blue2,
    borderBottomLeftRadius: 24 / 375 * Metrics.width,
    borderBottomRightRadius: 24 / 375 * Metrics.width,
  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375, 70),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent: 'center',
    paddingHorizontal: Metrics.paddingMedium,
    // backgroundColor:'red'
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent: 'center',
    // marginLeft: Metrics.marginXSmall,
    flex: 6,
  },
  block_image: {
    justifyContent: 'center',
    flex: 1,

  },
  sub_text: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // margin: Metrics.marginXTiny
  },
  caption: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.white,
    marginTop: Metrics.marginXTiny,


  },
  block2: {
    flexDirection: 'row',
  },
  position: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.white,
  },
  icon_arrow: {
    fontSize: wp(30 / 375 * 100),
    color: Colors.black,
  },
  icon: {
    justifyContent: 'center',
  },
  ListPaper: {
    // ...ApplicationStyles.specialApp.dimensisons(375, 220),
    // backgroundColor: 'red',
    zIndex: 1,
    // top: 97 / 375 * Metrics.width,
    // position: 'absolute',
    // flex: 1,
  },
  child: {
    // height: Metrics.width,
    width: Metrics.width,
    justifyContent: 'center',
  },
  imageNews: {
    ...ApplicationStyles.specialApp.dimensisons(375, 220),
    zIndex: 1,
  },
  block: {
    // backgroundColor: 'red',
    // ...ApplicationStyles.specialApp.dimensisons(120, 109),
    // padding: 10,
    width: wp(115 / 375 * 100),
    borderWidth: 0.3,
    borderColor: Colors.greenLight,
  },
  apphidden: {
    paddingVertical: Metrics.paddingXLarge,
    paddingHorizontal: Metrics.paddingTiny,
    marginVertical: Metrics.marginXTiny,
    // backgroundColor: 'red',
    opacity: 0,
    borderRadius: Metrics.boderSmall,
    alignSelf: 'center',
    alignItems: 'center',
    width: 109 / 375 * Metrics.width,
    height: 10 / 375 * Metrics.width,
  },
  text_block: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    textAlign: 'left',
  },
  appblock: {
    // marginHorizontal: Metrics.marginMedium,
    // marginVertical: Metrics.marginTiny,
    // paddingHorizontal: Metrics.paddingMedium,
    // borderRadius: Metrics.boderNomarl,
    flexDirection: 'row',
    flexWrap: 'wrap',
    // justifyContent: 'space-between',
    // justifyContent: 'flex-start',
    height: 'auto',
    // position: 'absolute',
    // width: 343 / 375 * Metrics.width,
    ...ApplicationStyles.specialApp.alignCenter,
    // padding :5
  },
  blockButton: {
    marginTop: Metrics.marginMedium,
    backgroundColor: Colors.white,
  },
  blockBorder: {
    // backgroundColor: 'red',
    // ...ApplicationStyles.specialApp.dimensisons(120, 109),
    // padding: 10,
    width: wp(115 / 375 * 100),
    borderRightWidth: 0.3,
    borderBottomWidth: 0.3,
    borderColor: Colors.greenLight,
  },
  blockBorder1: {
    width: wp(115 / 375 * 100),
    borderLeftWidth: 0.3,
    borderBottomWidth: 0.3,
    borderColor: Colors.greenLight,
  },
  panigation: {
    backgroundColor: 'red',
    ...ApplicationStyles.specialApp.dimensisons(10,10),
    marginBottom: Metrics.marginTiny,
    // padding:10,

  },
  textMore: {
    padding: wp(9 / 375 * 100),
    borderRadius: Metrics.boderXTiny,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    opacity: 1,
  },
  textImage:{
    fontFamily: Fonts.type.fontBold,
    fontSize:Fonts.size.textSmall,
    color:'white'
  },
  Modal:{
    ...ApplicationStyles.specialApp.dimensisons(340,240),
    backgroundColor:'white',
    ...ApplicationStyles.specialApp.alignCenter,
    marginLeft: - wp(2/375*100),
    borderRadius:  wp(16/375*100),
  },
  contentBarcode: {
    flex: 1,
    ...ApplicationStyles.specialApp.alignCenter,
    // transform: [{ rotate: '90deg' }],
  },
  barcode: {
    height: 89 / 375 * Metrics.width,
    // width: 50 / 375 * Metrics.width,
  },
  barcodeText: {
    color: Colors.black,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    // textAlign: 'center'
  }
});

export const BackgroundOverlayGradient = {
  colors: [Colors.greenMedium, Colors.greenDark],
  locations: [0.7, 1],
};
