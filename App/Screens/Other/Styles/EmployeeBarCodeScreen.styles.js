import { StyleSheet } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
// import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  lineStyle: {
    borderWidth: 0.3,
    borderColor: Colors.grey4,
    // width:311
    marginHorizontal: Metrics.marginMedium,
    backgroundColor: 'white',
    // flex: 1,
    // paddingVertical: Metrics.paddingXXSmall,
  },
  blockLogout: {
    marginTop: Metrics.marginHuge,
    // borderWidth: 0.3,
    // borderColor: Colors.greybold,
    // width:311
    // marginHorizontal:Metrics.marginMedium,
    backgroundColor: 'white',
  },
  logo: {
    alignItems: 'center',
    marginTop: widthPercentageToDP(42 / 375 * 100),
  },
  companyName: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textLarge,
    marginTop: widthPercentageToDP(42 / 375 * 100),
    paddingHorizontal: widthPercentageToDP(32 / 375 * 100),
    textAlign: 'center',
  },
  icon: {
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.greybold,
    marginRight: 8,
  },
  info: {
    flexDirection: 'row',
    paddingHorizontal: widthPercentageToDP(30 / 375 * 100),
    // backgroundColor:'red',
    marginTop: Metrics.marginXXLarge,
  },
  text1: {
    marginRight: Metrics.marginSmall,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    textAlign:'center',
    marginTop : widthPercentageToDP(5 / 375 * 100)
  },
  text1_2: {
    marginRight: Metrics.marginSmall,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    // textAlign:'center',
    marginTop : widthPercentageToDP(5 / 375 * 100),
    textDecorationLine:'underline'
  },
  text1_3: {
    marginLeft: - Metrics.marginTiny,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
    // textAlign:'center',
    marginTop : widthPercentageToDP(5 / 375 * 100),
    // textDecorationLine:'underline'
  },
  info1: {
    flexDirection: 'row',
    paddingHorizontal: widthPercentageToDP(30 / 375 * 100),
    // backgroundColor:'red',
    marginTop: Metrics.marginSmall,
  },
  info2: {
    flexDirection: 'row',
    paddingHorizontal: widthPercentageToDP(30 / 375 * 100),
    // backgroundColor:'red',
    marginTop: Metrics.marginSmall,
  },
  text1_1: {
    marginRight: Metrics.marginSmall,
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blue2,
    textAlign: 'center',
    marginTop : widthPercentageToDP(3 / 375 * 100)

  },
  info3: {
    flexDirection: 'row',
    paddingHorizontal: widthPercentageToDP(30 / 375 * 100),
    // backgroundColor:'red',
    marginTop: Metrics.marginXSmall,
    marginBottom: widthPercentageToDP(42 / 375 * 100),
  },
  version: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey7,
    textAlign:'center',
    marginTop: Metrics.marginMedium,

  },
  Modal:{
    ...ApplicationStyles.specialApp.dimensisons(343, 152),
    backgroundColor: Colors.white,
    borderRadius: widthPercentageToDP(16 / 375 * 100),
  },
  modal_title: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textXXLarge,
    textAlign: 'center',
    marginTop: Metrics.marginXXLarge,
  },
  cover: {
    ...ApplicationStyles.specialApp.dimensisons(343, 122),
    borderBottomLeftRadius: Metrics.boderLLLarge,
    borderBottomRightRadius: Metrics.boderLLLarge,
    // backgroundColor:'red',
    // paddingHorizontal: 10

  },
});
