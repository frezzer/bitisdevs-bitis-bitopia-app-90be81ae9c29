import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    // ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor:Colors.block
  }
});
