import { StyleSheet } from 'react-native';
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    paddingTop: Metrics.paddingMedium,
    // backgroundColor:'red',
    paddingHorizontal:Metrics.marginMedium,
  },
  title:{
    fontFamily:Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
     marginBottom:Metrics.marginTiny,

  },

  block_check: {
    height: wp(46 / 375 * 100),
    backgroundColor:'white',
    borderBottomWidth: 0.3,
    borderBottomColor:'#E8E8E8',
    // marginTop:Metrics.marginTiny,
    // borderTopWidth: 0.3,
    justifyContent:'center'

    // paddingHorizontal:Metrics.marginMedium,

  },
  block_check1: {
    height: wp(46 / 375 * 100),
    backgroundColor:'white',
    borderBottomWidth: 0.3,
    borderBottomColor:'#E8E8E8',
    // ...ApplicationStyles.specialApp.alignCenter,
    justifyContent:'center'
    // marginTop:Metrics.marginTiny,
    // borderTopWidth: 1,`
    // paddingHorizontal:Metrics.marginMedium,

  },
  check_box: {
    // height: 30 / 375 * Metrics.width,
    // width: '100%',
    backgroundColor:'white'
  },
  check:{
    // padding: Metrics.paddingXXSmall
    // justifyContent:'center'
  },

  block_check2: {
    height: wp(46 / 375 * 100),
    backgroundColor:'white',
    borderBottomWidth: 0.3,
    borderColor:'#E8E8E8',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
    // marginTop:Metrics.marginTiny,
    // borderTopWidth: 1,`
    // paddingHorizontal:Metrics.marginMedium,

  },
  button:{
    width: wp(79/375*100),
    height: wp (24/375*100),
    // backgroundColor:'red',
    borderRadius: Metrics.boderXTiny,
    ...ApplicationStyles.specialApp.alignCenter,
    borderWidth:1,
    borderColor: Colors.blue5
  },
  text_button:{
    color:Colors.blue5,
  },
  block_check3: {
    height: wp(46 / 375 * 100),
    backgroundColor:'white',
    justifyContent:'center',
    // flexDirection:'row-reverse'
    alignItems:'flex-end'
  

  },
  block_element1: {
    // paddingHorizontal: Metrics.paddingMedium,
    paddingLeft: Metrics.paddingSmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 0.3,
    height: wp(46 / 375 * 100),

  },
  block_element2: {
    // paddingHorizontal: Metrics.paddingMedium,
    paddingLeft: Metrics.paddingSmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: '#E8E8E8',
    borderBottomWidth: 0.3,
    height: wp(46 / 375 * 100),

  },
  text3_1: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
    // fontStyle: 'italic',
    // textAlign: 'center',
    // justifyContent:'center'
    paddingTop: 12
  },
});
