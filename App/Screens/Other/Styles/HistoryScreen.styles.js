import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    // ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.white,
  },

  block_boss: {
    ...ApplicationStyles.specialApp.dimensisons(375, 74),
    backgroundColor: Colors.white,
  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375, 69),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent: 'center',
    paddingHorizontal: Metrics.paddingMedium,
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent: 'center',
    // marginLeft: Metrics.marginXXTiny,
    flex: 6,
  },
  block_image: {
    justifyContent: 'center',
    flex: 1,

  },
  sub_text: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textSmall,
    textAlign: 'left',
    // margin: Metrics.marginXTiny
  },
  caption: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.grey7,
    marginTop: Metrics.marginXTiny,


  },
  block2: {
    flexDirection: 'row',
  },
  position: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.black,
  },
  icon_arrow: {
    fontSize: wp(30 / 375 * 100),
    color: Colors.black,
  },
  icon: {
    justifyContent: 'center',
  },
  table: {
    // flex: 1,
    ...ApplicationStyles.specialApp.dimensisons(375, 76),
    justifyContent: 'center',
    backgroundColor: Colors.block,
  },
  column1: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  column2: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  column3: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleTable: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,
  },
  tableData:{
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey8,
  },
  data:{
    ...ApplicationStyles.specialApp.dimensisons(375, 54),
    justifyContent: 'center',
    backgroundColor: Colors.white,
  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium,
    // paddingBottom:100,
  },
});
