import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.block,
    flex:1,
  },
  block_item:{
    height: wp(69 / 375 * 100),
    backgroundColor: Colors.white,
    marginBottom:Metrics.marginTiny,
    flexDirection:'row'
  },
  icon:{
    width: wp(70 / 375 * 100),
    height: wp(69 / 375 * 100),
    // backgroundColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon_feather:{
    color: 'black',
    fontSize: 30 / 375 * Metrics.width,

  },
  info:{
      flex:1,
    //   alignItems: 'center',
      justifyContent: 'center'

  },
  sub_text: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    // marginLeft:Metrics.marginSmall
}

})
