import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';
// import { App } from 'react-native-firebase';


export default StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor:'red',
    // width:100,
    // height: 100,
  },
  content: {
    flex: 1,
    // ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor: Colors.block,
  },
  block_boss: {
    ...ApplicationStyles.specialApp.dimensisons(375, 74),
    backgroundColor: Colors.blue1,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: Colors.greenLight,

  },
  image_info: {
    width: 38 / 375 * Metrics.width,
    height: 38 / 375 * Metrics.width,
    backgroundColor: Colors.grayDark,
    borderRadius: 19,
    borderWidth: 0,
    borderColor: 'transparent',

  },
  block1: {
    ...ApplicationStyles.specialApp.dimensisons(375, 69),
    flexDirection: 'row',
    // marginLeft: Metrics.marginSmall,
    // ...ApplicationStyles.specialApp.alignCenter
    justifyContent: 'center',
    paddingHorizontal: Metrics.paddingMedium,
  },
  block_info: {
    // paddingVertical: Metrics.paddingSmall,
    justifyContent: 'center',
    marginHorizontal: Metrics.marginTiny,
    flex: 6,
  },
  block_image: {
    justifyContent: 'center',
    flex: 1,

  },
  sub_text: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,
    fontSize: Fonts.size.textMedium,
    textAlign: 'left',
    // margin: Metrics.marginXTiny
  },
  caption: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
    color: Colors.white,
    marginTop: Metrics.marginXTiny,


  },
  block2: {
    flexDirection: 'row',
  },
  position: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textMedium,
    color: Colors.white,
  },
  icon_arrow: {
    fontSize: wp(20 / 375 * 100),
    color: Colors.white,
  },
  icon: {
    justifyContent: 'center',
  },

});
