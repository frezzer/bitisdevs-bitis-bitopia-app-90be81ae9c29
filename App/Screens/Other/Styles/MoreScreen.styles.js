import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Colors, Metrics, Fonts,
} from '../../../Themes';


function formatLineHeight(fontSize) {
  // adds required padding to lineHeight to support
  // different alphabets (Kanji, Greek, etc.)

  if (fontSize < 22) {
    // minimum lineHeight for different alphabets is 25
    return 25;
  }

  return (fontSize + 3);
}


export default StyleSheet.create({
  // ...ApplicationStyles.screen,
  captionText: {
    fontFamily: Fonts.type.fontRegular,
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: Fonts.size.textSmall,
    color: '#666666',
    letterSpacing: 0.5,
  },
  container: {
    backgroundColor: Colors.block,
    flex: 1,
  },
  part1: {
    marginTop: Metrics.marginSmall,
    marginBottom: Metrics.marginTiny,
  },
  block_item: {
    height: wp(69 / 375 * 100),
    backgroundColor: Colors.white,
    marginBottom: Metrics.marginTiny,
    flexDirection: 'row',
  },
  icon: {
    width: wp(70 / 375 * 100),
    height: wp(69 / 375 * 100),
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon_feather: {
    color: 'black',
    fontSize: 30 / 375 * Metrics.width,

  },
  info: {
    flex: 1,
    justifyContent: 'center',

  },
  sub_text: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
  },
  appblock: {
    // marginHorizontal: Metrics.marginMedium,
    // marginVertical: Metrics.marginTiny,
    paddingHorizontal: Metrics.paddingMedium,
    // borderRadius: Metrics.boderNomarl,
    flexDirection: 'row',
    flexWrap: 'wrap',
    // justifyContent: 'space-between',
    // justifyContent: 'flex-start',
    height: 'auto',
    position: 'absolute',
    // width: 343 / 375 * Metrics.width,
  },
  block: {
    backgroundColor: 'white',
    ...ApplicationStyles.specialApp.dimensisons(163, 163),
  },
  apphidden: {
    paddingVertical: Metrics.paddingXLarge,
    paddingHorizontal: Metrics.paddingTiny,
    marginVertical: Metrics.marginXTiny,
    // backgroundColor: 'red',
    opacity: 0,
    borderRadius: Metrics.boderSmall,
    alignSelf: 'center',
    alignItems: 'center',
    width: 109 / 375 * Metrics.width,
    height: 10 / 375 * Metrics.width,
  },
  text_block: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textSmall,
  },
  content: {
    ...ApplicationStyles.specialApp.dimensisons(375, 175),
    backgroundColor: 'white',
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },

});
