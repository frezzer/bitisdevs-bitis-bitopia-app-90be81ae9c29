import { StyleSheet } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
// import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import {
  ApplicationStyles, Metrics, Colors, Fonts,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  switch_button: {
    ...ApplicationStyles.specialApp.dimensisons(44, 22),
    ...ApplicationStyles.specialApp.alignCenter,
    // marginRight: Metrics.marginXSmall,
    // backgroundColor: 'red',


  },
  block: {
    flexDirection: 'row',
    ...ApplicationStyles.specialApp.dimensisons(375, 62),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.marginMedium,
  },
  blockSwitch: {
    justifyContent: 'center',
  },
  text1: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.grey9,
  },
  lineStyle: {
    borderWidth: 0.3,
    borderColor: Colors.grey4,
    marginHorizontal: Metrics.marginMedium,
  },
});
