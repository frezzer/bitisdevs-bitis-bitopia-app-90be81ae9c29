
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  Dimensions, InteractionManager, View, Text, SafeAreaView, ScrollView, TouchableOpacity, Animated, Platform,
} from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Colors, Metrics } from '../../Themes';
// Styles
import styles from './Styles/SampleScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

// utils
import { compareOmitState } from '../../Services/utils';

const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 80;
const HEADER_MIN_HEIGHT = 40;

const navigationOptions = (props) => ({
  headerStyle: {
    backgroundColor: Colors.blue1,
    borderBottomWidth: 0,
    // height: 30,
  },
});


TestScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function TestScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {};
  const state = {};


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    scrollY,
    setScrollY,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender, Actions, state, scrollY, setScrollY,
  } = props;
  // this will set a height for topbar
  const headerHeight = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
    extrapolate: 'clamp',
  });
  const renderItem = ({ item }) => {
    return (
      <View>
        <Text> abc </Text>
      </View>
    );
  };

  return (
    <View>
      <Animated.View style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: 'red',
        height: headerHeight,
        zIndex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
      }}
      >
        <Text>abc</Text>
      </Animated.View>
      <ScrollView
        // style={{ flex: 1}}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{ height: 1000, backgroundColor:'green' }}>
            <Text> 1231312321</Text>
        </View>
      </ScrollView>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}

const data = [

];
