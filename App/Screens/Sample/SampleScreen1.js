'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import { Dimensions, InteractionManager, View, Text } from 'react-native';
import LoadingInteractions from '../../Components/LoadingInteractions';

// Styles
import styles from './Styles/SampleScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux'

// utils
import { compareOmitState } from "../../Services/utils";
import { SafeAreaView } from 'react-navigation';
const { width } = Dimensions.get('window');



/**
 * Main
 */
export default function SampleScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {

  }
  const state = {

  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state

  }
  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const { initialRender, Actions,state } = props;
  const { } = state
  const { } = Actions

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>

      </View>
    </SafeAreaView>
  );


}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
