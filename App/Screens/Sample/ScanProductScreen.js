
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, TextInput,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Unicons } from '../../Components/Icons';
import DoubleButton from '../../Components/Button/DoubleButton';
import BlockDouble from '../../Components/Block/BlockDouble';
// Styles
import { Metrics } from '../../Themes';
import styles from './Styles/ScanProductScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ProductGetDetailActions, { ProductGetDetailSelectors } from '../../Redux/product/ProductGetDetailRedux';
import CartShoppingActions, { CartShoppingSelectors } from '../../Redux/cart/CartShoppingRedux';
// utils
import { compareOmitState } from '../../Services/utils';
import ScanQRCodeComponent from '../../Components/ScanQRCode';
import CircleBackButton from '../../Components/Button/CircleBackButton';

require('moment/locale/vi');

const { width } = Dimensions.get('window');

const navigationOptions = (props) => {
  return {
    header: null,
  };
};

ScanProductScreen.navigationOptions = navigationOptions;
/**
 * Main
 */
export default function ScanProductScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [scanSuccess, setScanSuccess] = React.useState(false);
  const [reactivate, setReactivate] = React.useState(false);

  const productDetail = useSelector((state) => ProductGetDetailSelectors.selectPayload(state.product_detail));
  // const productDetail = {
  //   uuid: "GFW007988KEM37",
  //   image_link: "",
  //   price: 990000,
  //   type: "Giày thể thao",
  //   size: "39",
  //   color: "ĐEN",
  //   promotion_uuid: "LOYPROM",
  //   stock: 20,
  //   sold: 100
  // }

  // effects
  React.useEffect(() => {
    console.tron.log('ScanProductScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);

  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressBackButton: () => {
      props.navigation.goBack();
    },
    onScanSuccess: (e) => {
      setScanSuccess(true); // flag confirm scan success
      dispatch(ProductGetDetailActions.productGetDetailRequest({ product_uuid: e.data }));
    },
    onPressAdd: () => {
      const cart_id = props.navigation.getParam('cart_id', null);
      setReactivate(true);
      if (scanSuccess === true && productDetail) {
        dispatch(CartShoppingActions.cartShoppingAddItemRequest({ cart_id, product_detail: productDetail }));
        dispatch(ProductGetDetailActions.productGetDetailInitial());
      }
    },
  };

  const state = {
    scanSuccess,
  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    state,
    setReactivate,
    reactivate,
    productDetail,
    title: 'Quét',
    Actions,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  // declare variables
  const {
    initialRender, state, setReactivate, reactivate, title, productDetail,
    Actions,
  } = props;
  const { scanSuccess } = state;
  console.tron.log('Render view', scanSuccess);

  return (
    <View style={styles.container}>
      <NavigationEvents
        onWillFocus={(payload) => setReactivate(true)}
        onDidBlur={(payload) => setReactivate(false)}
      />
      {reactivate && (
      <ScanQRCodeComponent
        isSearchcc
        onScanSuccess={Actions.onScanSuccess}
        reactivate={false}
        detailShiftText={null}
      />
      )}
      <View style={styles.content}>
        <View style={styles.block_top}>
          <View style={styles.btn_back}>
            <CircleBackButton onPress={Actions.onPressBackButton} />
          </View>
          <Text style={styles.text_title}>{title}</Text>
        </View>
        <View style={styles.block_search}>
          <Unicons style={styles.icon_search} name="search" />
          <TextInput
            style={styles.box_text_input}
            placeholder="Nhập SDT, MKH hoặc MSP"
            placeholderTextColor="white"
            onChangeText={(text) => console.tron.log('text change', text)}
          />
        </View>

        <View style={styles.block_bottom}>
          {productDetail && (
          <View style={styles.block_product}>
            <BlockDouble
              buttonText1={productDetail.uuid}
              buttonText2={`Giá sản phẩm: ${productDetail.price}`}
              hasArrow={false}
              btnDisalble
              imageLink="https://product.hstatic.net/1000230642/product/dsc_4725_57d568b4c8a04af090a3325525914bc6_1024x1024.jpg"
            />
          </View>
          )}

          <DoubleButton
            styles={{ backgroundColor: 'transparent' }}
            buttonText1="Thêm"
            styles1={styles.btn_bottom}
            onPressButton1={Actions.onPressAdd}
            buttonText2="Hoàn tất"
            styles2={styles.btn_bottom}
            onPressButton2={Actions.onPressBackButton}
          />
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
