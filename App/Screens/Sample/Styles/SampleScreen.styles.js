import { StyleSheet } from 'react-native';
import { ApplicationStyles,Colors, Metrics,Fonts} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    // ...ApplicationStyles.specialApp.alignCenter,
    backgroundColor:Colors.block
  },
  info_staff:{
    ...ApplicationStyles.specialApp.dimensisons(343,148),
    backgroundColor:Colors.white,
    marginTop:Metrics.marginMedium,
    ...ApplicationStyles.specialApp.alignCenter
  },
  barcodeblock: {
    paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingXXLarge,
    marginHorizontal: Metrics.marginMedium,
    marginTop: Metrics.marginMedium,
    // marginBottom: Metrics.marginTiny,
    backgroundColor: Colors.white,
    borderRadius: Metrics.boderLarge,
    borderColor: Colors.silver,
    borderWidth: 2,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    height: 150 / 375 * Metrics.width,
    width: 343 / 375 * Metrics.width,
},
barcode: {
    height: 93 / 375 * Metrics.width,
},
barcodeText: {
    color: Colors.grayMedium,
    fontSize: Fonts.size.textLarge,
    textAlign: 'center'
},
title:{
  fontFamily: Fonts.type.fontBold,
  color: Colors.black,
  fontSize: Fonts.size.textMedium,
  marginLeft: Metrics.marginMedium,

},
block_title:{
  ...ApplicationStyles.specialApp.dimensisons(375,44),
  backgroundColor:Colors.white,
  justifyContent:'center',
  marginTop: Metrics.marginSmall


}
});
