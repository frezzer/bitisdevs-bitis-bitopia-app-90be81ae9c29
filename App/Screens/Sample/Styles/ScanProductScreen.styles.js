
import { StyleSheet, Platform } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'

const backButtonWidth = 44;
const confirmButtonHeight = 44;

export default StyleSheet.create({
  // container: {
  //   backgroundColor: 'black'
  // },

  // QRcodeview: {
  //   position: 'absolute'
  // },

  // blackBackground: {
  //   ...ApplicationStyles.specialApp.dimensisonsPercentage(),
  //   position: 'absolute'
  // },

  // implement: {
  //   ...ApplicationStyles.specialApp.dimensisonsPercentage(),
  //   flexWrap: 'wrap',
  //   justifyContent: 'space-between',
  // },

  // backButton: {
  //   ...ApplicationStyles.specialApp.dimensisons(44, 44),
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   borderRadius: Metrics.boderMaxium,
  //   margin: Metrics.marginSmall,
  // },

  // backButtonTouchable: {
  //   ...ApplicationStyles.specialApp.dimensisons(44, 44),
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderMaxium,
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   position: 'absolute'
  // },

  // backButtonButtonText: {
  //   paddingTop: Metrics.paddingXTiny,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textHuge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  //   position: 'absolute',
  // },

  // titleView: {
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   width: (375 - (backButtonWidth + Metrics.marginSmall * 2) * 2) / 375 * Metrics.width,
  //   height: 44 / 375 * Metrics.width,
  //   marginVertical: Metrics.marginSmall,
  // },
  // titleText: {
  //   paddingTop: Metrics.paddingXTiny,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textXXLarge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  //   marginBottom:Metrics.marginMedium
  // },



  // confirmButton: {
  //   ...ApplicationStyles.specialApp.dimensisons(158, 44),
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   margin: Metrics.marginSmall,
  // },

  // confirmButtonTouchable: {
  //   ...ApplicationStyles.specialApp.dimensisons(158, 44),
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderSmall,
  //   opacity: 0.1,
  //   // alignSelf: 'center',
  //   // ...ApplicationStyles.specialApp.alignCenter,
  //   position: 'absolute'
  // },
  // confirmButtonText: {
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textLarge,
  //   fontWeight: 'bold',
  //   color: Colors.white,
  // },

  // infoblock: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 100),
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter
  // },

  // infoBlockDetail: {
  //   flexDirection: 'row',
  // },

  // infoblockbackground: {
  //   ...ApplicationStyles.specialApp.dimensisons(343, 100),
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderSmall,
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   position: 'absolute',
  // },

  // rectangleContainer: {
  //   flex: 1,
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: "transparent"
  // },

  // rectangle: {
  //   ...ApplicationStyles.specialApp.dimensisons(303, 303),
  //   borderWidth: 3,
  //   borderColor: Colors.white,
  //   ...ApplicationStyles.specialApp.alignCenter,
  //   backgroundColor: "transparent"
  // },

  // topOverlay: {
  //   flex: 1,
  //   ...ApplicationStyles.specialApp.dimensisons(375, 375),
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  // },

  // bottomOverlay: {
  //   flex: 3, // 6
  //   ...ApplicationStyles.specialApp.dimensisons(375, 375),
  // },

  // bottomOverlayBg: {
  //   ...ApplicationStyles.specialApp.dimensisonsPercentage(),
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  //   position: 'absolute',
  // },

  // leftAndRightOverlay: {
  //   height: 303 / 375 * Metrics.width,
  //   width: (Metrics.width - 303 / 375 * Metrics.width) / 2,
  //   backgroundColor: 'black',
  //   opacity: 0.7,
  // },

  // TextAlert: {
  //   margin: Metrics.marginMedium,
  //   textAlign: 'center',
  //   fontSize: Fonts.size.textSmall,
  //   fontStyle: 'italic',
  //   color: Colors.white,
  // },
  // block_search:{
  //   ...ApplicationStyles.specialApp.dimensisons(343,44),
  //   backgroundColor: Colors.white,
  //   borderRadius: Metrics.boderSmall,
  //   flexDirection: 'row',
  //   opacity: 0.1,
  //   alignSelf: 'center',
  //   alignItems: 'center',
  //   // justifyContent: 'center',
  // }

  container: {
    flex: 1,
    backgroundColor: Colors.black
  },
  content: {
    height: Metrics.height,
    width: Metrics.width
  },

  block_top: {
    width: '100%',
    // height: wp(88 / 375 * 100),
    height: Platform.OS == 'android' ? Metrics.navBarHeight : Metrics.navigationBarHeight,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingVertical: Metrics.paddingXSmall,
  },
  btn_back: {
    position: 'absolute',
    left: Metrics.marginMedium,
  },
  block_search: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection: 'row',
    alignItems: 'center',
    margin: Metrics.paddingMedium,
    borderRadius: Metrics.boderLarge,
  },
  box_text_input: {
    height: wp(44 / 375 * 100),
    flex: 1,
  },
  block_product: {
    marginBottom: Metrics.height < 670 ? 0 : Metrics.marginHuge,
  },
  block_bottom: {
    position: 'absolute',
    bottom: 0
  },
  btn_bottom: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    borderColor: Colors.white,
    borderWidth: 1
  },

  // Icon
  icon_search: {
    fontSize: Fonts.size.textHuge,
    color: Colors.white,
    marginHorizontal: Metrics.marginMedium
  },
  // Text
  text_title: {
    fontSize: Fonts.size.textXXLarge,
    fontWeight: 'bold',
    color: Colors.white,
  },

})
