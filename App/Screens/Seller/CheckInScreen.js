
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, AsyncStorage, Alert,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ScanQRCodeComponent from '../../Components/ScanQRCode';
import CircleBackButton from '../../Components/Button/CircleBackButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';
// Styles
import styles from './Styles/CheckInScreen.styles';

// Actions and Selectors Redux
import ShiftFindonlyoneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import AttendanceCreateoneActions, { AttendanceCreateoneSelectors } from '../../Redux/attendance/AttendanceCreateOneRedux';
import { MeSelectors } from '../../Redux/MeRedux';
import screensName from '../../Navigation/Screens';
import ShiftFindStoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';

// utils
import { compareOmitState, getFullName } from '../../Services/utils';
import 'moment/locale/vi';


// options

const navigationOptions = ({ navigation }) => ({
  header: null,
});


CheckInScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function CheckInScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);

  // useStates
  const [createTime, setCreateTime] = React.useState('');
  const [shiftUuid, setShiftUuid] = React.useState('');
  const [data, setdata] = React.useState(null);
  const [reactivate, setReactivate] = React.useState(true);
  const [attendance, setattendance] = React.useState(AttendanceCreateonePayload);
  const [storeName, setstoreName] = React.useState('');
  const [flagButton, setflagButton] = React.useState(false);



  // selector store
  // console.tron.log("TCL: CheckInScreen -> ShiftFindonlyonePayload", ShiftFindonlyonePayload)
  const AttendanceCreateonePayload = useSelector((state) => AttendanceCreateoneSelectors.selectPayload(state.attendance.createOne));
  const AttendanceCreateoneFetching = useSelector((state) => AttendanceCreateoneSelectors.selectFetching(state.attendance.createOne));
  const AttendanceCreateoneEror = useSelector((state) => AttendanceCreateoneSelectors.selectError(state.attendance.createOne));
  const ShiftFindonlyonePayload = useSelector((state) => ShiftFindonlyoneSelectors.selectPayload(state.shift.findOnlyOne));
  const ShiftFindonlyoneFetching = useSelector((state) => ShiftFindonlyoneSelectors.selectFetching(state.shift.findOnlyOne));
  const ShiftFindonlyoneError = useSelector((state) => ShiftFindonlyoneSelectors.selectError(state.shift.findOnlyOne));
  const MePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  // console.tron.log("debug: CheckInScreen -> storeName", storeName)


  // get store

  const findStoreShiftPayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  const findStoreShiftFetching = useSelector((state) => ShiftFindstoreSelectors.selectFetching(state.shift.findStore));

  // get info
  const store_name = _.get(ShiftFindonlyonePayload, ['merchant', 'store_name'], 'Quét thông tin ca');
  const shiftName = _.get(ShiftFindonlyonePayload, ['shifttype', 'name'], '');
  const shiftDate = _.get(ShiftFindonlyonePayload, ['created_at'], '');
  const date = moment(shiftDate).format('DD/MM/YYYY');


  // effects
  React.useEffect(() => {
    console.tron.log('SellerCheckInScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(AttendanceCreateoneActions.attendanceCreateoneInitial());
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneInitial());
    }
  }, [initialRender]);
  React.useEffect(() => {
    if (ShiftFindonlyoneFetching === false && ShiftFindonlyoneError == null) {
      setdata(ShiftFindonlyonePayload);
      dispatch(ShiftFindStoreActions.shiftFindstoreRequest({
        store_uuid: ShiftFindonlyonePayload.store_uuid,
      }));
    }
    // if (AttendanceCreateoneFetching === false && AttendanceCreateoneEror == null) {
    //   setattendance(AttendanceCreateonePayload);
    // }
  }, [ShiftFindonlyonePayload]);

  React.useEffect(() => {
    if (findStoreShiftPayload != null && findStoreShiftFetching === false) {
      setstoreName(findStoreShiftPayload.data.store_name);
    }
  }, [findStoreShiftPayload]);

  React.useEffect(() => {
    if (AttendanceCreateonePayload != null && AttendanceCreateoneFetching === false && AttendanceCreateonePayload !== attendance && flagButton) {
      setattendance(AttendanceCreateonePayload);
      props.navigation.navigate(screensName.SellerConfirmScreen, {
        attendance: AttendanceCreateonePayload, createTime, data, storeName,
      });
    }
  }, [AttendanceCreateonePayload]);
  // console.tron.log('debug: AttendanceCreateonePayload', attendance);
  /**

   * Actions
   *    for flows and onPress attribute button
   * */
  const Actions = {
    onScanSuccess: (params) => {
      const dataQR = _.get(params, ['data'], '');
      // console.tron.log('TCL: CheckInScreen -> data', dataQR);
      // if (!data) return alert('No data from scanning');
      setShiftUuid(dataQR);
      setCreateTime(moment().format('HH:mm'));
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneRequest({ uuid: dataQR }));
    },
    onPressContinue: async () => {
      // AsyncStorage.setItem('storeUuid', data.store_uuid);
      const loginTime = new Date();
      if (moment().isBefore(moment(data.time_in)) || moment().isAfter(moment(data.time_out))) return Alert.alert('THÔNG BÁO', 'Bạn đang không thuộc ca này ');
      console.tron.log('debug: CheckInScreen -> moment().isAfter(moment(data.time_out))', moment(), (moment(data.time_out)));
      dispatch(AttendanceCreateoneActions.attendanceCreateoneRequest({
        store_uuid: ShiftFindonlyonePayload.store_uuid,
        username: MePayload.username,
        shift_uuid: shiftUuid,
        login_time: loginTime,
      }));
      setflagButton(true);
    },
    onPressBackButton: (params) => props.navigation.goBack(),
  };
  // console.tron.log("debug: CheckInScreen -> data", data)

  // console.tron.log("debug: CheckInScreen -> props", props)
  /**

   * render
   *    declare variables for render
   *    render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    store_name,
    info_name: store_name,
    reactivate: true,
    current_time: createTime,
    title: 'Điểm Danh',
    continueText: 'Xác nhận thông tin',
    ShiftFindonlyonePayload,
    Actions,
    shiftName,
    storeName,
    date,
    data,
    // reactivate,
    setReactivate,

  };
  return <Render {...renderProps} />;
}
/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender,
    store_name,
    reactivate,
    current_time,
    title,
    continueText,
    Actions,
    shiftName,
    storeName,
    date,
    data,
    // reactivate,
    setReactivate,
  } = props;

  if (initialRender) return <LoadingInteractions size="large" />;
  // console.tron.log('abc', current_time)
  return (
    <View style={styles.container}>
      <NavigationEvents
        onWillFocus={(payload) => setReactivate(true)}
        onDidBlur={(payload) => setReactivate(false)}
      />
      {reactivate && (
      <ScanQRCodeComponent
        onScanSuccess={Actions.onScanSuccess}
        info_name={store_name}
        reactivate={false}
        visibleStore
      />
      )}

      <View style={styles.content}>
        <View style={styles.block_top}>
          <View style={styles.btn_back}>
            <CircleBackButton onPress={Actions.onPressBackButton} />
          </View>
          <Text style={styles.text_title}>{title}</Text>
        </View>

        <View style={styles.block_bottom}>
          <View style={styles.block_shift}>
            <View style={styles.block_shift_time}>
              <Text style={styles.text_scan_time}>Giờ quét</Text>
              <Text style={styles.text_time}>{current_time || '--:--'}</Text>
            </View>
            <View style={styles.block_store}>
              <Text style={styles.text_store}>{storeName || 'Thông tin ca'}</Text>

              <Text style={styles.text_detail_shift}>{shiftName ? `${shiftName} - ${date}` : ''}</Text>

            </View>
          </View>

          {data ? (
            <View style={styles.block_btn_bottom}>
              <ButtonHandle
                buttonText={continueText}
                onPressButton={Actions.onPressContinue}
                styles={styles.btn_bottom}
              />
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
}, areEqual);
/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}

// sample data
