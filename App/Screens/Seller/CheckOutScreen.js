
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, Alert,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ScanQRCodeComponent from '../../Components/ScanQRCode';
import CircleBackButton from '../../Components/Button/CircleBackButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';
// Styles
import styles from './Styles/CheckInScreen.styles';
import { Colors } from '../../Themes';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';

import ShiftFindonlyoneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import AttendanceFinishoneActions, { AttendanceFinishoneSelectors } from '../../Redux/attendance/AttendanceFinishOneRedux';
import ShiftFindStoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';


// utils
import { compareOmitState } from '../../Services/utils';
import screensName from '../../Navigation/Screens';

const { width } = Dimensions.get('window');

const navigationOptions = ({ navigation }) => ({
  header: null,
});


CheckOutScreen.navigationOptions = navigationOptions;


/**
 * Options
 *  - navigation options for Screens
 * */
// const options = () => ({ ...hideAllOptions });
// SellerCheckOutScreen.options = options;

/**
 * Main
 */
export default function CheckOutScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [reactivate, setReactivate] = React.useState(true);
  const [storeName, setstoreName] = React.useState('');
  // useStates
  const [createTime, setCreateTime] = React.useState('');
  const [shiftUuid, setShiftUuid] = React.useState('');
  const [data, setdata] = React.useState(null);
  const [attendance, setattendance] = React.useState(AttendanceFinishonePayload);
  const [flagButton, setflagButton] = React.useState(false);
  // selector store
  const ShiftFindonlyonePayload = useSelector((state) => ShiftFindonlyoneSelectors.selectPayload(state.shift.findOnlyOne));
  const AttendanceFinishonePayload = useSelector((state) => AttendanceFinishoneSelectors.selectPayload(state.attendance.finishOne));
  const AttendanceFinishoneFetching = useSelector((state) => AttendanceFinishoneSelectors.selectFetching(state.attendance.finishOne));
  const AttendanceFinishoneError = useSelector((state) => AttendanceFinishoneSelectors.selectPayload(state.attendance.finishOne));
  const MePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const ShiftFindonlyoneFetching = useSelector((state) => ShiftFindonlyoneSelectors.selectFetching(state.shift.findOnlyOne));
  const ShiftFindonlyoneError = useSelector((state) => ShiftFindonlyoneSelectors.selectError(state.shift.findOnlyOne));
  const store_name = _.get(ShiftFindonlyonePayload, ['merchant', 'store_name'], 'Quét thông tin ca');
  const shiftName = _.get(ShiftFindonlyonePayload, ['shifttype', 'name'], '');
  const shiftDate = _.get(ShiftFindonlyonePayload, ['created_at'], '');
  const date = moment(shiftDate).format('DD/MM/YYYY');

  // get store
  const findStoreShiftPayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  const findStoreShiftFetching = useSelector((state) => ShiftFindstoreSelectors.selectFetching(state.shift.findStore));


  // effects
  React.useEffect(() => {
    console.tron.log('SellerCheckOutScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(AttendanceFinishoneActions.attendanceFinishoneInitial());
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneInitial());
    }
  }, [initialRender]);

  // Attendance createOne success
  React.useEffect(() => {
    if (ShiftFindonlyoneFetching === false && ShiftFindonlyoneError == null) {
      setdata(ShiftFindonlyonePayload);
      dispatch(ShiftFindStoreActions.shiftFindstoreRequest({
        store_uuid: ShiftFindonlyonePayload.store_uuid,
      }));
    }
  }, [ShiftFindonlyonePayload]);

  React.useEffect(() => {
    if (findStoreShiftPayload != null && findStoreShiftFetching === false) {
      setstoreName(findStoreShiftPayload.data.store_name);
    }
  }, [findStoreShiftPayload]);

  React.useEffect(() => {
    if (AttendanceFinishonePayload != null && AttendanceFinishoneFetching === false && AttendanceFinishonePayload !== attendance && flagButton) {
      setattendance(AttendanceFinishonePayload);
      props.navigation.navigate(screensName.SellerConfirmScreen, {
        attendance: AttendanceFinishonePayload, createTime, data, storeName,
      });
    }
  }, [AttendanceFinishonePayload]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onScanSuccess: (params) => {
      const dataQR = _.get(params, ['data'], '');
      setCreateTime(moment().format('HH:mm'));
      setShiftUuid(dataQR);
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneRequest({ uuid: dataQR }));
    },
    onPressContinue: async () => {
      const logout_time = new Date();
      if (moment().isBefore(moment(data.time_in)) || moment().isAfter(moment(data.time_out))) return Alert.alert('THÔNG BÁO', 'Bạn đang không thuộc ca này ');
      dispatch(AttendanceFinishoneActions.attendanceFinishoneRequest({
        store_uuid: ShiftFindonlyonePayload.store_uuid,
        username: MePayload.username,
        shift_uuid: shiftUuid,
        logout_time,
      }));
      setflagButton(true);
    },
    onPressBackButton: () => props.navigation.goBack(),

  };


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */

  const renderProps = {
    initialRender,
    store_name,
    info_name: store_name,
    reactivate: true,
    current_time: createTime,
    title: 'Tan Ca',
    continueText: 'Xác nhận thông tin',
    Actions,
    shiftName,
    storeName,
    date,
    data,

  };

  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SellerCheckOutScreen render');
  // declare variables
  const {
    initialRender,
    store_name,
    reactivate,
    current_time,
    title,
    continueText,
    Actions,
    shiftName,
    date,
    data,
    setReactivate,
    storeName,


  } = props;
  if (initialRender) return <LoadingInteractions size="large" />;

  // console.tron.log('TCL: View', shiftDetail);
  return (


    <View style={styles.container}>
      <NavigationEvents
        onWillFocus={(payload) => setReactivate(true)}
        onDidBlur={(payload) => setReactivate(false)}
      />
      {reactivate && (
      <ScanQRCodeComponent
        onScanSuccess={Actions.onScanSuccess}
        reactivate={false}
        visibleStore
      />
      )}

      <View style={styles.content}>
        <View style={styles.block_top}>
          <View style={styles.btn_back}>
            <CircleBackButton onPress={Actions.onPressBackButton} />
          </View>
          <Text style={styles.text_title}>{title}</Text>
        </View>

        <View style={styles.block_bottom}>
          <View style={styles.block_shift}>
            <View style={styles.block_shift_time}>
              <Text style={styles.text_scan_time}>Giờ quét</Text>
              <Text style={styles.text_time}>{current_time || '--:--'}</Text>
            </View>
            <View style={styles.block_store}>
              <Text style={styles.text_store}>{storeName || 'Thông tin ca'}</Text>
              <Text style={styles.text_detail_shift}>{shiftName ? `${shiftName} - ${date}` : ''}</Text>
            </View>
          </View>
          {data ? (
            <View style={styles.block_btn_bottom}>
              <ButtonHandle
                buttonText={continueText}
                onPressButton={Actions.onPressContinue}
                styles={styles.btn_bottom}
              />
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
