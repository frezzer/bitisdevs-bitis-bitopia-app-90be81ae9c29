
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
// Components
import {
  InteractionManager, View, Text, Platform, TouchableOpacity, Image, SectionList, RefreshControl,
} from 'react-native';
import { IconFill, IconOutline } from '@ant-design/icons-react-native';

import moment from 'moment';
import LoadingInteractions from '../../Components/LoadingInteractions';
import screensName from '../../Navigation/Screens';
import { Metrics, Images, Colors } from '../../Themes';
import BlockDouble from '../../Components/Block/BlockDouble';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import ShiftItem from '../../Components/ShiftItem';
import BlockShift from '../../Components/Block/BlockShift';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';


// import DoubleButton from '../../Components/Button/ButtonHandle'
// Styles
import styles from './Styles/LeaveOffWorkListScreen.styles';
// Actions and Selectors Redux

// utils
import { compareOmitState } from '../../Services/utils';

// Redux
import { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../Redux/shift/ShiftFindRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';

/**
 * Main
 */

const onPressBackButton = (navigation) => {
  navigation.popToTop();
};

const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  const onPressRightButton = () => props.navigation.navigate(screensName.FilterScreen, {
    defineActionWillUse: ShiftFindActions.shiftFindRequest({
      _limit: 10,
      _populate: ['shifttype', 'attendances'],
    }),
    type: 'NEED_USER_DETAIL',
  });

  return {
    headerMode: Platform.select({
      android: 'screen',
      ios: 'float',
    }),
    title: 'Danh sách ca',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{
            paddingLeft: Platform.OS === 'android' ? 18 : 5,
            paddingBottom: Metrics.paddingXTiny,
          }}
          onPress={() => onPressBackButton(props.navigation)}
        >
          {/* <Image source={Images.buttonBack} /> */}
          <IconOutline name="left" color="white" style={styles.iconBack} />


        </TouchableOpacity>
      );
    },
    headerRight: () => {
      return (
        <RightButtonsTopBar
          name="filter"
          style={{ paddingRight: Metrics.paddingMedium }}
          onPress={onPressRightButton}

        />
      );
    },
  };
};

ManagerListShiftScreen.navigationOptions = navigationOptions;

export default function ManagerListShiftScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [loadMoreLimit, setLoadMoreLimit] = React.useState(0);
  const [selectedStartDate, setselectedStartDate] = React.useState(null);
  const [selectedEndDate, setselectedEndDate] = React.useState(null);

  // selector store
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const userUuid = _.get(mePayload, 'username', null);
  const listShifts = useSelector((state) => ShiftFindSelectors.selectPayload(state.shift.find)) || [];
  const listShiftsFetching = useSelector((state) => ShiftFindSelectors.selectFetching(state.shift.find));
  const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  const storeUuid = _.get(findStoreSellerPayload, ['store_uuid']);
  const roleTypeUser = _.get(mePayload, ['roles']);
  const roleseller = _.find(roleTypeUser, (i) => i === 'seller');

  // processing respone data
  const a = _.groupBy(listShifts, (item) => {
    return moment(item.created_at).format('DD/MM/YYYY');
  });

  const dataProcessed = _.reduce(a, (result = [], val, key) => {
    const temp = {
      created_at: key,
      data: val,
    };
    result.push(temp);
    return result;
  }, []);
  console.tron.log('TCL: chó trí 2-> dataProcessed', dataProcessed);


  // effects
  React.useEffect(() => {
    console.tron.log('ManagerListShiftScreen effect');
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
        dispatch(ShiftFindActions.shiftFindInitial());
      });
    }
    // (async function setStoreUuidFunction() {
    //   const store_uuid = await AsyncStorage.getItem('storeUuid');
    //   setStoreUuid(store_uuid);
    // }());
    return () => { };
  }, [/** input */]);

  React.useEffect(() => {
    if (userUuid) {
      setLoadMoreLimit(loadMoreLimit + 1);
      dispatch(ShiftFindActions.shiftFindRequest({
        'attendances.user_uuid': userUuid,
        _populate: ['shifttype', 'attendances'],
        deleted: false,
        store_uuid: storeUuid,
      }));
    }
  }, []);

  const onRefresh = React.useCallback(() => {
    dispatch(ShiftFindActions.shiftFindRequest({
      'attendances.user_uuid': userUuid,
      _populate: ['shifttype', 'attendances'],
      store_uuid: storeUuid,
      deleted: false,
    }));
  }, [listShiftsFetching]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressGoInfo: (data) => props.navigation.navigate(screensName.LeaveOffWorkInfoScreen),
    onPressSummary: () => { props.navigation.navigate(screensName.ManagerShiftSummaryScreen); },
    onPressCheckIn: () => { props.navigation.navigate(screensName.SellerCheckInScreen); },
    onPressShiftDetail: (data) => { props.navigation.navigate(screensName.SellerInforShiftScreen, { data, userUuid }); },
    onPressCheckOut: () => { props.navigation.navigate(screensName.SellerCheckOutScreen); },


  };
  const state = {
    selectedStartDate,
    selectedEndDate,
  };

  const Item = React.memo((props) => {
    const { data, index } = props;
    return (
      <View style={styles.item}>
        <View style={styles.divider}>
          <BlockShift
            buttonText1={data.shifttype.name}
            buttonText2={data.status ? 'Đã tan ca' : 'Chưa tan ca'}
            buttonText3={moment(data.shifttype.time_in, 'HH:mm:ss').format('HH:mm')}
            buttonText4={moment(data.shifttype.time_out, 'HH:mm:ss').format('HH:mm')}
            _styleText2={data.status ? styles.status : styles.status1}
            iconName=""
            flatItem={index === listShifts.length - 1}
            onPressButton={() => Actions.onPressShiftDetail(data)}
          />
        </View>
      </View>
    );
  });


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    dataProcessed,
    listShifts,
    Item,
    onRefresh,
    listShiftsFetching,
  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    Actions,
    Item,
    dataProcessed,
    onRefresh,
    listShiftsFetching,
  } = props;
  const handleDate = (createdAt) => {
    const Today = moment(new Date()).format('DD/MM/YYYY');
    const day = moment(new Date()).add(-1, 'days');
    const Yesterday = moment(day).format('DD/MM/YYYY');
    // console.tron.log('debug: Today', Yesterday);
    if (createdAt === Today) return `Hôm nay - ${Today}`;
    if (createdAt === Yesterday) return `Hôm qua - ${Yesterday}`;
    return createdAt;
  };

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={{ flex: 1 }}>
          {dataProcessed.length !== 0 ? (
            <SectionList
              sections={_.reverse(dataProcessed)}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item, index }) => <Item index={index} data={item} actions={Actions} />}
              renderSectionHeader={({ section: { created_at } }) => {
                return (
                  <View style={styles.header}>
                    <Text style={styles.title}>{handleDate(created_at)}</Text>
                  </View>
                );
              }}
              refreshControl={
                <RefreshControl refreshing={listShiftsFetching} onRefresh={onRefresh} />
            }
            />
          ) : <Text style={styles.noShift}>Bạn không có ca nào</Text>}

        </View>
      </View>
      <View style={styles.btn_bottom}>
        <ButtonHandle
          buttonText="Điểm danh vào ca"
          onPressButton={Actions.onPressCheckIn}
          styles={{ backgroundColor: Colors.blue2 }}
        />
      </View>
    </View>
    // </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
