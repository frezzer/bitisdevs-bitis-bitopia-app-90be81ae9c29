
// basics
import _ from 'lodash';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, TextInput, TouchableOpacity, View, Text, Image, Animated, Platform, ScrollView,
} from 'react-native';
import moment from 'moment';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import LoadingInteractions from '../../Components/LoadingInteractions';
import { Feather } from '../../Components/Icons';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import { Metrics, Images, Colors } from '../../Themes';
// Styles
import styles from './Styles/SellerConfirmScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindOnlyOneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';
import ShiftFindStoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';


// utils
import { compareOmitState } from '../../Services/utils';
import screenName from '../../Navigation/Screens';


/**
 * Options
 *  - navigation options for Screens
 * */

const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
SellerConfirmScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function SellerConfirmScreen(props) {
  const { navigation } = props;
  // const attendance = navigation.getParam('AttendanceFinishonePayload', null);
  // console.tron.log("debug: SellerConfirmScreen -> attendance", attendance)
  // const data = navigation.getParam('data', null);

  const {
    createTime, data, attendance, storeName,
  } = props.navigation.state.params;
  const { onPressButton } = props;
  // console.tron.log("TCL: SellerConfirmScreen -> ShiftFindonlyonePayload", data)
  console.tron.log('duyyyy', props.navigation.state.params);
  // console.tron.log('duyyyy2', attendance);


  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  // const data = useSelector(state => ShiftFindonlyoneSelectors.selectPayload(state.shift.findOnlyOne));
  // console.tron.log("TCL: SellerConfirmScreen -> ShiftFindonlyonePayload", data)
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  // console.tron.log("debug: SellerConfirmScreen -> user_detail", user_detail)
  console.tron.log('TCL: SellerConfirmScreen -> user_detail', user_detail);
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  // const middle_name = _.get(user_detail, ['data', 'middle_name'], '');
  const sellerName = _.concat([last_name, first_name]);
  // console.tron.log("debug: SellerConfirmScreen -> sellerName", sellerName)
  // const findStoreSellerPayload = useSelector((state) => FindstoreSellerSelectors.selectPayload(state.shift.findStorebySeller));
  // const findStoreShiftPayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  // console.tron.log("debug: SellerConfirmScreen -> findStoreShiftPayload", findStoreShiftPayload)
  // const findStoreShiftFetching = useSelector((state) => ShiftFindstoreSelectors.selectFetching(state.shift.findStore));
  // const findStoreShiftError = useSelector((state) => ShiftFindstoreSelectors.selectError(state.shift.findStore));

  // const storeName = _.get(findStoreSellerPayload, ['data', 'store_name']);
  // const storeUuidShift = _.get(attendance, ['shift', 'store_uuid']);
  // console.tron.log('Chó trí 2', storeUuidShift);

  // effects
  React.useEffect(() => {
    console.tron.log('SellerConfirmScreen effect');
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
    }
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressPopToTop: () => props.navigation.navigate(screenName.DashBoardScreen),
    onPressGoBack: () => {
      props.navigation.goBack();
    },
    onPressListShift: () => {
      props.navigation.navigate(screenName.SellerListShiftScreen);
    },
  };
  // const dataStatus = _.get(attendace, 'status');
  // console.tron.log("debug: SellerConfirmScreen -> dataStatus", dataStatus)
  const _handleStatus = () => {
    const { status } = attendance;
    switch (status) {
      case 'LATE':
        return 'Đi muộn';
      case 'DOING':
        return 'Đã điểm danh';
      case 'DONE':
        return 'Đã tan ca';
      default:
        break;
    }
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    onPressButton,
    Actions,
    createTime,
    // shiftDetail,
    sellerName,
    scrollY,
    storeName,
    data,
    _handleStatus,
    attendance,
    // fullName

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SellerConfirmScreen render');
  // declare variables
  const {
    initialRender,
    onPressButton,
    Actions,
    createTime,
    // shiftDetail,
    sellerName,
    scrollY,
    storeName,
    attendance,
    data,
    _handleStatus,
  } = props;
  // ANIMATION
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.blue1],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };
  // const date = moment(shiftDetail.created_at).format('DD/MM/YYYY');


  if (initialRender) return <LoadingInteractions size="large" />;
  return (


    <View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        />
      </Animated.View>
      {/* </Animated.View> */}
      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: 24,
        }}
        >
          <View style={styles.container}>
            <View style={styles.part1}>
              <Text style={styles.Title}> Bạn đã điểm danh thành công</Text>
            </View>

            <View style={styles.part2}>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Nhân viên</Text>
                <Text style={styles.text4}>{sellerName}</Text>
              </View>

              <View style={styles.block_element}>
                <Text style={styles.text4}>Cửa hàng</Text>
                <Text style={styles.text4}>{storeName}</Text>
              </View>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Ngày làm việc</Text>
                <Text style={styles.text4}>{moment(data.created_at).format('DD/MM/YYYY')}</Text>
              </View>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Ca làm việc</Text>
                <Text style={styles.text4}>{data.shifttype.name}</Text>
              </View>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Giờ vào</Text>
                <Text style={styles.text4}>{moment(attendance.login_time).format('HH:mm')}</Text>
              </View>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Trạng thái</Text>
                <Text style={styles.text4}>{_handleStatus()}</Text>
              </View>
              <View style={styles.block_element}>
                <Text style={styles.text4}>Giờ tan ca</Text>
                <Text style={styles.text4}>{attendance.logout_time == null ? 'chưa cập nhật' : moment(attendance.logout_time).format('HH:mm')}</Text>
              </View>
            </View>

          </View>
        </View>


      </ScrollView>
      <View style={styles.btn_bottom}>
        <ButtonHandle
          buttonText=" Về danh sách ca"
          stylesText={styles.Text12}
          onPressButton={Actions.onPressListShift}
          styles={{ backgroundColor: Colors.blue2 }}
        />
      </View>
      {/* </View> */}
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
