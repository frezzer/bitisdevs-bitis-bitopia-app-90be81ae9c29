
// basics
import _ from 'lodash';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, TextInput, TouchableOpacity, View, Text, Image, Animated, Platform, ScrollView,
} from 'react-native';
import moment from 'moment';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import { Feather } from '../../Components/Icons';
import TextInfo from '../../Components/TextInput/TextInfo';


// Styles
import styles from './Styles/SellerConfirmScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import ShiftFindOnlyOneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux';
import UserDetailActions, { UserDetailSelectors } from '../../Redux/user/UserDetailRedux';
import FindstoreSellerActions, { FindstoreSellerSelectors } from '../../Redux/shift/FindStoreSellerRedux';
import ShiftFindStoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';


// utils
import { compareOmitState } from '../../Services/utils';
import screenName from '../../Navigation/Screens';
import { Metrics, Colors } from '../../Themes';


/**
 * Options
 *  - navigation options for Screens
 * */


const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
SellerInforShiftScreen.navigationOptions = navigationOptions;

export default function SellerInforShiftScreen(props) {
  const { data, userUuid } = props.navigation.state.params;
  console.tron.log('TCL: SellerInforShiftScreen -> data', data);
  const attendances = _.find(data.attendances, (i) => i.user_uuid == userUuid);
  // console.tron.log('debug: chó tri -> attendances', attendances);
  const { onPressButton } = props;

  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [storeName, setstoreName] = React.useState('');
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  const user_detail = useSelector((state) => UserDetailSelectors.selectPayload(state.user.userDetail));
  const first_name = _.get(user_detail, 'firstName', '');
  const last_name = _.get(user_detail, 'lastName', '');
  const shift_leader_name = _.concat([last_name, first_name]);

  // get store

  const findStoreShiftPayload = useSelector((state) => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  const findStoreShiftFetching = useSelector((state) => ShiftFindstoreSelectors.selectFetching(state.shift.findStore));
  // effects
  React.useEffect(() => {
    console.tron.log('SellerInforShiftScreen effect');
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      dispatch(ShiftFindStoreActions.shiftFindstoreRequest({
        store_uuid: data.store_uuid,
      }));
    }
    return () => { };
  }, [/** input */]);
  React.useEffect(() => {
    if (findStoreShiftPayload != null && findStoreShiftFetching === false) {
      setstoreName(findStoreShiftPayload.data.store_name);
    }
  }, [findStoreShiftPayload]);


  /** Actions * */
  const Actions = {
    onPressPopToTop: () => props.navigation.navigate(screenName.DashBoardScreen),
    onPressGoBack: () => {
      props.navigation.navigate(screenName.SellerListShiftScreen);
    },
    onPressCheckOut: () => {
      props.navigation.navigate(screenName.SellerCheckOutScreen);
    },
  };

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    onPressButton,
    Actions,
    data,
    attendances,
    shift_leader_name,
    scrollY,
    setScrollY,
    storeName,

  };
  return <Render {...renderProps} />;
}


/**
 * Render
 */
const Render = React.memo((props) => {
  const {
    initialRender,
    Actions,
    data,
    attendances,
    shift_leader_name,
    scrollY,
    setScrollY,
    storeName,
  } = props;

  if (initialRender) return <LoadingInteractions size="large" />;

  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.blue1],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };


  const _handleStatus = () => {
    const { status } = attendances;
    switch (status) {
      case 'LATE':
        return 'Đi muộn';
      case 'DOING':
        return 'Đã điểm danh';
      case 'DONE':
        return 'Đã tan ca';
      default:
        break;
    }
  };
  return (

    <View style={{ flex: 1 }}>

      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>


        </View>
      </Animated.View>
      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0, height: Metrics.height,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: 24,
        }}
        >
          <View>
            <View style={styles.container}>
              <View style={styles.part1}>
                <Text style={styles.Title}> Thông tin chi tiết</Text>
              </View>
              <View style={styles.part2}>
                <TextInfo
                  Text1="Nhân viên"
                  Text2={shift_leader_name}
                />
                <TextInfo
                  Text1="Cửa hàng"
                  Text2={storeName}
                />
                <TextInfo
                  Text1="Ngày làm việc"
                  Text2={moment(attendances.created_at).format('DD/MM/YYYY')}
                />
                <TextInfo
                  Text1="Ca làm việc"
                  Text2={data.shifttype.name}
                />
                <TextInfo
                  Text1="Giờ vào ca"
                  Text2={moment(attendances.login_time).format('HH:mm')}
                />
                <TextInfo
                  Text1="Trạng thái"
                  Text2={_handleStatus()}
                />
                <TextInfo
                  Text1="Giờ tan ca"
                  Text2={attendances.logout_time == null ? 'chưa cập nhật' : moment(attendances.logout_time).format('HH:mm')}
                />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      {attendances.status !== 'DONE' ? (
        <View style={styles.btn_bottom}>
          <ButtonHandle
            buttonText="Tan ca"
            stylesText={styles.Text12}
            onPressButton={Actions.onPressCheckOut}
            styles={{ backgroundColor: Colors.blue2 }}
          />
        </View>
      )
        : (
          <View style={styles.btn_bottom}>
            <ButtonHandle
              buttonText=" Đã tan ca"
              stylesText={styles.Text12}
              styles={{ backgroundColor: Colors.greybold }}
            />
          </View>
        )}
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
