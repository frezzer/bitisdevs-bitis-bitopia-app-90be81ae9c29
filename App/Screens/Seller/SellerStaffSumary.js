
// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
// Components
import {
  Dimensions, InteractionManager, View, Text, TouchableOpacity, Image, Alert, ScrollView, ImageBackground, Button, Animated, Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import screensName from '../../Navigation/Screens';
import { Feather } from '../../Components/Icons';
import { Metrics, Images, Colors } from '../../Themes';
import DoubleButton from '../../Components/Button/DoubleButton';
import ButtonHandle from '../../Components/Button/ButtonHandle';

// Styles
import styles from './Styles/SummaryAttendanceScreen.styles';
// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux';
import AttendanceSubmitActions, { attendanceSubmitSelectors } from '../../Redux/attendance/AttendanceSubmitRedux';


// utils
import { compareOmitState } from '../../Services/utils';

const AnimatedFeather = Animated.createAnimatedComponent(Feather);
const { width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const navigationOptions = (props) => ({
  header: null,
});
SellerStaffSummaryScreen.navigationOptions = navigationOptions;

/**
 * Main
 */

export default function SellerStaffSummaryScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [isModalFlag, setIsModalFlag] = React.useState(false);
  const [scrollY, setScrollY] = React.useState(new Animated.Value(
    Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
  ));
  const mePayload = useSelector((state) => MeSelectors.selectPayload(state.me));
  const userUuidCheck = _.get(mePayload, ['username']);
  const attendanceSubmitFetching = useSelector((state) => attendanceSubmitSelectors.selectFetching(state.attendance.attendanceSubmit));
  const attendanceSubmitError = useSelector((state) => attendanceSubmitSelectors.selectError(state.attendance.attendanceSubmit));
  const attendanceSubmitPayload = useSelector((state) => attendanceSubmitSelectors.selectPayload(state.attendance.attendanceSubmit));

  const dataShow = props.navigation.getParam('dataAtendance', {});
  console.tron.log('new', dataShow);

  // effects
  React.useEffect(() => {
    console.tron.log('ShiftSummaryScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      // ...
    }
    // Get all attendance of saler in month
    // dispatch(NotifyActions.notifyShow());

    return () => { };
  }, [/** input */]);
  React.useEffect(() => {
    if (attendanceSubmitFetching === false && attendanceSubmitError == null && isModalFlag === true) {
      setIsModalVisible(!isModalVisible);
    }
  }, [attendanceSubmitPayload]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressSendSummary: () => {
      const dataAtendancePut = {
        userUuid: dataShow.meData.username,
        fromDate: moment(dataShow.fromDate).toISOString(),
        toDate: moment(dataShow.toDate).toISOString(),
        storeUuid: dataShow.storeData.store_uuid,
      };
      dispatch(AttendanceSubmitActions.attendanceSubmitRequest(dataAtendancePut));
    },
    toggleModal: () => {
      setIsModalVisible(!isModalVisible);
      setIsModalFlag(true);
    },
    onPressGoBack: () => {
      props.navigation.goBack();
    },
    onPressHistory: () => {
      props.navigation.navigate(screensName.HistoryScreen, { staffData: dataShow.meData });
    },

  };
  const state = {
  };
  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    ...props,
    initialRender,
    Actions,
    state,
    isModalVisible,
    scrollY,
    setScrollY,
    dataShow,
    attendanceSubmitPayload,
    userUuidCheck,
  };
  return <Render {...renderProps} />;
}

/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SummaryAttendanceScreen render');
  const {
    initialRender,
    Actions,
    isModalVisible,
    scrollY,
    dataShow,
    attendanceSubmitPayload,
    userUuidCheck,
  } = props;
  const animateColor = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.blue1, Colors.white],
  });

  const animateColorBtn = scrollY.interpolate({
    inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
    outputRange: [Colors.white, Colors.black],
  });

  const backgroundHeaderColor = {
    backgroundColor: animateColor,
  };

  const backgroundBtnColor = {
    color: animateColorBtn,
  };


  return (
    <View style={{ flex: 1 }}>
      <Animated.View style={[{ height: widthPercentageToDP(400 / 375 * 100) }, backgroundHeaderColor]}>
        <View style={{
          height: Metrics.navigationBarHeight, justifyContent: 'space-between', paddingTop: widthPercentageToDP(40 / 375 * 100), flexDirection: 'row',
        }}
        >
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressGoBack}>
            <View>
              <AnimatedFeather
                name="chevron-left"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.backButtonTouchable} onPress={Actions.onPressHistory}>
            <View>
              <AnimatedFeather
                name="clock"
                style={[styles.backButtonButtonText, backgroundBtnColor]}
              />
            </View>
          </TouchableOpacity>

        </View>
      </Animated.View>
      <ScrollView
        style={{
          backgroundColor: 'transparent', position: 'absolute', width: '100%', top: widthPercentageToDP(100 / 375 * 100), bottom: 0,
        }}
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
        )}
      >
        <View style={{
          backgroundColor: 'white', borderTopLeftRadius: 24, borderTopRightRadius: 24,
        }}
        >
          <View style={styles.part1}>
            <Text style={styles.title}> Bảng chấm công tháng</Text>
          </View>

          <View style={styles.part2}>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Nhân viên</Text>
              <Text style={styles.text1}>{ _.concat([dataShow.meData.lastName, dataShow.meData.firstName ? ' ' : '', dataShow.meData.firstName]) || 'Chưa cập nhật'}</Text>

            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Tên cửa hàng</Text>
              <Text style={styles.text1}>{dataShow.storeData ? dataShow.storeData.store_name : 'Chưa cập nhật'}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Ngày bắt đầu</Text>
              <Text style={styles.text1}>{moment(dataShow.fromDate).format('DD/MM/YYYY')}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Ngày kết thúc</Text>
              <Text style={styles.text1}>{moment(dataShow.toDate).format('DD/MM/YYYY')}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Số ca làm việc</Text>
              <Text style={styles.text1}>{dataShow.numberOfShifts}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Số giờ làm việc</Text>
              <Text style={styles.text1}>{Math.round(dataShow.numberOfTime * 10) / 10}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Số ca đi trễ</Text>
              <Text style={styles.text1}>{dataShow.numberOfShiftsLate || 0}</Text>
            </View>
            <View style={styles.block_element}>
              <Text style={styles.text2}>Tổng lương dự kiến</Text>
              <Text style={styles.text1}>{dataShow.sumSalaryReference}</Text>
            </View>
          </View>
        </View>
        <Modal isVisible={isModalVisible}>
          <View style={styles.Modal}>
            <Text style={styles.modal_title}>Bạn muốn gửi chấm công ?</Text>
            <Text style={styles.modal_content}>Thông tin sau khi gửi sẽ không thể điều chỉnh.</Text>
            <Text style={styles.modal_content1}>Nếu có sai sót, bạn phải báo cho HR điều chỉnh trước khi gửi chấm công</Text>
            <DoubleButton
              styles={styles.cover}
              onPressButton1={Actions.toggleModal}
              buttonText1="Hủy"
              onPressButton2={Actions.onPressSendSummary}
              buttonText2="Gửi thông tin đến HR"
            />

          </View>
        </Modal>
      </ScrollView>
      <View style={styles.btn_bottom}>
        <ButtonHandle
          buttonText="Gửi thông tin"
          stylesText={styles.Text12}
          onPressButton={Actions.toggleModal}
          disable={attendanceSubmitPayload != null && userUuidCheck === attendanceSubmitPayload.userUuid}
          styles={{ backgroundColor: (attendanceSubmitPayload != null && userUuidCheck === attendanceSubmitPayload.userUuid) ? Colors.greybold : Colors.orange1 }}
        />
      </View>
    </View>
  );
}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
