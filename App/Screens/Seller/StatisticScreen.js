'use strict';

// basics
import _ from 'lodash';
import React, { useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
// Components
import {

  ScrollView,
  Text,
  InteractionManager, View,
} from 'react-native';
import MissionList from '../../Components/Mission/MissionList';
import RankTime from '../../Components/Statistic/RankTime';
import DoubleButton from '../../Components/Button/DoubleButton';
import LoadingInteractions from '../../Components/LoadingInteractions';
import RevenueChart from '../../Components/Charts/RevenueChart';
import RightButtonsTopBar from '../../Components/RightButtonsTopBar';

// Styles
import styles from './Styles/StatisticScreen.styles';

// Actions and Selectors Redux 
import MeActions, { MeSelectors } from '../../Redux/MeRedux'
import EmployeeRevenueActions, { EmployeeRevenueSelectors } from '../../Redux/employee/EmployeeRevenueRedux'
import StoreGetStockActions, { StoreGetStockSelectors } from '../../Redux/store/StoreGetStockRedux'
import loadingSagaActions, { loadingSagaSelectors } from '../../Redux/LoadingSagaRedux'

import screensName from '../../Navigation/Screens';
// utils
import { compareOmitState } from "../../Services/utils";


const navigationOptions = (props) => {
  const onPressRightButton = () => props.navigation.navigate(screensName.MoreScreen);
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Bán Hàng',
    headerRight: () => {
      return <RightButtonsTopBar
        name={'align-right'}
        styleView={styles.headerRightView}
        onPress={onPressRightButton}
      />
    }
  };
}


SaleStatisticScreen.navigationOptions = navigationOptions;

/**
 * Main
 */
export default function SaleStatisticScreen(props) {

  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [DateInPickerVisible, setDateInPickerVisible] = useState(false);
  const [DateOutPickerVisible, setDateOutnPickerVisible] = useState(false);
  const [DatePickerVisible, setDatePickerVisible] = useState(false);

  // useStates

  const _onShowDateTimePicker = (key) => {
    setDatePickerVisible(true);
  }
  const _hideDateTimePicker = (key) => {
    const state = {}
    state[`is${key}PickerVisible`] = false
    //! FIXME: o day chung toi khong xai this.setState, co copy thi cung nen can than
  };

  const _handleDatePicked = (val, key) => {
    console.log("A date has been picked: ", val);
    //! FIXME: o day chung toi khong xai this.setState, co copy thi cung nen can than
  };

  // selector store
  const employee_revenue = useSelector(state => EmployeeRevenueSelectors.selectPayload(state.employee.revenue));
  const storeStock = useSelector(state => StoreGetStockSelectors.selectPayload(state.store.stock));
  const me_payload = useSelector(state => MeSelectors.selectPayload(state.me));

  const dataChart = _.get(employee_revenue, ['dataCharts'], []);
  const data = _.map(dataChart, (i) => (parseInt(i.totalBill) / 1000000));
  const datetime = _.map(dataChart, function (item) {
    return (moment(item.date).format("DD/MM/YYYY")).substr(0, 5)
  });
  const total_Quantity = _.sum(_.map(dataChart, 'totalBillQuantity'))
  const total_Cash = _.sum(_.map(dataChart, 'totalBillCash'))
  const store_uuid = _.get(me_payload, ['employee_id', 'merchant_uuid'], null);

  // console.tron.log("TCL: SaleScreen -> date ne", datetime)


  // effec ts
  React.useEffect(() => {
    console.tron.log('SaleScreen effect');
    if (initialRender) {
      dispatch(EmployeeRevenueActions.employeeRevenueRequest({
        startTime: moment().subtract(30, 'days').toISOString(),
        endTime: moment().toISOString(),
      }));
      dispatch(loadingSagaActions.loadingIndicatorHide());
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(StoreGetStockActions.storeGetstockRequest({ store_uuid: store_uuid }))
    }
  }, [/** input */]);
  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressCheckIn: () => {
      props.navigation.navigate(screensName.CheckInScreen);
    },
    onPressCheckOut: () => {
      props.navigation.navigate(screensName.CheckOutScreen);
    },

    _onShowDateTimePicker,
    _hideDateTimePicker,
    _handleDatePicked

  };
  const state = {
    DatePickerVisible,
    DateInPickerVisible,
    DateOutPickerVisible
  }
  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state,
    data,
    datetime,
    storeStock,
    total_Quantity,
    total_Cash,
  }
  return <Render {...renderProps} />
}

/**
 * Render
 */

const Render = React.memo((props) => {
  // declare variables
  // PROPS
  const {
    initialRender,
    data, datetime,
    storeStock,
    Actions,
    state
  } = props;

  if (initialRender) return <LoadingInteractions size={'large'} />

  // STATE
  const {
    DatePickerVisible,
    DateInPickerVisible,
    DateOutPickerVisible,
  } = state;

  //ACTIONS
  const {
    _onShowDateTimePicker, onPressCheckIn,
    onPressCheckOut, _hideDateTimePicker,
    _handleDatePicked
  } = Actions;


  const totalQuantity = _.get(storeStock, '[0].totalquantity', 0);
  const totalValue = _.get(storeStock, '[0].totalamount', 0);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.block} >
          <View style={styles.container}>
            {/* <MissionList /> */}
            <RankTime />
            <RevenueChart
              data={data} labels={datetime}

            />
            <View style={styles.block5}>
              <Text style={styles.Text3}>Tồn Kho</Text>
              <View style={styles.stockArea}>
                <View style={styles.block_quantity}>
                  <Text style={styles.Text11}>Số lượng </Text>
                  <Text style={styles.Text10}> {Number(totalQuantity).toLocaleString()}</Text>
                  <Text style={styles.Text2}> sản phẩm </Text>
                </View>
                <View style={styles.block_quantity1}>
                  <Text style={styles.Text11}>Giá trị</Text>
                  <Text style={styles.Text10}>{Number(totalValue).toLocaleString()}</Text>
                  <Text style={styles.Text2}> VNĐ</Text>
                </View>
              </View>
            </View>
            <View style={styles.block6}>

              <DoubleButton
                onPressButton1={onPressCheckIn}
                buttonText1='Điểm danh'
                onPressButton2={onPressCheckOut}
                buttonText2='Tan ca'
                style={styles.buttonGreenMedium}
              />
            </View>


          </View>
        </View>
      </ScrollView>
    </View>

  );


}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}





