import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  ApplicationStyles, Metrics, Fonts, Colors,
} from '../../../Themes';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.theme.background,
    // marginBottom:Metrics.paddingMedium
  },

  // header: {
  //   // backgroundColor: Colors.white,
  //   padding: Metrics.paddingMedium,
  //   // marginTop: Metrics.marginTiny,
  //   borderBottomColor: Colors.background,
  //   borderBottomWidth: 1,
  // },
  btn_bottom: {
    backgroundColor: 'white',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100 / 375 * Metrics.width,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.15,
    shadowRadius: 8,
    // android
    elevation: 30,
  },
  // Text

  footer: {
    backgroundColor: Colors.white,
    padding: Metrics.paddingMedium,
    marginTop: Metrics.marginTiny,
    borderBottomColor: Colors.block,
    borderBottomWidth: 1,
    ...ApplicationStyles.specialApp.alignCenter,
    marginBottom: Metrics.marginTiny,
  },
  title: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.grey8,
  },
  title_footer: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.blueOcean,

  },
  item: {
    // marginLeft: -Metrics.marginXSmall
    // ...ApplicationStyles.specialApp.dimensisons(375,62),
    // marginVertical:Metrics.paddingXXSmall


    // paddingHorizontal : 16,


  },
  divider: {
    // borderBottomColor: Colors.black,
    // borderBottomWidth: 1,
    // backgroundColor:'red',
  },
  status: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.green1,
    fontStyle: 'normal',
    marginTop: 4,
  },
  status1: {
    fontFamily: Fonts.type.fontBold,
    fontSize: Fonts.size.textSmall,
    color: Colors.orange1,
    fontStyle: 'normal',
    marginTop: 4,
  },
  // block_footer: {
  //   backgroundColor: Colors.white,
  //   padding: Metrics.paddingMedium,
  //   marginTop: Metrics.marginTiny,
  //   borderBottomColor: Colors.block,
  //   borderBottomWidth: 1
  // },
  // content:{

  // }
  headerRightView: {
    backgroundColor: 'red',
    // paddingRight: 100
  },
  header: {
    // backgroundColor: 'red',
    height: 44 / 375 * Metrics.width,
    justifyContent: 'center',
    backgroundColor: '#F0F0F0',
    paddingLeft: Metrics.marginMedium,


  },
  noShift: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.textMedium,
    color: Colors.grey9,
    marginTop: wp(20 / 375 * 100),
    textAlign: 'center',
  },
  iconBack: {
    fontSize: 20,
    marginLeft: Metrics.marginXTiny,
  },


});
