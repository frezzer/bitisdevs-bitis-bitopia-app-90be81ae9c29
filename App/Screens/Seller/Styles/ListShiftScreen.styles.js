import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({


  container: {
    flex: 1,
  },
  content: {
    ...ApplicationStyles.specialApp.alignCenter
  },
  block: {
    height: wp(50 / 375 * 100),
    backgroundColor: Colors.block,
    borderWidth:0.02,
    // boderRadius:Metrics.boderNomarl,
    justifyContent: 'center',
    alignItems:'center',
    // backgroundColor:'white'

  },
  block2: {
    // height: wp(50 / 375 * 100),
    backgroundColor: Colors.block,
    // paddingTop: Metrics.paddingHuge,\
    // marginTop: Metrics.marginXSmall,
    justifyContent: 'center',
    // backgroundColor:'red'

  },
  Text1: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginLeft: Metrics.marginMedium
  },
  lineStyle: {
    borderWidth: 0.2,
    borderColor: Colors.greenLight,
    // margin: 10,
  },
  shiftArea:{
    // flex:1,
    // marginHorizontal:Metrics.marginMedium,
    // borderRadius:14,
    // marginHorizontal:Metrics.paddingTiny
  },
  shiftArea1:{
    // flex:1,
    // marginTop:155

  }
})
