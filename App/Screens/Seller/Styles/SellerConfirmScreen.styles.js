import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default StyleSheet.create({
  container: {
    backgroundColor: Colors.block,
    flex: 1,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,

  },
  part1: {
    // padding: Metrics.paddingHuge,
    alignItems: 'center',
    backgroundColor: Colors.white,
    paddingBottom: Metrics.paddingMedium,
    paddingTop: Metrics.marginHuge,
    borderTopLeftRadius: Metrics.boderLLLarge,
    borderTopRightRadius: Metrics.boderLLLarge,
  },
  part2: {
    backgroundColor: Colors.white,
    // marginTop:Metrics.marginTiny
  },
  block_element: {
    paddingHorizontal: Metrics.paddingMedium,
    paddingVertical: Metrics.paddingSmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: Colors.block,
    borderBottomWidth: 1
  },
  // Text
  text1: {
    fontSize: Fonts.size.textXLarge,
    fontFamily: Fonts.type.fontBold,
    lineHeight: wp(24 / 375 * 100)
  },
  text2: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontBold,
    color: Colors.blue,
    marginVertical: Metrics.marginTiny
  },
  text3: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey8,
    // fontStyle: 'italic',
    textAlign: 'center'
  },
  text4: {
    fontSize: Fonts.size.textMedium,
    fontFamily: Fonts.type.fontRegular,
    color: Colors.grey9,
  },
  bottomTab: {
    ...ApplicationStyles.specialApp.coverBottomTab,
  },
  btn_bottom: {
    backgroundColor: 'white',
    position:'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 108 / 375 * Metrics.width,

  },
  Title:{
    fontFamily: Fonts.type.fontBold,
    fontSize : Fonts.size.textXXLarge,

  },
  backButtonButtonText: {
    paddingTop: Metrics.paddingXTiny,
    // textAlign: 'center',
    fontSize: Fonts.size.textHuge,
    fontWeight: 'bold',
    color: Colors.white,
  },
  backButtonTouchable: {
    ...ApplicationStyles.specialApp.dimensisons(44, 44),
    ...ApplicationStyles.specialApp.alignCenter,
    borderRadius: Metrics.boderLSmall,
    backgroundColor: 'rgba(255,255,255, 0.1)',
    opacity: 1,
    marginHorizontal: Metrics.marginMedium,
  },
})
