import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../../Themes'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const quantity_area = {
  backgroundColor: Colors.theme.block,
  borderRadius: Metrics.boderLarge,

}


export default StyleSheet.create({
  block: {
    backgroundColor: Colors.theme.block
  },

  container: {
    flex: 1,
    backgroundColor: Colors.block
  },
  Text2: {
    fontFamily: Fonts.type.fontRegular,
    fontSize: Fonts.size.textMedium,
    color: Colors.blueOcean,
    textAlign: 'center'
  },
  Text3: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.black,
    fontSize: Fonts.size.textLarge,
    marginBottom: Metrics.marginXSmall,
  },
  block5: {
    ...ApplicationStyles.specialApp.padding(16, 0, 0, 0, 0, 18),
    height: wp(176 / 375 * 100),
    backgroundColor: Colors.white,
    marginBottom:Metrics.marginXSTiny

  },
  stockArea: {
    flexDirection: 'row',
    ...ApplicationStyles.specialApp.alignCenter,
    paddingHorizontal: Metrics.paddingXLarge
  },
  block_quantity: {
    ...ApplicationStyles.specialApp.dimensisons(168, 92),
    ...quantity_area,
    ...ApplicationStyles.specialApp.layoutDirection,
    marginRight: Metrics.marginTiny,
    justifyContent: 'center'
  },
  block_quantity1: {
    ...ApplicationStyles.specialApp.dimensisons(168, 92),
    ...ApplicationStyles.specialApp.layoutDirection,
    ...quantity_area,
    justifyContent: 'center',
  },
  Text10: {
    fontFamily: Fonts.type.fontBold,
    color: Colors.blueOcean,
    fontSize: Fonts.size.textLarge,
    textAlign: 'center',
    paddingVertical: 2
  },
  Text11: {
    fontFamily: Fonts.type.fontRegular,
    color: Colors.black,
    fontSize: Fonts.size.textMedium,
    textAlign: 'center',
    paddingVertical: 2
  },
  block6: {
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',

  },
  buttonBlueOcean: {
    backgroundColor: Colors.blueOcean,
  },
})


