import React from 'react';
import { Provider } from 'react-redux';
import createStore from '../Redux';
import AppNavigation from '../Navigation';
import RootAlert from '../Components/RootAlert';
import CloudMessaging from '../Lib/react-native-firebase/CloudMessaging';

export const store = createStore();
export default function RootContainer() {
  return (
    <Provider store={store}>
      <AppNavigation />
      <RootAlert />
      {/* <CloudMessaging /> */}
    </Provider>
  );
}
