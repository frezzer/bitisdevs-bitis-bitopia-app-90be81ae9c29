/* eslint-disable max-len */
// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import _ from 'lodash';
// our "constructor"
const createAPI = (baseURL = 'http://apistaging.bitis.tech', basePosURL = 'http://apidev.bitis.tech:1509') => {
// const create = (baseURL = 'http://apidev.bitis.tech:1503') => {
  const apiPos = apisauce.create({
    // base URL is read from the "constructor"
    baseURL: basePosURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 10000,
  });

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
    },
    // 10 second timeout...
    timeout: 10000,
  });

  // auth
  const userLogin = ({ identifier, password }) => api.post('/authentication/auth/local', { identifier, password });
  const userGetMe = ({ Authorization }) => api.get('/authentication/users/me', {}, { headers: { Authorization: `Bearer ${Authorization}` } });
  const userGetDetail = ({ Authorization, data }) => api.get(`/authentication/users/findOne/${data.username}`, {}, { headers: { Authorization: `Bearer ${Authorization}` } });
  const userFind = ({ Authorization, data }) => api.get('/authentication/users', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });


  // employee
  const getMeEmployee = ({ Authorization, data }) => api.get('/employee/getMe', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const getRevenueEmployee = ({ Authorization, data }) => api.get('/employee/getRevenue', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });

  // Shifth
  const createOneShift = ({ Authorization, data }) => api.post('/bitopia/shifts/createOne', { ..._.omit(data, ['username']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const findStoreShift = ({ Authorization, data }) => api.get(`/bitopia/loyalty/store/${data.store_uuid}`, {}, { headers: { Authorization: `Bearer ${Authorization}` } });
  const findOnlyOneShift = ({ Authorization, data }) => api.get(`/bitopia/shifts/${data.uuid}`, {}, { headers: { Authorization: `Bearer ${Authorization}` } });
  const finishOneShift = ({ Authorization, data }) => api.put('/bitopia/shifts/finishOne', { ..._.omit(data, ['username'], ['store_Uuid']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const getTypesShift = ({ data }) => api.get('/bitopia/shifttypes', { ...data });
  const findShift = ({ Authorization, data }) => api.get('/bitopia/shifts', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });
  // const findStore = ({ Authorization, data }) => api.get('bitopia/attendances/store', {}, { headers: { Authorization: `Bearer ${Authorization}` } });
  const findStoreSeller = ({ Authorization, data }) => api.get('bitopia/attendances/store', {}, { headers: { Authorization: `Bearer ${Authorization}` } });

  // Attendance
  const createOneAttendance = ({ Authorization, data }) => api.post('/bitopia/attendances/createOne', { ..._.omit(data, ['username', 'store_uuid']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const finishOneAttendance = ({ Authorization, data }) => api.put('/bitopia/attendances/finishOne', { ..._.omit(data, ['username','store_uuid']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const findAttendance = ({ Authorization, data }) => api.get('/bitopia/attendances', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const attendanceMonth = ({ Authorization, data }) => api.get(`/bitopia/attendances/month/${data.userUuid}`, { ..._.omit(data, ['userUuid']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const attendanceSubmit = ({ Authorization, data }) => api.put(`/bitopia/attendances/month/${data.userUuid}`, { ..._.omit(data, ['userUuid']) }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const userAttendance = ({ Authorization, data }) => api.get('/bitopia/attendances/users', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const attendanceMonthSeller = ({ Authorization, data }) => api.get(`/bitopia/attendances/month/seller-of-leader/${data.userUuid}`, { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });

  // store
  const getstockStore = ({ data }) => api.get('/bitopia/shift/store/getStock', { ...data });

  // product
  // const getProductDetail = ({ Authorization, data }) => api.get(`/pos/products/${data.product_uuid}`, { headers: { Authorization: `Bearer ${Authorization}` } });

  // bill
  // const createTempBill = ({ Authorization, data }) => api.post('/pos/bills', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });
  const getBillHistory = ({ Authorization, data }) => api.get('/pos/bills', { ...data }, { headers: { Authorization: `Bearer ${Authorization}` } });

  // bill craft
  const getBillCraftHistory = ({ Authorization, data }) => apiPos.get('/bills/find', { ...data }, { headers: { Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNDRlN2RiZjRkNjk1MjM4MmE4YTgzZiIsImlhdCI6MTU4MjcxMTY1MywiZXhwIjoxNTg1MzAzNjUzfQ.fAUZQHv45-EUKaiPsdnZC2eGlYaR1JksE3k6aEY0o4Y' } });
  const getProductDetail = ({ Authorization, data }) => apiPos.get('/warehouses/store/find', { ...data }, { headers: { Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNDRlN2RiZjRkNjk1MjM4MmE4YTgzZiIsImlhdCI6MTU4MjcxMTY1MywiZXhwIjoxNTg1MzAzNjUzfQ.fAUZQHv45-EUKaiPsdnZC2eGlYaR1JksE3k6aEY0o4Y' } });
  const createBillCraft = ({ Authorization, data }) => apiPos.post('/bills/createDraft', { ...data }, { headers: { Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlNDRlN2RiZjRkNjk1MjM4MmE4YTgzZiIsImlhdCI6MTU4MjcxMTY1MywiZXhwIjoxNTg1MzAzNjUzfQ.fAUZQHv45-EUKaiPsdnZC2eGlYaR1JksE3k6aEY0o4Y' } });

  return {
    // a list of the API functions from step 2
    userLogin,
    userGetMe,
    userFind,
    // employee
    getMeEmployee,
    getRevenueEmployee,
    // Shift
    createOneShift,
    findStoreShift,
    findOnlyOneShift,
    finishOneShift,
    getTypesShift,
    findShift,
    // findStore,
    findStoreSeller,
    // Attendance
    createOneAttendance,
    finishOneAttendance,
    findAttendance,
    getstockStore,
    attendanceMonth,
    attendanceSubmit,
    attendanceMonthSeller,
    userAttendance,
    // Product
    getProductDetail,
    // Bill
    // createTempBill,
    getBillHistory,
    userGetDetail,
    // Bill Craft
    getBillCraftHistory,
    createBillCraft,
  };
};

// let's return back our create method as the default.
export default {
  createAPI,
};
