
import React from 'react';
import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
// import moment from 'moment';
// import 'moment/locale/vi';

import ReactNativeBiometrics from 'react-native-biometrics';

export function turnOnLoginBiometric() {
  ReactNativeBiometrics.simplePrompt({ promptMessage: 'Confirm fingerprint' }).then(({ success }) => {
    if (success) {
      AsyncStorage.getItem('Authorization').then((token) => {
        if (token) {
          AsyncStorage.setItem('AuthorizationBiometric', token);
        }
      });
    }
  });
}


export function turnOffLoginBiometric() {
  ReactNativeBiometrics.simplePrompt({ promptMessage: 'Confirm fingerprint' }).then(({ success }) => {
    if (success) {
      AsyncStorage.removeItem('AuthorizationBiometric');
    }
  });
}
