
const colors = {
  /**
  |--------------------------------------------------
  | Global Theme : đây là bộ color theme global ==> thay đổi theo theme
  |--------------------------------------------------
  */
  theme: {
    background: '#F0F0F0',
    shadow: 'rgba(0, 0, 0, 0.1)',
    block: '#F2F6F9',
    title: '#434341',
    text: '#888888',
    border: '#979797',
    busy: 'rgba(255, 80, 80, 0.2)',
  },

  /**
  |--------------------------------------------------
  | Color Theme : ....
  |--------------------------------------------------
  */
  transparent: 'transparent',
  transparent_w: 'rgba(255,255,255,0)',
  transparent_w_50: 'rgba(55,255,255,.1)',
  white: '#FFFFFF',
  black: '#000000',
  yellow: '#F7B500',
  red: '#FF5050',
  blue: '#2883E7',
  greenDark: '#00524F',
  greenMedium: '#4BB67D',
  greenLight: '#EDF7F3',
  graySPLight: '#BBBBBB',
  grayLight: '#888888',
  grayMedium: '#444444',
  grayDark: '#222222',
  silver: '#F2F6F9',
  silverDark: '#C7C9D5',
  blueOcean: '#007AFF',
  bar: '#D3E1F1',
  blueheavy: '#0D61FF',
  block: '#F2F6F9',

  // / Bitopia
  grey7: '#8C8C8C',
  blue5: '#4657B1',
  blue4: '#E4F3FD',
  grey8: '#595959',
  grey9: '#262626',
  green1: '#72C9AF',
  green3: '#F1FFFB',
  orange1: '#FF641A',
  orange2: '#FF9461',
  blue1: '#181D51',
  blue2: '#4657B1',
  blue3: '#7E95DC',
  greybold: '#BFBFBF',
  grey3: '#F5F5F5',
  grey4: '#E8E8E8',
  grey5: '#D9D9D9',
  background: '#F0F0F0',
};

export default colors;
