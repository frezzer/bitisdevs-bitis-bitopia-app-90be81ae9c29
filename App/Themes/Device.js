import { Platform, Dimensions } from 'react-native';
import _ from 'lodash';

const IPHONE_X_LONG_SIDE = 812;
const IPHONE_XR_LONG_SIDE = 896;

const { OS, isPad, isTVOS } = Platform;
const { width, height } = Dimensions.get('window');

const xDimensionsMatch = (
  (height === IPHONE_X_LONG_SIDE) || (width === IPHONE_X_LONG_SIDE)
);

const xrDimensionsMatch = (
  (height === IPHONE_XR_LONG_SIDE) || (width === IPHONE_XR_LONG_SIDE)
);

const isIphoneX = (OS === 'ios' && !isPad && !isTVOS && xDimensionsMatch);
const isIphoneXR = (OS === 'ios' && !isPad && !isTVOS && xrDimensionsMatch);

/**
 * Receives settings for different devices
 * If the device is recognized, it returns only settings for that device
 * If not, it returns settings for 'default'
 *
 * @param {object} settings The settings provided for
 * @return {settings} Returns device specific (or 'default') settings
 */

function select(settings) {
  if (settings.iPhoneX && isIphoneX) {
    return settings.iPhoneX;
  }

  if (settings.iPhoneXR && isIphoneXR) {
    return settings.iPhoneXR;
  }

  return _.get(settings, 'default');
}

const Device = {
  isIphoneX,
  isIphoneXR,
  select,
  NAVIGATION_HEADER_HEIGHT: 64,
  IPHONE_X_NOTCH_PADDING: 20,
  IPHONE_X_HOME_INDICATOR_PADDING: 24,
  IPHONE_XR_NOTCH_PADDING: 24,
  IPHONE_X_LONG_SIDE,
  IPHONE_XR_LONG_SIDE,
};


export default Device;