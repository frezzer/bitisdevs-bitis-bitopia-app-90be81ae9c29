import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Colors } from './index';


const type = {

  /**
  |--------------------------------------------------
  | Khanh font font family for new version
  |--------------------------------------------------
  */

  fontLight: 'SFProText-Light',
  fontRegular: 'SFProText-Regular',
  fontItalic: 'SFProText-RegularItalic',
  fontBold: 'SFProText-Bold',
  fontExtraBold: 'SFProText-Heavy',
  fontBlack: 'SFProText-Medium',

};

const size = {

  /**
  |--------------------------------------------------
  | Khanh size for new version
  |--------------------------------------------------
  */
  textXXHuge: wp(40 / 375 * 100), // 40
  textXHuge: wp(30 / 375 * 100), // 30
  textHuge: wp(24 / 375 * 100), // 24
  textXXLarge: wp(20 / 375 * 100), // 20
  textXLarge: wp(18 / 375 * 100), // 18
  textLarge: wp(16 / 375 * 100), // 16
  textMedium: wp(14 / 375 * 100), // 14
  textSmall: wp(12 / 375 * 100), // 12
  textXSmall: wp(10 / 375 * 100), // 10
};

const style = {
  heading: {
    fontFamily: type.fontRegular,
    fontSize: size.textXXHuge, // 40
    lineHeight: size.textXXHuge * 1.2,
    color: Colors.theme.title,
  },
  title: {
    fontFamily: type.fontRegular,
    fontSize: size.textHuge, // 24
    lineHeight: size.textHuge * 1.33,
    color: Colors.black,
  },
  titleExtra: {
    fontFamily: type.fontRegular,
    fontSize: size.textXHuge, // 30
    lineHeight: size.textXHuge * 1.37,
    color: Colors.black,
  },
  subtitleExtra: {
    fontFamily: type.fontRegular,
    fontSize: size.textXLarge, // 18
    lineHeight: size.textXLarge * 1.33,
    color: Colors.black,
  },
  subtitle: {
    fontFamily: type.fontRegular,
    fontSize: size.textLarge, // 16
    lineHeight: size.textLarge * 1.37,
    color: Colors.black,
  },
  text: {
    fontFamily: type.fontRegular,
    fontSize: size.textMedium, // 14
    lineHeight: size.textMedium * 1.35,
    color: Colors.theme.text,
  },
  caption: {
    fontFamily: type.fontRegular,
    fontSize: size.textSmall, // 12
    lineHeight: size.textSmall * 1.33,
    color: Colors.theme.text,
  },
  navBarText: {
    fontFamily: type.fontRegular,
    fontSize: size.textMedium,
    lineHeight: size.textMedium * 1.2,
    color: Colors.black,
  },
  primaryButtonText: {
    fontFamily: type.fontRegular,
    fontSize: size.textMedium,
    lineHeight: size.textMedium * 1.35,
    color: Colors.black,
  },
};

export default {
  type,
  size,
  style,
};
