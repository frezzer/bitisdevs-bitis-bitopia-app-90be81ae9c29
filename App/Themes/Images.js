// leave off @2x/@3x
import { Platform } from 'react-native';
const BackbuttonIos = require('../Images/Icons/Vector.png');
const BackbuttonAndroid = require('../Images/Icons/Vector.png');

export default {
  logo: require('../Images/ir.png'),
  //mapo
  // buttonBack : require('../Images/Icons/feather-arrow-left.png'),
  buttonBack: Platform.select({
    ios: BackbuttonIos, android: BackbuttonAndroid
  }),
  buttonClose: require('../Images/Icons/button-close.png'),
  buttonUser: require('../Images/Icons/button-user.png'),
}
