import { Dimensions, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { normalize } from './ScaleReponsive';
import Colors from './Colors';
import Fonts from './Fonts';
import Device from './Device';

const {
  NAVIGATION_HEADER_HEIGHT,
  IPHONE_X_NOTCH_PADDING,
  IPHONE_X_HOME_INDICATOR_PADDING,
  IPHONE_XR_NOTCH_PADDING,
} = Device;

const NAVIGATION_BAR_HEIGHT = Device.select({
  iPhoneX: NAVIGATION_HEADER_HEIGHT + IPHONE_X_NOTCH_PADDING,
  iPhoneXR: NAVIGATION_HEADER_HEIGHT + IPHONE_XR_NOTCH_PADDING,
  default: NAVIGATION_HEADER_HEIGHT,
});

const STATUS_BAR_HEIGHT = Device.select({
  iPhoneX: IPHONE_X_NOTCH_PADDING + 5,
  iPhoneXR: IPHONE_XR_NOTCH_PADDING + 5,
  default: 20,
});
const { width, height } = Dimensions.get('window');

export const initMetricsConstants = (value) => {
  Constants = value;
};

const metrics = {
  statusBarHeight: STATUS_BAR_HEIGHT,
  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  navigationBarHeight: NAVIGATION_BAR_HEIGHT,
  width,
  height,
  smallGutter: 5,
  mediumGutter: 15,
  largeGutter: 30,
  extraLargeGutter: 45,


  /**
  |--------------------------------------------------
  | Khanh metric for new version
  |--------------------------------------------------
  */
  // MARGIN
  marginXXTiny: wp(2 / 375 * 100), // 2,
  marginXTiny: wp(4 / 375 * 100), // 4,
  marginXSTiny: wp(6 / 375 * 100), // 6,
  marginTiny: wp(8 / 375 * 100), // 8,
  marginXSmall: wp(12 / 375 * 100), // 12,
  marginSmall: wp(14 / 375 * 100), // 14,
  marginMedium: wp(16 / 375 * 100), // 16,
  marginLarge: wp(18 / 375 * 100), // 18,
  marginXLarge: wp(20 / 375 * 100), // 20,
  marginXXLarge: wp(24 / 375 * 100), // 24,
  marginHuge: wp(32 / 375 * 100), // 32,
  marginXHuge: wp(48 / 375 * 100), // 48,

  // PADDING
  paddingXXTiny: wp(2 / 375 * 100), // 2,
  paddingXTiny: wp(4 / 375 * 100), // 4,
  paddingTiny: wp(8 / 375 * 100), // 8,
  paddingXXSmall: wp(10 / 375 * 100), // 10,
  paddingXSmall: wp(12 / 375 * 100), // 12,
  paddingSmall: wp(14 / 375 * 100), // 14,
  paddingMedium: wp(16 / 375 * 100), // 16,
  paddingLarge: wp(18 / 375 * 100), // 18,
  paddingXLarge: wp(20 / 375 * 100), // 22,
  paddingXsLarge: wp(22 / 375 * 100), // 20,
  paddingXXLarge: wp(24 / 375 * 100), // 24,
  paddingHuge: wp(32 / 375 * 100), // 32,
  paddingXHuge: wp(48 / 375 * 100), // 48,


  // BODER RADIUS
  boderTiny: wp(3 / 375 * 100), // 3,
  boderXTiny: wp(4 / 375 * 100), // 4,
  boderSmall: wp(6 / 375 * 100), // 6,
  boderLSmall: wp(8 / 375 * 100), // 8,
  boderNomarl: wp(12 / 375 * 100), // 12,
  boderLarge: wp(20 / 375 * 100), // 20,
  boderLLLarge: wp(20 / 375 * 100), // 24,
  boderXLarge: wp(40 / 375 * 100), // 40,
  boderMaxium: 9999,

  // ///////// QUAN CREATE ///////////
  button: {
    fontSize: Fonts.size.textLarge,
    fontFamily: Fonts.type.fontBold,
    color: Colors.white,

  },
};

export default metrics;
