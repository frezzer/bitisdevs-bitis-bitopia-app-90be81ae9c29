const moment = require('moment');


let from = moment(moment("02/02/2020").format("YYYY-MM-DD 00:00:00")).toISOString();
console.log("debug: a", from);
let to = moment(moment("03/02/2020").format("YYYY-MM-DD 23:59:59")).toISOString();
console.log("debug: b", to);

const _ = require('lodash');
console.log("debug: b", _.isEmpty(null));
console.log("debug: b", _.isEmpty(undefined));
console.log("debug: b", _.isEmpty({}));
console.log("debug: b", _.isEmpty([]));
