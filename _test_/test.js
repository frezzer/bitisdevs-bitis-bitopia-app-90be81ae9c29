'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import { Dimensions, InteractionManager, View, Text, TouchableOpacity } from 'react-native';
// import LoadingInteractions from '../../Components/LoadingInteractions';

// Styles
// import styles from './Styles/SampleScreen.styles';

// Actions and Selectors Redux
// import MeActions, { MeSelectors } from '../../Redux/MeRedux'

// utils
import { compareOmitState } from "../App/Services/utils";
import { SafeAreaView } from 'react-navigation';
import { setContext } from '@redux-saga/core/effects';
const { width } = Dimensions.get('window');



/**
 * Main
 */
export default function TestingScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [text, setChangeText] = React.useState('')


  // effects
  React.useEffect(() => {
    console.tron.log('SampleScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => {
        setInitialRender(false);
      });
      //...
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onButtonPress: () => {
      alert('123')
    }
  }
  const state = {

  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */
  const renderProps = {
    initialRender,
    Actions,
    state

  }
  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('SampleScreen render');
  // declare variables
  const { initialRender, Actions, state } = props;
  const { } = state
  const [value, onChangeText] = React.useState('Useless Placeholder')

  return (

    <View testID='welcome' style={{ flex: 1, paddingTop: 20, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{ fontSize: 25, marginBottom: 30 }}>
        Welcome
        </Text>
      <TouchableOpacity testID='hello_button' onPress={Actions.onButtonPress}>
        <Text style={{ color: 'blue', marginBottom: 20 }}>Say Hello</Text>
      </TouchableOpacity>
      <TouchableOpacity testID='world_button' onPress={Actions.onButtonPress}>
        <Text style={{ color: 'blue', marginBottom: 20 }}>Say World</Text>
      </TouchableOpacity>
      <TouchableOpacity testID='goodbye_button' onPress={Actions.onButtonPress}>
        <Text style={{ color: 'blue', marginTop: 50, marginBottom: 20 }}>Say Goodbye</Text>
      </TouchableOpacity>
      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
        onChangeText={text => onChangeText(text)}
        value={value}
      />
    </View>

  );


}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
