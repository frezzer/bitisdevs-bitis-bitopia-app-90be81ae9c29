'use strict';

// basics
import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import propTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, View, Text,
  TouchableOpacity, ScrollView
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import LoadingInteractions from '../../Components/LoadingInteractions';
import ButtonHandle from '../../Components/Button/ButtonHandle';
import TextInfo from '../../Components/TextInput/TextInfo';

// Styles
import styles from './Styles/CloseShiftScreen.styles';

// Actions and Selectors Redux
import MeActions, { MeSelectors } from '../../Redux/MeRedux'
import ShiftFindonlyoneActions, { ShiftFindonlyoneSelectors } from '../../Redux/shift/ShiftFindOnlyOneRedux'
import ShiftFinishoneActions, { ShiftFinishoneSelectors } from '../../Redux/shift/ShiftFinishOneRedux'
import ShiftFindstoreActions, { ShiftFindstoreSelectors } from '../../Redux/shift/ShiftFindStoreRedux';
import ShiftFindActions, { ShiftFindSelectors } from '../../Redux/shift/ShiftFindRedux'

// Navigation
// import { Navigation } from '../../Navigation1/Navigation';
// import { screensName, navComponentScreen } from "../../Navigation1/navScreens";
// import { hideBottomTabsOptions, topBarTitleOptions, appOptions } from '../../Navigation1/Options';
import screensName from '../../Navigation/Screens'

// utils
import { compareOmitState } from "../../Services/utils";
const { width } = Dimensions.get('window');


/**
 * Options
 *  - navigation options for Screens
 * */
// const options = (props) => {
//   return {
//     ...appOptions,
//     topBar: {
//       ...appOptions.topBar,
//       title: {
//         ...appOptions.topBar.title,
//         text: 'Thông tin ca',
//       },
//     },
//   };
// };
// ManagerInfoShiftScreen.options = options;
const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Thông Tin Ca',

  };
}

ManagerCloseShiftScreen.navigationOptions = navigationOptions;

/**
 * Main
 */

ManagerCloseShiftScreen.propTypes = {
  id: propTypes.number
}


export default function ManagerCloseShiftScreen(props) {
  // maybe you need
  const data=props.navigation.state.params;
  console.tron.log("TCL: ManagerCloseShiftScreen -> data", data)
  
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  // useStates
  // const uuid = data.uuid;
  // const status = props.navigation.state.params.status
  // console.tron.log("TCL: ManagerInfoShiftScreen -> status", status)
  // selector store
  const me_payload = useSelector(state => MeSelectors.selectPayload(state.me));
  // const employee_uuid = _.get(me_payload, ['employee_id', 'uuid'], null);

  const shiftFindOnlyOnePayload = useSelector(state => ShiftFindonlyoneSelectors.selectPayload(state.shift.findOnlyOne));
  const ShiftFinishonePayload = useSelector(state => ShiftFinishoneSelectors.selectPayload(state.shift.finishOne));
  const findStorePayload = useSelector(state => ShiftFindstoreSelectors.selectPayload(state.shift.findStore));
  console.tron.log("TCL: ManagerInfoShiftScreen -> findStorePayload", shiftFindOnlyOnePayload)
  // const status = _.get() ,
  // const isClose = 
  // effects
  React.useEffect(() => {
    console.tron.log('ManagerInfoShiftScreen effect');
    // didmount
    // dispatch(ShiftFindonlyoneActions.shiftFindonlyoneRequest({ uuid }));
    if (initialRender) {
      //... dispatch in didmount
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      // get info this shift
      dispatch(ShiftFinishoneActions.shiftFinishoneInitial());
      dispatch(ShiftFindonlyoneActions.shiftFindonlyoneInitial());
      // testing
      // dispatch(ShiftFindonlyoneActions.shiftFindonlyoneRequest({ uuid: 'SHYXWU9FAPC7' }));
      // setShiftUuid('SHYXWU9FAPC7');
    }


    // clear when Screen off
    return () => { };
  }, []);

  React.useEffect(() => {
    // listen event from input
    if (findStorePayload && ShiftFinishonePayload) {
      const attendances = _.get(findStorePayload, ['attendances'], []);
      const openTime = _.get(findStorePayload, ['openTime'], []);
      const closeTime = _.get(findStorePayload, ['closeTime'], []);


      const infoText = [
        `Tên cửa hàng: ${findStorePayload.data.store_name}`,
        `Tên ca: ${ShiftFinishonePayload.name}`,
        `Thời gian bắt đầu: ${moment(openTime).toISOString()}`,
        `Thời gian kết thúc: ${moment(closeTime).toISOString()}`,
        `Số người điểm danh: ${attendances.length}`,
        `Số người tan ca: `
      ];
      // Navigation.push(props.componentId, navComponentScreen({
      //   name: screensName.ManagerConfirmScreen,
      //   passProps: {
      //     titleText: "Xác nhận chốt ca thành công",
      //     infoText: infoText,
      //     descriptionText: ["Sau khi bạn chốt ca, các nhân viên khác chưa thực hiện tan ca sẽ không thể điểm danh trên ca trực này."],
      //     buttonTitle: "Xác nhận",
      //   },
      //   options: { ...topBarTitleOptions('Xác nhận') }
      // }));
      props.navigation.navigate(screensName.ManagerConfirmCreateScreen, {
        titleText: "Xác nhận chốt ca thành công",
        infoText: infoText,
        descriptionText: ["Sau khi bạn chốt ca, các nhân viên khác chưa thực hiện tan ca sẽ không thể điểm danh trên ca trực này."],
        buttonTitle: "Xác nhận",
      })

      dispatch(ShiftFinishoneActions.shiftFinishoneInitial());
      dispatch(ShiftFindActions.shiftFindInitial());
      // dispatch(ShiftFindActions.shiftFindRequest({
      //   createBy: employee_uuid,
      //   _sort: "id:desc",
      // }));
    }
  }, [ShiftFinishonePayload]);



  /**
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressFinish: () => {
      // console.tron.log("TCL: ManagerInfoShiftScreen -> uuid", data.uuid)
      // dispatch(ShiftFinishoneActions.shiftFinishoneRequest({
      //   uuid: data.uuid,
      //   ready: true
      // }));
      props.navigation.navigate(screensName.ManagerConfirmScreen)
    }
  }

  // Navigation.popToRoot(props.componentId)

  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */

  const base64Logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAA..';
  const renderProps = {
    initialRender,
    shiftFindOnlyOnePayload,
    Actions,
    // uuid,
    base64Logo,
    data
    // status
  }
  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerInfoShiftScreen render');
  // declare variables
  const {
    initialRender,
    Actions,
    data,
    // uuid,

    base64Logo,
    //  status
  } = props;


  if (initialRender) return <LoadingInteractions size={'large'} />
  // if (!uuid)
  //   return <View style={styles.container}>
  //     <View style={styles.content}>
  //       <View style={styles.warningBox}>
  //         <Text style={styles.warningText}>{"Không tìm thấy thông tin ca."}</Text>
  //       </View>
  //     </View>
  //   </View>

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.qrcode}>
          <QRCode
            value={data.uuid}
            logo={{ uri: base64Logo }}
            logoSize={50}
            logoBackgroundColor='transparent'
            size={250}
          />
          <View style={styles.valueQrCodeView}>
            <Text style={styles.valueQrCodeText}>{data.uuid}</Text>
            <Text style={styles.shiftName}>{" Tên Ca Đã Tạo"}</Text>
            <Text style={styles.shiftNote}>{"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}</Text>
          </View>
        </View>
        <View style={styles.block_info}>
          <TextInfo
            Text1={'Người tạo ca'}
            Text2={data.user_uuid}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Tên cửa hàng'}
            Text2={'Bitis Hunter Nguyễn Trãi'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Thời điểm tạo ca'}
            Text2={'14:15'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Thời điểm kết ca'}
            Text2={'14:15'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Giờ bắt đầu'}
            Text2={'14:15'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Giờ kết thúc'}
            Text2={'14:15'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Nhân viên đã điểm danh'}
            Text2={'ac'}
          />
          <View style={styles.lineStyle} />
          <TextInfo
            Text1={'Nhân viên đã tan ca'}
            Text2={'7'}
          />
        </View>



      {/* {status == true ?  */}
      <View style={styles.btn_bottom} >
        <ButtonHandle
          buttonText={'Chốt ca'}
          stylesText={styles.Text12}
          onPressButton={Actions.onPressFinish}
          styles={{ backgroundColor: 'red' }}
          />
      </View>
      {/* //  : null} */}
          </ScrollView>
    </View>
  );

}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
