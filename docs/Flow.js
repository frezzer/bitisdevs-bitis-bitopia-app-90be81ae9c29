//* WARNING: Logic Flow in here
const navigationFlow = {
    initialRoot: {
        SplashScreen: {
            authRoot: null,
            appRoot: null,
        }
    },
    authRoot: {
        LoginScreen: {
            appRoot: null
        },
    },
    appRoot: {
        DashBoardScreen: {
            SaleManager: {
                CartScreen: {
                    CartDetailScreen: null,
                    CartScanScreen: null
                },
                StatisticScreen: null,
                BillScreen: null,
                MoreScreen: {
                    ListShiftScreen: {
                        ScanShiftScreen: {
                            CreateShiftScreen: {
                                ConfirmCreateShiftScreen: null
                            }
                        },
                        CloseShiftScreen: {
                            ConfirmCloseShiftScreen: null

                        },
                        ShiftSumaryScreen: null
                    },
                    ListStaffScreen: {
                        StaffSumaryScreen: null
                    }
                }
            },
            StaffScreen: {
                CartScreen: {
                    CartDetailScreen: null,
                    CartScanScreen: null
                },
                StatisticScreen: null,
                BillScreen: null,
                MoreScreen (ListShiftScreen){
                    CheckInScreen:{
                        ScanShiftScreen:null
                        ConfirmScreen:null
                    }
                    CheckOutScreen:{
                        ScanShiftScreen:null
                        ConfirmScreen:null
                    }

                }


            }
        }
    }
}
