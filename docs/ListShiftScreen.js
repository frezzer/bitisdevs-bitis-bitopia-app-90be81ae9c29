'use strict';

// basics
import _ from 'lodash';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Components
import {
  Dimensions, InteractionManager, SafeAreaView,
  View, Text, FlatList
} from 'react-native';
import LoadingInteractions from '../App/Components/LoadingInteractions';
import ShiftItem from '../App/Components/ShiftItem'

// Styles
import styles from '../App/Screens/Manager/Styles/ListShiftScreen.styles';

// Actions and Selectors Redux

import { MeSelectors } from '../App/Redux/MeRedux'
import ShiftFindActions, { ShiftFindSelectors } from '../App/Redux/shift/ShiftFindRedux'

import screensName from '../App/Navigation/Screens';

// utils
import { compareOmitState } from "../App/Services/utils";
import { ScrollView } from 'react-native-gesture-handler';
const { width } = Dimensions.get('window');




const navigationOptions = (props) => {
  const screenProps = _.get(props, ['screenProps'], {});
  return {
    title: 'Danh sách ca',
  };
}

ManagerListShiftScreen.navigationOptions = navigationOptions;



/**
 * Main
 */
export default function ManagerListShiftScreen(props) {
  // maybe you need
  const dispatch = useDispatch();
  const [initialRender, setInitialRender] = React.useState(true);
  const [loadMoreLimit, setLoadMoreLimit] = React.useState(0);

  // useStates

  // selector store
  const me_payload = useSelector(state => MeSelectors.selectPayload(state.me));
  const employee_uuid = _.get(me_payload, ['employee_id', 'uuid'], null);
  const listShifts = useSelector(state => ShiftFindSelectors.selectPayload(state.shift.find)) || [];
  console.tron.log("TCL: ManagerListShiftScreen -> listShifts", listShifts)
  const openAndCloseShift = _.groupBy(listShifts,'status')
  const status = _.map(listShifts,'status')
  console.tron.log("TCL: ManagerListShiftScreen -> status", status)
  


  // effects
  React.useEffect(() => {
    console.tron.log('ManagerListShiftScreen effect');
    // didmount
    if (initialRender) {
      InteractionManager.runAfterInteractions(() => setInitialRender(false));
      dispatch(ShiftFindActions.shiftFindInitial());
    }

    if (employee_uuid) {
      // console.log("TCL: ManagerListShiftScreen -> employee_uuid", employee_uuid)
      setLoadMoreLimit(loadMoreLimit + 1);
      dispatch(ShiftFindActions.shiftFindRequest({
        createBy: employee_uuid,
        _sort: "id:desc",
        // status: true
      }));
    }
    // listen event from input
    // Actions.actionsfunction


    // clear when Screen off
    return () => { };
  }, [/** input */]);


  /**I
   * Actions
   *  - for flows and onPress attribute button
   * */
  const Actions = {
    onPressButton: (item) => {
      requestAnimationFrame(() => {
            props.navigation.navigate(screensName.ManagerCloseShiftScreen,{
            uuid:item.uuid,
            status: item.status
          })
        });
    },
    // onEndReachedOpen: () => {
    //   console.tron.log('onEndReached', employee_uuid)
    //   if (employee_uuid && loadMoreLimit <= 20) {
    //     setLoadMoreLimit(loadMoreLimit + 1);
    //     dispatch(ShiftFindActions.shiftFindRequest({
    //       createBy: employee_uuid,
    //       _sort: "id:desc",
    //       status: true
    //     }));
    //   }
    // },
    // onEndReachedClose: () => {
    //   console.tron.log('onEndReached', employee_uuid)
    //   if (employee_uuid && loadMoreLimit <= 5) {
    //     setLoadMoreLimit(loadMoreLimit + 1);
    //     dispatch(ShiftFindActions.shiftFindRequest({
    //       createBy: employee_uuid,
    //       _sort: "id:desc",
    //       status: true
    //     }));
    //   }
    //  }
  }


  /**
   * render
   *  - declare variables for render
   *  - render loading when initialRender is true
   */

  // Items in Flatlist
  const renderItemFunction = ({ item }) => <RenderItem item={item} />
  const RenderItem = React.memo((props) => {
    const { item } = props;
    return (
      <View style={styles.element}>
        <ShiftItem data_item={item} onPressButton={() => Actions.onPressButton(item)} />
      </View>
    );
  }, areEqual);


  const renderProps = {
    initialRender,
    Actions,
    listShifts,
    renderItemFunction,
    openAndCloseShift
  };

  return <Render {...renderProps} />

}


/**
 * Render
 */
const Render = React.memo((props) => {
  console.tron.log('ManagerListShiftScreen render');
  // declare variables
  const {
    initialRender, Actions, listShifts,
    renderItemFunction,
    setOnEndReachedCalledDuringMomentum,
    openAndCloseShift
  } = props;

  if (initialRender) return <LoadingInteractions size={'large'} />
  return (
    <ScrollView style={styles.container}>
      <View style={styles.shiftArea}>
        <View style={styles.block}>
          <Text style={styles.Text1}>Ca Mở</Text>
        </View>

        <View >
          <FlatList
            data={openAndCloseShift.true}
            renderItem={renderItemFunction}
            keyExtractor={(item, index) => item.id}
            // onEndReached={Actions.onEndReachedOpen}
            // onEndReachedThreshold={0.5}
          />
        </View>
      </View>

      <View style={styles.shiftArea1}>
        <View style={styles.block}>
          <Text style={styles.Text1}>Ca Đóng</Text>
        </View>
        <FlatList
            data={openAndCloseShift.false}
            renderItem={renderItemFunction}
            keyExtractor={(item, index) => item.id}
            // onEndReached={Actions.onEndReachedClose}
            // onEndReachedThreshold={0.5}
          />
      </View>


    </ScrollView>
  )

}, areEqual);

/**
 * areEqual
 * declare function should be update screen or not
 */
function areEqual(prevProps, nextProps) {
  return compareOmitState(nextProps, prevProps, ['Actions']);
}
