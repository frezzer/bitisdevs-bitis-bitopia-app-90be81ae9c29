'use strict';
import _ from 'lodash';
import { AppState, Linking, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import registerScreens, { store } from './Screens';
import MeActions from '../Redux/MeRedux';
import NavigationReduxActions from '../Redux/NavigationRedux';
import { Colors } from '../Themes';
import { Feather } from '../Components/Icons';
import { screensName, navComponentScreen } from './Screens';
import { hideAllOptions } from './Options';


export { Navigation };

export const appRoot = (args) => ({
  root: {
    stack: {
      children: [navComponentScreen({
        name: screensName.HomeScreen,
      })],
    }
  }
});


export const initialRoot = {
  root: navComponentScreen({
    name: screensName.SaleStatisticScreen,
    options: { ...hideAllOptions }
  })
};

export const authRoot = {
  root: {
    stack: {
      children: [
        navComponentScreen({
          name: screensName.LoginScreen,
          options: { ...hideAllOptions }
        }),
      ],
    },
  }
};
export const testRoot = {
  root: navComponentScreen({
    name: screensName.TEST_SCREEN,
    options: { ...hideAllOptions }
  }),
};
/**
|                                                  
| Nạp tất cả screen vào App
|                                                  
*/


export function registerScreensAndStartApp() {
  /**
   |                                                  
   | Set Options mặc đinh cho tất cả screen ở trên 
   |                                                  
   */
  registerScreens();
  console.tron.log('All screens have been registered...');
  Navigation.events().registerAppLaunchedListener(async () => {
    // TODO: Design in here: hide switchInitialRouter() and visible switchTestRouter()
    // await switchTestRouter();
    await switchInitialRouter();
    // handle app state and deep links
    // AppState.addEventListener('change', handleAppStateChange)
    // Linking.addEventListener('url', handleOpenURL)

  });
}

export async function switchInitialRouter() {
  store.dispatch(NavigationReduxActions.navigationSetRoot('initial'));
  await Navigation.setDefaultOptions({
    options: { ...hideAllOptions }
  });
  await Navigation.setRoot(initialRoot);
}
export function switchTestRouter() {
  store.dispatch(NavigationReduxActions.navigationSetRoot('test'));
  Navigation.setRoot(testRoot)
}
export async function switchAuthRouter() {
  Navigation.setDefaultOptions({
    options: { ...hideAllOptions }
  })
  store.dispatch(NavigationReduxActions.navigationSetRoot('auth'));
  await Navigation.setRoot(authRoot)
}
export async function switchAppRouter() {
  store.dispatch(NavigationReduxActions.navigationSetRoot('app'));
  console.tron.log('go to home 2');
  const sources = await Promise.all([
    Feather.getImageSource("home", 27),
    Feather.getImageSource("star", 27),
    Feather.getImageSource("menu", 27),
    Feather.getImageSource("arrow-left", 24),
  ]);
  console.tron.log('go to home 3')
  await Navigation.setRoot(appRoot(sources))
}
/**
|                                                  
| add Listeners
| Long Phạm : khi đang run background  > mở app lên thì refesh lại Me
|                                                  
*/
let lastAppState = 'active';

function handleAppStateChange(nextAppState) {
  if (lastAppState.match(/inactive|background/) && nextAppState === 'active' && !__DEV__) refreshAccount(store)
  lastAppState = nextAppState
}

async function refreshAccount() {
  const jwt = await AsyncStorage.getItem('Authorization');
  store.dispatch(MeActions.meRequest({ Authorization: jwt }))
}
/**
|                                                  
| Long Phạm : xử lý cho các trường hợp link sâu vô app
| Deep link : url1
|                                                  
*/
function handleOpenURL(event) {
  // console.tron.log(event.url);
  let splitUrl = event.url.split('/') // ['https:', '', 'domain', 'route', 'params']
  let importantParameters = splitUrl.splice(3) // ['route', 'params']
  if (importantParameters.length === 0) {
    console.tron.log('Sending to home page')
    return null
  }
  if (importantParameters.length === 1) {
    switch (importantParameters[0]) {
      case 'register':
        console.tron.log(`Sending to Register Page`)
        registerScreen()
        break
      default:
        console.tron.warn(`Unhandled deep link: ${event.url}`)
      // default code block
    }
  }
}




