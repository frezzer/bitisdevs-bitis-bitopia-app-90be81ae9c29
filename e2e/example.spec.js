describe('Detox Testing', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });
  
  it('should have welcome screen', async () => {
    await expect(element(by.id('input_user'))).toBeVisible();
  });
  
  it('should show hello screen after tap', async () => {
    await element(by.id('input_user')).tap();
    // await element
  });
  
  it('should show world screen after tap', async () => {
    await element(by.id('world_button')).tap();
    // await expect(element(by.text('Hello1'))).toBeVisible();
  });
});